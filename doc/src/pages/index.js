import React from 'react';
import { Pie } from 'react-chartjs-2';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link'
import ProgressBar from "@ramonak/react-progress-bar";;
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import stats from '../../stats';
import styles from './styles.module.css';

const features = [
  {
    title: "Interpreter simulations",
    url: "docs/status/proto_k_simulations#script_interpreterv",
    description: (
      <>
        Definition of the Michelson interpreter using dependent types instead of GADTs.
      </>
    ),
    progress: stats.progress.simulations.interpreter,
  },
  {
    title: "Translator simulations",
    url: "docs/status/proto_k_simulations#proofs-1",
    description: (
      <>
        Definition of the Michelson type-checker and parser using dependent types instead of GADTs.
      </>
    ),
    progress: stats.progress.simulations.translator,
  },
  {
    title: "Interpreter backward compatibility",
    url: "docs/status/proto_j_k_backward_compatibility",
    description: (
      <>
        Verification of the backward compatibility of the interpreter between the protocols J and K.
      </>
    ),
    progress: stats.progress.backward,
  },
];

function Feature({title, url, description, progress}) {
  return (
    <div
      className={clsx('col col--4', styles.feature)}
      style={{paddingTop: "2rem"}}
    >
      <h3><a href={useBaseUrl(url)}>{title}</a></h3>
      <p>{description}</p>
      <ProgressBar
        bgColor="#0f62ff"
        completed={progress}
        labelAlignment="right"
      />
    </div>
  );
}

function Stats() {
  const data = {
    labels: ['Code', 'Environment', 'Proofs'],
    datasets: [
      {
        data: [stats.code, stats.environment, stats.proofs],
        backgroundColor: [
          'rgb(54, 162, 235)', // blue
          'rgb(255, 99, 132)', // red
          // 'rgb(75, 192, 192)', // green
          'rgb(255, 205, 86)', // yellow
        ],
        borderColor: [
          'white',
          'white',
          'white',
        ],
        borderWidth: 1,
      },
    ],
  };
  const options = {
    plugins: {
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        font: {
          size: 14,
        },
        position: 'top',
        text: 'Number of lines',
      },
    },
  };

  return (
    <div
      style={{
        marginBottom: 30,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 30,
        maxWidth: 330,
      }}
    >
      <Pie data={data} options={options} />
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;

  return (
    <Layout
      title={siteConfig.title}
      description="Translation in Coq of the economic protocol of Tezos">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <div className="row">
            {/* <div className="col col--3">
              <p style={{fontSize: 150, margin: -20}}>λ=∀</p>
            </div> */}
            {/* <div className="col col--9"> */}
            <div className="col col--12">
              <h1 className="hero__title">{siteConfig.title}</h1>
              <p className="hero__subtitle">{siteConfig.tagline}</p>
              <div className={styles.buttons}>
                <Link
                  className={clsx(
                    'button button--lg',
                    styles.button,
                  )}
                  to={useBaseUrl('docs/proto_alpha/')}>
                  Protocol
                </Link>
                <Link
                  className={clsx(
                    'button button--lg',
                    styles.button,
                  )}
                  to={useBaseUrl('docs/environment/v7/')}>
                  Environment
                </Link>
                <Link
                  className={clsx(
                    'button button--lg',
                    styles.button,
                  )}
                  to={useBaseUrl('docs/proto_alpha/proofs/')}>
                  Proofs
                </Link>
              </div>
            </div>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        <section>
          <div className="container">
            <div className="row">
              <div className="col col--2" />
              <div className="col col--8">
                <div className={styles.introduction}>
                  <p>
                    We present a translation and verification in the <a href="https://coq.inria.fr/">Coq language</a> of the <a href="https://gitlab.com/tezos/tezos/-/tree/master/src/proto_alpha/lib_protocol">source code</a> of the economic protocol of <a href="https://tezos.com/">Tezos</a>. The Coq language is a formal language used to write mathematical reasoning, such as theorems and proofs, and verify their correctness. We use this translation to Coq to <a href="https://en.wikipedia.org/wiki/Formal_verification">formally verify</a> the absence of certain classes of bugs in all possible execution scenarios. We use the tool <a href="https://github.com/formal-land/coq-of-ocaml">coq-of-ocaml</a> to automatically generate this translation from the implementation.
                  </p>
                  <Stats />
                  <p>
                    We also show our formalization of the <a href="https://gitlab.com/tezos/tezos/-/tree/master/src/lib_protocol_environment/sigs">environment interface</a> of the protocol given in the&nbsp;<a href="https://gitlab.com/tezos/tezos">tezos/tezos</a> node. This environment interface is the set of functions of values that the protocol can call. It is a subset of the standard library of OCaml and is specified as a module signature. We axiomatize or define each of its functions and values in Coq.
                  </p>
                  {/* <p>
                    We believe&nbsp;Tezos to be the only crypto-currency with a formally specified implementation. We think that this effort would require a complete rewriting or the development of new tools for other crypto-currencies. We believe this to bring more long-term value and trust to Tezos.
                  </p> */}
                  <p>
                    Together with this project, there are other initiatives to do formal verification on Tezos. This includes&nbsp;<a href="https://gitlab.com/nomadic-labs/mi-cho-coq">Mi-Cho-Coq</a> to formally verify smart contracts and a project in development at&nbsp;<a href="https://www.nomadic-labs.com/">Nomadic Labs</a> for a high-level certification of the protocol. {/* One of our goals is to show that all these projects are compatible with this formalization. We also aim to formally verify smaller properties, that would otherwise be expressed as unit tests on specific cases. */}
                  </p>
                </div>
              </div>
              <div className="col col--2" />
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Home;

const baseUrl = '/coq-tezos-of-ocaml/';

module.exports = {
  title: 'Coq Tezos of OCaml 💫',
  tagline: 'A translation & verification in Coq of the code of the economic protocol of Tezos',
  url: 'https://formal-land.gitlab.io',
  baseUrl,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/rooster-tezos.png',
  organizationName: 'formal-land', // Usually your GitHub org/user name.
  projectName: 'coq-tezos-of-ocaml', // Usually your repo name.
  themeConfig: {
    announcementBar: {
      id: 'read_our_blog',
      content:
        'To follow what we are working on, see our <a href="/coq-tezos-of-ocaml/docs/changelog">Changelog</a> 📰',
      backgroundColor: '#fafbfc',
      textColor: '#091E42',
      isCloseable: false,
    },
    colorMode: {
      // Hides the switch in the navbar
      // Useful if you want to support a single color mode
      disableSwitch: true,
    },
    image: 'img/rooster-tezos.png',
    prism: {
      additionalLanguages: ['coq', 'ocaml'],
    },
    algolia: {
      appId: 'MNX6EZCQ2F',
      apiKey: 'fd0b785af38d01dcd8142e4a73932a95',
      indexName: 'coq-tezos',
      contextualSearch: false,
    },
    navbar: {
      hideOnScroll: true,
      title: 'Coq Tezos of OCaml 💫',
      logo: {
        alt: 'Logo',
        src: 'img/rooster-tezos.png',
      },
      items: [
        {
          to: 'docs/proto_alpha/',
          activeBasePath: 'docs/proto_alpha',
          label: 'Protocol',
          position: 'left',
        },
        {
          to: 'docs/environment/v7/',
          activeBasePath: 'docs/environment/v7',
          label: 'Environment',
          position: 'left',
        },
        {
          to: 'docs/proto_alpha/proofs/',
          activeBasePath: 'docs/proto_alpha/proofs',
          label: 'Proofs',
          position: 'left',
        },
        {
          to: 'blog',
          label: 'Blog ⚈',
          position: 'left',
        },
        // {
        //   label: 'Securing money',
        //   className: 'slogan_src-pages-',
        //   position: 'right',
        // },
        {
          href: 'https://gitlab.com/formal-land/coq-tezos-of-ocaml',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Formalization',
          items: [
            {
              label: 'Protocol',
              to: 'docs/proto_alpha/',
            },
            {
              label: 'Environment',
              to: 'docs/environment/v7/',
            },
            {
              label: 'Proofs',
              to: 'docs/proto_alpha/proofs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              html: `
              <a
                class="footer__link-item"
                href="https://tezos.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/tezos.png"
                  style="vertical-align: middle;"
                />
                Tezos
              </a>
              `,
            },
            {
              html: `
              <a
                class="footer__link-item"
                href="https://formal.land/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/foobar_land_new.png"
                  style="vertical-align: middle;"
                />
                Formal Land
              </a>
              `,
            },
            {
              html: `
              <a
                class="footer__link-item"
                href="https://www.nomadic-labs.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/nomadic_labs.png"
                  style="vertical-align: middle;"
                />
                Nomadic Labs
              </a>
              `,
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/formal-land/coq-tezos-of-ocaml',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} <a class="footer__link-item" href="https://formal.land/" target="_blank">Formal Land</a> &amp; <a class="footer__link-item" href="https://www.nomadic-labs.com/" target="_blank">Nomadic Labs</a>`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-client-redirects',
      {
        // The returned values are the path from which we redirect.
        createRedirects(existingPath) {
          if (existingPath.includes('/proto_alpha/')) {
            return [
              existingPath.replace('/proto_alpha/', '/'),
            ];
          }
          if (existingPath.includes('/environment/v5/')) {
            return [
              existingPath.replace('/environment/v5/', '/environment/'),
            ];
          }
          return undefined; // Return a falsy value: no redirect created
        },
      },
    ],
  ],
};

# coding: utf-8

def gsub(content, pattern, replacement)
  init = content.clone
  content.gsub!(pattern, replacement )
  if content == init
    puts "Useless replacement for #{pattern}"
    puts "Please update the replacement rules"
    puts
    puts caller
  end
end

def patch_environment(target_path)
  path = "Environment_mli.v"
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content.gsub!(/^Module (\w+)\.$/) do
    "Module Type #{$1}_signature."
  end
  content.gsub!(/^End (\w+)\.$/) do
    if $1 == "Lwt" then
      replacement = <<-END
End #{$1}_signature.
(* We do not implement the Lwt module, as it is the identity monad for us since
   the protocol code is sequential and interactions with the store can be
   implemented in a purely functional way. *)
END
    else
      replacement = <<-END
End #{$1}_signature.
Require Export TezosOfOCaml.Environment.V8.#{$1}.
Module #{$1}_check : #{$1}_signature := #{$1}.
END
    end
    replacement.chop
  end

  pattern = "Module Error_monad_check : Error_monad_signature := Error_monad."
  replacement = pattern + "\n\n" + "Export Error_monad.Notations."
  gsub(content, pattern, replacement)

  pattern = "  Parameter Cache_value : Set.\n  \n"
  gsub(content, pattern, "")

  gsub(content, "Cache_value", "cache_value")

  gsub(content,
    "| Node : hash -> kinded_hash", "| KNode : hash -> kinded_hash")
  gsub(content,
    "| Value : hash -> kinded_hash", "| KValue : hash -> kinded_hash")

  pattern = <<-END
      Public_key_hash :
        SIGNATURE_PUBLIC_KEY_HASH (t := Public_key_hash_t) (Set_elt := t)
          (Set_t := Public_key_hash_Set_t) (Map_key := t)
          (Map_t := Public_key_hash_Map_t);
      Public_key : SIGNATURE_PUBLIC_KEY (t := Public_key_t);
END
  replacement = <<-END
      Public_key_hash :
        SIGNATURE_PUBLIC_KEY_HASH (t := Public_key_hash_t)
          (Set_t := Public_key_hash_Set_t) (Map_t := Public_key_hash_Map_t);
      Public_key :
        SIGNATURE_PUBLIC_KEY (t := Public_key_t)
          (public_key_hash_t := Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.t));
END
  gsub(content, pattern, replacement)

  pattern = "  iter_ep : forall {a : Set}, (key -> a -> M? unit) -> t a -> M? unit;"
  replacement = <<-END
  iter_ep :
    forall {a _error: Set},
    (key -> a -> Pervasives.result unit (Error_monad.trace _error)) ->
    t a ->
    Pervasives.result unit (Error_monad.trace _error);
END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

# We add a number to the module init functions to differentiate them.
def index_init_module(content)
  n = 0
  content.gsub("init_module ") { n += 1; "init_module#{n} " }
end

def patch_alpha_context(target_path)
  path = File.join(target_path, "Alpha_context.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "Fitness.t", "Environment.V8.Fitness.t")
  gsub(content,
    "Tezos_sapling.Core.Client.UTXO.Legacy",
    "Sapling.Legacy")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_apply(target_path)
  path = File.join(target_path, "Apply.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_baking(target_path)
  path = File.join(target_path, "Baking.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_blinded_public_key_hash(target_path)
  path = File.join(target_path, "Blinded_public_key_hash.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition rpc_arg := H.(S.HASH.rpc_arg).",
    "Definition rpc_arg := H.(S.HASH.rpc_arg).

Definition Map := H.(S.HASH.Map)."
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_cache_repr(target_path)
  path = File.join(target_path, "Cache_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition list_keys := Raw_context.Cache.(Context.CACHE.list_keys).
  
  Definition key_rank := Raw_context.Cache.(Context.CACHE.key_rank).
  
  END
  gsub(content, pattern, "")
  gsub(content,
    "Definition future_cache_expectation :=",
    "(* Definition future_cache_expectation :=")
  gsub(content,
    "Raw_context.Cache.(Context.CACHE.future_cache_expectation).",
    "Raw_context.Cache.(Context.CACHE.future_cache_expectation). *)")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_contract_repr(target_path)
  path = File.join(target_path, "Contract_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = "Signature.Ed25519 "
  replacement = "Signature.Ed25519Hash "
  gsub(content, pattern, replacement)
  pattern = "Signature.Secp256k1 "
  replacement = "Signature.Secp256k1Hash "
  gsub(content, pattern, replacement)
  pattern = "Signature.P256 "
  replacement = "Signature.P256Hash "
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_dal_apply(target_path)
  path = File.join(target_path, "Dal_apply.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_dal_slot_repr(target_path)
  path = File.join(target_path, "Dal_slot_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_delegate_sampler(target_path)
  path = File.join(target_path, "Delegate_sampler.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = "(* Cannot unpack first-class modules at top-level due to a universe inconsistency *)"
  replacement = "Axiom Cache : Cache_repr.INTERFACE (cached_value := Cache_client.cached_value)."
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_delegate_storage(target_path)
  path = File.join(target_path, "Delegate_storage.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_delegate_services(target_path)
  path = File.join(target_path, "Delegate_services.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_entrypoint_repr(target_path)
  path = File.join(target_path, "Entrypoint_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_fixed_point_repr(target_path)
  path = File.join(target_path, "Fixed_point_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "Set -> Set", "Set")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_indexable(target_path)
  path = File.join(target_path, "Indexable.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "t := fun (state : Set) => t v_t;",
    "t := t v_t;")
  gsub(content,
    "INDEXABLE.compare _ _ := compare;",
    "INDEXABLE.compare := compare;")
  gsub(content,
    "INDEXABLE.pp _ := pp",
    "INDEXABLE.pp := pp")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_lazy_storage_diff(target_path)
  path = File.join(target_path, "Lazy_storage_diff.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
      "OPS.Next := Big_map.Next;",
      "OPS.Next := cast (Next) Big_map.Next;")
  gsub(content,
      "OPS.Total_bytes := Big_map.Total_bytes;",
      "OPS.Total_bytes := cast (Total_bytes) Big_map.Total_bytes;")
  gsub(content,
      "OPS.Next := Sapling_state.Next",
      "OPS.Next := axiom")
  gsub(content,
      "OPS.Total_bytes := Sapling_state.Total_bytes;",
      "OPS.Total_bytes := cast (Total_bytes) Sapling_state.Total_bytes;")

  gsub(content, "fresh {i : Set}", "fresh {i a u : Set}")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_lazy_storage_kind(target_path)
  path = File.join(target_path, "Lazy_storage_kind.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition title := MakeId_include.(TitleWithId.title).
  
  Module alloc.
  END
  replacement = <<-END
  Definition title := MakeId_include.(TitleWithId.title).

  Definition Id := MakeId_include.(TitleWithId.Id).

  Definition Temp_id := MakeId_include.(TitleWithId.Temp_id).

  Definition IdSet := MakeId_include.(TitleWithId.IdSet).

  Module alloc.
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_level_repr(target_path)
  path = File.join(target_path, "Level_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_merkle_list(target_path)
  path = File.join(target_path, "Merkle_list.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "(path := _)", "(path := list Make.H.(HASH.t))")
  File.open(path, "w") do |file|
    file << content
  end
end

def patch_raw_context(target_path)
  path = File.join(target_path, "Raw_context.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  pattern = "  Definition empty := Context.Tree.(Context.TREE.empty)."
  replacement = "  (* Definition empty := Context.Tree.(Context.TREE.empty). *)"
  gsub(content, pattern, replacement)

  pattern = <<-END
(* ❌ This kind of signature (ident) is not handled. *)
(* unhandled_module_type *)
  END
  replacement = <<-END
Definition T {t local_context : Set} : Set :=
  Raw_context_intf.T
    (root := root) (t := t) (tree := tree) (local_context := local_context).
  END
  gsub(content, pattern, replacement)

  pattern = "      T."
  replacement = "      Raw_context_intf.T."
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_round_repr(target_path)
  path = File.join(target_path, "Round_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)
  gsub(content, "while", "tt")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollup_arith(target_path)
  path = File.join(target_path, "Sc_rollup_arith.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Internal_for_tests_insert_failure : state -> state;",
    "(* Internal_for_tests_insert_failure : state -> state; *)"
  )
  gsub(content, "Context.Kind.Value", "Context.Proof.KValue")
  gsub(content, "Context.Kind.t", "Context.Proof.kinded_hash")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollup_game(target_path)
  path = File.join(target_path, "Sc_rollup_game.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)
  gsub(content,
    "Definition PVM `{FArgs} := PVM.",
    "(* Definition PVM `{FArgs} := PVM. *)")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollup_inbox_repr(target_path)
  path = File.join(target_path, "Sc_rollup_inbox_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition add_external_messages :=
  MakeHashingScheme_include.(MerkelizedOperations.add_external_messages).",
    "(* Definition add_external_messages :=
  MakeHashingScheme_include.(MerkelizedOperations.add_external_messages). *)")

  gsub(content, "Context.Kind.Value", "Context.Proof.KValue")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollup_repr(target_path)
  path = File.join(target_path, "Sc_rollup_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  gsub(content,
    "Definition rpc_arg := H.(S.HASH.rpc_arg).",
    "Definition rpc_arg := H.(S.HASH.rpc_arg).\n\n" +
    "  Definition Map := H.(S.HASH.Map).")
  gsub(content,
    "Axiom hash_bytes : forall {A : Set}, unreachable -> A.",
    "(* Axiom hash_bytes : forall {A : Set}, unreachable -> A. *)"
  )
  gsub(content,
    "Axiom hash_string : forall {A : Set}, unreachable -> A.",
    "(* Axiom hash_string : forall {A : Set}, unreachable -> A. *)"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollup_wasm(target_path)
  path = File.join(target_path, "Sc_rollup_wasm.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Internal_for_tests_insert_failure : state -> state;",
    "(* Internal_for_tests_insert_failure : state -> state; *)"
  )
  gsub(content, "Context.Kind.Value", "Context.Proof.KValue")
  gsub(content,
    "let '_ := Make.Build_FArgs Make_backend Context in",
    "let '_ := Make.Build_FArgs (@Make_backend) Context in"
  )
  gsub(content, "Wasm_2_0_0.Make", "(@Wasm_2_0_0.Make)")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_sc_rollups(target_path)
  path = File.join(target_path, "Sc_rollups.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Internal_for_tests_insert_failure : state -> state;",
    "(* Internal_for_tests_insert_failure : state -> state; *)"
  )
  gsub(content, "Context.Kind.Value", "Context.Proof.KValue")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_big_map(target_path)
  path = File.join(target_path, "Script_big_map.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition empty",
    "Definition empty {a b : Set}")
  gsub(content,
    "Definition get_by_hash {A : Set}",
    "Definition get_by_hash {A B : Set}")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_cache(target_path)
  path = File.join(target_path, "Script_cache.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = "(* Cannot unpack first-class modules at top-level due to a universe inconsistency *)"
  replacement = "Axiom Cache : Cache_repr.INTERFACE (cached_value := Client.cached_value)."
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_expr_hash(target_path)
  path = File.join(target_path, "Script_expr_hash.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, " init_module ", " init_module_hash ")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_interpreter(target_path)
  path = File.join(target_path, "Script_interpreter.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = ".(Script_typed_ir.logger.log_interp)"
  replacement = ".(Script_typed_ir.logger.log_interp) _ _"
  gsub(content, pattern, replacement)

  # imap_map
  pattern = "let imap_map {e f a b c d} := 'imap_map e f a b c d in"
  replacement = "let imap_map {e f a b c d g} := 'imap_map e f a b c d g in"
  gsub(content, pattern, replacement)

  pattern = "and \"'imap_map\" :=\n  (fun (e f a b c d : Set) => fun"
  replacement = "and \"'imap_map\" :=\n  (fun (e f a b c d g : Set) => fun"
  gsub(content, pattern, replacement)

  pattern = "Definition imap_map {e f a b c d : Set} := 'imap_map e f a b c d."
  replacement = "Definition imap_map {e f a b c d g : Set} := 'imap_map e f a b c d g."
  gsub(content, pattern, replacement)

  # ilist_map
  pattern = "let ilist_map {e a b c d} := 'ilist_map e a b c d in"
  replacement = "let ilist_map {e a b c d f} := 'ilist_map e a b c d f in"
  gsub(content, pattern, replacement)

  pattern = "and \"'ilist_map\" :=\n  (fun (e a b c d : Set) => fun"
  replacement = "and \"'ilist_map\" :=\n  (fun (e a b c d f : Set) => fun"
  gsub(content, pattern, replacement)

  pattern = "Definition ilist_map {e a b c d : Set} := 'ilist_map e a b c d."
  replacement = "Definition ilist_map {e a b c d f : Set} := 'ilist_map e a b c d f."
  gsub(content, pattern, replacement)

  # kmap_enter
  pattern = "and \"'kmap_enter\" :=\n  (fun (j k a b c d e : Set) => fun"
  replacement = "and \"'kmap_enter\" :=\n  (fun (j k a b c d f : Set) => fun"
  gsub(content, pattern, replacement)

  # iview
  pattern = "let iview {i a b e f} := 'iview i a b e f in"
  replacement = "let iview {i a b e f o} := 'iview i a b e f o in"
  gsub(content, pattern, replacement)

  pattern = "and \"'iview\" :=\n  (fun (i a b e f : Set) => fun"
  replacement = "and \"'iview\" :=\n  (fun (i a b e f o : Set) => fun"
  gsub(content, pattern, replacement)

  pattern = "Definition iview {i a b e f : Set} := 'iview i a b e f."
  replacement = "Definition iview {i a b e f o : Set} := 'iview i a b e f o."
  gsub(content, pattern, replacement)

  # failwith
  gsub(content,
    "Script_interpreter_defs.ifailwith_type.ifailwith _ _ :=",
    "Script_interpreter_defs.ifailwith_type.ifailwith A B :=")
  gsub(content, "ifailwith None", "ifailwith _ _ None")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_interpreter_logging(target_path)
  path = File.join(target_path, "Script_interpreter_logging.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = ".(Script_typed_ir.logger.log_exit)"
  replacement = ".(Script_typed_ir.logger.log_exit) _ _"
  gsub(content, pattern, replacement)

  pattern = ".(Script_typed_ir.logger.log_entry)"
  replacement = ".(Script_typed_ir.logger.log_entry) _ _"
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_ir_translator(target_path)
  path = File.join(target_path, "Script_ir_translator.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition find_entrypoint_for_type",
    "Definition find_entrypoint_for_type {full : Set}")
  gsub(content,
    "pairs {a b : Set}",
    "pairs")
  gsub(
    content,
    "Definition parse_and_unparse_script_unaccounted",
    "Definition parse_and_unparse_script_unaccounted {a : Set}")
  
  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_ir_unparser(target_path)
  path = File.join(target_path, "Script_ir_unparser.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')


  gsub(content,
    "Definition unparse_items_rec",
    "Definition unparse_items_rec `{FArgs}"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_map(target_path)
  path = File.join(target_path, "Script_map.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Tezos_raw_protocol_alpha.Script_typed_ir.Boxed_map",
    "Script_typed_ir.Boxed_map"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_set(target_path)
  path = File.join(target_path, "Script_set.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Tezos_raw_protocol_alpha.Script_typed_ir.Boxed_set",
    "Script_typed_ir.Boxed_set"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_typed_ir(target_path)
  path = File.join(target_path, "Script_typed_ir.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
Module TYPE_SIZE.
  Record signature {t : Set -> Set} : Set := {
  END
  replacement = <<-END
Module TYPE_SIZE.
  Record signature {t : Set} : Set := {
  END
  gsub(content, pattern, replacement)

  pattern = <<-END
      TYPE_SIZE.check_eq _ _ _ _ := check_eq;
      TYPE_SIZE.to_int _ := to_int;
      TYPE_SIZE.one _ := one;
      TYPE_SIZE.two _ := two;
      TYPE_SIZE.three _ := three;
      TYPE_SIZE.four _ _ := four;
      TYPE_SIZE.compound1 _ _ := compound1;
      TYPE_SIZE.compound2 _ _ _ := compound2
  END
  replacement = <<-END
      TYPE_SIZE.check_eq _ _ := check_eq;
      TYPE_SIZE.to_int := to_int;
      TYPE_SIZE.one := one;
      TYPE_SIZE.two := two;
      TYPE_SIZE.three := three;
      TYPE_SIZE.four := four;
      TYPE_SIZE.compound1 := compound1;
      TYPE_SIZE.compound2 := compound2
  END
  gsub(content, pattern, replacement)

  gsub(content,
    "and \"'comparable_ty\" := (ty)",
    "and \"'comparable_ty\" := ((fun _ => ty) tt)")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_script_typed_ir_size(target_path)
  path = File.join(target_path, "Script_typed_ir_size.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content, "let apply\n", "let apply {a : Set}\n")

  gsub(content, "(function_parameter : a * option b)", "function_parameter")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_stake_storage(target_path)
  path = File.join(target_path, "Stake_storage.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "(* Cannot unpack first-class modules at top-level due to a universe inconsistency *)",
    "Axiom Cache : Cache_repr.INTERFACE (cached_value := Cache_client.cached_value)."
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage(target_path)
  path = File.join(target_path, "Storage.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
:=
    {|
      Storage_functors.INDEX.to_path := to_path;
  END
  replacement = <<-END
: Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.to_path := to_path;
  END
  gsub(content, pattern, replacement)

  gsub(content, "Signature.Ed25519", "Signature.Ed25519Hash")
  gsub(content, "Signature.Secp256k1", "Signature.Secp256k1Hash")
  gsub(content, "Signature.P256", "Signature.P256Hash")

  unfolded_raw_context_pattern = "Raw_context_intf.T.mem :="
  if content.include?(unfolded_raw_context_pattern) then
    puts
    puts "In storage.ml:"
    puts "Make sure to use 'Raw_context.M' when possible to avoid unfolding the"
    puts "Raw_context file to convert it to a record."
    puts "Look for the string '#{unfolded_raw_context_pattern}' in Storage.v"
    puts
  end

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_storage_functors(target_path)
  path = File.join(target_path, "Storage_functors.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition functor `{FArgs} :=
    {|
      INDEX.to_path := to_path;
  END
  replacement = <<-END
  Definition functor `{FArgs} : INDEX.signature (ipath := ipath) :=
    {|
      INDEX.to_path := to_path;
  END
  gsub(content, pattern, replacement)

  pattern = "C.Tree."
  replacement = "C.(Raw_context_intf.T.Tree)."
  gsub(content, pattern, replacement)

  pattern = "  Definition empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty)."
  replacement = "  (* Definition empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty). *)"
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_ticket_accounting(target_path)
  path = File.join(target_path, "Ticket_accounting.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Include Ticket_token_map.",
    "Include Proto_alpha.Ticket_token_map."
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_ticket_lazy_storage_diff(target_path)
  path = File.join(target_path, "Ticket_lazy_storage_diff.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition collect_token_diffs_of_node",
    "Definition collect_token_diffs_of_node {a : Set}"
  )
  gsub(content,
    "Definition collect_token_diffs_of_big_map_update\n",
    "Definition collect_token_diffs_of_big_map_update {a : Set}\n"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_ticket_operations_diff(target_path)
  path = File.join(target_path, "Ticket_operations_diff.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Include Ticket_token_map.",
    "Include Proto_alpha.Ticket_token_map."
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_ticket_scanner(target_path)
  path = File.join(target_path, "Ticket_scanner.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition tickets_of_node",
    "Definition tickets_of_node {a : Set}"
  )
  gsub(content,
    "let tickets_of_big_map {ret} := 'tickets_of_big_map ret in",
    "let tickets_of_big_map {v ret} := 'tickets_of_big_map v ret in"
  )
  gsub(content,
    "and \"'tickets_of_big_map\" :=\n" +
    "    (fun (ret : Set) => fun",
    "and \"'tickets_of_big_map\" :=\n" +
    "    (fun (v ret : Set) => fun"
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_tx_rollup_commitment_repr(target_path)
  path = File.join(target_path, "Tx_rollup_commitment_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  pattern = <<-END
  Definition rpc_arg := H.(S.HASH.rpc_arg).
  
  (** Init function; without side-effects in Coq *)
  END
  replacement = <<-END
  (* Definition rpc_arg := H.(S.HASH.rpc_arg). *)
  END
  gsub(content, pattern, replacement)

  pattern = <<-END
  Definition rpc_arg := H.(S.HASH.rpc_arg).
  END
  replacement = <<-END
  Definition rpc_arg := H.(S.HASH.rpc_arg).
  
  Definition _Set := H.(S.HASH._Set).
  
  Definition Map := H.(S.HASH.Map).
  END
  gsub(content, pattern, replacement)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_tx_rollup_errors_repr(target_path)
  path = File.join(target_path, "Tx_rollup_errors_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_tx_rollup_l2_address(target_path)
  path = File.join(target_path, "Tx_rollup_l2_address.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  gsub(content,
    "Definition compare {state state' : Set} :=",
    "Definition compare :=")
  gsub(content, "(state := state)", "")
  gsub(content, "(state' := state')", "")
  gsub(content,
    "(x_value : Indexable.t t)",
    "(x_value : Indexable.t)")
  gsub(content,
    "{A : Set} (x_value : Indexable.t A)",
    "(x_value : Indexable.t)")
  gsub(content,
    "Definition pp {state : Set} :=",
    "Definition pp :="
  )

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_tx_rollup_l2_context(target_path)
  path = File.join(target_path, "Tx_rollup_l2_context.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)
  gsub(content, "S.Syntax.(", "S.(Tx_rollup_l2_storage_sig.STORAGE.Syntax).(")

  gsub(content,
    "Tx_rollup_l2_context_sig.SYNTAX.op_letplus _ _ := op_letplus;",
    "Tx_rollup_l2_context_sig.SYNTAX.op_letplus _ _ := op_letplus (H := H);")

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_validate_errors(target_path)
  path = File.join(target_path, "Validate_errors.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

def patch_zk_rollup_repr(target_path)
  path = File.join(target_path, "Zk_rollup_repr.v")
  return unless File.exists?(path)
  content = File.read(path, :encoding => 'utf-8')

  content = index_init_module(content)

  File.open(path, "w") do |file|
    file << content
  end
end

# We do this change because it generates collisions on include in alpha_context.
def patch_init_module_repr(target_path)
  Dir.glob(File.join(target_path, "*_repr.v")) do |path|
    content = File.read(path, :encoding => 'utf-8')

    pattern = "init_module "
    replacement = "init_module_repr "
    if content.include?(pattern) then
      gsub(content, pattern, replacement)
    end

    File.open(path, "w") do |file|
      file << content
    end
  end
end

def patch_generated_files(environment_path, protocol_path)
  patch_environment(environment_path)
  patch_alpha_context(protocol_path)
  patch_apply(protocol_path)
  patch_baking(protocol_path)
  patch_blinded_public_key_hash(protocol_path)
  patch_cache_repr(protocol_path)
  patch_contract_repr(protocol_path)
  patch_dal_apply(protocol_path)
  patch_dal_slot_repr(protocol_path)
  patch_delegate_sampler(protocol_path)
  patch_delegate_storage(protocol_path)
  patch_delegate_services(protocol_path)
  patch_entrypoint_repr(protocol_path)
  patch_fixed_point_repr(protocol_path)
  patch_indexable(protocol_path)
  patch_lazy_storage_diff(protocol_path)
  patch_lazy_storage_kind(protocol_path)
  patch_level_repr(protocol_path)
  patch_merkle_list(protocol_path)
  patch_raw_context(protocol_path)
  patch_round_repr(protocol_path)
  patch_sc_rollup_arith(protocol_path)
  patch_sc_rollup_game(protocol_path)
  patch_sc_rollup_inbox_repr(protocol_path)
  patch_sc_rollup_repr(protocol_path)
  patch_sc_rollup_wasm(protocol_path)
  patch_sc_rollups(protocol_path)
  patch_script_big_map(protocol_path)
  patch_script_cache(protocol_path)
  patch_script_expr_hash(protocol_path)
  patch_script_interpreter(protocol_path)
  patch_script_interpreter_logging(protocol_path)
  patch_script_ir_translator(protocol_path)
  patch_script_ir_unparser(protocol_path)
  patch_script_map(protocol_path)
  patch_script_set(protocol_path)
  patch_script_typed_ir(protocol_path)
  patch_script_typed_ir_size(protocol_path)
  patch_stake_storage(protocol_path)
  patch_storage(protocol_path)
  patch_storage_functors(protocol_path)
  patch_ticket_accounting(protocol_path)
  patch_ticket_lazy_storage_diff(protocol_path)
  patch_ticket_operations_diff(protocol_path)
  patch_ticket_scanner(protocol_path)
  patch_tx_rollup_commitment_repr(protocol_path)
  patch_tx_rollup_errors_repr(protocol_path)
  patch_tx_rollup_l2_address(protocol_path)
  patch_tx_rollup_l2_context(protocol_path)
  patch_validate_errors(protocol_path)
  patch_zk_rollup_repr(protocol_path)
  patch_init_module_repr(protocol_path)
end

patch_generated_files(ARGV[0], ARGV[1])

require 'set'
require 'optparse'

def get_requires(fil)
  filter = ->(lin) { /^Require (?:Import )?(.*)\.$/.match(lin)&.[](1) }
  File.readlines(fil)
    .select(&filter)
    .map(&filter)
end

def filename_to_module(file_name)
  file_name
    .gsub(/(\.\/)?src\//, "")
    .gsub(/.v$/, "")
    .gsub(/\//, ".")
    .gsub(/^/, "TezosOfOCaml.")
end

def transitive_deps(deps_hash, mod)
  return Set.new unless deps_hash.key? mod
  (Set.new(deps_hash[mod]) | deps_hash[mod].map {|dep| transitive_deps(deps_hash, dep)}).flatten
end

def create_deps(proto_folder:)
  deps = Dir.glob("#{proto_folder}/*.v", File::FNM_PATHNAME)
  deps.map do |fil|
    mod = filename_to_module("#{fil}")
    mod_deps = get_requires(fil)
    [mod, mod_deps]
  end.to_h
end

def print_mod_deps(mod_deps_h)
  mod_deps_h.each do |mod, deps|
    puts "- [ ] #{mod}"
    deps.each do |dep|
      puts "  * #{dep}"
    end
  end
end

def main
  options = {}
  OptionParser.new do |opts|
    opts.banner = <<~EOS
        Usage: ruby deps.rb [options]

        Print the dependencies of each file in the folder passed to
        the option [--folder]. The dependencies are the [Require]
        lines.

    EOS
    opts.on("-d", "--folder FOLDER", "Choose the folder to run") do |opt|
      options[:proto_folder] = opt
    end
  end.parse!

  deps_h = create_deps(**options)
  deps_h_trans = deps_h.map do |mod, _|
    [mod, transitive_deps(deps_h, mod)]
  end.to_h.sort_by do |_, deps|
    deps.size
  end

  print_mod_deps deps_h_trans
end

main

require "graphql/client"
require "graphql/client/http"

module GITLAB_API
  HTTP = GraphQL::Client::HTTP.new("https://gitlab.com/api/graphql") do
    def headers(context)
      { "User-Agent": "My Client" }
    end
  end  

  Schema = GraphQL::Client.load_schema(HTTP)

  Client = GraphQL::Client.new(schema: Schema, execute: HTTP)
end

HeroNameQuery = GITLAB_API::Client.parse <<-'GRAPHQL'
  query($sprint: String!) {
    project(fullPath: "formal-land/coq-tezos-of-ocaml") {
      issues(labelName: [$sprint]) {
        count
        nodes {
          assignees {
            nodes {
              id
              name
              username
            }
          }
          id
          iid
          state
          title
          webUrl
          weight
        }
      }
      name
    }
  }
GRAPHQL

$output = ""

def add_sprint(sprint, dates)
  result =
    GITLAB_API::Client.query(
      HeroNameQuery,
      variables: {sprint: "sprint-#{sprint}"}
    )
  $output << "## Sprint #{sprint}\n"
  $output << "> #{dates}\n\n"
  total_weight = 0
  done_weight = 0
  body = ""
  body << "| Issue | Assigned | Weight | Done | Title |\n"
  body << "|-------|:--------:|:------:|:----:|-------|\n"
  for issue in result.data.project.issues.nodes.sort_by {|issue| issue.id} do
    total_weight += issue.weight || 0
    done_weight += issue.state == "closed" ? issue.weight || 0 : 0
    assignees =
      issue.assignees.nodes.map do |assignee|
        assignee.name.split.map {|part| part[0]}.map(&:capitalize).join("")
      end
    body << "| [Issue \##{issue.iid}](#{issue.web_url}) | #{assignees.join(", ")} | #{issue.weight || "?"}&nbsp;💪 | #{issue.state == "closed" ? "🟢" : "🔴"} | #{issue.title} |\n"
  end
  $output << "Done: #{done_weight}/#{total_weight}\n\n"
  body << "\n"
  $output << body
end

add_sprint(8, "2022-10-17 - 2022-10-28")
add_sprint(7, "2022-08-22 - 2022-08-26")
add_sprint(6, "2022-08-03 - 2022-08-19")
add_sprint(5, "2022-06-20 - 2022-08-01")
add_sprint(4, "2022-05-06 - 2022-06-17")
add_sprint(3, "2022-05-06 - 2022-06-03")

File.open("src/Status/sprints.md", "w") do |file|
  file << $output
end

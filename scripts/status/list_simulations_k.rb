# coding: utf-8
output = ""

# Interpreter
begin
  output << "## [Script_interpreter.v](/docs/proto_k/proofs/script_interpreter)\n"
  output << "### Proofs\n"

  proofs = File.read("src/Proto_K/Proofs/Script_interpreter.v")
  body = ""
  index = 0
  nb_successful = 0
  instructions = proofs.to_enum(:scan, /grep @With_family\.(\w+)\./).map {Regexp.last_match}

  # We check if an instruction is correct by checking if there is an "admit"
  # before the next instruction or end of the whole proof.
  for instruction in instructions do
    next_admit = instruction.post_match.match("admit")
    next_grep_or_end = instruction.post_match.match(/(grep)|(Admitted)|(Qed)/)
    next_admit_offset = next_admit && next_admit.offset(0)[0]
    next_grep_or_end_offset = next_grep_or_end && next_grep_or_end.offset(0)[0]
    index += 1
    is_complete =
      !next_admit_offset || next_admit_offset > next_grep_or_end_offset
    nb_successful += is_complete ? 1 : 0
    body << "#{index}. #{instruction[1]} #{is_complete ? "🟢" : "🔴"}\n"
  end

  $progress_interpreter = 100 * nb_successful / index
  output << "#{$progress_interpreter}%\n"
  output << body
  output << "\n"
end

# Translator
output << "## [Script_ir_translator.v](/docs/proto_k/proofs/script_ir_translator)\n"

# Look into [file] for defintions and iterate over them
def get_definitions(file)
  result = []
  File.readlines(file).each do |line|
    next if line.strip.empty?
    next if line.match(/record/) # skip records
    m = line.match(/(?:Fixpoint|Definition|with) ([a-z]\S+)/)
    next if not m
    next if m[1].empty?
    next if m[1].start_with?("with_") # record update syntax constructors
    result << m[1]
  end
  result
end

translator_definitions = get_definitions("src/Proto_K/Script_ir_translator.v")

# Translator simulations
begin
  simulations = File.read("src/Proto_K/Simulations/Script_ir_translator.v")
  index = 0
  nb_successful = 0
  body = ""

  # We should find the simulation in the order stated above.
  translator_definitions.each do |definition|
    simulation_name = "dep_#{definition}"
    simulation_match = simulations.match(/(Definition|Fixpoint) #{simulation_name}/)
    is_complete = !!simulation_match
    simulations = simulation_match ? simulation_match.post_match : simulations
    index += 1
    nb_successful += is_complete ? 1 : 0
    if is_complete then
      body << "#{index}. [#{simulation_name}](/docs/proto_k/simulations/script_ir_translator##{simulation_name}) 🟢\n"
    else
      body << "#{index}. #{simulation_name} 🔴\n"
    end
  end

  output << "### Simulations\n"
  output << "We also check that the simulations are in the right order.\n\n"
  output << "#{100 * nb_successful / index}% #{nb_successful == index ? "🏆" : ""}\n"
  output << body
  output << "\n"
end

# Translator proofs
begin
  proofs = File.read("src/Proto_K/Proofs/Script_ir_translator.v")
  index = 0
  nb_successful = 0
  body = ""

  # We should find the simulation lemmas in the order stated above.
  translator_definitions.each do |definition|
    lemma_name = "dep_#{definition}_eq"
    lemma_match = proofs.match(/(Lemma|Fixpoint) #{lemma_name}/)
    is_complete = !!lemma_match
    proofs = lemma_match ? lemma_match.post_match : proofs
    index += 1
    nb_successful += is_complete ? 1 : 0
    if is_complete then
      body << "#{index}. [#{lemma_name}](/docs/proto_k/proofs/script_ir_translator##{lemma_name}) 🟢\n"
    else
      body << "#{index}. #{lemma_name} 🔴\n"
    end
  end

  output << "### Proofs\n"
  output << "We also check that the proofs are in the right order.\n\n"
  $progress_translator = 100 * nb_successful / index
  output << "#{$progress_translator}% #{nb_successful == index ? "🏆" : ""}\n"
  output << body
  output << "\n"
end

File.open("src/Status/Proto_K_Simulations.md", "w") do |file|
  file << output
end

# Output the score for the homepage
puts "{interpreter: #{$progress_interpreter}, translator: #{$progress_translator}}"

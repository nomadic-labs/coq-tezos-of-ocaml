Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_lazy_storage_diff.

Require TezosOfOCaml.Proto_alpha.Proofs.Carbonated_map.
Require TezosOfOCaml.Proto_alpha.Proofs.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_scanner.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

Import Error.Tactics_letP.

(** In this file, one considers a lot of ticket diffs, whose amounts can be
    negative. *)

(** The function [token_and_amount] is valid. *)
Lemma token_and_amount_is_valid ctxt ex_ticket :
  Raw_context.Valid.t ctxt ->
  Ticket_scanner.ex_ticket.Valid.t ex_ticket ->
  letP? '(token, amount, ctxt) :=
    Ticket_lazy_storage_diff.token_and_amount ctxt ex_ticket in
  Ticket_token.ex_token.Valid.t token /\
  0 <> amount /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_ex_ticket.
  unfold Ticket_lazy_storage_diff.token_and_amount.
  esplit_letP. {
    apply Raw_context.consume_gas_is_valid; try assumption.
    apply Ticket_costs.Constants.cost_collect_tickets_step_is_valid.
  }
  clear ctxt H_ctxt; intros ctxt H_ctxt.
  pose proof (Ticket_token.token_and_amount_of_ex_ticket_is_valid _ H_ex_ticket)
    as H_token_and_amount.
  destruct Ticket_token.token_and_amount_of_ex_ticket.
  destruct H_token_and_amount as [H_token H_amount]. simpl.
  repeat split ; try easy.
  destruct t.
  unfold Script_int.n_num.Valid.t, Script_int.repr.Valid.non_negative.
  unfold Ticket_amount.Valid.t,  Script_int.n_num.Valid.positive,
  Script_int.repr.Valid.positive in H_amount. lia.
Qed.

(** The function [neg_token_and_amount] is valid. *)
Lemma neg_token_and_amount_is_valid ctxt ex_ticket :
  Raw_context.Valid.t ctxt ->
  Ticket_scanner.ex_ticket.Valid.t ex_ticket ->
  letP? '((token, amount), ctxt) :=
    Ticket_lazy_storage_diff.neg_token_and_amount ctxt ex_ticket in
  Ticket_token.ex_token.Valid.t token /\
  amount <> 0 /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_ex_ticket.
  unfold Ticket_lazy_storage_diff.neg_token_and_amount.
  esplit_letP. {
    now apply Ticket_lazy_storage_diff.token_and_amount_is_valid.
  }
  clear ctxt H_ctxt; intros [[token amount] ctxt] [H_token [H_amount H_ctxt]].
  esplit_letP. {
    apply Raw_context.consume_gas_is_valid; try assumption.
    apply Ticket_costs.negate_cost_is_valid.
  }
  clear ctxt H_ctxt; intros ctxt H_ctxt.
  simpl.
  repeat (split; try assumption).
  clear -H_amount.
  lia.
Qed.

(** The function [parse_value_type] is valid. *)
Lemma parse_value_type_is_valid ctxt value_type :
  Raw_context.Valid.t ctxt ->
  letP? '(ty, ctxt) :=
    Ticket_lazy_storage_diff.parse_value_type ctxt value_type
  in
  Script_typed_ir.ex_ty.Valid.t ty /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H.
  unfold Ticket_lazy_storage_diff.parse_value_type.
  apply Script_ir_translator.parse_big_map_value_ty_is_valid.
  assumption.
Qed.

(** The function [collect_token_diffs_of_node] is valid. *)
Lemma collect_token_diffs_of_node_is_valid {a : Ty.t} {A : Set} (P : A -> Prop)
  ctxt has_tickets_value node_value get_token_and_amount acc_value :
  Raw_context.Valid.t ctxt ->
  Ticket_scanner.has_tickets.Valid.t has_tickets_value ->
  (** @Question: should we define getters to obtain the implicit [a: Ty.t]
      or [ty : Script_typed_ir.ty] parameters in some types?
      E.g. to get the variable [ty] just below.
  *)
  let 'Ticket_scanner.Has_tickets _ ty := has_tickets_value in
  a = Script_typed_ir.ty.Valid.to_Ty_t ty ->
  (forall ctxt ticket,
    Raw_context.Valid.t ctxt ->
    Ticket_scanner.ex_ticket.Valid.t ticket ->
    letP? '(value, ctxt) := get_token_and_amount ctxt ticket in
    P value /\
    Raw_context.Valid.t ctxt
  ) ->
  List.Forall P acc_value ->
  letP? '(values, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_node (a := Ty.to_Set a)
      ctxt has_tickets_value node_value get_token_and_amount acc_value in
  List.Forall P values /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_has_tickets_value.
  destruct has_tickets_value as [has_tickets ty].
  intros Ha H_get_token_and_amount H_acc_value.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_node.
  esplit_letP. {
    (** @TODO unification problem, automatize *)
    apply (@Ticket_scanner.tickets_of_node_is_valid a ctxt true
    (Ticket_scanner.Has_tickets has_tickets ty)
      (@root_value Alpha_context.Script.prim node_value)) ; assumption.
  }
  clear ctxt H_ctxt; intros [ex_tickets ctxt] [H_ex_tickets H_ctxt].
  unfold op_gtgtquestioneq.
  esplit_letP. {
    eapply List.fold_left_e_is_valid with
      (P_a := fun '(acc, ctxt) =>
        List.Forall P acc /\ Raw_context.Valid.t ctxt);
      try apply H_ex_tickets;
      try easy.
    intros.
    Tactics.destruct_pairs.
    esplit_letP. {
      now apply H_get_token_and_amount.
    }
    hauto l: on.
  }
  trivial.
Qed.

Module Key_hash_map.
  Module Valid.
    (** Validity predicate for [Key_hash_map]. *)
    Definition t {a : Set} (P : a -> Prop)
      (x : Ticket_lazy_storage_diff.Key_hash_map.(Carbonated_map.S.t) a) :
      Prop :=
      Carbonated_map.Make.Valid.t (fun _ => True) P x.
  End Valid.
End Key_hash_map.

(* Speed-up the proofs by preventing evaluation in the map definition. *)
#[global] Opaque Ticket_lazy_storage_diff.Key_hash_map.

Module List_token_and_amount.
  Module Valid.
    (** Validity predicate for a list of tokens and amounts. *)
    (** TODO: check whether they can be negative or not.
    *)
    Definition t (x : list (Ticket_token.ex_token * Z.t)) : Prop :=
      List.Forall (fun '(token, z) => Ticket_token.ex_token.Valid.t token
        /\ z <> 0)
      x.
  End Valid.
End List_token_and_amount.

(** The function [collect_token_diffs_of_big_map_update] is valid. *)
Lemma collect_token_diffs_of_big_map_update_is_valid {a : Ty.t}
  ctxt big_map_id has_tickets_value update map tokens :
  Raw_context.Valid.t ctxt ->
  Ticket_scanner.has_tickets.Valid.t has_tickets_value ->
  let 'Ticket_scanner.Has_tickets _ ty := has_tickets_value in
  a = Script_typed_ir.ty.Valid.to_Ty_t ty ->
  let 'Ticket_scanner.Has_tickets has_tickets ty := has_tickets_value in
  Ticket_scanner.Ticket_inspection.has_tickets.Valid.t a has_tickets ->
  Key_hash_map.Valid.t (fun _ => True) map ->
  List_token_and_amount.Valid.t tokens ->
  letP? '(tokens, map, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_update
      (a := Ty.to_Set a)
      ctxt big_map_id has_tickets_value update map tokens in
  List_token_and_amount.Valid.t tokens /\
  Key_hash_map.Valid.t (fun _ => True) map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H H_has_tickets_value.
  destruct has_tickets_value as [has_tickets ty].
  intros Ha Hhas_tickets H_map H_tokens.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_update.
  eapply Error.split_letP  with
    (P := fun '(_, ctxt) => Raw_context.Valid.t ctxt).
  { esplit_letP. {
      apply Carbonated_map.Make.find_is_valid with
        (P_ctxt := Raw_context.Valid.t) (P_key := fun _ => True) ; trivial ;
        apply H_map.
    }
    clear ctxt H; intros [val_opt ctxt] [H_val_opt H].
    destruct val_opt; simpl; [assumption|].
    esplit_letP. {
      now apply Alpha_context.Big_map.get_opt_is_valid.
    }
    clear ctxt H ; intros [ctxt old_value] H.
    trivial.
  }
  clear ctxt H ; intros [old_value ctxt] H.
  eapply Error.split_letP with
    (P := fun '(tokens, ctxt) =>
      List_token_and_amount.Valid.t tokens /\
      Raw_context.Valid.t ctxt
    ). {
    destruct old_value; simpl; [|easy].
    unfold List_token_and_amount.Valid.t.
  (* @TODO unification problem, automatize *)
    eapply (
      @collect_token_diffs_of_node_is_valid
        a (Ticket_token.ex_token * Z.t)
      (fun '(token, z) => Ticket_token.ex_token.Valid.t token /\ z <> 0)
      ctxt
      (Ticket_scanner.Has_tickets has_tickets ty) e
      Ticket_lazy_storage_diff.neg_token_and_amount tokens); try assumption.
    intros.
    apply Error.follow_letP with (fun '(value,ctxt) =>
      let '(token,z) := value in
      Ticket_token.ex_token.Valid.t token /\ z <> 0 /\
      Raw_context.Valid.t ctxt).
    { i_des_pairs. repeat split ; assumption. }
    { apply neg_token_and_amount_is_valid ; assumption. }
  }
  intros.
  step.
  esplit_letP. {
    apply Carbonated_map.Make.update_is_valid with
      (P_ctxt := Raw_context.Valid.t) (P_key := fun _ => True);
      try apply H_map;
      easy. }
  clear ctxt H; intros [already_updated ctxt] [H_already_updated H].
  eapply Error.split_letP with
    (P := fun '(acc_value, ctxt) =>
      List_token_and_amount.Valid.t acc_value /\
      Raw_context.Valid.t ctxt
    ). {
    step.
    { unfold List_token_and_amount.Valid.t.
      unfold Z.t.
      eapply (@collect_token_diffs_of_node_is_valid
        a (Ticket_token.ex_token * BinNums.Z)
        (fun '(token, z) => Ticket_token.ex_token.Valid.t token /\ z <> 0)
        ctxt (Ticket_scanner.Has_tickets has_tickets ty) e
        Ticket_lazy_storage_diff.token_and_amount l) ; try assumption.
      { intros.
      (** This (trivial) [follow_letP], which just reorganizes predicates,
          could probably be avoided. *)
        apply Error.follow_letP with (P :=
          fun '(p, ctxt) => let '(token, amount) := p in
            Ticket_token.ex_token.Valid.t token /\
            0 <> amount /\ Raw_context.Valid.t ctxt).
        { clear. i_des_pairs. repeat split ; try assumption. lia. }
        { apply token_and_amount_is_valid ; assumption. }
      }
      { destruct H0 as [H0 H1].
        inversion H0 as [ | [ex_tok z] l0 [Hex_tok Hz] Hl0 eq_l] ; [
        constructor |].
        constructor ; [ split ; assumption | assumption ].
      }
    }
    { simpl. destruct H0. split ; assumption. }
  }
  clear ctxt H ; intros [tickets ctxt] [H_tickets H].
  simpl. repeat split; try assumption.
Qed.

(** The function [collect_token_diffs_of_big_map_updates] is valid. *)
Lemma collect_token_diffs_of_big_map_updates_is_valid
  ctxt big_map_id value_type updates acc_value :
  Raw_context.Valid.t ctxt ->
  List_token_and_amount.Valid.t acc_value ->
  letP? '(acc_value, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_updates
      ctxt big_map_id value_type updates acc_value in
  List_token_and_amount.Valid.t acc_value /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_acc_value.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_updates.
  esplit_letP. {
    now apply Ticket_lazy_storage_diff.parse_value_type_is_valid.
  }
  clear ctxt H_ctxt.
  intros [[ty] ctxt] [Hty H].  (* destructing [exty] into [ty] seems to
     help us avoid the [cast_exists_eval_1] if we use step
     two lines below for some reason. *)
  simpl in Hty.
  (* step. *)
  match goal with
  | |- context[cast_exists ?T ?x] =>
    rewrite (cast_exists_eval_1 (T := T) (x := x)
       (* (E1 :=  (Ty.to_Set (Script_typed_ir.ty.Valid.to_Ty_t ty))) *)
      (E1 := [Alpha_context.context ** Script_typed_ir.ty])
    )
  end.
  esplit_letP ;
      [ apply (@Ticket_scanner.type_has_tickets_is_valid
        (Script_typed_ir.ty.Valid.to_Ty_t ty))
      ; try assumption ; reflexivity |].
  clear ctxt H.
  intros [[has_tickets ty0] ctxt] [H_has_tickets_value [H Hty0]]. subst.
  esplit_letP. {
    eapply List.fold_left_e_is_valid with
      (P_a := fun '(acc_value, already_updated, ctxt) =>
        List_token_and_amount.Valid.t acc_value /\
        Key_hash_map.Valid.t (fun _ => True) already_updated /\
        Raw_context.Valid.t ctxt
      )
      (P_b := fun _ => True).
    {
       clear dependent acc_value; clear dependent ctxt;
        intros [[acc_value0 already_updated] ctxt0] item;
        intros [H_acc_value0 [H_already_updated H_ctxt0]] H_item.
        apply Error.follow_letP with (P := fun '(y, ctxt) =>
        let
        '(acc_value, already_updated) := y in
         List_token_and_amount.Valid.t acc_value /\
         Key_hash_map.Valid.t (fun _ : M* Script_repr.expr => True)
           already_updated /\ Raw_context.Valid.t ctxt
        ).
        { clear.
          intros. step. step. destruct H as [H1 [H2 H3]].
          repeat split ; assumption.
        }
        inversion H_has_tickets_value. subst.
        (** @TODO : unifiy the implicit argument
          [(Ty.to_Set (Script_typed_ir.ty.Valid.to_Ty_t ty))] with
          [Alpha_context.context ** Script_typed_ir.ty].
          Possible missing conditions in the former lemmas.
        *)
        assert (rewrite1 :  [Alpha_context.context ** Script_typed_ir.ty]
          = (Ty.to_Set (Script_typed_ir.ty.Valid.to_Ty_t ty))).
        { admit. }
        rewrite rewrite1.
        apply (@collect_token_diffs_of_big_map_update_is_valid
          (Script_typed_ir.ty.Valid.to_Ty_t ty) ctxt0 big_map_id
          (Ticket_scanner.Has_tickets has_tickets ty)) ; try assumption ;
          reflexivity.
    }
    { repeat split; trivial.
      apply Carbonated_map.Make.empty_is_valid.
    }
    { now apply List.Forall_True. }
  }
  clear dependent acc_value; clear dependent ctxt;
  i_des_pairs.
  easy.
Admitted.

(** The function [collect_token_diffs_of_big_map] is valid. *)
Lemma collect_token_diffs_of_big_map_is_valid {A : Set} (P : A -> Prop)
  ctxt get_token_and_amount big_map_id acc_value :
  Raw_context.Valid.t ctxt ->
  (forall ctxt ticket,
    Raw_context.Valid.t ctxt ->
    Ticket_scanner.ex_ticket.Valid.t ticket ->
    letP? '(value, ctxt) := get_token_and_amount ctxt ticket in
    P value /\
    Raw_context.Valid.t ctxt
  ) ->
  List.Forall P acc_value ->
  letP? '(values, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_big_map
      ctxt get_token_and_amount big_map_id acc_value in
  List.Forall P values /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H H_get_token_and_amount H_acc_value.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_big_map.
  esplit_letP ; [
    now apply Raw_context.consume_gas_is_valid |].
  clear ctxt H ; intros ctxt H.
  esplit_letP ; [
    now apply Alpha_context.Big_map._exists_is_valid |].
  clear ctxt H; intros [ctxt key_val_tys] H.
  destruct key_val_tys as [[? value_ty]|] ; simpl ; [|easy].
  esplit_letP ; [
    now apply parse_value_type_is_valid |].
  clear ctxt H; intros [ex_value_type ctxt] [Hex_value_type H].
  step.
  match goal with
  | |- context[cast_exists ?T ?x] =>
    rewrite (cast_exists_eval_1 (T := T) (x := x)
      (E1 := [Alpha_context.context ** Script_typed_ir.ty]))
  end.
  esplit_letP ; [
     apply (@Ticket_scanner.type_has_tickets_is_valid
      (@Script_typed_ir.ty.Valid.to_Ty_t t)) ; try assumption ; reflexivity |].
  clear ctxt H;
    intros [has_tickets_value ctxt] [H_has_tickets_value H].
  esplit_letP ; [
    now apply Alpha_context.Big_map.list_key_values_is_valid |].
  clear ctxt H; intros [ctxt exprs] H.
  apply List.fold_left_e_is_valid with (P_b := fun _ => True).
  { intros ; Tactics.destruct_pairs.
    (** need to rewrite:
       [(fun '(values, ctxt) => Forall P values /\ Raw_context.Valid.t ctxt) x]
       into [Forall ?M4679 values /\ Raw_context.Valid.t ctxt0]
       where [x = (values,ctxt0)].
       Why this mismatch?
     *)
    Fail now apply collect_token_diffs_of_node_is_valid.
    admit.
  }
  { easy. }
  { auto. }
Admitted.

(** The function [collect_token_diffs_of_big_map_and_updates] is valid. *)
Lemma collect_token_diffs_of_big_map_and_updates_is_valid
  ctxt big_map_id updates acc_value :
  Raw_context.Valid.t ctxt ->
  List_token_and_amount.Valid.t acc_value ->
  letP? '(acc_value, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_and_updates
      ctxt big_map_id updates acc_value in
  List_token_and_amount.Valid.t acc_value /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H H_acc_value.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_and_updates.
  esplit_letP ; [
    now apply Raw_context.consume_gas_is_valid |].
  clear ctxt H ; intros ctxt H.
  esplit_letP ; [
    now apply Alpha_context.Big_map._exists_is_valid |].
  clear ctxt H; intros [ctxt key_val_opt] H.
  destruct key_val_opt ; simpl ; [| easy ].
  Tactics.destruct_pairs.
  now apply collect_token_diffs_of_big_map_updates_is_valid.
Qed.

(** The function [collect_token_diffs_of_big_map_diff] is valid. *)
Lemma collect_token_diffs_of_big_map_diff_is_valid ctxt diff_item acc_value :
  Raw_context.Valid.t ctxt ->
  Lazy_storage_diff.diffs_item.Valid.t diff_item ->
  List_token_and_amount.Valid.t acc_value ->
  letP? '(acc_value, ctxt) :=
    Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_diff
      ctxt diff_item acc_value in
  List_token_and_amount.Valid.t acc_value /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H H_diff_item H_acc_value.
  unfold Ticket_lazy_storage_diff.collect_token_diffs_of_big_map_diff.
  esplit_letP ; [
    now apply Raw_context.consume_gas_is_valid |].
  clear ctxt H ; intros ctxt H.
  destruct H_diff_item; simpl; [| easy ].
  destruct diff; rewrite cast_eval.
  { apply collect_token_diffs_of_big_map_is_valid; try easy.
    intros.
    eapply Error.follow_letP ; [| apply neg_token_and_amount_is_valid ] ;
      try assumption.
    (* ; hauto lq: on. *)
    (** @TODO : below, fragile, correct *)
    intros. step. destruct x. destruct p. destruct H2. destruct H3.
    repeat split ; try assumption.
  }
  { step.
    { grep @Lazy_storage_diff.Existing.
      now apply collect_token_diffs_of_big_map_and_updates_is_valid.
    }
    { grep @Lazy_storage_diff.Copy.
      esplit_letP. {
        apply collect_token_diffs_of_big_map_is_valid;
          try easy;
          try apply H_acc_value.
        intros.
        eapply Error.follow_letP ; [| apply token_and_amount_is_valid] ;
          try assumption.
          (* ; hauto lq: on. *)
         (** @TODO : below, fragile, correct *)
        intros  [[ex_tok z] ctxt1] [? [Hz ?]].
        repeat split ; try assumption.
        clear -Hz. lia.
      }
      intros [] [].
      now apply collect_token_diffs_of_big_map_and_updates_is_valid.
    }
    { grep @Lazy_storage_diff.Alloc.
      now apply collect_token_diffs_of_big_map_updates_is_valid.
    }
  }
Qed.

(** The function [ticket_diffs_of_lazy_storage_diff] is valid. *)
Lemma ticket_diffs_of_lazy_storage_diff_is_valid ctxt diff_items :
  Raw_context.Valid.t ctxt ->
  List.Forall Lazy_storage_diff.diffs_item.Valid.t diff_items ->
  letP? '(acc_value, ctxt) :=
    Ticket_lazy_storage_diff.ticket_diffs_of_lazy_storage_diff
      ctxt diff_items in
  List_token_and_amount.Valid.t acc_value /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_diff_items.
  unfold Ticket_lazy_storage_diff.ticket_diffs_of_lazy_storage_diff.
  eapply List.fold_left_e_is_valid with
    (P_b := Lazy_storage_diff.diffs_item.Valid.t).
  { intros; Tactics.destruct_pairs.
    now apply collect_token_diffs_of_big_map_diff_is_valid.
  }
  { hauto l: on.
  }
  { assumption.
  }
Qed.

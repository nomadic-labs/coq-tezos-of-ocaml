Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Vote_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.

(** The encoding [ballot_encoding] is valid. *)
Lemma ballot_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Vote_repr.ballot_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros []; hfcrush.
Qed.
#[global] Hint Resolve ballot_encoding_is_valid : Data_encoding_db.

(** The equality [equal_ballot] is valid. *)
Lemma equal_ballot_is_valid :
  Compare.Equal.Valid.t (fun _ => True) Vote_repr.equal_ballot.
Proof.
  constructor; sauto q: on.
Qed.

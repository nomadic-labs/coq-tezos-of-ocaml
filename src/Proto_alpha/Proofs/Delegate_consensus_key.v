Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Delegate_consensus_key.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.

(** The function [active_pubkey_is_valid] is valid *)
Lemma active_pubkey_is_valid
    ctxt delegate :
  Raw_context.Valid.t ctxt ->
  letP? _ := Delegate_consensus_key.active_pubkey ctxt delegate in
    True.
Proof.
Admitted.

(** [register_update] is valid *)
Lemma register_update_is_valid ctxt delegate pk :
  Raw_context.Valid.t ctxt ->
  letP? ctxt := Delegate_consensus_key.register_update ctxt delegate pk in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.
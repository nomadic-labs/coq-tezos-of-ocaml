Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_balance_key.

Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_hash_builder.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

(** The function [of_ex_token] is valid. *)
Lemma of_ex_token_is_valid ctxt owner ex_token :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t ex_token ->
  letP? '(_, ctxt) := Ticket_balance_key.of_ex_token ctxt owner ex_token in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_ex_token.
  unfold Ticket_balance_key.of_ex_token.
  destruct H_ex_token.
  eapply Error.split_letP ;[
    apply Script_ir_unparser.unparse_ty_is_valid ; assumption |].
  clear ctxt H_ctxt; intros [cont_ty_unstripped ctxt] H_ctxt.
  eapply Error.split_letP. {
    apply Raw_context.consume_gas_is_valid; try assumption.
    apply Script_repr.strip_annotations_cost_is_valid.
  }
  clear ctxt H_ctxt; intros ctxt H_ctxt.
  eapply Error.split_letP. {
    apply (@Script_ir_unparser.unparse_comparable_data_is_valid
      a (Ty.to_Set a)) ; assumption.
  }
  clear dependent contents; clear ctxt H_ctxt; intros [contents ctxt] H_ctxt.
  change Script_typed_ir.address.record with (Ty.to_Set Ty.Address).
  eapply Error.split_letP. {
    apply Script_ir_translator.unparse_data_is_valid ; [
      assumption | constructor | constructor].
  }
  clear ticketer ctxt H_ctxt; intros [ticketer ctxt] H_ctxt.
  eapply Error.split_letP. {
    apply Script_ir_translator.unparse_data_is_valid ; [
      assumption | reflexivity | constructor].
  }
  clear owner ctxt H_ctxt; intros [owner ctxt] H_ctxt.
  now apply Ticket_hash_builder.make_is_valid.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_accounting.

Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_operations_diff.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token_map.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

Module Ticket_token_map.
  (** The function [balance_diff] is valid. *)
  Lemma balance_diff_is_valid ctxt token map :
    Raw_context.Valid.t ctxt ->
    Ticket_token.ex_token.Valid.t token ->
    Ticket_token_map.Valid.t (fun _ => True) map ->
    letP? '(_, ctxt) :=
      Ticket_accounting.Ticket_token_map.balance_diff ctxt token map in
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt H_token H_map.
    unfold Ticket_accounting.Ticket_token_map.balance_diff.
    eapply Error.split_letP. {
      eapply Ticket_token_map.find_is_valid;
        try apply H_map;
        assumption.
    }
    clear ctxt H_ctxt; intros [amnt_opt ctxt] [H_amnt_opt H_ctxt].
    simpl.
    trivial.
  Qed.

  (** The function [merge_overlap] is valid. *)
  Lemma merge_overlap_is_valid ctxt b1 b2 :
    Raw_context.Valid.t ctxt ->
    letP? '(_, ctxt) :=
      Ticket_accounting.Ticket_token_map.merge_overlap ctxt b1 b2 in
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt.
    unfold Ticket_accounting.Ticket_token_map.merge_overlap.
    eapply Error.split_letP. {
      apply Raw_context.consume_gas_is_valid; trivial.
      apply Ticket_costs.add_z_cost_is_valid.
    }
    trivial.
  Qed.

  (** The function [of_list_with_merge] is valid. *)
  Lemma of_list_with_merge_is_valid ctxt token_amounts :
    Raw_context.Valid.t ctxt ->
    Ticket_lazy_storage_diff.List_token_and_amount.Valid.t token_amounts ->
    letP? '(map, ctxt) :=
      Ticket_accounting.Ticket_token_map.of_list_with_merge
        ctxt token_amounts in
    Ticket_token_map.Valid.t (fun _ => True) map /\
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt H_token_amounts.
    unfold Ticket_accounting.Ticket_token_map.of_list_with_merge.
    apply Ticket_token_map.of_list_is_valid; try easy.
    { clear; intros.
      eapply Error.follow_letP; [|apply merge_overlap_is_valid]; hauto l: on.
    }
    { eapply List.Forall_impl; [|apply H_token_amounts].
      hauto l: on.
    }
  Qed.

  (** The function [add] is valid. *)
  Lemma add_is_valid ctxt m1 m2 :
    Raw_context.Valid.t ctxt ->
    Ticket_token_map.Valid.t (fun _ => True) m1 ->
    Ticket_token_map.Valid.t (fun _ => True) m2 ->
    letP? '(map, ctxt) :=
      Ticket_accounting.Ticket_token_map.add ctxt m1 m2 in
    Ticket_token_map.Valid.t (fun _ => True) map /\
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt H_m1 H_m2.
    unfold Ticket_accounting.Ticket_token_map.add.
    apply Ticket_token_map.merge_is_valid; try assumption.
    intros.
    eapply Error.follow_letP; [|apply merge_overlap_is_valid]; hauto l: on.
  Qed.

  (** The function [sub] is valid. *)
  Lemma sub_is_valid ctxt m1 m2 :
    Raw_context.Valid.t ctxt ->
    Ticket_token_map.Valid.t (fun _ => True) m1 ->
    Ticket_token_map.Valid.t (fun _ => True) m2 ->
    letP? '(map, ctxt) :=
      Ticket_accounting.Ticket_token_map.sub ctxt m1 m2 in
    Ticket_token_map.Valid.t (fun _ => True) map /\
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt H_m1 H_m2.
    unfold Ticket_accounting.Ticket_token_map.sub.
    eapply Error.split_letP. {
      apply Ticket_token_map.map_is_valid with
        (P_a := fun _ => True) (P_b := fun _ => True);
        try assumption.
      intros.
      eapply Error.split_letP. {
        apply Raw_context.consume_gas_is_valid; trivial.
        apply Ticket_costs.negate_cost_is_valid.
      }
      easy.
    }
    clear m2 H_m2 ctxt H_ctxt; intros [m2 ctxt] [H_m2 H_ctxt].
    now apply add_is_valid.
  Qed.
End Ticket_token_map.

(** The function [ticket_balances_of_value] is valid. *)
Lemma ticket_balances_of_value_is_valid {a : Ty.t}
  ctxt include_lazy ty_value value_value ht ty :
  Raw_context.Valid.t ctxt ->
  (Ticket_scanner.Has_tickets ht ty = ty_value) ->
  (a = Script_typed_ir.ty.Valid.to_Ty_t ty) ->
  Ticket_scanner.has_tickets.Valid.t ty_value ->
  Script_typed_ir.Valid.value a value_value ->
  letP? '(map, ctxt) :=
    Ticket_accounting.ticket_balances_of_value
      ctxt include_lazy ty_value value_value in
  Ticket_token_map.Valid.t (fun _ => True) map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt ty_value_eq Ha H_ty_value H_value_value.
  destruct ty_value as [ht' ty'].
  inversion  ty_value_eq.
  subst ht'. subst ty'.
  unfold Ticket_accounting.ticket_balances_of_value.
  eapply Error.split_letP. {
    now (apply (@Ticket_scanner.tickets_of_value_is_valid a _ _  _ ht ty)).
  }
  clear ctxt H_ctxt; intros [tickets ctxt] [H_tickets H_ctxt].
  eapply Error.split_letP. {
    eapply List.fold_left_e_is_valid with
      (P_a := fun '(acc_value, ctxt) =>
        Ticket_lazy_storage_diff.List_token_and_amount.Valid.t acc_value /\
        Raw_context.Valid.t ctxt
      );
      try apply H_tickets.
    { intros; Tactics.destruct_pairs.
      match goal with
      | |- context[Ticket_token.token_and_amount_of_ex_ticket ?x] =>
        pose proof (Ticket_token.token_and_amount_of_ex_ticket_is_valid x)
      end.
      destruct Ticket_token.token_and_amount_of_ex_ticket.
      eapply Error.split_letP. {
        now apply Raw_context.consume_gas_is_valid.
      }
      intros; simpl.
      split ; [| assumption].
      constructor ; [| assumption ].
      (** Below: pedestrian, should be automatized. *)
      apply H2 in H0.
      destruct H0.
      split ; try assumption.
      clear -H4. unfold Ticket_amount.Valid.t in H4.
      specialize (Script_int.to_zint_positive t0) as aux1.
      apply aux1 in H4. lia.
    }
    { hauto l: on. }
  }
  clear ctxt H_ctxt; intros [list_value ctxt] [H_list_value H_ctxt].
  now apply Ticket_token_map.of_list_with_merge_is_valid.
Qed.

(** The function [update_ticket_balances_raw] is valid. *)
Lemma update_ticket_balances_raw_is_valid
  ctxt total_storage_diff token destinations :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t token ->
  letP? '(_, ctxt) :=
    Ticket_accounting.update_ticket_balances_raw
      ctxt total_storage_diff token destinations in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_token.
  unfold Ticket_accounting.update_ticket_balances_raw.
  apply List.fold_left_e_is_valid with
    (P_b := fun _ => True);
    auto.
  clear ctxt H_ctxt;
    intros [tot_storage_diff ctxt] [owner delta];
    intros H_ctxt H_item.
  eapply Error.split_letP. {
    apply Ticket_balance_key.of_ex_token_is_valid; trivial.
  }
  clear ctxt H_ctxt; intros [key_hash ctxt] H_ctxt.
  eapply Error.split_letP. {
    now apply Ticket_storage.adjust_balance_is_valid.
  }
  clear ctxt H_ctxt; intros [storage_diff ctxt] H_ctxt.
  eapply Error.split_letP. {
    apply Raw_context.consume_gas_is_valid; trivial.
    apply Ticket_costs.add_z_cost_is_valid.
  }
  intros x H.
  simpl.
  trivial.
Qed.

(** The error returned by [invalid_ticket_transfer_error] is not internal.
    TODO: question: should it be considered an internal error?
*)
Lemma invalid_ticket_transfer_error_not_internal token amount :
  Error.not_internal [
    Ticket_accounting.invalid_ticket_transfer_error token amount
  ].
Proof.
  sauto lq: on.
Qed.

(** The function [update_ticket_balances_for_self_contract] is valid. *)
Lemma update_ticket_balances_for_self_contract_is_valid
  ctxt self ticket_diffs :
  Raw_context.Valid.t ctxt ->
  Ticket_lazy_storage_diff.List_token_and_amount.Valid.t ticket_diffs ->
  letP? '(_, ctxt) :=
    Ticket_accounting.update_ticket_balances_for_self_contract
      ctxt self ticket_diffs in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_ticket_diffs.
  unfold Ticket_accounting.update_ticket_balances_for_self_contract.
  eapply List.fold_left_e_is_valid;
    try apply H_ticket_diffs;
    try assumption.
  clear ctxt ticket_diffs H_ctxt H_ticket_diffs;
    intros [total_storage_diff ctxt] [ticket_token amount];
    intros H_ctxt H_ticket_token.
  match goal with
  | |- context[Error_monad.error_unless ?e] =>
    destruct e; simpl; [|apply invalid_ticket_transfer_error_not_internal]
  end.
  eapply update_ticket_balances_raw_is_valid.
  apply H_ctxt.
  apply H_ticket_token.
Qed.

(** The function [ticket_diffs_of_lazy_storage_diff] is valid. *)
Lemma ticket_diffs_of_lazy_storage_diff_is_valid
  ctxt storage_type_has_tickets lazy_storage_diff :
  Raw_context.Valid.t ctxt ->
  List.Forall Lazy_storage_diff.diffs_item.Valid.t lazy_storage_diff ->
  letP? '(map, ctxt) :=
    Ticket_accounting.ticket_diffs_of_lazy_storage_diff
      ctxt storage_type_has_tickets lazy_storage_diff in
  Ticket_token_map.Valid.t (fun _ => True) map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_lazy_storage_diff.
  unfold Ticket_accounting.ticket_diffs_of_lazy_storage_diff.
  destruct Ticket_scanner.has_tickets_value; simpl.
  { eapply Error.split_letP. {
      now apply
        Ticket_lazy_storage_diff.ticket_diffs_of_lazy_storage_diff_is_valid.
    }
    clear ctxt H_ctxt; intros [diffs ctxt] [H_diffs H_ctxt].
    now apply Ticket_accounting.Ticket_token_map.of_list_with_merge_is_valid.
  }
  { split; trivial.
    apply Carbonated_map.Make.empty_is_valid.
  }
Qed.

(** The function [to_ticket_receipt] is valid. *)
Lemma to_ticket_receipt_is_valid ctxt owner ticket_token_map :
  Raw_context.Valid.t ctxt ->
  letP? '(_, ctxt) :=
    Ticket_accounting.Ticket_token_map.to_ticket_receipt
      ctxt owner ticket_token_map in
  Raw_context.Valid.t ctxt.
Proof.
  intro H.
  unfold Ticket_accounting.Ticket_token_map.to_ticket_receipt.
  simpl.
  (** @TODO define function : Carbonated_map.Make.fold_es_is_valid. *)
Admitted.

(** The function [ticket_diffs] is valid. *)
Lemma ticket_diffs_is_valid {arg storage : Ty.t}
  ctxt has_contract arg_type_has_tickets storage_type_has_tickets arg_value
  old_storage new_storage lazy_storage_diff
  ht_arg ty_arg ht_storage ty_storage :
  Raw_context.Valid.t ctxt ->
  Ticket_scanner.Has_tickets ht_arg ty_arg = arg_type_has_tickets ->
  Ticket_scanner.Has_tickets ht_storage ty_storage = storage_type_has_tickets ->
  Ticket_scanner.has_tickets.Valid.t arg_type_has_tickets ->
  Ticket_scanner.has_tickets.Valid.t storage_type_has_tickets ->
  arg = Script_typed_ir.ty.Valid.to_Ty_t ty_arg ->
  storage = Script_typed_ir.ty.Valid.to_Ty_t ty_storage ->
  Script_typed_ir.Valid.value arg arg_value ->
  Script_typed_ir.Valid.value storage old_storage ->
  Script_typed_ir.Valid.value storage new_storage ->
  List.Forall Lazy_storage_diff.diffs_item.Valid.t lazy_storage_diff ->
  letP? '(map, ctxt) :=
    Ticket_accounting.ticket_diffs
      (A := Ty.to_Set arg) (B := Ty.to_Set storage)
      ctxt has_contract arg_type_has_tickets storage_type_has_tickets
      arg_value
      old_storage
      new_storage
      lazy_storage_diff in
  (* Ticket_token_map.Valid.t (fun _ => True) map /\ *)
  Raw_context.Valid.t ctxt.
Proof.
  intros
  H_ctxt arg_eq storage_eq H_arg_type_has_tickets H_storage_type_has_tickets
  Harg Hstorage Harg_value Hold_storage Hnew_storage H_lazy_storage_diff.
  destruct arg_type_has_tickets as [ht' ty'].
  inversion arg_eq. subst ht'. subst ty'.
  clear arg_eq.
  destruct storage_type_has_tickets as [ht' ty'].
  inversion storage_eq. subst ht'. subst ty'.
  clear storage_eq.
  unfold Ticket_accounting.ticket_diffs.
  eapply Error.split_letP. {
    apply (@ticket_balances_of_value_is_valid arg _ _ _ _ ht_arg ty_arg) ;
    try assumption ; reflexivity.
  }
  intros [] [].
  eapply Error.split_letP. {
    now apply ticket_diffs_of_lazy_storage_diff_is_valid.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply (@ticket_balances_of_value_is_valid storage _ _ _ _ ht_storage
    ty_storage) ; try assumption ; reflexivity.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply (@ticket_balances_of_value_is_valid storage _ _ _ _ ht_storage
    ty_storage) ; try assumption ; try reflexivity.
  }
  intros [] [].
  eapply Error.split_letP. {
    now apply Ticket_token_map.add_is_valid.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply Ticket_token_map.sub_is_valid; trivial.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply Ticket_token_map.sub_is_valid; trivial.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply Ticket_token_map.to_ticket_receipt_is_valid; trivial.
  }
  intros [] [].
  hauto l: on.
Qed.

(** The function [update_ticket_balances] is valid. *)
(** @TODO redefine [Script_typed_ir.packed_internal_operation.Valid.t] without
    simulation. The old proof is kept as a future hint.
*)
Lemma update_ticket_balances_is_valid ctxt self ticket_diffs operations :
  Raw_context.Valid.t ctxt ->
  Ticket_token_map.Valid.t (fun _ => True) ticket_diffs ->
  (* List.Forall Script_typed_ir.packed_internal_operation.Valid.t operations
  -> *)
  letP? '(_, ctxt) :=
    Ticket_accounting.update_ticket_balances
      ctxt self ticket_diffs operations
  in Raw_context.Valid.t ctxt.
Proof.
  (* intros H_ctxt H_ticket_diffs H_operations.
  unfold Ticket_accounting.update_ticket_balances.
  eapply Error.split_letP. {
    now apply Ticket_operations_diff.ticket_diffs_of_operations_is_valid.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply Ticket_token_map.to_list_is_valid; trivial; apply H_ticket_diffs.
  }
  intros [] [].
  eapply Error.split_letP. {
    apply update_ticket_balances_for_self_contract_is_valid; trivial.
    eapply List.Forall_impl; [|
      match goal with
      | H : _ |- _ => apply H
      end
    ].
    hauto lq: on.
  }
  intros [] ?.
  apply List.fold_left_e_is_valid with
    (P_b := Ticket_operations_diff.ticket_token_diff.Valid.t);
    trivial.
  intros; Tactics.destruct_pairs.
  eapply Error.split_letP with
    (P := fun '(_, ctxt) => Raw_context.Valid.t ctxt). {
    destruct_all Ticket_operations_diff.ticket_token_diff.
    match goal with
    | H : Ticket_operations_diff.ticket_token_diff.Valid.t _ |- _ =>
      destruct H
    end; simpl in *.
    match goal with
    | H : Ticket_token.ex_token.Valid.t  _ |- _ =>
      destruct H
    end; simpl in *.
    destruct Contract_repr.equal; simpl; [easy|].
    eapply Error.split_letP. {
      now apply Ticket_token_map.balance_diff_is_valid.
    }
    intros [] ?; simpl.
    trivial.
  }
  intros [] ?.
  match goal with
  | |- context[Error_monad.error_unless ?e] =>
    destruct e; simpl; [|apply invalid_ticket_transfer_error_not_internal]
  end.
  eapply Error.split_letP with
    (P := fun '(_, ctxt) => Raw_context.Valid.t ctxt). {
    apply List.fold_left_e_is_valid with (P_b := fun _ => True); auto.
    intros; Tactics.destruct_pairs.
    eapply Error.split_letP. {
      now apply Raw_context.consume_gas_is_valid.
    }
    trivial.
  }
  intros; Tactics.destruct_pairs.
  eapply update_ticket_balances_raw_is_valid; trivial.
  dtauto.
*)
Admitted.

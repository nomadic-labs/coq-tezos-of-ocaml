Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V8.Proofs.Context.
Require TezosOfOCaml.Proto_alpha.Main.
Require TezosOfOCaml.Proto_alpha.Proofs.Apply.
Require TezosOfOCaml.Proto_alpha.Proofs.Baking.
Require TezosOfOCaml.Proto_alpha.Proofs.Block_header_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Context.
Require TezosOfOCaml.Proto_alpha.Proofs.Migration_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Operation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Stake_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Validate.
Require TezosOfOCaml.Proto_alpha.Simulations.Context.

(** The encoding [block_header_data_encoding] is valid. *)
Lemma block_header_data_encoding_is_valid :
  Data_encoding.Valid.t Block_header_repr.Protocol_data.Valid.t
    Main.block_header_data_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve block_header_data_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom block_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Apply_results.block_metadata_encoding.
#[global] Hint Resolve block_metadata_encoding_is_valid : Data_encoding_db.

(** The encoding [block_header_metadata_encoding] is valid. *)
Lemma block_header_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Main.block_header_metadata_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve block_header_metadata_encoding_is_valid :
  Data_encoding_db.

(** The encoding [operation_data_encoding] is valid. *)
Lemma operation_data_encoding_is_valid :
  Data_encoding.Valid.t Operation_repr.Packed_protocol_data.Valid.t
    Main.operation_data_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_data_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom operation_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Apply_results.operation_metadata_encoding.
#[global] Hint Resolve operation_metadata_encoding_is_valid : Data_encoding_db.

(** The encoding [operation_receipt_encoding] is valid. *)
Lemma operation_receipt_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Main.operation_receipt_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_receipt_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom operation_data_and_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Apply_results.operation_data_and_metadata_encoding.
#[global] Hint Resolve operation_data_and_metadata_encoding_is_valid :
  Data_encoding_db.

(** The encoding [operation_data_and_receipt_encoding] is valid. *)
Lemma operation_data_and_receipt_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Main.operation_data_and_receipt_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_data_and_receipt_encoding_is_valid :
  Data_encoding_db.

Module validation_state.
  Module Valid.
    Import Proto_alpha.Main.validation_state.

    (** Validity predicate for the type [validation_state]. *)
    Record t (x : Main.validation_state) : Prop := {
      validity_state : Validate.validation_state.Valid.t x.(validity_state);
      application_state :
        Apply.application_state.Valid.t x.(application_state);
    }.
  End Valid.
End validation_state.

(** The function [prepare_context] is valid. *)
Lemma prepare_context_is_valid ctxt level predecessor_timestamp timestamp
  (H_ctxt : Context.Valid.t ctxt)
  (H_level : Int32.Valid.t level)
  (H_predecessor_timestamp : Time.Valid.t predecessor_timestamp)
  (H_timestamp : Time.Valid.t timestamp) :
  letP? '(ctxt, updates, migrations) :=
    Main.prepare_context (Context.to_t ctxt)
      level predecessor_timestamp timestamp in
  Raw_context.Valid.t ctxt /\
  Receipt_repr.balance_updates.Valid.t updates /\
  List.Forall Migration_repr.origination_result.Valid.t migrations.
Proof.
  unfold Main.prepare_context.
  now apply Alpha_context.prepare_is_valid.
Qed.

(** The function [init_allowed_consensus_operations] is valid. *)
Lemma init_allowed_consensus_operations_is_valid
  ctxt endorsement_level preendorsement_level
  (H_ctxt : Raw_context.Valid.t ctxt)
  (H_endorsement_level : Level_repr.Valid.t endorsement_level)
  (H_preendorsement_level : Level_repr.Valid.t preendorsement_level) :
  letP? ctxt :=
    Main.init_allowed_consensus_operations
      ctxt endorsement_level preendorsement_level in
  Raw_context.Valid.t ctxt.
Proof.
  unfold Main.init_allowed_consensus_operations.
  eapply Error.split_letP. {
    apply Stake_storage.prepare_stake_distribution_is_valid; trivial.
  }
  intros.
  eapply Error.split_letP with
    (P := fun '(ctxt, _, _) => Raw_context.Valid.t ctxt). {
    step.
    { eapply Error.split_letP. {
        now apply Baking.endorsing_rights_by_first_slot_is_valid.
      }
      now intros; Tactics.destruct_pairs.
    }
    { eapply Error.split_letP. {
        now apply Baking.endorsing_rights_by_first_slot_is_valid.
      }
      intros; Tactics.destruct_pairs.
      eapply Error.split_letP. {
        now apply Baking.endorsing_rights_by_first_slot_is_valid.
      }
      intros; Tactics.destruct_pairs.
      easy.
    }
  }
  intros; Tactics.destruct_pairs.
  simpl.
  now apply Raw_context.Consensus.initialize_consensus_operation_is_valid.
Qed.

(** The function [begin_application] is valid. *)
Lemma begin_application_is_valid
  chain_id predecessor_context predecessor_timestamp predecessor_fitness
  block_header
  (H_predecessor_context : Context.Valid.t predecessor_context)
  (H_predecessor_timestamp : Time.Valid.t predecessor_timestamp)
  (H_block_header : Block_header_repr.Valid.t block_header) :
  letP? state :=
    Main.begin_application
      chain_id
      (Context.to_t predecessor_context)
      predecessor_timestamp
      predecessor_fitness
      block_header in
  validation_state.Valid.t state.
Proof.
  unfold Main.begin_application.
  eapply Error.split_letP. {
    apply prepare_context_is_valid; sauto lq: on rew: off.
  }
  intros; Tactics.destruct_pairs.
  eapply Error.split_letP. {
    apply Raw_level_repr.of_int32_is_valid.
    lia.
  }
  intros.
  eapply Error.split_letP. {
    now apply Level_storage.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply init_allowed_consensus_operations_is_valid; try assumption.
    now apply Level_storage.current_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Fitness_repr.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Validate.begin_application_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Apply.begin_application_is_valid.
  }
  easy.
Qed.

(** The function [begin_partial_application] is valid. *)
Lemma begin_partial_application_is_valid
  chain_id ancestor_context predecessor_timestamp predecessor_fitness
  block_header
  (H_ancestor_context : Context.Valid.t ancestor_context)
  (H_predecessor_timestamp : Time.Valid.t predecessor_timestamp)
  (H_block_header : Block_header_repr.Valid.t block_header) :
  letP? state :=
    Main.begin_partial_application
      chain_id
      (Context.to_t ancestor_context)
      predecessor_timestamp
      predecessor_fitness
      block_header in
  validation_state.Valid.t state.
Proof.
  unfold Main.begin_partial_application.
  eapply Error.split_letP. {
    apply prepare_context_is_valid; sauto l: on.
  }
  intros; Tactics.destruct_pairs.
  eapply Error.split_letP. {
    apply Raw_level_repr.of_int32_is_valid.
    lia.
  }
  intros.
  eapply Error.split_letP. {
    now apply Level_storage.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply init_allowed_consensus_operations_is_valid; trivial.
    now apply Level_storage.current_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Fitness_repr.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Validate.begin_partial_application_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Apply.begin_partial_application_is_valid.
  }
  easy.
Qed.

(** The function [begin_full_construction] is valid. *)
Lemma begin_full_construction_is_valid
  (chain_id : Chain_id.t) (predecessor_context : Context.t)
  (predecessor_timestamp : Time.t) (predecessor_level : int32)
  (predecessor_fitness : Fitness.t) (predecessor : Block_hash.t)
  (timestamp : Time.t)
  (block_header_contents : Alpha_context.Block_header.contents)
  (H_predecessor_context : Context.Valid.t predecessor_context)
  (H_predecessor_timestamp : Time.Valid.t predecessor_timestamp)
  (H_predecessor_level : Int32.Valid.t predecessor_level)
  (H_timestamp : Time.Valid.t timestamp)
  (H_block_header_contents :
    Block_header_repr.contents.Valid.t block_header_contents) :
  letP? state :=
    Main.begin_full_construction
      chain_id
      (Context.to_t predecessor_context)
      predecessor_timestamp
      predecessor_level
      predecessor_fitness
      predecessor
      timestamp
      block_header_contents in
  validation_state.Valid.t state.
Proof.
  unfold Main.begin_full_construction.
  eapply Error.split_letP. {
    apply prepare_context_is_valid; trivial.
    lia.
  }
  intros; Tactics.destruct_pairs.
  eapply Error.split_letP. {
    apply Raw_level_repr.of_int32_is_valid.
    lia.
  }
  intros.
  eapply Error.split_letP. {
    now apply Level_storage.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply init_allowed_consensus_operations_is_valid; trivial.
    now apply Level_storage.current_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Fitness_repr.round_from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Round_repr.round_of_timestamp_is_valid; trivial.
    now apply Alpha_context.Constants.round_durations_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Validate.begin_full_construction_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Apply.begin_full_construction_is_valid.
  }
  easy.
Qed.

(** The function [begin_partial_construction] is valid. *)
Lemma begin_partial_construction_is_valid
  (chain_id : Chain_id.t) (predecessor_context : Context.t)
  (predecessor_timestamp : Time.t) (predecessor_level : int32)
  (predecessor_fitness : Alpha_context.Fitness.raw) (predecessor : Block_hash.t)
  (timestamp : Time.t)
  (H_predecessor_context : Context.Valid.t predecessor_context)
  (H_predecessor_timestamp : Time.Valid.t predecessor_timestamp)
  (H_predecessor_level : Int32.Valid.t predecessor_level)
  (H_timestamp : Time.Valid.t timestamp) :
  letP? state :=
    Main.begin_partial_construction
      chain_id
      (Context.to_t predecessor_context)
      predecessor_timestamp
      predecessor_level
      predecessor_fitness
      predecessor
      timestamp in
  validation_state.Valid.t state.
Proof.
  unfold Main.begin_partial_construction.
  eapply Error.split_letP. {
    apply Alpha_context.prepare_is_valid; trivial.
    lia.
  }
  intros; Tactics.destruct_pairs.
  eapply Error.split_letP. {
    apply Raw_level_repr.of_int32_is_valid.
    lia.
  }
  intros.
  eapply Error.split_letP. {
    now apply Level_storage.from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply init_allowed_consensus_operations_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Fitness_repr.round_from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    apply Fitness_repr.predecessor_round_from_raw_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Validate.begin_partial_construction_is_valid.
  }
  intros.
  eapply Error.split_letP. {
    now apply Apply.begin_partial_construction_is_valid.
  }
  easy.
Qed.

(** The function [begin_construction] is valid. *)
Lemma begin_construction_is_valid
  (chain_id : Chain_id.t) (predecessor_context : Context.t)
  (predecessor_timestamp : Time.t) (predecessor_level : int32)
  (predecessor_fitness : Fitness.t) (predecessor : Block_hash.t)
  (timestamp : Time.t) (protocol_data : option Main.block_header_data)
  (function_parameter : unit) :
  Context.Valid.t predecessor_context ->
  Time.Valid.t predecessor_timestamp ->
  Int32.Valid.t predecessor_level ->
  Time.Valid.t timestamp ->
  Option.Forall Block_header_repr.Protocol_data.Valid.t protocol_data ->
  letP? state :=
    Main.begin_construction
      chain_id
      (Context.to_t predecessor_context)
      predecessor_timestamp
      predecessor_level
      predecessor_fitness
      predecessor
      timestamp
      protocol_data
      function_parameter in
  validation_state.Valid.t state.
Proof.
  intros.
  unfold Main.begin_construction.
  destruct protocol_data.
  { now apply begin_full_construction_is_valid. }
  { now apply begin_partial_construction_is_valid. }
Qed.

(** The function [validate_operation] is valid. *)
Lemma validate_operation_is_valid validity_state packed_operation :
  Validate.validation_state.Valid.t validity_state ->
  letP? state :=
    Main.validate_operation
      validity_state
      packed_operation in
  Validate.state.Valid.t state.
Proof.
  intros.
  unfold Main.validate_operation.
  destruct packed_operation, protocol_data.
  now apply Validate.validate_operation_is_valid.
Qed.

(** The function [apply_operation] is valid. *)
Lemma apply_operation_is_valid state_value packed_operation :
  validation_state.Valid.t state_value ->
  letP? '(state, _) :=
    Main.apply_operation
      state_value
      packed_operation in
  validation_state.Valid.t state.
Proof.
  intros.
  unfold Main.apply_operation.
  eapply Error.split_letP. {
    apply validate_operation_is_valid.
    dtauto.
  }
  intros.
  eapply Error.split_letP. {
    apply Apply.apply_operation_is_valid.
  }
  intros; Tactics.destruct_pairs; simpl.
  dtauto.
Qed.

(** The function [finalize_block] is valid. *)
Lemma finalize_block_is_valid
  (state_value : Main.validation_state)
  (shell_header : option Alpha_context.Block_header.shell_header) :
  validation_state.Valid.t state_value ->
  letP? _ :=
    Main.finalize_block
      state_value
      shell_header in
  True.
Proof.
  intros.
  unfold Main.finalize_block.
  eapply Error.split_letP. {
    apply Validate.finalize_block_is_valid.
    dtauto.
  }
  intros.
  apply Apply.finalize_block_is_valid.
  dtauto.
Qed.

(** The function [init_value] is valid. *)
Lemma init_value_is_valid chain_id ctxt block_header :
  Context.Valid.t ctxt ->
  Block_header.shell_header.Valid.t block_header ->
  letP? _ :=
    Main.init_value
      chain_id
      (Context.to_t ctxt)
      block_header in
  True.
Proof.
  intros.
  unfold Main.init_value.
  eapply Error.split_letP. {
    apply Raw_level_repr.of_int32_is_valid.
    dtauto.
  }
  intros.
  eapply Error.split_letP. {
    apply Alpha_context.prepare_first_block_is_valid; try dtauto.
    intros.
    eapply Error.split_letP with
      (P := fun '(ex_script, ctxt) =>
        exists dep_ex_script,
        Script_ir_translator.to_ex_script dep_ex_script = ex_script /\
        Script_ir_translator.dep_ex_script.Valid.t dep_ex_script /\
        Raw_context.Valid.t ctxt
      ). {
      rewrite <- Script_ir_translator.dep_parse_script_eq.
      eapply Error.split_letP. {
        now apply Script_ir_translator.dep_parse_script_is_valid.
      }
      intros; Tactics.destruct_pairs; simpl.
      now eexists.
    }
    intros [] [dep_ex_script [H_dep_ex_script_eq [H_dep_ex_script ?]]].
    rewrite <- H_dep_ex_script_eq.
    destruct_all Script_ir_translator.dep_ex_script; simpl in *.
    eapply Error.split_letP. {
      apply Script_ir_translator.extract_lazy_storage_diff_is_valid; dtauto.
    }
    intros [[]] [? [dep_storage_value [H_dep_storage_value_eq]]].
    rewrite H_dep_storage_value_eq.
    eapply Error.split_letP. {
      rewrite <- Script_ir_translator.dep_unparse_data_eq.
      apply Script_ir_translator.dep_unparse_data_is_valid; dtauto.
    }
    intros; Tactics.destruct_pairs; simpl.
    trivial.
  }
  intros.
  eapply Error.split_letP. {
    apply Alpha_context.finalize_is_valid.
    now apply Raw_context.Cache.sync_is_valid.
  }
  easy.
Qed.

(** The function [value_of_key] is valid. *)
Lemma value_of_key_is_valid {A B C : Set}
  (a : A) (ctxt : Context.t)
  (predecessor_timestamp : Time.t)
  (pred_level : int32) (b : B) (c : C)
  (timestamp : Time.t) :
  Context.Valid.t ctxt ->
  Time.Valid.t predecessor_timestamp ->
  Time.Valid.t timestamp ->
  letP? _ :=
    Main.value_of_key
      a
      (Context.to_t ctxt)
      predecessor_timestamp
      pred_level
      b
      c
      timestamp in
  True.
Proof.
  intros.
  unfold Main.value_of_key.
  eapply Error.split_letP. {
    apply Alpha_context.prepare_is_valid; trivial.
    lia.
  }
  intros; Tactics.destruct_pairs.
  easy.
Qed.

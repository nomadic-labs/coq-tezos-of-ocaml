Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_commitment_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_repr.

Module Hash.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_commitment_repr.Hash.encoding.
  Proof.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Sc_rollup_commitment_repr.Hash.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.
End Hash.

Module V1.
  Module Valid.
    Import Sc_rollup_commitment_repr.V1.t.

    Record t (x : Sc_rollup_commitment_repr.V1.t) : Prop := {
      inbox_level : Raw_level_repr.Valid.t x.(inbox_level);
    }.
  End Valid.

  (** The encoding [V1.encoding] is valid. *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t Valid.t Sc_rollup_commitment_repr.V1.encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  (** The [V1.genesis_commitment] is valid *)
  Lemma genesis_commitment_is_valid origination_level genesis_state_hash :
    letP? res := Sc_rollup_commitment_repr.V1.genesis_commitment
      origination_level genesis_state_hash in True.
  Proof.
    unfold Sc_rollup_commitment_repr.V1.genesis_commitment.
    sfirstorder.
  Qed.  
  
  Module genesis_info.
    Module Valid.
      Import Proto_alpha.Sc_rollup_commitment_repr.V1.genesis_info.

      (** Validity predicate for [genesis_info]. *)
      Record t (x : Sc_rollup_commitment_repr.V1.genesis_info) : Prop := {
        level : Raw_level_repr.Valid.t x.(level);
      }.
    End Valid.
  End genesis_info.

  (** The encoding [V1.genesis_info_encoding] is valid. *)
  Lemma genesis_info_encoding_is_valid :
    Data_encoding.Valid.t genesis_info.Valid.t
      Sc_rollup_commitment_repr.V1.genesis_info_encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve genesis_info_encoding_is_valid : Data_encoding_db.
End V1.

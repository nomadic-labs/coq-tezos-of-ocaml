Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.

Require TezosOfOCaml.Proto_alpha.Tx_rollup_ticket.

Require TezosOfOCaml.Proto_alpha.Proofs.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Error.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_balance_key.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.

(** The function [parse_ticket] is valid. *)
Lemma parse_ticket_is_valid
  consume_deserialization_gas
  ticketer
  contents
  ty_value
  ctxt :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, token) :=
    Tx_rollup_ticket.parse_ticket
      consume_deserialization_gas
      ticketer
      contents
      ty_value
      ctxt
  in Raw_context.Valid.t ctxt /\
     Ticket_token.ex_token.Valid.t token.
Proof.
  intro Hv.
  unfold Tx_rollup_ticket.parse_ticket.
  Error.split_error_with Alpha_context.Script.force_decode_in_context_is_valid.
  Tactics.destruct_pairs.
  Error.split_error_with Alpha_context.Script.force_decode_in_context_is_valid.
  Tactics.destruct_pairs.
  rewrite <- Script_ir_translator.dep_parse_comparable_ty_eq.
  pose (P :=
    fun '(Script_ir_translator.Ex_comparable_ty
      cty, ctxt') =>
    Raw_context.Valid.t ctxt' /\
    exists a (ty' : Script_typed_ir.With_family.ty a), cty = Script_typed_ir.With_family.to_ty ty' /\
      Script_typed_ir.With_family.Valid.ty ty' /\
      Script_typed_ir.With_family.is_Comparable ty').
  eapply (Error.split_letP _ _ P).
  { pose proof (Script_ir_translator.dep_parse_comparable_ty_is_valid
    ltac:(assumption) (root_value e) ltac:(assumption)) as Hdp.
    destruct Script_ir_translator.dep_parse_comparable_ty; [|exact Hdp].
    destruct p; destruct d; simpl in *.
    split; [tauto|].
    exists t1, ty; tauto.
  }
  unfold P; clear P.
  intros [] He1.
  Tactics.destruct_pairs. destruct e1.
  destruct He1 as [Hc [a [ty Hty]]].
  rewrite (@cast_exists_eval_eq _ _ (fun _ : Set =>
    [Alpha_context.context ** Script_typed_ir.comparable_ty])
    (Script_family.Ty.to_Set a) _ eq_refl); simpl.
  eapply Error.split_letP.
  { apply Script_ir_translator.parse_comparable_data_is_valid.
    assumption.
  }
  intros [val ctxt'] [Hc' [val' Hval']]; simpl.
  split; [assumption|].
  rewrite (proj1 Hty).
  rewrite <- (proj1 Hval').
  constructor; tauto.
Qed.

(** The function [parse_ticket_and_operation] is valid. *)
Lemma parse_ticket_and_operation_is_valid
  consume_deserialization_gas
  ticketer
  contents
  ty_value
  source destination
  entrypoint amount
  ctxt :
  Raw_context.Valid.t ctxt ->
  Ticket_amount.Valid.t amount ->
  letP? '(ctxt, token, op) :=
    Tx_rollup_ticket.parse_ticket_and_operation
      consume_deserialization_gas
      ticketer
      contents
      ty_value
      source
      destination
      entrypoint
      amount
      ctxt
  in Raw_context.Valid.t ctxt /\
     Ticket_token.ex_token.Valid.t token /\
     exists op', Script_typed_ir.With_family.to_packed_internal_operation op' = op /\
     Script_typed_ir.packed_internal_operation.Valid.t op'.
Proof.
  intros Hctxt Hamount. unfold Ticket_amount.Valid.t in Hamount.
  unfold Tx_rollup_ticket.parse_ticket_and_operation.
  unfold Tx_rollup_ticket.parse_ticket.
  Error.split_error_with Alpha_context.Script.force_decode_in_context_is_valid.
  Tactics.destruct_pairs.
  Error.split_error_with Alpha_context.Script.force_decode_in_context_is_valid.
  Tactics.destruct_pairs.
  rewrite <- Script_ir_translator.dep_parse_comparable_ty_eq.
  pose (P :=
    fun '(Script_ir_translator.Ex_comparable_ty
      cty, ctxt') =>
    Raw_context.Valid.t ctxt' /\
    exists a (ty' : Script_typed_ir.With_family.ty a), cty = Script_typed_ir.With_family.to_ty ty' /\
      Script_typed_ir.With_family.Valid.ty ty' /\
      Script_typed_ir.With_family.is_Comparable ty').
  eapply (Error.split_letP _ _ P).
  { pose proof (Script_ir_translator.dep_parse_comparable_ty_is_valid
     ltac:(assumption) (root_value e) ltac:(assumption)) as Hdp.
    destruct Script_ir_translator.dep_parse_comparable_ty; [|exact Hdp].
    destruct p; destruct d; simpl in *.
    split; [tauto|].
    exists t1, ty; tauto.
  }
  unfold P; clear P.
  intros [] He1.
  Tactics.destruct_pairs. destruct e1.
  destruct He1 as [Hc [a [ty Hty]]].
  rewrite (@cast_exists_eval_eq _ _ (fun _ : Set =>
    [Alpha_context.context ** Script_typed_ir.comparable_ty])
    (Script_family.Ty.to_Set a) _ eq_refl); simpl.
  eapply Error.split_letP.
  { apply Script_ir_translator.parse_comparable_data_is_valid.
    assumption.
  }
  intros [val ctxt'] [Hc' [val' Hval']]; simpl.
Admitted.
  (* @TODO fix proof
  unfold value_e.
  destruct (_ <? _) eqn:?; [lia|].
  simpl.
  rewrite (proj1 Hty).
  rewrite <- Script_typed_ir.dep_ticket_t_eq.
  pose proof (Script_typed_ir.dep_ticket_t_is_valid dummy_location ty
    ltac:(tauto) ltac:(tauto)
  ) as Hdt.
  destruct Script_typed_ir.dep_ticket_t; [|assumption].
  simpl in *.
  rewrite <- (proj1 Hval').
  pose (ticket1 :=
         {|
           Script_typed_ir.ticket.ticketer := ticketer;
           Script_typed_ir.ticket.contents := Script_typed_ir.With_family.to_value val';
           Script_typed_ir.ticket.amount :=
             Script_int.Num_tag amount
         |}).
  fold ticket1.
  pose (ticket2 :=
         {|
           Script_typed_ir.ticket.ticketer := ticketer;
           Script_typed_ir.ticket.contents := val';
           Script_typed_ir.ticket.amount :=
             Script_int.Num_tag amount
         |}).
  assert (ticket1 =
    (@Script_typed_ir.With_family.to_value
    (Script_family.Ty.Ticket a)) ticket2)
    as ticket_eq by reflexivity.
  rewrite ticket_eq.
  pose proof
    (Script_ir_translator.unparse_data_is_valid
    _ ctxt' Script_ir_unparser.Optimized
    t2 ticket2
    ltac:(tauto) ltac:(tauto) ltac:(simpl; tauto)).
  destruct Script_ir_translator.unparse_data; [|assumption].
  Tactics.destruct_pairs; simpl in *.
  eapply Error.split_letP.
  apply Alpha_context.fresh_internal_nonce_is_valid.
  apply H1.
  (* { apply Alpha_context.Gas.consume_is_valid; [assumption|]. *)
  (*   apply Script_repr.strip_locations_cost_is_valid. *)
  (* } *)
  intros ctxt1 Hctxt1.
  (* Error.split_error_with Alpha_context.fresh_internal_nonce_is_valid. *)
  Tactics.destruct_pairs.
  simpl.
  repeat split; try tauto.
  epose (op :=
    Script_typed_ir.With_family.Transaction_to_smart_contract
	    destination
	    Alpha_context.Tez.zero
	    entrypoint
	    (location n)
	    t2
	    ticket2
	    (strip_locations n)).
  exists (Script_typed_ir.With_family.Internal_operation source op i).
  split; [reflexivity|].
  constructor.
  simpl.
  repeat split; try lia.
  { exists (Apply.Dep_typed_arg (location n) t2 ticket2).
    reflexivity.
  }
  { assumption. }
Qed. *)

(** The function [make_withdraw_order] is valid. *)
Lemma make_withdraw_order_is_valid
  ctxt tx_rollup ex_ticket claimer amount :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t ex_ticket ->
  Tx_rollup_l2_qty.Valid.t amount ->
  letP? '(ctxt, o) := Tx_rollup_ticket.make_withdraw_order
    ctxt tx_rollup ex_ticket claimer amount in
  Raw_context.Valid.t ctxt /\ Tx_rollup_withdraw_repr.order.Valid.t o.
Proof.
  intros Hctxt Hex_ticket Hamount.
  unfold Tx_rollup_ticket.make_withdraw_order.
  eapply Error.split_letP. {
    apply Ticket_balance_key.of_ex_token_is_valid.
    apply Hctxt.
    apply Hex_ticket.
  }
  intros. Tactics.destruct_pairs.
  simpl. split ; [ apply H | constructor; assumption].
Qed.

(** The function [transfer_ticket_with_hashes] is valid. *)
Lemma transfer_ticket_with_hashes_is_valid
  ctxt src_hash dst_hash qty :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) := Tx_rollup_ticket.transfer_ticket_with_hashes
    ctxt src_hash dst_hash qty in
  Raw_context.Valid.t ctxt.
Proof.
  intro Hctxt.
  unfold Tx_rollup_ticket.transfer_ticket_with_hashes.
  Error.split_error_with Ticket_storage.adjust_balance_is_valid.
  Tactics.destruct_pairs.
  Error.split_error_with Ticket_storage.adjust_balance_is_valid.
  Tactics.destruct_pairs.
  Error.split_error_with Ticket_storage.adjust_storage_space_is_valid.
  now Tactics.destruct_pairs.
Qed.

(** The function [transfer_ticket] is valid. *)
Lemma transfer_ticket_is_valid
  ctxt src dst ex_token qty :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t ex_token ->
  letP? '(ctxt, _) := Tx_rollup_ticket.transfer_ticket
    ctxt src dst ex_token qty in
  Raw_context.Valid.t ctxt.
Proof.
  unfold Tx_rollup_ticket.transfer_ticket.
  intros Hctxt Hex_token.
  eapply Error.split_letP. {
    apply Ticket_balance_key.of_ex_token_is_valid.
    apply Hctxt.
    apply Hex_token.
  }
  intros x H.
  Tactics.destruct_pairs.
  eapply Error.split_letP. {
    apply Ticket_balance_key.of_ex_token_is_valid.
    apply H.
    apply Hex_token.
  }
  intros.
  Tactics.destruct_pairs.
  apply Tx_rollup_ticket.transfer_ticket_with_hashes_is_valid.
  apply H0.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Script_interpreter.

Require TezosOfOCaml.Environment.V8.Proofs.List.
Require TezosOfOCaml.Environment.V8.Proofs.Z.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_interpreter_defs.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_map.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_set.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_ir_translator.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

(** @TODO PLACE WELL *)
Module execution_result.
  Module Valid.
    Import Proto_alpha.Script_interpreter.execution_result.

    (** Validity predicate for the [Script_interpreter.execution_result] type *)
    (*
    Record t (x : Script_interpreter.execution_result ) : Prop :=
      { code_size : Pervasives.Int.Valid.non_negative x.(code_size);
        operations : exists dep_operations,
          List.map With_family.to_packed_internal_operation dep_operations =
            x.(Script_interpreter.execution_result.operations) /\
              List.Forall Script_typed_ir.packed_internal_operation.Valid.t
                dep_operations
        (* dep_operations : list With_family.packed_internal_operation ;
        to_dep_packed_operations : (List.map With_family.to_packed_internal_operation
          dep_operations) = x.(Script_interpreter.execution_result.operations) ;
        val_dep_operations :  List.Forall Script_typed_ir.packed_internal_operation.Valid.t
        dep_operations *)
      }. *)
  End Valid. 

    (** 2022/11/15: this commentary should be fixed soon or altogether removed *)
    (* Lemma lem1 operation :
    exists dep_operation, operation =
    With_family.to_internal_operation_contents dep_operation.
    Proof.
    Admitted.

    Lemma lem2 operation: exists dep_operation,
     With_family.to_packed_internal_operation dep_operation = operation.
    Proof.
      (* exists operation. *)
      destruct operation eqn:eq_op ; eexists.
      destruct i.
    Admitted.

    Lemma lem3 operations :  (exists dep_operations,
    List.map With_family.to_packed_internal_operation dep_operations =
      operations
    ).
    Proof.
      induction operations as [ |op operations0 IH] ; [
        exists [] ; reflexivity |].
      eexists.
      (* split.  *)
      Admitted. *)
End execution_result.

(** The function [execute_with_typed_parameter] is valid. *)
Lemma execute_with_typed_parameter_is_valid {a : Ty.t}
  ctxt cached_script mode steps_constants script entrypoint
  parameter_ty location
  (parameter : Ty.to_Set a) internal :
  Script_typed_ir.Valid.value a parameter ->
  Script_typed_ir.ty.Valid.proj_rel a parameter_ty ->
  Raw_context.Valid.t ctxt ->
  letP? '(_, ctxt) :=
    Script_interpreter.execute_with_typed_parameter
      None ctxt cached_script mode steps_constants script entrypoint
      parameter_ty location
      parameter internal in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [execute] is valid. *)
Lemma execute_is_valid ctxt cached_script mode steps_constants script entrypoint
  parameter internal :
  Raw_context.Valid.t ctxt ->
  letP? '(_,ctxt) := Script_interpreter.execute
    None ctxt cached_script mode steps_constants script entrypoint
    parameter internal in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

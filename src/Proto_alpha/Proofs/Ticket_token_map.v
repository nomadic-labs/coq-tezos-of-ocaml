Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_token_map.

Require TezosOfOCaml.Proto_alpha.Proofs.Carbonated_map.
Require TezosOfOCaml.Proto_alpha.Proofs.Carbonated_map_costs.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_balance_key.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.

Import Error.Tactics_letP.

Module Valid.
  (** Validity predicate for the type [t] given a certain domain [P] on [a]. *)
  Definition t {a : Set} (P : a -> Prop) (x : Ticket_token_map.t a) :
    Prop :=
    Carbonated_map.Make.Valid.t
      (fun _ => True)
      (fun '(token, v) => Ticket_token.ex_token.Valid.t token /\ P v)
      x.
End Valid.

(** The function [empty] is valid. *)
Lemma empty_is_valid {a : Set} (P : a -> Prop) :
  Valid.t P Ticket_token_map.empty.
Proof.
  apply Carbonated_map.Make.empty_is_valid.
Qed.

(** The function [key_of_ticket_token] is valid. *)
Lemma key_of_ticket_token_is_valid ctxt token :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t token ->
  letP? '(_, ctxt) := Ticket_token_map.key_of_ticket_token ctxt token in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_token.
  unfold Ticket_token_map.key_of_ticket_token.
  destruct H_token.
  apply Ticket_balance_key.of_ex_token_is_valid.
  apply H_ctxt.
  hauto l: on.
Qed.

(** The function [update] is valid. *)
Lemma update_is_valid {a : Set} P ctxt key_value f_value m_value :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t key_value ->
  (forall ctxt (value : option a),
    Raw_context.Valid.t ctxt ->
    Option.Forall P value ->
    letP? '(value, ctxt) := f_value ctxt value in
    Option.Forall P value /\
    Raw_context.Valid.t ctxt
  ) ->
  Valid.t P m_value ->
  letP? '(map, ctxt) :=
    Ticket_token_map.update ctxt key_value f_value m_value in
  Valid.t P map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_key_value H_f_value H_m_value.
  unfold Ticket_token_map.update.
  eapply Error.split_letP. {
    now apply key_of_ticket_token_is_valid.
  }
  clear ctxt H_ctxt; intros [key_hash ctxt] H_ctxt.
  apply Carbonated_map.Make.update_is_valid; trivial.
  intros.
  destruct value as [[]|]; simpl in *.
  all: eapply Error.split_letP; [now apply H_f_value |].
  all: clear ctxt H_ctxt; intros [val_opt ctxt] [H_val_opt H_ctxt].
  all: simpl.
  all: sauto.
Qed.

(** The function [fold] is valid. *)
Lemma fold_is_valid {a state : Set} (P : a -> Prop) (P_state : state -> Prop)
  ctxt f acc map :
  Raw_context.Valid.t ctxt ->
  (forall ctxt (acc : state) token (value : a),
    Raw_context.Valid.t ctxt ->
    P_state acc ->
    Ticket_token.ex_token.Valid.t token ->
    P value ->
    letP? '(acc, ctxt) := f ctxt acc token value in
    P_state acc /\
    Raw_context.Valid.t ctxt
  ) ->
  P_state acc ->
  Valid.t P map ->
  letP? '(acc, ctxt) := Ticket_token_map.fold ctxt f acc map in
  P_state acc /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_f H_acc H_map.
  unfold Ticket_token_map.fold.
  eapply Carbonated_map.Make.fold_is_valid; try easy; try apply H_map.
  hauto lq: on.
Qed.

(** The function [find] is valid. *)
Lemma find_is_valid {a : Set} (P : a -> Prop) ctxt token map :
  Raw_context.Valid.t ctxt ->
  Ticket_token.ex_token.Valid.t token ->
  Valid.t P map ->
  letP? '(value, ctxt) := Ticket_token_map.find ctxt token map in
  Option.Forall P value /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_token H_map.
  unfold Ticket_token_map.find.
  eapply Error.split_letP. {
    now apply key_of_ticket_token_is_valid.
  }
  clear ctxt H_ctxt; intros [key_hash ctxt] H_ctxt.
  eapply Error.split_letP. {
    apply Carbonated_map.Make.find_is_valid
      with (P_ctxt := Raw_context.Valid.t) (P_key := fun _ => True); try easy.
    apply H_map.
  }
  intros [o c] [Ho Hc].
  repeat split ; try assumption.
  unfold Option.Forall, Option.map in *.
  destruct o as [ p |]; [| constructor].
  unfold Option.map in *. destruct p. destruct Ho. assumption.
Qed.

(** The function [of_list] is valid. *)
Lemma of_list_is_valid {a : Set} (P : a -> Prop) ctxt merge token_values :
  Raw_context.Valid.t ctxt ->
  (forall ctxt value1 value2,
    Raw_context.Valid.t ctxt ->
    P value1 ->
    P value2 ->
    letP? '(value, ctxt) := merge ctxt value1 value2 in
    P value /\
    Raw_context.Valid.t ctxt
  ) ->
  List.Forall
    (fun '(token, value) =>
      Ticket_token.ex_token.Valid.t token /\
      P value
    )
    token_values ->
  letP? '(map, ctxt) := Ticket_token_map.of_list ctxt merge token_values in
  Valid.t P map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_merge H_token_values.
  unfold Ticket_token_map.of_list.
Admitted.

(** The function [map] is valid. *)
Lemma map_is_valid {a b : Set} (P_a : a -> Prop) (P_b : b -> Prop)
  ctxt f map :
  Raw_context.Valid.t ctxt ->
  (forall ctxt token value,
    Raw_context.Valid.t ctxt ->
    Ticket_token.ex_token.Valid.t token ->
    P_a value ->
    letP? '(value, ctxt) := f ctxt token value in
    P_b value /\
    Raw_context.Valid.t ctxt
  ) ->
  Valid.t P_a map ->
  letP? '(map, ctxt) := Ticket_token_map.map ctxt f map in
  Valid.t P_b map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_f H_map.
  unfold Ticket_token_map.map.
  eapply Carbonated_map.Make.map_is_valid
    with (P_value := (fun '(token, value) =>
      Ticket_token.ex_token.Valid.t token /\ P_a value)
    ); try easy.
  intros.
  Tactics.destruct_pairs.
  eapply Error.split_letP; [now apply H_f|].
  hauto l: on.
Qed.

(** The function [to_list] is valid. *)
Lemma to_list_is_valid {a : Set} (P : a -> Prop) ctxt map :
  Raw_context.Valid.t ctxt ->
  Valid.t P map ->
  letP? '(token_values, ctxt) := Ticket_token_map.to_list ctxt map in
  List.Forall
    (fun '(token, value) =>
      Ticket_token.ex_token.Valid.t token /\
      P value
    )
    token_values /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_map.
  unfold Ticket_token_map.to_list.
  eapply Error.split_letP. {
    apply Carbonated_map.Make.to_list_is_valid;
      try apply H_ctxt;
      try apply H_map.
  }
  clear ctxt H_ctxt; intros [list_value ctxt] [H_list_value H_ctxt].
  eapply Error.split_letP. {
    apply Raw_context.consume_gas_is_valid; try assumption.
    apply Carbonated_map_costs.fold_cost_is_valid.
  }
  clear ctxt H_ctxt; intros ctxt H_ctxt.
  simpl.
  split; try easy.
  clear - H_list_value.
  induction list_value; constructor; sauto l: on.
Qed.

(** The function [merge] is valid. *)
Lemma merge_is_valid {a : Set} (P : a -> Prop) ctxt merge_overlap map1 map2 :
  Raw_context.Valid.t ctxt ->
  (forall ctxt value1 value2,
    Raw_context.Valid.t ctxt ->
    P value1 ->
    P value2 ->
    letP? '(value, ctxt) := merge_overlap ctxt value1 value2 in
    P value /\
    Raw_context.Valid.t ctxt
  ) ->
  Valid.t P map1 ->
  Valid.t P map2 ->
  letP? '(map, ctxt) := Ticket_token_map.merge ctxt merge_overlap map1 map2 in
  Valid.t P map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_merge_overlap H_map1 H_map2.
  unfold Ticket_token_map.merge.
  apply Carbonated_map.Make.merge_is_valid; try easy.
  intros.
  unfold Ticket_token_map.lift_merge_overlap.
  destruct value1, value2.
  eapply Error.split_letP; [now apply H_merge_overlap|].
  hauto l: on.
Qed.

(** The function [to_ticket_receipt] is valid. *)
Lemma to_ticket_receipt_is_valid
  ctxt owner ticket_token_map :
  Raw_context.Valid.t ctxt ->
  letP? '(_, ctxt) :=
    Ticket_token_map.to_ticket_receipt
      ctxt owner ticket_token_map in
  Raw_context.Valid.t ctxt.
Proof.
  intro H.
  unfold Ticket_token_map.to_ticket_receipt.
  simpl.
  (** @TODO define function : Carbonated_map.Make.fold_es_is_valid. *)
Admitted.

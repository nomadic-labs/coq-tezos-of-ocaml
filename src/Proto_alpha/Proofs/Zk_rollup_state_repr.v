Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V8.Proofs.Operation.

Require TezosOfOCaml.Proto_alpha.Zk_rollup_state_repr.

(** [Zk_rollup_state_repr.encoding] is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

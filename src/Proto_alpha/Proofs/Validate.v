Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Validate.

Require TezosOfOCaml.Proto_alpha.Proofs.Block_header_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_delegate_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_manager_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Constants_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Dal_apply.
Require TezosOfOCaml.Proto_alpha.Proofs.Dal_endorsement_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_slashed_deposits_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_consensus_key.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_sampler.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Error.
Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Level_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Operation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Round_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_gas.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_errors_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_generated.
Require TezosOfOCaml.Proto_alpha.Proofs.Vote_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Voting_period_storage.

Module consensus_state.
  Import Proto_alpha.Validate.consensus_state.

  (** Validity for the type [consensus_state]. *)
  Module Valid.
    Record t (x : Validate.consensus_state) : Prop := {
      locked_round_evidence :
        Option.Forall
          (fun '(expected, power) =>
            Round_repr.Valid.t expected /\ Pervasives.Int.Valid.t power)
          x.(locked_round_evidence);
    }.
  End Valid.
End consensus_state.

Module mode.
  (** Validity for the type [mode]. *)
  Module Valid.
    Definition t (x : Validate.mode) : Prop :=
      match x with
      | Validate.Construction {|
          Validate.mode.Construction.predecessor_round :=
            predecessor_round;
          Validate.mode.Construction.round := round;
        |} =>
        Round_repr.Valid.t predecessor_round /\
        Round_repr.Valid.t round
      | _ => True
      end.
  End Valid.
End mode.

Module expected_features.
  (** Validity for the type [expected_features] *)
  Module Valid.
    Import Proto_alpha.Validate.expected_features.
    Record t (x : Validate.expected_features) : Prop := {
      level : Raw_level_repr.Valid.t x.(level);
      round : Option.Forall Round_repr.Valid.t x.(round);
    }.
  End Valid.
End expected_features.

Module expected_preendorsement.
  (** Validity for the type
      ['expected_preendorsement.Expected_preendorsement] *)
  Module Valid.
    Import Validate.
    Import ConstructorRecords_expected_preendorsement
      .expected_preendorsement.Expected_preendorsement.
    Record expected_preendorsement
      (x : 'expected_preendorsement.Expected_preendorsement) : Prop := {
        expected_features :
          expected_features.Valid.t x.(expected_features);
        block_round:
          Option.Forall Round_repr.Valid.t x.(block_round);
      }.
    Definition t (x : Validate.expected_preendorsement) : Prop :=
      match x with
      | Expected_preendorsement e => expected_preendorsement e
      | No_locked_round_for_block_validation_preendorsement
      | Fresh_proposal_for_block_construction_preendorsement
      | No_expected_branch_for_mempool_preendorsement _
      | No_predecessor_info_cannot_validate_preendorsement => True
      end.
  End Valid.
End  expected_preendorsement.

Module expected_endorsement.
  (** Validity for the type
      ['expected_endorsement.Expected_endorsement] *)
  Module Valid.
    Import Validate.
    Import ConstructorRecords_expected_endorsement
      .expected_endorsement.Expected_endorsement.
    Record expected_endorsement
      (x : 'expected_endorsement.Expected_endorsement) : Prop := {
        expected_features:
          expected_features.Valid.t x.(expected_features)
      }.
    Definition t (x : Validate.expected_endorsement) : Prop :=
      match x with
      | Expected_endorsement e =>
          expected_endorsement e
      | No_expected_branch_for_block_endorsement
      | No_expected_branch_for_mempool_endorsement _
      | No_predecessor_info_cannot_validate_endorsement => True
      end.
  End Valid.
End  expected_endorsement.

Module all_expected_consensus_features.
  (** Validity for the type
      [all_expected_consensus_features] *)
  Module Valid.
    Import Proto_alpha.Validate.all_expected_consensus_features.
    Record t (x : Validate.all_expected_consensus_features) : Prop := {
      expected_preendorsement :
        expected_preendorsement.Valid.t x.(expected_preendorsement);
      expected_endorsement :
        expected_endorsement.Valid.t x.(expected_endorsement);
      expected_grandparent_endorsement_for_mempool :
        Option.Forall expected_features.Valid.t
          x.(expected_grandparent_endorsement_for_mempool);
    }.
  End Valid.
End all_expected_consensus_features.

Module consensus_info.
  Module Valid.
    Import Proto_alpha.Validate.consensus_info.

    (** Validity predicate for [consensus_info] type
        depending on preendorsement domain [pe_domain]
        and endorsement domain [e_domain] *)
    Record on
      (pe_domain :
        (Alpha_context.Slot.Map.(Map.S.t)
          (Alpha_context.Consensus_key.pk * int) -> Prop))
      (e_domain :
        (Alpha_context.Slot.Map.(Map.S.t)
          (Alpha_context.Consensus_key.pk * int) -> Prop))
      (x : Validate.consensus_info)
      : Prop := {
      all_expected_features :
        all_expected_consensus_features.Valid.t
          x.(all_expected_features);
      preendorsement_slot_map :
        pe_domain x.(preendorsement_slot_map);
      endorsement_slot_map : e_domain
        x.(endorsement_slot_map);
    }.

    (** Validity for the type [consensus_info] *)
    Definition t (x : Validate.consensus_info) : Prop :=
      on (fun _ => True) (fun _ => True) x.
  End Valid.
End consensus_info.

Module manager_info.
  Module Valid.
    Import Proto_alpha.Validate.manager_info.

    Record t (x : Validate.manager_info) : Prop := {
      hard_gas_limit_per_operation :
        Gas_limit_repr.Arith.Integral.Valid.t x.(hard_gas_limit_per_operation);
    }.
  End Valid.
End manager_info.

Module info.
  Import Proto_alpha.Validate.info.
  Module Valid.
    (** Validity predicate for [info] type depending on preendorsement domain
        [pe_domain] and endorsement domain [e_domain] and context domain
        [ctxt_domain] *)
    Record on ctxt_domain pe_domain e_domain (x : Validate.info) : Prop := {
      ctxt : Raw_context.Valid.on ctxt_domain x.(ctxt);
      mode : mode.Valid.t x.(mode);
      consensus_info : consensus_info.Valid.on
        pe_domain e_domain x.(consensus_info);
      manager_info : manager_info.Valid.t x.(manager_info);
    }.

    (** Validity for the type [info]. *)
    Definition t (x : Validate.info) : Prop :=
      on (fun _ => True) (fun _ => True) (fun _ => True) x.
  End Valid.
End info.

Module manager_state.
  Import Proto_alpha.Validate.manager_state.
  (** Validity for the type [manager_state]. *)
  Module Valid.
    Record t (x : Validate.manager_state) : Prop := {
      remaining_block_gas :
        Saturation_repr.Strictly_valid.t x.(remaining_block_gas);
    }.
  End Valid.
End manager_state.

Module state.
  Import Proto_alpha.Validate.state.
  (** Validity for the type [state]. *)
  Module Valid.
    Record t (x : Validate.state) : Prop := {
      consensus_state : consensus_state.Valid.t x.(consensus_state);
      managner_state : manager_state.Valid.t x.(manager_state);
    }.
  End Valid.
End state.

Module validation_state.
  Import Proto_alpha.Validate.validation_state.
  (** Validity for the type [validation_state]. *)
  Module Valid.
    Record t (x : Validate.validation_state) : Prop := {
      info : info.Valid.t x.(info);
      state : state.Valid.t x.(state);
    }.
  End Valid.

  (** From a valid [validation_state] we can get a valid simulation
      context. *)
  Lemma exists_ctxt (x : Validate.validation_state) :
    Valid.t x ->
    exists sim_ctxt,
    x.(info).(Validate.info.ctxt) = Raw_context.to_t sim_ctxt /\
    Raw_context.Valid.simulation sim_ctxt.
  Proof.
    intros [[]].
    Raw_context.Valid.destruct_rewrite ctxt.
    now exists sim_ctxt.
  Qed.
End validation_state.

(** The function [are_endorsements_required] is valid. *)
Lemma are_endorsements_required_is_valid (vi : Validate.info) :
  info.Valid.t vi ->
  letP? _ := Validate.are_endorsements_required vi in
  True.
Proof.
  intros H.
  destruct H, ctxt as [sim_ctxt [H_sim_ctxt_eq ?]].
  unfold Validate.are_endorsements_required.
  unfold Alpha_context.First_level_of_protocol.get.
  simpl.
  rewrite H_sim_ctxt_eq.
  rewrite Storage_generated.Tenderbake.First_level_of_protocol.get.
  step; simpl; [trivial|].
  dtauto.
Qed.

(** The function [check_endorsement_power] is valid. *)
Lemma check_endorsement_power_is_valid (vi : Validate.info) vs :
  letP? _ := Validate.check_endorsement_power vi vs in
  True.
Proof.
  intros.
  unfold Validate.check_endorsement_power, Error_monad.error_unless.
  now step.
Qed.

(** The function [finalize_validate_block_header] is valid. *)
Lemma finalize_validate_block_header_is_valid
  vi vs checkable_payload_hash block_header_contents round fitness :
  letP? _ :=
    Validate.finalize_validate_block_header
      vi
      vs
      checkable_payload_hash
      block_header_contents
      round
      fitness in
  True.
Proof.
  unfold Validate.finalize_validate_block_header.
  apply Block_header_repr.finalize_validate_block_header_is_valid.
Qed.

(** The function [finalize_block] is valid. *)
Lemma finalize_block_is_valid (s : Validate.validation_state) :
  Validate.validation_state.Valid.t s ->
  letP? _ := Validate.finalize_block s in
  True.
Proof.
  intros H.
  destruct (validation_state.exists_ctxt _ H)
    as [sim_ctxt [H_sim_ctxt_eq H_sim_ctxt]].
  unfold Validate.finalize_block.
  step;
    try (
      eapply Error.split_letP; [
        apply are_endorsements_required_is_valid; apply H |
        intros [] _; simpl
      ]
    );
    try (
      eapply Error.split_letP; [
        apply check_endorsement_power_is_valid |
        intros _ _
      ]
    );
    try (
      eapply Error.split_letP; [
        apply finalize_validate_block_header_is_valid | easy
      ]
    );
    try easy.
  all:
    match goal with
    | |- context[let? x := ?e in return? x] =>
      replace (let? x := e in return? x) with e by now destruct e
    end.
  all:
    eapply Error.split_letP; [
      apply Fitness_repr.create_is_valid |
      intros
    ].
  all: try (
    eapply Error.split_letP; [
      apply finalize_validate_block_header_is_valid |
      easy
    ]
  ).
  all:
    destruct_all Validate.validation_state;
    destruct_all Validate.info;
    destruct_all Validate.state;
    destruct_all Validate.consensus_state;
    simpl in *.
  all: try rewrite H_sim_ctxt_eq.
  all: try apply H_sim_ctxt.
  all:
    try match goal with
    | H_mode : _ = _ |- _ =>
      rewrite H_mode in H;
      apply H
    end.
  all: destruct H, state, consensus_state; simpl in *.
  all: hauto lq: on.
Qed.

Module Consensus.
  (** The function [check_frozen_deposits_are_positive] is valid *)
  Lemma check_frozen_deposits_are_positive_is_valid ctxt delegate_pkh :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Consensus.check_frozen_deposits_are_positive
        ctxt delegate_pkh in
    True.
  Proof.
    Raw_context.Valid.start_error_proof.
     Error.split_error_with Delegate_storage.frozen_deposits_is_valid.
     unfold fail_unless.
     step; cbn; [easy|].
     reflexivity.
  Qed.

  (** The function [check_level_equal] is valid *)
  Lemma check_level_equal_is_valid
    kind_value expected_features consensus_content :
    letP? x :=
      Validate.Consensus.check_level_equal
        kind_value expected_features consensus_content in
    True.
  Proof.
    unfold Validate.Consensus.check_level_equal, error_unless.
    step; cbn; [easy|].
    step; cbn; reflexivity.
  Qed.

  (** The function [check_round_equal] is valid *)
  Lemma check_round_equal_is_valid
    vs kind_value expected_features consensus_content :
    letP? x :=
      Validate.Consensus.check_round_equal vs kind_value
        expected_features consensus_content in
    True.
  Proof.
    unfold Validate.Consensus.check_round_equal. step; [|step];
      unfold error_unless; [..|easy]; step; [easy|..|step];
      try reflexivity; step; reflexivity.
  Qed.

  (** The function [check_payload_hash_equal] is valid *)
  Lemma check_payload_hash_equal_is_valid
    kind_value expected_features consensus_content :
    letP? x :=
      Validate.Consensus.check_payload_hash_equal kind_value
        expected_features consensus_content in
    True.
  Proof.
    intros.
    unfold Validate.Consensus.check_payload_hash_equal.
    unfold error_unless. step; reflexivity.
  Qed.

  (** The function [check_branch_equal] is valid *)
  Lemma check_branch_equal_is_valid
    consensus_operation_kind expected_features operation :
    letP? x :=
      Validate.Consensus.check_branch_equal consensus_operation_kind
        expected_features operation in
    True.
  Proof.
    unfold Validate.Consensus.check_branch_equal.
    unfold error_unless. step; easy.
  Qed.

  (** The function [check_consensus_features] is valid *)
  Lemma check_consensus_features_is_valid
    vs kind_value expected_features consensus_content operation :
    expected_features.Valid.t expected_features ->
    letP? x :=
      Validate.Consensus.check_consensus_features vs kind_value
        expected_features consensus_content operation in
    True.
  Proof.
    intros.
    unfold Validate.Consensus.check_consensus_features.
    Error.split_error_with check_level_equal_is_valid.
    Error.split_error_with check_round_equal_is_valid.
    Error.split_error_with check_branch_equal_is_valid.
    apply check_payload_hash_equal_is_valid.
  Qed.

  Lemma ensure_conflict_free_preendorsement_is_valid vs slot :
    letP? x :=
      Validate.Consensus.ensure_conflict_free_preendorsement vs slot in
    True.
  Proof.
    unfold Validate.Consensus.ensure_conflict_free_preendorsement.
    unfold error_unless. now step.
  Qed.

  (** The function [expected_features_for_block_validation] is
      valid *)
  Lemma expected_features_for_block_validation_is_valid
    ctxt fitness block_payload_hash level round block_hash :
    Raw_context.Valid.t ctxt ->
    Fitness_repr.Valid.t fitness ->
    Level_repr.Valid.t level ->
    Round_repr.Valid.t round ->
    all_expected_consensus_features.Valid.t
      (Validate.Consensus.expected_features_for_block_validation
        ctxt fitness block_payload_hash level round block_hash).
  Proof.
    intros H_ctxt_valid H_fitness_valid H_level_valid H_round_valid.
    unfold Validate.Consensus.expected_features_for_block_validation.
    split; cbn; trivial.
    { destruct fitness, locked_round; cbn in *; trivial. split; cbn;
        [|apply H_fitness_valid].
      split; cbn.
      { Raw_context.Valid.destruct_rewrite H_ctxt_valid.
        unfold Alpha_context.Level.current. cbn.
        now destruct H_sim_ctxt, back, level, level1.
      }
      { destruct H_fitness_valid; now cbn in *.
      }
    }
    { unfold Validate.Consensus.expected_endorsement_for_block,
        Alpha_context.Consensus.endorsement_branch. cbn.
      red.
      step; cbn; trivial.
      step; cbn. split; cbn.
      unfold Validate.Consensus.expected_endorsement_features. split; cbn; sauto lq:on.
    }
  Qed.

  (** The function [get_expected_preendorsements_features] is valid *)
  Lemma get_expected_preendorsements_features_is_valid
    consensus_info consensus_content :
    consensus_info.Valid.t consensus_info ->
    letP? '(expected_features, round) :=
      Validate.Consensus.get_expected_preendorsements_features
        consensus_info consensus_content in
    expected_features.Valid.t expected_features /\
    Option.Forall Round_repr.Valid.t round.
  Proof.
    intros [].
    unfold Validate.Consensus.get_expected_preendorsements_features.
    step; try easy.
    cbn. destruct e; cbn in *.
    destruct all_expected_features; split; sauto q: on dep: on.
  Qed.

  (** The function [check_round_not_too_high] is valid *)
  Lemma check_round_not_too_high_is_valid block_round provided :
    letP? x :=
      Validate.Consensus.check_round_not_too_high
        block_round provided in
    True.
  Proof.
    unfold Validate.Consensus.check_round_not_too_high.
    step; unfold error_unless; [step|]; reflexivity.
  Qed.

  (** The function [get_delegate_details] is valid *)
  Lemma get_delegate_details_is_valid {a : Set}
    (domain : a -> Prop)
    (slot_map : Slot_repr.Map.(Map.S.t) a)
    kind_value consensus_content :
    Option.Forall domain
      (Slot_repr.Map.(S.find)
        consensus_content.(Operation_repr.consensus_content.slot)
        slot_map) ->
    letP? x :=
      Validate.Consensus.get_delegate_details
        slot_map kind_value consensus_content in
      domain x.
  Proof.
    intros H_domain.
    unfold Validate.Consensus.get_delegate_details.
    unfold of_option. now step.
  Qed.

  (** The function [update_validity_state_preendorsement] is valid *)
  Lemma update_validity_state_preendorsement_is_valid vs slot round
    voting_power :
    Pervasives.Int.Valid.t voting_power ->
    state.Valid.t vs ->
    Slot_repr.Valid.t slot ->
    Round_repr.Valid.t round ->
    state.Valid.t
      (Validate.Consensus.update_validity_state_preendorsement
        vs slot round voting_power).
  Proof.
    intros H_i_valid H_state_valid H_slot_valid H_round_valid.
    destruct H_state_valid.
    unfold Validate.Consensus.update_validity_state_preendorsement;
      split; cbn; [|sauto lq: on].
    destruct consensus_state; cbn.
    destruct vs, consensus_state,
      locked_round_evidence0 eqn:H_evidence; cbn; [step|];
    split; cbn in *; split; try easy; lia.
  Qed.

  (** The function [validate_preendorsement] is valid
      if the preendorsement values are valid
   *)
  Lemma validate_preendorsement_is_valid
    vi vs should_check_signature operation :
    info.Valid.on
      (fun _ => True)
      (fun slot_map =>
        forall k, Option.Forall (fun '(_, x) => Pervasives.Int.Valid.t x)
          (Slot_repr.Map.(S.find) k slot_map))
      (fun _ => True)
      vi ->
    state.Valid.t vs ->
    Operation_repr.Preendorsement_operation.Valid.t operation ->
    letP? x :=
      Validate.Consensus.validate_preendorsement
        vi vs should_check_signature operation in
    state.Valid.t x.
  Proof.
    intros H_valid_info H_valid_state H_valid_operation.
    unfold Validate.Consensus.validate_preendorsement.
    destruct operation, protocol_data, contents, c; cbn in *;
      [|easy..].
    Error.split_error_with
      ensure_conflict_free_preendorsement_is_valid.
    destruct H_valid_info.
    eapply Error.split_letP. {
      apply get_expected_preendorsements_features_is_valid; sauto.
    }
    intros. step.
    Error.split_error_with
      Validate.Consensus.check_round_not_too_high_is_valid.
    Error.split_error_with
      Validate.Consensus.check_consensus_features_is_valid.
    eapply Error.split_letP.
    { apply Validate.Consensus.get_delegate_details_is_valid
        with (domain := (fun '(_, i) => Pervasives.Int.Valid.t i)).
      destruct consensus_info; cbn in *.
      now specialize (preendorsement_slot_map
        c.(Operation_repr.consensus_content.slot)).
    }
    { intros.
      destruct x3 as [consensus_keys voting_power] eqn:?.
      Error.split_error_with
        Validate.Consensus.check_frozen_deposits_are_positive_is_valid.
      step; cbn; [
        Error.split_error_with
          Operation_repr.check_signature_is_valid|]; cbn;
        split; cbn; [|sauto| |sauto]; split; cbn; step; cbn;
        match goal with
        | |- Option.Forall _ _ => step; cbn
        | |- _ => idtac
        end; split; first [easy|lia|sauto].
    }
  Qed.

  (** The function [ensure_conflict_free_grandparent_endorsement] is valid *)
  Lemma ensure_conflict_free_grandparent_endorsement_is_valid vs
    delegate :
    letP? x :=
      Validate.Consensus.ensure_conflict_free_grandparent_endorsement
        vs delegate in
    True.
  Proof.
    unfold Validate.Consensus.ensure_conflict_free_grandparent_endorsement.
    unfold error_unless. step; easy.
  Qed.

  (** The function [update_validity_state_grandparent_endorsement] is valid *)
  Lemma update_validity_state_grandparent_endorsement_is_valid
    vs delegate :
    state.Valid.t vs ->
    let x :=
      Validate.Consensus.update_validity_state_grandparent_endorsement
        vs delegate in
    state.Valid.t x.
  Proof.
    intros H_valid_state.
    unfold
      Validate.Consensus.update_validity_state_grandparent_endorsement.
    sauto lq: on.
  Qed.

  (** The function [validate_grandparent_endorsement] is valid *)
  Lemma validate_grandparent_endorsement_is_valid
    vi vs should_check_signature expected
    consensus_content operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Consensus_content.Valid.t consensus_content ->
    expected_features.Valid.t expected ->
    letP? x :=
      Validate.Consensus.validate_grandparent_endorsement
        vi vs should_check_signature expected
        consensus_content operation in
    state.Valid.t x.
  Proof.
    intros H_info_valid H_state_valid H_consensus_content_valid
      H_expected_valid.
    unfold Validate.Consensus.validate_grandparent_endorsement.
    destruct H_info_valid, H_consensus_content_valid.
    Error.split_error_with Level_storage.from_raw_is_valid.
    unfold Alpha_context.Stake_distribution.slot_owner.
    Error.split_error_with Delegate_sampler.slot_owner_is_valid.
    step.
    Error.split_error_with
      ensure_conflict_free_grandparent_endorsement_is_valid.
    Error.split_error_with check_consensus_features_is_valid.
    step; cbn;
     [Error.split_error_with
       Operation_repr.check_signature_is_valid|]; cbn; sauto.
  Qed.

  (** The function [ensure_conflict_free_endorsement] is valid *)
  Lemma ensure_conflict_free_endorsement_is_valid vs slot :
    letP? x :=
      Validate.Consensus.ensure_conflict_free_endorsement
        vs slot in
    True.
  Proof.
    unfold Validate.Consensus.ensure_conflict_free_endorsement,
      error_unless; step; reflexivity.
  Qed.

  (** The function [update_validity_state_endorsement] is valid *)
  Lemma update_validity_state_endorsement_is_valid vs slot
    voting_power :
    state.Valid.t vs ->
    let x :=
      Validate.Consensus.update_validity_state_endorsement
        vs slot voting_power in
    state.Valid.t x.
  Proof.
    intros.
    unfold Validate.Consensus.update_validity_state_endorsement.
    sauto lq: on.
  Qed.

  (** The function [get_expected_endorsements_features] is valid *)
  Lemma get_expected_endorsements_features_is_valid consensus_info
    consensus_content :
    consensus_info.Valid.t consensus_info ->
    letP? x :=
      Validate.Consensus.get_expected_endorsements_features
        consensus_info consensus_content in
    expected_features.Valid.t x.
  Proof.
    intros.
    unfold Validate.Consensus.get_expected_endorsements_features.
    step; cbn; [sauto q: on dep: on|sfirstorder|..]; reflexivity.
  Qed.

  (** The function [validate_normal_endorsement] is valid *)
  Lemma validate_normal_endorsement_is_valid
    vi vs should_check_signature consensus_content operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    letP? x :=
      Validate.Consensus.validate_normal_endorsement
        vi vs should_check_signature consensus_content operation in
    state.Valid.t x.
  Proof.
    intros [] [].
    unfold Validate.Consensus.validate_normal_endorsement.
    Error.split_error_with
      Validate.Consensus.ensure_conflict_free_endorsement_is_valid.
    Error.split_error_with
      Validate.Consensus.get_expected_endorsements_features_is_valid.
    Error.split_error_with
      Validate.Consensus.check_consensus_features_is_valid.
    eapply Error.split_letP; [
      apply  Validate.Consensus.get_delegate_details_is_valid;
      instantiate (1 := (fun _ => True));
      match goal with
      | |- Option.Forall _ ?x => now destruct x
      end|intros].
    step.
    Error.split_error_with
      Validate.Consensus.check_frozen_deposits_are_positive_is_valid.
    step; [
       Error.split_error_with
         Operation_repr.check_signature_is_valid|]; cbn; sauto.
  Qed.

  (** The function [validate_endorsement] is valid *)
  Lemma validate_endorsement_is_valid
    vi vs should_check_signature operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Endorsement_operation.Valid.t operation ->
    letP? x :=
      Validate.Consensus.validate_endorsement
        vi vs should_check_signature operation in
    state.Valid.t x.
  Proof.
    intros [] [] ?.
    unfold Validate.Consensus.validate_endorsement.
    destruct operation, protocol_data, contents, c; cbn; try easy.
    step; cbn; [|
      now apply validate_normal_endorsement_is_valid].
    step; cbn; [|now apply validate_normal_endorsement_is_valid].
    apply validate_grandparent_endorsement_is_valid; try easy.
    destruct consensus_info, all_expected_features.
    now rewrite Heqo in
      expected_grandparent_endorsement_for_mempool.
  Qed.

  (** The function [ensure_conflict_free_dal_slot_availability] is valid *)
  Lemma ensure_conflict_free_dal_slot_availability_is_valid
    vs endorser :
    letP? x :=
      Validate.Consensus.ensure_conflict_free_dal_slot_availability
        vs endorser in
    True.
  Proof.
    unfold Validate
      .Consensus.ensure_conflict_free_dal_slot_availability,
      error_unless; step; reflexivity.
  Qed.

  (** The function [update_validity_state_dal_slot_availabitiy] is valid *)
  Lemma update_validity_state_dal_slot_availabitiy_is_valid
    vs endorser :
    state.Valid.t vs ->
    let x :=
      Validate.Consensus.update_validity_state_dal_slot_availabitiy
        vs endorser in
    state.Valid.t x.
  Proof.
    intros H_state_valid.
    unfold Validate
      .Consensus.update_validity_state_dal_slot_availabitiy.
    sauto lq: on.
  Qed.

  (** The function [validate_dal_slot_availability] is valid *)
  Lemma validate_dal_slot_availability_is_valid {a : Set}
    vi vs (arg : a) operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Dal_operation.Valid.t operation ->
    letP? x :=
      Validate.Consensus.validate_dal_slot_availability
        vi vs arg operation in
    state.Valid.t x.
  Proof.
    intros [] [] ?.
    unfold Validate.Consensus.validate_dal_slot_availability.
    destruct operation, protocol_data, contents, c; cbn; try easy.
    Error.split_error_with
      ensure_conflict_free_dal_slot_availability_is_valid.
    Error.split_error_with
      Dal_apply.validate_data_availability_is_valid.
    cbn. sauto lq: on.
  Qed.

  (** The function [expected_features_for_mempool] is valid *)
  Lemma expected_features_for_mempool_is_valid
    ctxt predecessor_level predecessor_round round :
    Round_repr.Valid.t round ->
    Round_repr.Valid.t predecessor_round ->
    Level_repr.Valid.t predecessor_level ->
    all_expected_consensus_features.Valid.t
      (Validate.Consensus.expected_features_for_mempool
         ctxt predecessor_level predecessor_round round).
  Proof.
    intros H_round_valid H_predround_valid H_level_valid.
    unfold Validate.Consensus.expected_features_for_mempool,
      Alpha_context.Consensus.endorsement_branch; cbn.
    unfold Raw_context.Consensus.endorsement_branch,
      Raw_context.Consensus.grand_parent_branch.
    repeat (step; cbn); do 2 (split; cbn; trivial);
      match goal with
      | |- expected_features.Valid.t _ => split; cbn
      | |- _ => idtac
      end; try assumption;
      try (apply H_level_valid || apply H_predround_valid);
      pose proof (@Raw_level_repr.pred_is_valid)
        as H_pred;
      specialize (H_pred
        predecessor_level.(Alpha_context.Level.t.level));
      rename_by_type (Alpha_context.Raw_level.pred _ = Some i)
        into H_pred';
      rewrite H_pred' in H_pred;
      apply H_pred; sauto lq: on.
  Qed.
End Consensus.

Module Voting.
  Module check_count_conflict.
    Module Valid.
      Definition t (count_previous_blocks : int)
        (proposer_history : Validate.proposer_history) : Prop :=
        (count_previous_blocks +i
              proposer_history.(Validate.proposer_history.count) <=
              Constants_repr.max_proposals_per_delegate).
      End Valid.
  End check_count_conflict.

  (** The function [check_count_conflict] is valid *)
  Lemma check_count_conflict_is_valid count_previous_blocks
    count_operation proposer_history :
    check_count_conflict.Valid.t count_previous_blocks proposer_history ->
    letP? x :=
      Validate.Voting.check_count_conflict count_previous_blocks
        count_operation proposer_history in
    True.
  Proof.
    intros H_valid. red in H_valid.
    unfold Validate.Voting.check_count_conflict.
    unfold Internal_errors.do_assert. cbn.
    match goal with
    | |- context [if ?e then _ else _] =>
        replace e with true by lia
    end. cbn. unfold error_unless. now step.
  Qed.

  (** The function [check_proposals_conflicts_and_update_state] is valid *)
  Lemma check_proposals_conflicts_and_update_state_is_valid
    state_value oph proposer proposals count_in_ctxt proposals_length :
    Option.Forall
      (fun proposer_history =>
         check_count_conflict.Valid.t count_in_ctxt proposer_history)
      (Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.Map)
         .(INDEXES_MAP.find) proposer
         state_value.(Validate.voting_state.proposals_validated)) ->
    letP? x :=
      Validate.Voting.check_proposals_conflicts_and_update_state
        state_value oph proposer proposals count_in_ctxt
        proposals_length in
    True.
  Proof.
    intros H_ccc_valid.
    unfold Validate.Voting.check_proposals_conflicts_and_update_state.
    step; [|sauto].
    apply Error.split_letP_triv.
    { Error.split_error_with
        Validate.Voting.check_count_conflict_is_valid.
      apply Error.split_letP with (P := fun _ => True).
      { eapply List.fold_left_e_is_valid
          with (P_a := fun _ => True)
            (P_b := fun _ => True); cbn; trivial; [|hauto l:on].
        intros. step; cbn; [reflexivity|constructor].
      }
      { intros; cbn; trivial. }
    } intros. step; cbn; [easy|]. simpl in *. constructor.
  Qed.

  (** The function [check_dictator_proposals_conflicts_and_update_state] is valid *)
  Lemma check_dictator_proposals_conflicts_and_update_state_is_valid
    state_value oph :
    letP? x :=
      Validate.Voting
        .check_dictator_proposals_conflicts_and_update_state
          state_value oph in
    True.
  Proof.
    unfold Validate.Voting
      .check_dictator_proposals_conflicts_and_update_state,
      error_unless; step; reflexivity.
  Qed.


  (** The function [check_ballot_conflicts] is valid *)
  Lemma check_ballot_conflicts_is_valid state_value voter :
    letP? x :=
      Validate.Voting.check_ballot_conflicts state_value voter in
    True.
  Proof.
    unfold Validate.Voting.check_ballot_conflicts.
    step; cbn; [reflexivity|].
    now step.
  Qed.

  (** The function [check_testnet_dictator_proposals] is valid *)
  Lemma check_testnet_dictator_proposals_is_valid {a : Set}
    chain_id (proposals : list a) :
    Chain_id.op_ltgt
      chain_id Alpha_context.Constants.mainnet_id = true ->
    letP? x :=
      Validate.Voting.check_testnet_dictator_proposals
        chain_id proposals in
    True.
  Proof.
    intros H_chain_id_eq.
    unfold Validate.Voting.check_testnet_dictator_proposals.
    unfold Internal_errors.do_assert.
    rewrite H_chain_id_eq; cbn.
    step; [easy|]. now step.
  Qed.

  (** The function [check_period_index] is valid *)
  Lemma check_period_index_is_valid expected period_index :
    letP? x :=
      Validate.Voting.check_period_index
        expected period_index in
    True.
  Proof.
    unfold Validate.Voting.check_period_index, error_unless.
    now step.
  Qed.

  (** The function [check_proposals_source_is_registered] is valid *)
  Lemma check_proposals_source_is_registered_is_valid ctxt source :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_proposals_source_is_registered
        ctxt source in
    True.
  Proof.
    unfold Validate.Voting.check_proposals_source_is_registered,
      fail_unless. now step.
  Qed.

  (** The function [check_proposal_list_sanity] is valid *)
  Lemma check_proposal_list_sanity_is_valid proposals :
    letP? x :=
      Validate.Voting.check_proposal_list_sanity proposals in
    True.
  Proof.
    unfold Validate.Voting.check_proposal_list_sanity.
    step; cbn; [reflexivity|].
    grep Lists.List.fold_left. admit.
  Admitted.

  (** The function [check_period_kind_for_proposals] is valid *)
  Lemma check_period_kind_for_proposals_is_valid current_period :
    letP? x :=
      Validate.Voting.check_period_kind_for_proposals current_period in
    True.
  Proof.
    unfold Validate.Voting.check_period_kind_for_proposals.
    now step.
  Qed.

  (** The function [check_in_listings] is valid *)
  Lemma check_in_listings_is_valid ctxt source :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_in_listings ctxt source in
    True.
  Proof.
    unfold Validate.Voting.check_in_listings, fail_unless.
    now step.
  Qed.

  (** The function [check_count] is valid *)
  Lemma check_count_is_valid count_in_ctxt proposals_length :
    count_in_ctxt <= Constants_repr.max_proposals_per_delegate ->
    letP? x :=
      Validate.Voting.check_count count_in_ctxt proposals_length in
    count_in_ctxt +i proposals_length <=
      Constants_repr.max_proposals_per_delegate.
   Proof.
    intros H_count_in_ctxt_eq.
    unfold Validate.Voting.check_count.
    unfold Internal_errors.do_assert.
    cbn in *. step; cbn; [|lia].
    unfold error_unless.
    step; cbn; [|reflexivity]. lia.
  Qed.

  (** The function [check_already_proposed] is valid *)
  Lemma check_already_proposed_is_valid ctxt proposer proposals :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_already_proposed ctxt proposer
        proposals in
    True.
  Proof.
    intros H_ctxt_valid.
    unfold Validate.Voting.check_already_proposed.
    unfold fail_when.
    induction proposals; [easy|].
    cbn. step; hauto lq:on.
  Qed.

  (** The function [check_period_kind_for_ballot] is valid *)
  Lemma check_period_kind_for_ballot_is_valid current_period :
    letP? x :=
      Validate.Voting.check_period_kind_for_ballot current_period in
    True.
  Proof.
    unfold Validate.Voting.check_period_kind_for_ballot.
    now step.
  Qed.

  (** The function [check_current_proposal] is valid *)
  Lemma check_current_proposal_is_valid ctxt op_proposal :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_current_proposal ctxt op_proposal in
    True.
  Proof.
    intros H_ctxt_valid.
    unfold Validate.Voting.check_current_proposal.
    Error.split_error_with Vote_storage.get_current_proposal_is_valid.
    unfold fail_unless. now step.
  Qed.

  (** The function [check_source_has_not_already_voted] is valid *)
  Lemma check_source_has_not_already_voted_is_valid ctxt source :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_source_has_not_already_voted ctxt source in
    True.
  Proof.
    intros H_ctxt_valid.
    unfold Validate.Voting.check_source_has_not_already_voted,
      fail_when. now step.
  Qed.

  (** The function [check_ballot_source_is_registered] is valid *)
  Lemma check_ballot_source_is_registered_is_valid ctxt source :
    Raw_context.Valid.t ctxt ->
    letP? x :=
      Validate.Voting.check_ballot_source_is_registered ctxt source in
    True.
  Proof.
    intros H_ctxt_valid.
    unfold Validate.Voting.check_ballot_source_is_registered,
      fail_unless. now step.
  Qed.

  (** The function [validate_proposals] is valid *)
  Lemma validate_proposals_is_valid vi vs should_check_signature oph
    operation :
    info.Valid.on
      (* propagated from Vote_storage.get_delegate_proposal_count_is_valid *)
      (fun sim_ctxt => forall proposer,
         Context.Indexed_data_storage.value_exists
           (fun proposals_count => proposals_count <= Constants_repr.max_proposals_per_delegate)
           proposer
           sim_ctxt
            .(Raw_context.t.back)
            .(Raw_context.back.context)
            .(Context.t.Storage)
            .(Context_generated.t.Vote)
            .(Context_generated.Vote.t.Proposals_count)
      )
      (fun _ => True)
      (fun _ => True)
      vi ->
    state.Valid.t vs ->
    Operation_repr.Proposals.Valid.on
      (fun p => forall count_in_ctxt ,
         count_in_ctxt +i List.length
            p.(Operation_repr.ConstructorRecords_contents_manager_operation
              .contents.Proposals.proposals) <=
           Constants_repr.max_proposals_per_delegate ->
       (* propagated from check_proposals_conflicts_and_update_state_is_valid *)
        Option.Forall
        (fun proposer_history : Validate.proposer_history =>
           check_count_conflict.Valid.t count_in_ctxt proposer_history)
        (Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.Map).(INDEXES_MAP.find)
           p.(Alpha_context.ConstructorRecords_contents_manager_operation
              .contents.Proposals.source)
           vs.(Validate.state.voting_state)
             .(Validate.voting_state.proposals_validated)))
      operation ->
    letP? x :=
      Validate.Voting.validate_proposals vi vs should_check_signature
        oph operation in
    state.Valid.t x.
  Proof.
    intros [] [] ?.
    unfold Validate.Voting.validate_proposals.
    destruct operation, protocol_data, contents, c; cbn; try easy.
    eapply Error.split_letP. {
      apply Voting_period_storage.get_current_is_valid.
      hauto l: on.
    }
    intros.
    Error.split_error_with check_period_index_is_valid.
    step; [step; [step|] |].
    { eapply Error.split_letP.
      { eapply Error.split_letP; [
          apply check_testnet_dictator_proposals_is_valid; easy|
            intros].
        apply
          check_dictator_proposals_conflicts_and_update_state_is_valid.
      }
      { intros.
        unfold when_.
        step; [|easy].
        eapply Error.split_letP; [|easy].
        unfold Alpha_context.Contract.get_manager_key.
        eapply Error.split_letP. {
          apply Contract_manager_storage.get_manager_key_is_valid;
            [constructor|].
          hauto l: on.
        }
        intros.
        apply Operation_repr.check_signature_is_valid.
      }
    }
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          apply
            check_proposals_source_is_registered_is_valid; hauto l:on.
        } intros.
        Error.split_error_with check_proposal_list_sanity_is_valid.
        Error.split_error_with
          check_period_kind_for_proposals_is_valid.
        eapply Error.split_letP. {
          apply check_in_listings_is_valid; hauto l:on.
        } intros.
        eapply Error.split_letP. {
          apply Vote_storage.get_delegate_proposal_count_is_valid.
          hauto l:on.
        } intros.
        Error.split_error_with check_count_is_valid.
        { intros.
          eapply Error.split_letP. {
            apply
              Validate.Voting.check_already_proposed_is_valid.
            hauto l:on.
          } intros. cbn in *.
          apply check_proposals_conflicts_and_update_state_is_valid.
          hauto lq:on.
        }
      }
      { intros. unfold when_.
        step; [|easy].
        unfold Alpha_context.Contract.get_manager_key.
        eapply Error.split_letP; [|easy].
        eapply Error.split_letP. {
          apply Contract_manager_storage.get_manager_key_is_valid;
          hauto l: on.
        }
        intros.
        apply Operation_repr.check_signature_is_valid.
      }
    }
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          apply Validate.Voting
            .check_proposals_source_is_registered_is_valid; hauto l: on.
        }
        intros. eapply Error.split_letP. {
          apply
            Validate.Voting.check_proposal_list_sanity_is_valid.
        }
        intros. eapply Error.split_letP.  {
          apply
            Validate.Voting.check_period_kind_for_proposals_is_valid.
        } intros. eapply Error.split_letP. {
          apply
            Validate.Voting.check_in_listings_is_valid; hauto l:on.
        } intros. eapply Error.split_letP. {
          apply
            Vote_storage.get_delegate_proposal_count_is_valid; hauto lq: on.
        } intros. eapply Error.split_letP. {
          now apply check_count_is_valid.
        } intros. eapply Error.split_letP. {
          apply check_already_proposed_is_valid; hauto l:on.
        } intros.
          apply check_proposals_conflicts_and_update_state_is_valid.
          cbn in *.
          match goal with
          | H : forall _, _ |-
            context [check_count_conflict.Valid.t ?x _ ] =>
              specialize (H x)
          end.
          hauto q: on.
        } intros. unfold when_.
        step; [|easy].
        eapply Error.split_letP. {
          eapply Error.split_letP. {
          apply
            Contract_manager_storage.get_manager_key_is_valid; hauto l:on.
        } intros.
          apply Operation_repr.check_signature_is_valid.
        } intros. cbn. hauto l:on.
    }
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          apply check_proposals_source_is_registered_is_valid; hauto l: on.
        } intros. eapply Error.split_letP. {
          apply Validate.Voting.check_proposal_list_sanity_is_valid; hauto l:on.
        } intros. eapply Error.split_letP. {
          apply Validate.Voting.check_period_kind_for_proposals_is_valid.
        } intros.  eapply Error.split_letP. {
          apply check_in_listings_is_valid; hauto l:on.
        } intros. eapply Error.split_letP. {
          apply Vote_storage.get_delegate_proposal_count_is_valid; hauto lq:on.
        } intros. eapply Error.split_letP. {
          now apply Validate.Voting.check_count_is_valid.
        } intros. eapply Error.split_letP. {
          apply check_already_proposed_is_valid; hauto l: on.
        } intros.
          apply check_proposals_conflicts_and_update_state_is_valid.
          cbn in *.
          match goal with
          | H : forall _, _ |-
            context [check_count_conflict.Valid.t ?x _ ] =>
              specialize (H x)
          end.
          hauto q: on.
      }
      intros. unfold when_; step; cbn.
      { eapply Error.split_letP. {
          eapply Error.split_letP. {
            apply Contract_manager_storage.get_manager_key_is_valid; hauto l: on.
          } intros.
          apply Operation_repr.check_signature_is_valid.
        }
        intros. easy.
      }
      { hauto l: on. }
    }
  Qed.

  (** The function [validate_ballot] is valid *)
  Lemma validate_ballot_is_valid vi vs should_check_signature oph
    operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Ballot.Valid.t operation ->
    letP? x :=
      Validate.Voting.validate_ballot vi vs should_check_signature oph
        operation in
    state.Valid.t x.
  Proof.
    intros [] [] H_op_valid.
    unfold Validate.Voting.validate_ballot.
    destruct operation, protocol_data, contents, c; cbn; try easy.
    Error.split_error_with
      Validate.Voting.check_ballot_source_is_registered_is_valid.
    Error.split_error_with check_ballot_conflicts_is_valid.
    Error.split_error_with Voting_period_storage.get_current_is_valid.
    Error.split_error_with check_period_index_is_valid.
    Error.split_error_with check_period_kind_for_ballot_is_valid.
    Error.split_error_with check_current_proposal_is_valid.
    Error.split_error_with check_source_has_not_already_voted_is_valid.
    Error.split_error_with check_in_listings_is_valid.
    unfold when_; step.
    { eapply Error.split_letP; [|easy].
      Error.split_error_with
        Contract_manager_storage.get_manager_key_is_valid.
      apply Operation_repr.check_signature_is_valid.
    }
    { cbn. hauto l: on. }
  Qed.
End Voting.

Module Anonymous.
  (** The function [validate_activate_account] is valid *)
  Lemma validate_activate_account_is_valid vi vs oph contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    match contents with
    | Operation_repr.Activate_account _ => True
    | _ => False
    end ->
    Operation_repr.Contents.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_activate_account vi
        vs oph contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_contents_valid.
    unfold Validate.Anonymous.validate_activate_account.
    step; try contradiction.
    step; cbn; [easy|]. unfold error_unless; step; [hauto l: on|].
    cbn. reflexivity.
  Qed.

  (** The function [check_denunciation_age] is valid *)
  Lemma check_denunciation_age_is_valid vi kind_value given_level :
    info.Valid.t vi ->
    Raw_level_repr.Valid.t given_level ->
    letP? x :=
      Validate.Anonymous.check_denunciation_age vi kind_value
        given_level in
    True.
  Proof.
    intros [] H_level_valid.
    unfold Validate.Anonymous.check_denunciation_age.
    Error.split_error_with Level_storage.from_raw_is_valid.
    eapply Error.split_letP.
    { apply Cycle_repr.add_is_valid.
      unfold Alpha_context.Constants.max_slashing_period.
      Raw_context.Valid.destruct_rewrite ctxt.
      cbn.
      destruct H_sim_ctxt, back, constants.
      clear - max_slashing_period.
      lia.
    }
    { intros. unfold error_unless; step; cbn; [|reflexivity].
      now step.
    }
  Qed.

  (** The function [validate_double_consensus] is valid *)
  Lemma validate_double_consensus_is_valid denunciation_kind vi vs
    oph op1 op2 :
    info.Valid.t vi ->
    state.Valid.t vs ->
    ((Operation_repr.Preendorsement_operation.Valid.t op1 /\
      Operation_repr.Preendorsement_operation.Valid.t op2) \/
     (Operation_repr.Endorsement_operation.Valid.t op1 /\
      Operation_repr.Endorsement_operation.Valid.t op2)) ->
    letP? x :=
      Validate.Anonymous.validate_double_consensus denunciation_kind
        vi vs oph op1 op2 in
    state.Valid.t x.
  Proof.
    intros [] [] H_ops_valid.
    unfold Validate.Anonymous.validate_double_consensus.
    destruct op1, protocol_data, contents, c, op2, protocol_data,
      contents; cbn;
    try destruct c0; cbn in *; try hauto lq:on;
      destruct H_ops_valid as
        [[H_ops_valid1 H_ops_valid2] |
         [H_ops_valid1 H_ops_valid2]]; try easy.
    { unfold error_unless; step; cbn; [|reflexivity].
      eapply Error.split_letP.
      { apply Level_storage.from_raw_is_valid; [easy|].
        now destruct H_ops_valid1.
      }
      { intros ? [].
        Error.split_error_with Validate.Anonymous
          .check_denunciation_age_is_valid.
        unfold Alpha_context.Stake_distribution.slot_owner.
        destruct H_ops_valid1, H_ops_valid2.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        step.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        rename t into ctxt0.
        destruct x2 as [ctxt1 consensus_key2] eqn:?.
        do 2 step; cbn; try easy.
        eapply Error.split_letP.
        { now apply
            Delegate_slashed_deposits_storage
              .already_slashed_for_double_endorsing_is_valid.
        }
        { intros.
          step; cbn; [|easy].
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid.
          cbn.
          hauto l: on.
        }
      }
    }
    { unfold error_unless; step; cbn; [|easy].
      eapply Error.split_letP.
      { destruct H_ops_valid1.
        apply
          Level_storage.from_raw_is_valid; try easy.
      }
      { intros ? [].
        destruct H_ops_valid1, H_ops_valid2.
        Error.split_error_with check_denunciation_age_is_valid.
        unfold Alpha_context.Stake_distribution.slot_owner.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt0 consensus_key1] eqn:?; cbn.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x2 as [ctxt1 consensus_key2] eqn:?; cbn.
        step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        {  eapply Error.split_letP.
          { apply Storage.Slashed_deposits.find_is_valid.
            scongruence.
          }
          { intros.
            step; cbn.
            { now instantiate (1 := (fun _ => True)). }
            { easy. }
          }
        }
        { intros. step; cbn; [|easy].
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          hauto l: on.
        }
      }
    }
  Qed.

  (** The function [validate_double_preendorsement_evidence] is valid *)
  Lemma validate_double_preendorsement_evidence_is_valid vi vs oph
    contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Double_preendorsement_evidence.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_double_preendorsement_evidence vi vs
        oph contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_contents_valid.
    unfold Validate.Anonymous.validate_double_preendorsement_evidence.
    destruct contents; cbn; try easy.
    red in H_contents_valid.
    destruct H_contents_valid as
      [[H_contents_valid1 H_contents_valid2] |
       [H_contents_valid1 H_contents_valid2]].
    destruct d, op1, protocol_data, contents, c; cbn; try easy.
    { destruct op2, protocol_data, contents, c0; cbn; try easy.
      destruct H_contents_valid1.
      unfold error_unless; step; cbn in *; [|easy].
      Error.split_error_with Level_storage.from_raw_is_valid.
      eapply Error.split_letP.
      { apply check_denunciation_age_is_valid; try easy.
        now destruct H.
      }
      { intros.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt0 consensus_key1]; cbn.
        destruct H_contents_valid2.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt1 consensus_key2]; cbn.
        step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        { eapply Error.split_letP.
          { now apply Storage.Slashed_deposits.find_is_valid.
          }
          { intros. step; cbn.
            { instantiate (1 := fun _ => True); easy. }
            { easy. }
          }
        }
        { intros.
          step; [|easy]. cbn.
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          hauto l: on.
        }
      }
    }
    { destruct d, op1, protocol_data, contents, c; cbn; try easy.
      destruct op2, protocol_data, contents, c0; cbn; try easy.
      destruct H_contents_valid1.
      unfold error_unless; step; cbn in *; [|easy].
      Error.split_error_with Level_storage.from_raw_is_valid.
      eapply Error.split_letP.
      { apply check_denunciation_age_is_valid; try easy.
        now destruct H.
      }
      { intros.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt0 consensus_key1]; cbn.
        destruct H_contents_valid2.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt1 consensus_key2]; cbn.
        step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        { eapply Error.split_letP.
          { now apply Storage.Slashed_deposits.find_is_valid.
          }
          { intros. step; cbn.
            { instantiate (1 := fun _ => True); easy. }
            { easy. }
          }
        }
        { intros.
          step; [|easy]. cbn.
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          hauto l: on.
        }
      }
    }
  Qed.

  (** The function [validate_double_endorsement_evidence] is valid *)
  Lemma validate_double_endorsement_evidence_is_valid vi vs oph
    contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Double_endorsement_evidence.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_double_endorsement_evidence vi vs
        oph contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_contents_valid.
    unfold Validate.Anonymous.validate_double_endorsement_evidence.
    destruct contents; cbn; try easy.
    red in H_contents_valid.
    destruct H_contents_valid as
      [[H_contents_valid1 H_contents_valid2] |
       [H_contents_valid1 H_contents_valid2]].
    destruct d, op1, protocol_data, contents, c; cbn; try easy.
    { destruct op2, protocol_data, contents, c0; cbn; try easy.
      destruct H_contents_valid1.
      unfold error_unless; step; cbn in *; [|easy].
      Error.split_error_with Level_storage.from_raw_is_valid.
      eapply Error.split_letP.
      { apply check_denunciation_age_is_valid; try easy.
        now destruct H.
      }
      { intros.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt0 consensus_key1]; cbn.
        destruct H_contents_valid2.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt1 consensus_key2]; cbn.
        step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        { eapply Error.split_letP.
          { now apply Storage.Slashed_deposits.find_is_valid.
          }
          { intros. step; cbn.
            { instantiate (1 := fun _ => True); easy. }
            { easy. }
          }
        }
        { intros.
          step; [|easy]. cbn.
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          hauto l: on.
        }
      }
    }
    { destruct d, op1, protocol_data, contents, c; cbn; try easy.
      destruct op2, protocol_data, contents, c0; cbn; try easy.
      destruct H_contents_valid1.
      unfold error_unless; step; cbn in *; [|easy].
      Error.split_error_with Level_storage.from_raw_is_valid.
      eapply Error.split_letP.
      { apply check_denunciation_age_is_valid; try easy.
        now destruct H.
      }
      { intros.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt0 consensus_key1]; cbn.
        destruct H_contents_valid2.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        destruct x1 as [ctxt1 consensus_key2]; cbn.
        step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        { eapply Error.split_letP.
          { now apply Storage.Slashed_deposits.find_is_valid. }
          { intros. step; cbn.
            { instantiate (1 := fun _ => True); easy. }
            { easy. }
          }
        }
        { intros.
          step; [|easy]. cbn.
          do 2 Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          hauto l: on.
        }
      }
    }
  Qed.

  (** The function [validate_double_baking_evidence] is valid *)
  Lemma validate_double_baking_evidence_is_valid vi vs oph
    contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Double_baking_evidence.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_double_baking_evidence vi vs
        oph contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_content_valid.
    unfold Validate.Anonymous.validate_double_baking_evidence.
    destruct contents; try easy.
    red in H_content_valid.
    destruct H_content_valid
      as [[H_content_valid1] [H_content_valid2]], shell, shell0.
    do 2 Error.split_error_with Fitness_repr.from_raw_is_valid.
    do 2 Error.split_error_with Raw_level_repr.of_int32_is_valid.
    unfold error_unless; step; cbn; [|easy].
    Error.split_error_with
      Validate.Anonymous.check_denunciation_age_is_valid.
    Error.split_error_with Level_storage.from_raw_is_valid.
    eapply Error.split_letP.
    { apply Round_repr.to_slot_is_valid.
      { pose proof (Fitness_repr.round_is_valid x ltac:(scongruence)).
        lia.
      }
      { Raw_context.Valid.destruct_rewrite ctxt.
        destruct H_sim_ctxt, back, constants.
        unfold Alpha_context.Constants.consensus_committee_size.
        cbn. lia.
      }
    }
    { intros.
      Error.split_error_with Delegate_sampler.slot_owner_is_valid;
      destruct x6 as [ctxt0 consensus_key1]; cbn;
      eapply Error.split_letP.
      { apply Round_repr.to_slot_is_valid.
        { pose proof
            (Fitness_repr.round_is_valid x ltac:(scongruence)).
          lia.
        }
        { Raw_context.Valid.destruct_rewrite ctxt.
          destruct H_sim_ctxt, back, constants.
          unfold Alpha_context.Constants.consensus_committee_size.
          cbn. lia.
        }
      }
      { intros.
        Error.split_error_with Delegate_sampler.slot_owner_is_valid.
        do 2 step; cbn; [|easy].
        step; cbn; [easy|].
        unfold Alpha_context
          .Delegate.already_slashed_for_double_endorsing.
        eapply Error.split_letP.
        { eapply Error.split_letP.
          { now apply Storage.Slashed_deposits.find_is_valid. }
          { intros. step; cbn.
            { instantiate (1 := fun _ => True); easy. }
            { easy. }
          }
        }
        { intros.
          step; [|easy]. cbn.
          do 2 Error.split_error_with
            Block_header_repr.check_signature_is_valid. cbn.
          hauto l: on.
        }
      }
    }
  Qed.

  (** The function [validate_drain_delegate] is valid *)
  Lemma validate_drain_delegate_is_valid vi vs should_check_signature
  oph (operation : Operation_repr.operation) :
    info.Valid.t vi ->
    state.Valid.t vs ->
    let contents :=
      operation
        .(Operation_repr.operation.protocol_data)
        .(Operation_repr.protocol_data.contents) in
    match contents with
    | Operation_repr.Single contents =>
        Operation_repr.Drain_delegate.Valid.t contents
    | _ => False
    end ->
    letP? x :=
      Validate.Anonymous.validate_drain_delegate vi vs
        should_check_signature oph operation in
    state.Valid.t x.
  Proof.
    intros [] [[] []] H_valid_op.
    unfold  Validate.Anonymous.validate_drain_delegate.
    destruct operation, protocol_data, contents, c; cbn; try easy;
      remaining_goals 1.
    unfold fail_unless. step; cbn; [|easy]. intro.
    Error.split_error_with
      Delegate_consensus_key.active_pubkey_is_valid.
    do 3 (step; cbn; [easy|]).
    Error.split_error_with Contract_storage.get_balance_is_valid.
    step; cbn.
    eapply Error.split_letP.
    { eapply Error.split_letP.
      { unfold  Alpha_context.Tez.op_divquestion.
        destruct x0. step; [easy|].
        cbn. now instantiate (1 := fun _ => True).
      }
      { intros. cbn. now instantiate (1 := fun _ => True). }
    }
    { intros.
      eapply Error.split_letP.
      { destruct x1. step; [easy|]. cbn.
        now instantiate (1 := fun _ => True). }
      { intros.
        unfold fail_when.
        step; [easy|]. cbn. step; cbn.
        { Error.split_error_with
            Operation_repr.check_signature_is_valid; cbn.
          step; cbn; [easy|].
	  hauto l: on.
        }
        { now step. }
      }
    }
    { Raw_context.Valid.destruct_rewrite ctxt.
      destruct H_sim_ctxt, back, constants.
      eapply Error.split_letP.
      { Raw_context.Valid.destruct_rewrite ctxt.
        destruct H_sim_ctxt, back, constants.
        apply Tez_repr.op_starquestion_is_valid; [lia|].
        unfold Int64.of_int.
        unfold Alpha_context.Constants.origination_size; cbn.
        lia.
      }
      { intros.
        eapply Error.split_letP.
        { unfold  Alpha_context.Tez.op_divquestion.
          destruct x0. step; [easy|].
          cbn. now instantiate (1 := fun _ => True).
        }
        { intros.
          eapply Error.split_letP.
          { apply Tez_repr.op_plusquestion_is_valid; lia.
          }
          { intros. unfold fail_when. step; cbn; [easy|].
            step; cbn.
            { Error.split_error_with
                Operation_repr.check_signature_is_valid.
              step; cbn; [easy|].
              split; cbn; [easy|].
              split. cbn. scongruence.
            }
            { step; cbn; [easy|].
              split; cbn; [|split; cbn]; [easy|].
              scongruence.
            }
          }
        }
      }
    }
  Qed.

  (** The function [validate_seed_nonce_revelation] is valid *)
  Lemma validate_seed_nonce_revelation_is_valid vi vs contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Seed_nonce_revelation.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_seed_nonce_revelation vi vs
        contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_contents_valid.
    unfold Validate.Anonymous.validate_seed_nonce_revelation.
    step; try easy.
    Error.split_error_with Level_storage.from_raw_is_valid.
    unfold error_unless; step; cbn; [|easy].
    Error.split_error_with Nonce_storage.check_unrevealed_is_valid.
    cbn.
    hauto l: on.
  Qed.

  (** The function [validate_vdf_revelation] is valid *)
  Lemma validate_vdf_revelation_is_valid vi vs contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Vdf_revelation.Valid.t contents ->
    letP? x :=
      Validate.Anonymous.validate_vdf_revelation vi vs
        contents in
    state.Valid.t x.
  Proof.
    intros [] [] H_contents_valid.
    unfold Validate.Anonymous.validate_vdf_revelation.
    step; cbn; try easy.
    unfold error_unless; step; cbn; [|easy].
    Error.split_error_with Seed_storage.check_vdf_is_valid.
    cbn. hauto l: on.
  Qed.
End Anonymous.

Module Manager.
  Module batch_state.
    Module Valid.
      Import Proto_alpha.Validate.Manager.batch_state.
      Record t (x : Proto_alpha.Validate.Manager.batch_state) : Prop := {
        balance : Tez_repr.Valid.t x.(balance);
        remaining_block_gas :
          (* The [Gas_limit_repr.Arith_z_fp_encoding_is_valid]
             domain *)
          Saturation_repr.Strictly_valid.t x.(remaining_block_gas);
      }.
    End Valid.
  End batch_state.

  (** The function [check_gas_limit_and_consume_from_block_gas] is
      valid *)
  Lemma check_gas_limit_and_consume_from_block_gas_is_valid vi
    remaining_block_gas gas_limit :
    info.Valid.t vi ->
    Saturation_repr.Strictly_valid.t remaining_block_gas ->
    Gas_limit_repr.Arith.Integral.Valid.t gas_limit ->
    letP? x :=
      Validate.Manager.check_gas_limit_and_consume_from_block_gas vi
        remaining_block_gas gas_limit in
    Saturation_repr.Strictly_valid.t x.
  Proof.
    intros [] H_sat_valid H_gas_valid.
    unfold Validate.Manager.check_gas_limit_and_consume_from_block_gas.
    step; cbn;
      unfold Alpha_context.Gas.check_limit_and_consume_from_block_gas.
    { eapply Error.split_letP.
      { apply Gas_limit_repr.check_gas_limit_is_valid; [|lia].
        destruct vi, manager_info; cbn in *.
        scongruence.
      }
      { intros. unfold error_unless; step; cbn; [|easy].
        unfold Saturation_repr.Strictly_valid.t,
          Gas_limit_repr.Arith.Integral.Valid.t,
          Gas_limit_repr.Is_rounded.t,
          Saturation_repr.Valid.t in *.
        destruct H_gas_valid as [H_gas_valid1 H_gas_valid2].
        unfold Saturation_repr.saturated.
        set (y := Z.max _ _).
        assert (y >= 0) by lia; split; try lia.
        unfold Alpha_context.Gas.Arith.fp_value in *.
        lia.
      }
    }
    { eapply Error.split_letP.
      { apply Gas_limit_repr.check_gas_limit_is_valid; [|lia].
        destruct vi, manager_info; cbn in *. scongruence.
      }
      { intros. unfold error_unless; step; cbn; [|easy].
        unfold Saturation_repr.Strictly_valid.t,
          Gas_limit_repr.Arith.Integral.Valid.t,
          Gas_limit_repr.Is_rounded.t,
          Saturation_repr.Valid.t in *.
        destruct H_gas_valid as [H_gas_valid1 H_gas_valid2].
        unfold Saturation_repr.saturated.
        set (y := Z.max _ _).
        assert (y >= 0) by lia; split; try lia.
        unfold Alpha_context.Gas.Arith.fp_value in *.
        lia.
      }
    }
    { eapply Error.split_letP.
      { apply Gas_limit_repr.check_gas_limit_is_valid; [|lia].
        destruct vi, manager_info; cbn in *. scongruence.
      }
      { intros. unfold error_unless; step; cbn; [|easy].
        unfold Saturation_repr.Strictly_valid.t,
          Gas_limit_repr.Arith.Integral.Valid.t,
          Gas_limit_repr.Is_rounded.t,
          Saturation_repr.Valid.t in *.
        destruct H_gas_valid as [H_gas_valid1 H_gas_valid2].
        unfold Saturation_repr.saturated.
        set (y := Z.max _ _).
        assert (y >= 0) by lia; split; try lia.
        unfold Alpha_context.Gas.Arith.fp_value in *.
        lia.
      }
    }
    { apply Error_monad.letP_Build_extensible_elim; [easy|].
      step; cbn in *.
      { unfold error_unless; step; cbn; [|reflexivity].
        unfold Alpha_context.Gas.Arith.fp_value in *.
        red in H_sat_valid; red in H_gas_valid;
          destruct H_gas_valid.
        replace (V0.Z.max (remaining_block_gas -i gas_limit) 0) with
          (remaining_block_gas -i gas_limit) by
          (unfold "-i"; cbn;
           rewrite Pervasives.normalize_identity; lia).
        lia.
      }
      {
        pose proof Gas_limit_repr.check_gas_limit_is_valid as H_check_gas_limit.
        specialize (H_check_gas_limit
           vi.(Validate.info.manager_info)
            .(Validate.manager_info.hard_gas_limit_per_operation)
           gas_limit).
        rewrite Heqt in H_check_gas_limit.
        cbn in *.
        unfold Error.not_internal in *; cbn in *.
        apply H_check_gas_limit; [|easy].
        apply manager_info.
      }
    }
  Qed.

  (** The function [check_storage_limit] is valid *)
  Lemma check_storage_limit_is_valid vi storage_limit :
    letP? x :=
      Validate.Manager.check_storage_limit vi storage_limit in
    True.
  Proof.
    unfold Validate.Manager.check_storage_limit.
    unfold error_unless; cbn; step; easy.
  Qed.

  (** The function [assert_tx_rollup_feature_enabled] is valid *)
  Lemma assert_tx_rollup_feature_enabled_is_valid vi :
    info.Valid.t vi ->
    letP? x :=
      Validate.Manager.assert_tx_rollup_feature_enabled vi in
    True.
  Proof.
    intros [].
    unfold Validate.Manager.assert_tx_rollup_feature_enabled.
    eapply Error.split_letP.
    { apply Raw_level_repr.of_int32_is_valid.
      unfold Alpha_context.Constants.tx_rollup_sunset_level,
        Raw_context.tx_rollup.
      Raw_context.Valid.destruct_rewrite ctxt.
      cbn. destruct H_sim_ctxt, back, constants, tx_rollup,
        sunset_level.
      lia.
    }
    { intros. unfold error_unless; now step.
    }
  Qed.

  (** The function [assert_sc_rollup_feature_enabled] is valid *)
  Lemma assert_sc_rollup_feature_enabled_is_valid vi :
    letP? x :=
      Validate.Manager.assert_sc_rollup_feature_enabled vi in
    True.
  Proof.
    unfold Validate.Manager.assert_sc_rollup_feature_enabled,
      error_unless; now step.
  Qed.

  (** The function [assert_dal_feature_enabled] is valid *)
  Lemma assert_dal_feature_enabled_is_valid vi :
    letP? x :=
      Validate.Manager.assert_dal_feature_enabled vi in
    True.
  Proof.
    unfold Validate.Manager.assert_dal_feature_enabled,
      error_unless; now step.
  Qed.

  (** The function [assert_not_zero_messages] is valid *)
  Lemma assert_not_zero_messages_is_valid {a : Set}
    (messages : list a) :
    letP? x :=
      Validate.Manager.assert_not_zero_messages messages in
    True.
  Proof.
    unfold Validate.Manager.assert_not_zero_messages. now step.
  Qed.

  (** The function [assert_zk_rollup_feature_enabled] is valid *)
  Lemma assert_zk_rollup_feature_enabled_is_valid vi :
    letP? x :=
      Validate.Manager.assert_zk_rollup_feature_enabled vi in
    True.
  Proof.
    unfold Validate.Manager.assert_zk_rollup_feature_enabled,
      error_unless; now step.
  Qed.

  (** The function [consume_decoding_gas] is valid *)
  Lemma consume_decoding_gas_is_valid gas_fp lexpr :
    Saturation_repr.Strictly_valid.t gas_fp ->
    Script_repr.stable_force_decode_cost lexpr <= gas_fp ->
    letP? x :=
      Validate.Manager.consume_decoding_gas gas_fp lexpr in
    Saturation_repr.Strictly_valid.t x.
  Proof.
    intros.
    unfold Validate.Manager.consume_decoding_gas.
    apply Error_monad.letP_Build_extensible_elim; [easy|].
    unfold Alpha_context.Script.consume_decoding_gas,
      Alpha_context.Gas.consume_from.
    pose proof Gas_limit_repr.raw_consume_is_valid as H_raw_consume.
    match goal with
    | |-
      context [Alpha_context.Gas.raw_consume ?a ?b] =>
        specialize (H_raw_consume a b)
    end.
    step; cbn in *; [|reflexivity].
    apply H_raw_consume; try easy.
    apply Script_repr.stable_force_decode_cost_is_valid.
  Qed.

  (** The function [validate_tx_rollup_submit_batch] is valid *)
  Lemma validate_tx_rollup_submit_batch_is_valid vi remaining_gas
    content :
    Saturation_repr.Strictly_valid.t remaining_gas ->
    info.Valid.t vi ->
    letP? x :=
      Validate.Manager.validate_tx_rollup_submit_batch vi remaining_gas
        content in
    Saturation_repr.Strictly_valid.t x.
  Proof.
    intros H_remgas_valid H_info_valid.
    unfold Validate.Manager.validate_tx_rollup_submit_batch.
    Error.split_error_with assert_tx_rollup_feature_enabled_is_valid.
    unfold Alpha_context.Tx_rollup_message.make_batch.
    unfold Alpha_context.Tx_rollup_message.make_message.
    Error.split_error_with Tx_rollup_gas.hash_cost_is_valid.
    unfold Alpha_context.Gas.consume_from.
    step; cbn; [|easy].
    unfold error_unless; step; cbn; [|easy].
    apply Gas_limit_repr.raw_consume_succ_impl in Heqo; lia.
  Qed.

  (** The function [validate_tx_rollup_dispatch_tickets] is valid *)
  Lemma validate_tx_rollup_dispatch_tickets_is_valid vi remaining_gas
    content :
    info.Valid.t vi ->
    Operation_repr.Tx_rollup_dispatch_tickets.Valid.t
      content remaining_gas ->
    Saturation_repr.Strictly_valid.t remaining_gas ->
    letP? x :=
      Validate.Manager.validate_tx_rollup_dispatch_tickets
        vi remaining_gas content in
    Saturation_repr.Strictly_valid.t x /\ x <= remaining_gas.
  Proof.
    intros H_info_valid H_op_valid H_sat_valid.
    unfold Validate.Manager.validate_tx_rollup_dispatch_tickets.
    Error.split_error_with
      assert_tx_rollup_feature_enabled_is_valid.
    destruct content; try contradiction. cbn in *.
    Error.split_error_with
      Tx_rollup_errors_repr.check_path_depth_is_valid.
    unfold error_when; step; cbn; [reflexivity|].
    step; cbn; [easy|].
    apply Error_monad.letP_Build_extensible_elim; [easy|].
    unfold fold_left_e.
    rewrite List.fold_left_rev_right_eq.
    apply List.Forall_rev in H_op_valid. rewrite List.rev_eq.
    revert H_op_valid.
    set (items := Lists.List.rev _).
    intros. induction items.
    { cbn. easy. }
    { cbn. 
      eapply Error.split_letP. {
        apply IHitems; now inversion H_op_valid.
      }
      intros gas **.
      unfold Alpha_context.Script.consume_decoding_gas,
       Alpha_context.Gas.consume_from.
      clear IHitems.
      change Alpha_context.Gas.raw_consume
        with Gas_limit_repr.raw_consume. cbn in *.
      ez destruct (Gas_limit_repr.raw_consume _ _)
        as [gas1|] eqn:H_gas1; cbn.
      ez destruct (Gas_limit_repr.raw_consume gas1 _)
        as [gas2|] eqn:H_gas2; cbn.
      apply Gas_limit_repr.raw_consume_is_valid' in H_gas2; 
        apply Gas_limit_repr.raw_consume_is_valid' in H_gas1.
      { destruct H_gas2 as [H_gas2 H_gas2'].
        destruct H_gas1 as [H_gas1 H_gas1'].
        rewrite H_gas2, H_gas1.
        inversion H_op_valid as [|? ? H_valid ?].
        destruct H_valid as [H_valid [H_valid' _]].
        change Alpha_context.Tx_rollup_reveal.t.contents with
          Tx_rollup_reveal_repr.t.contents.
        change Alpha_context.Tx_rollup_reveal.t.ty with
          Tx_rollup_reveal_repr.t.ty.
        revert H_valid H_valid' H_gas1 H_gas2.
        set (cost_contents := Script_repr.stable_force_decode_cost _).
        set (cost_ty := Script_repr.stable_force_decode_cost _).
        intros; split; lia.
      }
      all : inversion H_op_valid; cbn in *; try easy; remaining_goals 1.
      destruct H_gas1. subst gas1.
      intros. Tactics.destruct_conjs.
      repeat match goal with
      | H : context [Script_repr.stable_force_decode_cost _] |- _ =>
          revert H
      end.
      set (cost_content := Script_repr.stable_force_decode_cost _).
      intros. lia.
    }
  Qed.

  (** The function [validate_tx_rollup_rejection] is valid *)
  Lemma validate_tx_rollup_rejection_is_valid vi operation :
    info.Valid.t vi ->
    Operation_repr.Tx_rollup_rejection.Valid.t operation ->
    letP? x :=
      Validate.Manager.validate_tx_rollup_rejection vi operation in
    True.
  Proof.
    intros H_info_valid H_op_valid.
    unfold Validate.Manager.validate_tx_rollup_rejection.
    Error.split_error_with
      assert_tx_rollup_feature_enabled_is_valid.
    destruct operation; try destruct H_op_valid.
    do 2 Error.split_error_with
      Tx_rollup_errors_repr.check_path_depth_is_valid.
    apply Tx_rollup_errors_repr.check_path_depth_is_valid.
  Qed.

  (** The function [validate_contents] is valid *)
  Lemma validate_contents_is_valid vi batch_state_value contents :
    info.Valid.t vi ->
    batch_state.Valid.t batch_state_value ->
    Operation_repr.Manager.Manager_operation.Valid.t contents ->
    letP? x :=
      Validate.Manager.validate_contents vi batch_state_value
        contents in
    batch_state.Valid.t x.
  Proof.
    intros H_info_valid [] H_op_valid.
    unfold Validate.Manager.validate_contents.
    destruct contents; destruct H_op_valid.
    Error.split_error_with
      check_gas_limit_and_consume_from_block_gas_is_valid.
    eapply Error.split_letP. {
      apply Error_monad.letP_Build_extensible_elim; [easy|].
      unfold Alpha_context.Gas.Arith.fp_value,
        Alpha_context.Gas.consume_from.
      step; cbn; [|reflexivity].
      { shelve. (* @TODO ?P (t : Alpha_context.Gas.S.t) *) }
    } intros.
    Error.split_error_with
      Validate.Manager.check_storage_limit_is_valid.
    unfold error_unless; step; cbn; [|reflexivity].
    step; cbn.
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          unfold Alpha_context.Contract.check_public_key.
          apply Contract_manager_storage.check_public_key_is_valid.
        } intros; cbn.
        instantiate (1 :=
          fun x => Saturation_repr.Strictly_valid.t x).
        cbn.
        hauto l: on.
      } intros.
      eapply Error.split_letP. {
       apply Contract_storage.simulate_spending_is_valid; try easy.
       apply H_info_valid.
      } intros. now step.
    }
    { eapply Error.split_letP. {
        apply Validate.Manager.consume_decoding_gas_is_valid;
          try easy.
        (* @TODO
           Script_repr.stable_force_decode_cost
           t.(Alpha_context
           .ConstructorRecords_contents_manager_operation
           .manager_operation.Transaction.parameters) <= x0 *)
        admit.
      }
      intros. eapply Error.split_letP. {
        apply Contract_storage.simulate_spending_is_valid;
          try easy. apply H_info_valid.
      } intros. step; cbn. hauto l: on.
    }
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          apply consume_decoding_gas_is_valid; try easy.
          (* @TODO
            Script_repr.stable_force_decode_cost
            o.(Alpha_context.ConstructorRecords_contents_manager_operation
            .manager_operation.Origination.script)
            .(Alpha_context.Script.t.code) <= x0 *)
          admit.
        } intros.
        apply consume_decoding_gas_is_valid; try easy.
        (* @TODO
             Script_repr.stable_force_decode_cost
             o.(Alpha_context.ConstructorRecords_contents_manager_operation
            .manager_operation.Origination.script).(Alpha_context.Script.t.storage)
           <= x2 *)
        admit.
      } intros.
      intros. eapply Error.split_letP. {
        apply Contract_storage.simulate_spending_is_valid; try easy.
        apply H_info_valid.
      }
      intros. step; hauto l: on.
    }
    { eapply Error.split_letP. {
        eapply Error.split_letP. {
          now apply Contract_storage.spend_from_balance_is_valid.
        } intros. step; cbn.
        { shelve. (* @TODO ?P (x2, true) *) }
        { eapply Error.split_letP. {
            eapply Error.split_letP. {
              eapply Error.split_letP. {
                apply Contract_delegate_storage.find_is_valid;
                  apply H_info_valid.
              }
              intros. step; cbn.
              { step; cbn; [|easy].
                instantiate (1 := fun x : unit => True); easy.
              }
              { easy. }
            } intros; step; cbn.
            { instantiate (1 := fun x : bool => True). easy. }
            { eapply Error.split_letP. {
                apply Contract_delegate_storage.find_is_valid;
                  apply H_info_valid.
              } intros; now step.
            }
          } intros. cbn. shelve. (* @TODO ?P (x2, x3) *)
        }
      } intros. step; cbn. split; try easy. cbn.
      (* TODO
         H4 : ?P (t, b)
         ============================
         Tez_repr.Valid.t t *)
      admit.
    }
    { eapply Error.split_letP. {
        apply consume_decoding_gas_is_valid; try easy.
        (* @TODO Script_repr.stable_force_decode_cost
           r
           .(Alpha_context.ConstructorRecords_contents_manager_operation
           .manager_operation.Register_global_constant.value) <=
               x0 *)
        admit.
      } intros. eapply Error.split_letP. {
        apply Contract_storage.simulate_spending_is_valid; try easy.
        apply H_info_valid.
      } intros. step; cbn; split; easy.
    }
    { eapply Error.split_letP. {
        apply Contract_storage.simulate_spending_is_valid; try easy.
        apply H_info_valid.
      } intros. step; cbn; split; easy.
    } eapply Error.split_letP. {
      apply Contract_storage.simulate_spending_is_valid; try easy.
      apply H_info_valid.
    }
    all : admit. (* 22 subgoals *)
  (* @TODO shelved goals
   goal 1 (ID 1081) is:
      Saturation_repr.Strictly_valid.t t
    goal 2 (ID 2990) is:
     Tez_repr.t * bool -> Prop
    goal 3 (ID 3015) is:
     ?P (x2, true)
    goal 4 (ID 3120) is:
     ?P (x2, x3) *)
  Admitted.

  (** The function [validate_contents_list] is valid *)
  Lemma validate_contents_list_is_valid vi batch_state_value
    contents_list :
    info.Valid.t vi ->
    batch_state.Valid.t batch_state_value ->
    Operation_repr.Manager.Contents_list.Valid.t
      Operation_repr.Manager.Operation_kind.Manager contents_list ->
    letP? x :=
      Validate.Manager.validate_contents_list vi batch_state_value
        contents_list in
    batch_state.Valid.t x.
  Proof.
    intros.
    unfold Validate.Manager.validate_contents_list.
    (* TODO, recursive function *)
  Admitted.

  (** The function [maybe_update_remaining_block_gas] is valid *)
  Lemma maybe_update_remaining_block_gas_is_valid vi vs
    batch_state_value :
    info.Valid.t vi ->
    state.Valid.t vs ->
    batch_state.Valid.t batch_state_value ->
    let x :=
      Validate.Manager.maybe_update_remaining_block_gas vi vs
        batch_state_value in
    Saturation_repr.Strictly_valid.t x.
  Proof.
    intros [] [[] []] [].
    unfold Validate.Manager.maybe_update_remaining_block_gas.
    destruct vi, mode0 eqn:?; cbn in *; easy.
  Qed.

  (** The function [check_sanity_and_find_public_key] is valid *)
  Lemma check_sanity_and_find_public_key_is_valid vi vs contents :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Manager.Manager_operation.Valid.nested contents ->
    letP? '(x, _) := Validate.Manager.check_sanity_and_find_public_key
      vi vs contents in
      batch_state.Valid.t x.
  Proof.
    intros [] [[] []] H_valid_contents.
    unfold Validate.Manager.check_sanity_and_find_public_key.
    (* The below destruct is taking too much time (40 secs locally) *)
    destruct contents, c; try destruct H_valid_contents.
    { let crush := Error.split_error_with
          Contract_storage.check_allocated_and_get_balance_is_valid;
        Error.split_error_with
          Contract_storage.check_counter_increment_is_valid;
        unfold Alpha_context.Contract.get_manager_key;
        Error.split_error_with
          Contract_manager_storage.get_manager_key_is_valid;
        cbn; hauto l: on in
      step; cbn; try crush.
      Error.split_error_with
          Contract_storage.check_allocated_and_get_balance_is_valid.
        Error.split_error_with
          Contract_storage.check_counter_increment_is_valid.
        cbn. hauto l: on.
    }
    { (* step; cbn; Taking too long  *)
      (* @TODO recursive function check_batch_tail_sanity *)
        admit.
    }
  Admitted.

  (** The function [validate_manager_operation] is valid *)
  Lemma validate_manager_operation_is_valid vi vs
    should_check_signature source oph operation :
    info.Valid.t vi ->
    state.Valid.t vs ->
    Operation_repr.Manager.Manager_operation.Valid.nested
      operation
        .(Operation_repr.operation.protocol_data)
        .(Operation_repr.protocol_data.contents) ->
    Operation_repr.Manager.Contents_list.Valid.t
      Operation_repr.Manager.Operation_kind.Manager
      operation
        .(Operation_repr.operation.protocol_data)
        .(Operation_repr.protocol_data.contents) ->
    letP? x :=
      Validate.Manager.validate_manager_operation vi vs
        should_check_signature source oph operation in
    state.Valid.t x.
  Proof.
    intros [] [] H_valid_op1 H_valid_op2.
    unfold Validate.Manager.validate_manager_operation.
    step; cbn; [easy|].
    Error.split_error_with
      check_sanity_and_find_public_key_is_valid.
    step; cbn.
    Error.split_error_with
      Validate.Manager.validate_contents_list_is_valid.
    step; cbn.
    { Error.split_error_with
        Operation_repr.check_signature_is_valid.
      cbn. (* @TODO show the validity of the returned state *)
      split; try easy. split; cbn.
      now apply maybe_update_remaining_block_gas_is_valid.
    }
    { split; try easy; cbn.
      split; try easy; cbn.
      now apply maybe_update_remaining_block_gas_is_valid.
    }
  Qed.
End Manager.

(** The function [init_info_and_state_is_valid] is valid *)
Lemma init_info_and_state_is_valid ctxt mode chain_id features :
  Raw_context.Valid.t ctxt ->
  mode.Valid.t mode ->
  all_expected_consensus_features.Valid.t features ->
  let state := Validate.init_info_and_state
    ctxt mode chain_id features in
  validation_state.Valid.t state.
Proof.
  intros H_ctxt_valid H_mode_valid H_features_valid.
  Raw_context.Valid.destruct_rewrite H_ctxt_valid.
  destruct H_sim_ctxt, back, constants. cbn.
  unfold Validate.init_info_and_state.
  split; cbn; try easy.
  unfold Validate.init_state; split; try easy.
  cbn.
  unfold Alpha_context.Gas.Arith.fp_value.
  unfold Alpha_context.Constants.hard_gas_limit_per_block.
  cbn. clear - hard_gas_limit_per_block.
  red in hard_gas_limit_per_block. easy.
Qed.

(** The function [begin_application_aux] is valid *)
Lemma begin_application_aux_is_valid ctxt chain_id predecessor_level
  predecessor_timestamp block_header fitness is_partial :
  Raw_context.Valid.t ctxt ->
  Fitness_repr.Valid.t fitness ->
  Block_header_repr.Valid.t block_header ->
  Level_repr.Valid.t predecessor_level ->
  letP? x :=
    Validate.begin_application_aux ctxt chain_id predecessor_level
      predecessor_timestamp block_header fitness is_partial in
  validation_state.Valid.t x.
Proof.
  intros H_ctxt_valid H_fitness_valid H_block_valid H_level_valid.
  unfold Validate.begin_application_aux,
    Alpha_context.Stake_distribution.baking_rights_owner.
  eapply Error.split_letP.
  { apply Delegate_sampler.baking_rights_owner_is_valid; try easy;
    Raw_context.Valid.destruct_rewrite H_ctxt_valid;
      destruct H_sim_ctxt, back, level; [easy|].
    unfold Alpha_context.Fitness.round.
    now destruct H_fitness_valid.
  }
  { intros. do 2 step.
    Error.split_error_with
      Block_header_repr.begin_validate_block_header_is_valid.
    Error.split_error_with
      Validate.Consensus.check_frozen_deposits_are_positive_is_valid.
    eapply Error.split_letP.
    { apply Delegate_sampler.baking_rights_owner_is_valid; try easy;
      Raw_context.Valid.destruct_rewrite H_ctxt_valid;
        destruct H_sim_ctxt, back, level; [easy|].
      clear - H_block_valid.
      now destruct H_block_valid, protocol_data.
    }
    { intros.
      do 3 step; cbn; try (
        apply init_info_and_state_is_valid; try easy);
        apply
          Consensus.expected_features_for_block_validation_is_valid;
          try easy; unfold Alpha_context.Fitness.predecessor_round;
          now destruct H_fitness_valid.
    }
  }
Qed.

(** The function [begin_partial_application] is valid *)
Lemma begin_partial_application_is_valid ancestor_context chain_id
  predecessor_level predecessor_timestamp block_header fitness :
  Raw_context.Valid.t ancestor_context ->
  Fitness_repr.Valid.t fitness ->
  Block_header_repr.Valid.t block_header ->
  Level_repr.Valid.t predecessor_level ->
  letP? x :=
    Validate.begin_partial_application ancestor_context chain_id
      predecessor_level predecessor_timestamp block_header fitness in
  validation_state.Valid.t x.
Proof.
  unfold Validate.begin_partial_application.
  apply Validate.begin_application_aux_is_valid.
Qed.

(** The function [begin_application] is valid *)
Lemma begin_application_is_valid ancestor_context chain_id
  predecessor_level predecessor_timestamp block_header fitness :
  Raw_context.Valid.t ancestor_context ->
  Fitness_repr.Valid.t fitness ->
  Block_header_repr.Valid.t block_header ->
  Level_repr.Valid.t predecessor_level ->
  letP? x :=
    Validate.begin_application ancestor_context chain_id
      predecessor_level predecessor_timestamp block_header fitness in
  validation_state.Valid.t x.
Proof.
  unfold Validate.begin_application.
  apply begin_application_aux_is_valid.
Qed.

(* @TODO *)
(** The function [expected_endorsement_for_block_is_valid] is valid *)
Lemma expected_endorsement_for_block_is_valid ctxt predecessor_level
  predecessor_round :
  Level_repr.Valid.t predecessor_level ->
  Round_repr.Valid.t predecessor_round ->
  expected_endorsement.Valid.t
    (Validate.Consensus.expected_endorsement_for_block
       ctxt predecessor_level predecessor_round).
Proof.
  intros H_valid_level ?.
  unfold Validate.Consensus.expected_endorsement_for_block.
  step; cbn; [|easy]. step; cbn.
  split; try easy; cbn.
  unfold Validate.Consensus.expected_endorsement_features.
  split; cbn; try easy.
  apply H_valid_level.
Qed.

(** The function [begin_full_construction] is valid *)
Lemma begin_full_construction_is_valid ctxt chain_id predecessor_level
  predecessor_round predecessor_timestamp predecessor_hash round
  header_contents :
  Raw_context.Valid.t ctxt ->
  Round_repr.Valid.t predecessor_round ->
  Round_repr.Valid.t round ->
  Block_header_repr.contents.Valid.t header_contents ->
  Level_repr.Valid.t predecessor_level ->
  letP? x :=
    Validate.begin_full_construction ctxt chain_id predecessor_level
      predecessor_round predecessor_timestamp predecessor_hash round
      header_contents in
  validation_state.Valid.t x.
Proof.
  intros H_ctxt_valid H_preround_valid H_round_valid H_header_valid
    H_level_valid.
  unfold Validate.begin_full_construction.
  Error.split_error_with
    Block_header_repr.check_timestamp_is_valid.
  eapply Error.split_letP.
  { apply Delegate_sampler.baking_rights_owner_is_valid; try easy.
    Raw_context.Valid.destruct_rewrite H_ctxt_valid.
    now destruct H_sim_ctxt, back, level.
  }
  { intros.
    do 2 step.
    Error.split_error_with
      Consensus.check_frozen_deposits_are_positive_is_valid.
    eapply Error.split_letP.
    { apply Delegate_sampler.baking_rights_owner_is_valid; try easy;
      Raw_context.Valid.destruct_rewrite H_ctxt_valid;
        destruct H_sim_ctxt, back, level; try easy.
      now destruct H_header_valid.
    }
    { intros.
      do 2 step; cbn.
      apply init_info_and_state_is_valid; try easy.
      { split; cbn; trivial.
        { step; cbn; [easy|].
          split; cbn; [|easy]. split; cbn; [|easy].
          Tactics.destruct_pairs.
          rename_by_type (Raw_context.Valid.t t1) into H_ctxt_valid'.
          Raw_context.Valid.destruct_rewrite H_ctxt_valid'.
          destruct H_ctxt_valid', H_sim_ctxt, back, constants, level.
          clear - level.
          unfold Level_storage.current. cbn.
          easy.
        }
        { now apply expected_endorsement_for_block_is_valid. }
      }
    }
  }
Qed.

(** The function [begin_partial_construction] is valid *)
Lemma begin_partial_construction_is_valid {a : Set} ctxt chain_id
  predecessor_level predecessor_round (function_parameter : a) round :
  Round_repr.Valid.t round ->
  Round_repr.Valid.t predecessor_round ->
  Level_repr.Valid.t predecessor_level ->
  Raw_context.Valid.t ctxt ->
  letP? x :=
    Validate.begin_partial_construction ctxt chain_id
      predecessor_level predecessor_round function_parameter round in
  validation_state.Valid.t x.
Proof.
  intros.
  unfold Validate.begin_partial_construction.
  cbn. apply init_info_and_state_is_valid; try easy.
  now apply Validate.Consensus.expected_features_for_mempool_is_valid.
Qed.

(** The function [begin_no_predecessor_info] is valid *)
Lemma begin_no_predecessor_info_is_valid ctxt chain_id :
  Raw_context.Valid.t ctxt ->
  let x :=
    Validate.begin_no_predecessor_info ctxt chain_id in
  validation_state.Valid.t x.
Proof.
  intro H_ctxt_valid.
  unfold Validate.begin_no_predecessor_info.
  now apply init_info_and_state_is_valid.
Qed.

(** The function [check_validation_pass_consistency] is valid *)
Lemma check_validation_pass_consistency_is_valid vi vs
  validation_pass :
  info.Valid.t vi ->
  state.Valid.t vs ->
  letP? x :=
    Validate.check_validation_pass_consistency vi vs validation_pass in
  state.Valid.t x.
Proof.
  intros [] [].
  unfold Validate.check_validation_pass_consistency.
  step; cbn; try easy; step; try easy; step; try easy;
    unfold fail_unless; step; cbn; easy.
Qed.

(** The function [validate_operation] is valid *)
Lemma validate_operation_is_valid state op hash packop :
  validation_state.Valid.t state ->
  (* @TODO : figure out the right pre-condition and
     move it to [Pperation_repr]
  let 'Operation_repr.Operation_data p :=
     packop.(Alpha_context.packed_operation.protocol_data) in
  match p.(Operation_repr.protocol_data.contents) with
  | Operation_repr.Single _ => True
  | Operation_repr.Cons _ _ => False
  end -> *)
  letP? x :=
    Validate.validate_operation state op hash packop in
  state.Valid.t x.
Proof.
  intros [] **.
  unfold Validate.validate_operation.
  destruct packop, protocol_data eqn:?; cbn; cbn.
  destruct p, contents; cbn;
    try match goal with
    | |- False -> _ => discriminate
    end; intros.
  eapply Error.split_letP. {
    now apply Validate.check_validation_pass_consistency_is_valid.
  } intros.
  destruct state eqn:?, info0 eqn:?, mode eqn:?, c eqn:?; cbn in *.
  admit.
Admitted.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Contract_repr.
Require TezosOfOCaml.Proto_K.Contract_repr.

Module Old := TezosOfOCaml.Proto_J.Contract_repr.
Module New := TezosOfOCaml.Proto_K.Contract_repr.

(** Migrate [Contract_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Implicit signature => New.Implicit signature
  | Old.Originated hash => New.Originated hash
  end.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_J.Constants_repr.
Require TezosOfOCaml.Proto_K.Constants_repr.
Require TezosOfOCaml.Proto_K.Constants_parametric_repr.
Require TezosOfOCaml.Proto_K.Ratio_repr.

Require TezosOfOCaml.Proto_J_K.Error.
Require TezosOfOCaml.Proto_J_K.Tez_repr.

Module Old := TezosOfOCaml.Proto_J.Constants_repr.
Module New := TezosOfOCaml.Proto_K.Constants_repr.

Module ratio.
  Module Old := Old.ratio.
  Module New := TezosOfOCaml.Proto_K.Ratio_repr.

  (** Migrate [Constants_repr.ratio.record] *)
  Definition migrate (r : Old.record) : New.t :=
  {|
    New.t.numerator := r.(Old.numerator);
    New.t.denominator := r.(Old.denominator);
  |}.
End ratio.

Module parametric.
  Module Old := Old.parametric.
  Module New := TezosOfOCaml.Proto_K.Constants_parametric_repr.

  (** Migrate [Constants_repr.parametric.record] *)
  Definition migrate (p : Old.record) : New.t :=
  {|
    New.t.preserved_cycles := p.(Old.preserved_cycles);
    New.t.blocks_per_cycle := p.(Old.blocks_per_cycle);
    New.t.blocks_per_commitment := p.(Old.blocks_per_commitment);
    New.t.nonce_revelation_threshold :=
      axiom "nonce_revelation_threshold"; (* TODO *)
    New.t.blocks_per_stake_snapshot := p.(Old.blocks_per_stake_snapshot);
    New.t.cycles_per_voting_period := p.(Old.cycles_per_voting_period);
    New.t.hard_gas_limit_per_operation := p.(Old.hard_gas_limit_per_operation);
    New.t.hard_gas_limit_per_block := p.(Old.hard_gas_limit_per_block);
    New.t.proof_of_work_threshold := p.(Old.proof_of_work_threshold);
    New.t.tokens_per_roll := Tez_repr.migrate p.(Old.tokens_per_roll);
    New.t.vdf_difficulty := axiom "vdf_difficulty"; (* TODO *)
    New.t.seed_nonce_revelation_tip :=
      Tez_repr.migrate p.(Old.seed_nonce_revelation_tip);
    New.t.origination_size := axiom "TODO"; (* TODO *)
    New.t.baking_reward_fixed_portion :=
      Tez_repr.migrate p.(Old.baking_reward_fixed_portion);
    New.t.baking_reward_bonus_per_slot :=
      Tez_repr.migrate p.(Old.baking_reward_bonus_per_slot);
    New.t.endorsing_reward_per_slot :=
      Tez_repr.migrate p.(Old.endorsing_reward_per_slot);
    New.t.cost_per_byte := Tez_repr.migrate p.(Old.cost_per_byte);
    New.t.hard_storage_limit_per_operation :=
      p.(Old.hard_storage_limit_per_operation);
    New.t.quorum_min := p.(Old.quorum_min);
    New.t.quorum_max := p.(Old.quorum_max);
    New.t.min_proposal_quorum := p.(Old.min_proposal_quorum);
    New.t.liquidity_baking_subsidy :=
      Tez_repr.migrate p.(Old.liquidity_baking_subsidy);
    New.t.liquidity_baking_sunset_level :=
      p.(Old.liquidity_baking_sunset_level);
    New.t.liquidity_baking_toggle_ema_threshold :=
      p.(Old.liquidity_baking_toggle_ema_threshold);
    New.t.max_operations_time_to_live := p.(Old.max_operations_time_to_live);
    New.t.minimal_block_delay := p.(Old.minimal_block_delay);
    New.t.delay_increment_per_round := p.(Old.delay_increment_per_round);
    New.t.minimal_participation_ratio :=
      ratio.migrate p.(Old.minimal_participation_ratio);
    New.t.consensus_committee_size := p.(Old.consensus_committee_size);
    New.t.consensus_threshold := p.(Old.consensus_threshold);
    New.t.max_slashing_period := p.(Old.max_slashing_period);
    New.t.frozen_deposits_percentage := p.(Old.frozen_deposits_percentage);
    New.t.double_baking_punishment :=
      Tez_repr.migrate p.(Old.double_baking_punishment);
    New.t.ratio_of_frozen_deposits_slashed_per_double_endorsement :=
      ratio.migrate
        p.(Old.ratio_of_frozen_deposits_slashed_per_double_endorsement);
    New.t.testnet_dictator := axiom "testnet_dictator" (* TODO *);
    New.t.initial_seed := p.(Old.initial_seed);
    New.t.cache_script_size := p.(Old.cache_script_size);
    New.t.cache_stake_distribution_cycles :=
      p.(Old.cache_stake_distribution_cycles);
    New.t.cache_sampler_state_cycles := p.(Old.cache_sampler_state_cycles);
    New.t.tx_rollup :=
      {|
        New.tx_rollup.enable := p.(Old.tx_rollup_enable);
        New.tx_rollup.origination_size := p.(Old.tx_rollup_origination_size);
        New.tx_rollup.hard_size_limit_per_inbox :=
          p.(Old.tx_rollup_hard_size_limit_per_inbox);
        New.tx_rollup.hard_size_limit_per_message :=
          p.(Old.tx_rollup_hard_size_limit_per_message);
        New.tx_rollup.commitment_bond :=
          Tez_repr.migrate p.(Old.tx_rollup_commitment_bond);
        New.tx_rollup.finality_period := p.(Old.tx_rollup_finality_period);
        New.tx_rollup.withdraw_period := p.(Old.tx_rollup_withdraw_period);
        New.tx_rollup.max_inboxes_count := p.(Old.tx_rollup_max_inboxes_count);
        New.tx_rollup.max_messages_per_inbox :=
          p.(Old.tx_rollup_max_messages_per_inbox);
        New.tx_rollup.max_commitments_count :=
          p.(Old.tx_rollup_max_commitments_count);
        New.tx_rollup.cost_per_byte_ema_factor :=
          p.(Old.tx_rollup_cost_per_byte_ema_factor);
        New.tx_rollup.max_ticket_payload_size :=
          p.(Old.tx_rollup_max_ticket_payload_size);
        New.tx_rollup.max_withdrawals_per_batch :=
          p.(Old.tx_rollup_max_withdrawals_per_batch);
        New.tx_rollup.rejection_max_proof_size :=
          p.(Old.tx_rollup_rejection_max_proof_size);
        New.tx_rollup.sunset_level := p.(Old.tx_rollup_sunset_level);
      |};
    New.t.dal :=
      {|
        New.dal.feature_enable := axiom "TODO"; (* TODO *)
        New.dal.number_of_slots := axiom "TODO"; (* TODO *)
        New.dal.number_of_shards := axiom "TODO"; (* TODO *)
        New.dal.endorsement_lag := axiom "TODO"; (* TODO *)
        New.dal.availability_threshold := axiom "TODO"; (* TODO *)
      |};
    New.t.sc_rollup :=
      {|
        New.sc_rollup.enable := p.(Old.sc_rollup_enable);
        New.sc_rollup.origination_size := p.(Old.sc_rollup_origination_size);
        New.sc_rollup.challenge_window_in_blocks :=
          p.(Old.sc_rollup_challenge_window_in_blocks);
        New.sc_rollup.max_available_messages :=
          p.(Old.sc_rollup_max_available_messages);
        New.sc_rollup.stake_amount := axiom "TODO"; (* TODO *)
        New.sc_rollup.commitment_period_in_blocks := axiom "TODO"; (* TODO *)
        New.sc_rollup.max_lookahead_in_blocks := axiom "TODO"; (* TODO *)
        New.sc_rollup.max_active_outbox_levels := axiom "TODO"; (* TODO *)
        New.sc_rollup.max_outbox_messages_per_level := axiom "TODO"; (* TODO *)
      |};
  |}.
End parametric.

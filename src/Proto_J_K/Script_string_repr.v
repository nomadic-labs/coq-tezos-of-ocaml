Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_string_repr.
Require TezosOfOCaml.Proto_K.Script_string.

Module Old := TezosOfOCaml.Proto_J.Script_string_repr.
Module New := TezosOfOCaml.Proto_K.Script_string.

(** Migrate [Script_string_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.String_tag x => New.String_tag x
  end.

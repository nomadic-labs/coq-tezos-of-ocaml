Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_J.Round_repr.
Require TezosOfOCaml.Proto_K.Round_repr.

Module Old := TezosOfOCaml.Proto_J.Round_repr.
Module New := TezosOfOCaml.Proto_K.Round_repr.

Module Durations.
  Module Old := Old.Durations.
  Module New := New.Durations.

  (** Mirgate [Round_repr.Durations.t] *)
  Definition migrate (duration : Old.t) : New.t :=
  {|
    New.t.first_round_duration := duration.(Old.t.first_round_duration);
    New.t.delay_increment_per_round :=
      duration.(Old.t.delay_increment_per_round);
  |}.
End Durations.

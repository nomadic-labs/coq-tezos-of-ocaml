Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Tx_rollup_l2_address.
Require TezosOfOCaml.Proto_K.Tx_rollup_l2_address.
Require TezosOfOCaml.Proto_J_K.Indexable.

Module Old := TezosOfOCaml.Proto_J.Tx_rollup_l2_address.
Module New := TezosOfOCaml.Proto_K.Tx_rollup_l2_address.

Module Indexable.
  (** Migrate [Indexable.value]. *)
  Definition migrate_value (x : Old.Indexable.value) : New.Indexable.value :=
    Indexable.migrate x.
End Indexable.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Sapling_storage.
Require TezosOfOCaml.Proto_K.Sapling_storage.
Require TezosOfOCaml.Proto_J_K.Sapling_repr.

Module Old := TezosOfOCaml.Proto_J.Sapling_storage.
Module New := TezosOfOCaml.Proto_K.Sapling_storage.

(** Migrate [state]. *)
Definition migrate_state (x : Old.state) : New.state :=
  {|
    New.state.id := x.(Old.state.id);
    New.state.diff := Sapling_repr.migrate_diff x.(Old.state.diff);
    New.state.memo_size := x.(Old.state.memo_size);
  |}.

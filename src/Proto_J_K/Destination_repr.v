Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Destination_repr.
Require TezosOfOCaml.Proto_K.Destination_repr.
Require TezosOfOCaml.Proto_J_K.Contract_repr.

Module Old := TezosOfOCaml.Proto_J.Destination_repr.
Module New := TezosOfOCaml.Proto_K.Destination_repr.

(** Migrate [Destination_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Contract x => New.Contract (Contract_repr.migrate x)
  | Old.Tx_rollup x => New.Tx_rollup x
  end.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_int_repr.
Require TezosOfOCaml.Proto_K.Script_int.

Module Old := TezosOfOCaml.Proto_J.Script_int_repr.
Module New := TezosOfOCaml.Proto_K.Script_int.

(** Migrate [Script_int_repr.num]. *)
Definition migrate (x : Old.num) : New.num :=
  match x with
  | Old.Num_tag x => New.Num_tag x
  end.

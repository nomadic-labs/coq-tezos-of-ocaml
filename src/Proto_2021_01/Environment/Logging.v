Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Format.
Require Proto_2021_01.Environment.Lwt.
Require Proto_2021_01.Environment.Pervasives.

Parameter debug : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter log_info : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter log_notice : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter warn : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter log_error : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter fatal_error : forall {a : Set},
  Pervasives.format4 a Format.formatter unit unit -> a.

Parameter lwt_debug : forall {a : Set},
  Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.

Parameter lwt_log_info : forall {a : Set},
  Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.

Parameter lwt_log_notice : forall {a : Set},
  Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.

Parameter lwt_warn : forall {a : Set},
  Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.

Parameter lwt_log_error : forall {a : Set},
  Pervasives.format4 a Format.formatter unit (Lwt.t unit) -> a.

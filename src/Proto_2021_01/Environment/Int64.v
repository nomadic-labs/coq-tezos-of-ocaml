Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Definition t : Set := Z.

Definition zero : t := 0.

Definition one : t := 1.

Definition minus_one : t := -1.

Definition neg : t -> t := fun z => z.

Definition add : t -> t -> t := Z.add.

Definition sub : t -> t -> t := Z.sub.

Definition mul : t -> t -> t := Z.mul.

Definition div : t -> t -> t := Z.div.

Definition rem : t -> t -> t := Z.rem.

Definition succ : t -> t := fun z => Z.add z 1.

Definition pred : t -> t := fun z => Z.sub z 1.

Definition abs : t -> t := Z.abs.

Parameter max_int : t.

Parameter min_int : t.

Parameter logand : int64 -> int64 -> int64.

Parameter logor : int64 -> int64 -> int64.

Parameter logxor : int64 -> int64 -> int64.

Parameter lognot : int64 -> int64.

Parameter shift_left : int64 -> int -> int64.

Parameter shift_right : int64 -> int -> int64.

Parameter shift_right_logical : int64 -> int -> int64.

Definition of_int : int -> t := fun z => z.

Definition to_int : t -> int := fun z => z.

Parameter of_int32 : int32 -> int64.

Parameter to_int32 : int64 -> int32.

Parameter of_string_opt : string -> option int64.

Parameter to_string : int64 -> string.

Definition compare : t -> t -> int := Z.sub.

Definition equal : t -> t -> bool := Z.eqb.

Module Notations.
  Infix "+i64" := add (at level 50, left associativity).
  Infix "-i64" := sub (at level 50, left associativity).
  Infix "*i64" := mul (at level 40, left associativity).
  Infix "/i64" := div (at level 40, left associativity).
End Notations.

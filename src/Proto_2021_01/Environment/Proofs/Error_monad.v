Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Import Environment.Notations.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Lwt.

Definition eval {A : Set} (x : M? A) : M* A :=
  match x with
  | Pervasives.Error _ => None
  | Pervasives.Ok v => Some v
  end.

Definition eval_lwt {A : Set} (x : M=? A) : M* A :=
  match Lwt.eval x with
  | Pervasives.Error _ => None
  | Pervasives.Ok v => Some v
  end.

Lemma eval_bind {A B : Set} (e1 : M? A) (e2 : A -> M? B) :
  eval (Error_monad.op_gtgtquestion e1 e2) =
  Option.bind (eval e1) (fun x => eval (e2 x)).
  now destruct e1.
Qed.

Lemma eval_lwt_bind {A B : Set} (e1 : M=? A) (e2 : A -> M=? B) :
  eval_lwt (Error_monad.op_gtgteqquestion e1 e2) =
  Option.bind (eval_lwt e1) (fun x => eval_lwt (e2 x)).
  now destruct e1 as [e1]; destruct e1.
Qed.

Definition terminates {A : Set} (x : M? A) : Prop :=
  match x with
  | Pervasives.Error _ => False
  | Pervasives.Ok _ => True
  end.

Definition terminates_lwt {A : Set} (x : M=? A) : Prop :=
  match Lwt.eval x with
  | Pervasives.Error _ => False
  | Pervasives.Ok _ => True
  end.

Lemma bind_return {A : Set} (e : M? A) :
  (let? x := e in return? x) =
  e.
  now destruct e.
Qed.

Lemma bind_return_lwt {A : Set} (e : M=? A) :
  (let=? x := e in return=? x) =
  e.
  destruct e as [e].
  now destruct e.
Qed.

Lemma rewrite_bind {A B : Set} (e1 : M? A) (e2 e2' : A -> M? B)
  (H : forall v, e2 v = e2' v) :
  Error_monad.op_gtgtquestion e1 e2 =
  Error_monad.op_gtgtquestion e1 e2'.
  destruct e1; now simpl.
Qed.

Lemma rewrite_bind_lwt {A B : Set} (e1 : M=? A) (e2 e2' : A -> M=? B)
  (H : forall v, e2 v = e2' v) :
  Error_monad.op_gtgteqquestion e1 e2 =
  Error_monad.op_gtgteqquestion e1 e2'.
  destruct e1 as [e1]; destruct e1; now simpl.
Qed.

Lemma elim_record_trace_eval {A : Set} (mk_error : unit -> M? _error)
  (x : M? A) (v : A)
  : x = return? v ->
    record_trace_eval mk_error x = return? v.
  intro H; now rewrite H.
Qed.

Lemma elim_trace_eval {A : Set} (mk_error : unit -> M=? _error)
  (x : M=? A) (v : A)
  : x = return=? v ->
    trace_eval mk_error x = return=? v.
  intro H; now rewrite H.
Qed.

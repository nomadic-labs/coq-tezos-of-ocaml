Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Parameter get_int32 : bytes -> int -> int32.

Parameter set_int32 : bytes -> int -> int32 -> unit.

Parameter set_int8 : bytes -> int -> int -> unit.

Parameter get_int8 : bytes -> int -> int.

Parameter set_int16 : bytes -> int -> int -> unit.

Parameter get_int16 : bytes -> int -> int.

Parameter set_int64 : bytes -> int -> int64 -> unit.

Parameter get_int64 : bytes -> int -> int64.

Parameter get_uint8 : bytes -> int -> int.

Parameter get_uint16 : bytes -> int -> int.

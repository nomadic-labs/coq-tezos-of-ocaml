Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.

Definition read_access (path_length : int) (read_bytes : int)
  : Gas_limit_repr.cost :=
  let base_cost := Z.of_int (200000 +i (5000 *i path_length)) in
  Gas_limit_repr.atomic_step_cost
    (base_cost +Z ((Z.of_int 2) *Z (Z.of_int read_bytes))).

Definition write_access (written_bytes : int) : Gas_limit_repr.cost :=
  Gas_limit_repr.atomic_step_cost
    ((Z.of_int 200000) +Z ((Z.of_int 4) *Z (Z.of_int written_bytes))).

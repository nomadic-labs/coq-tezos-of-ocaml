Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Main.

Definition Protocol : Updater.PROTOCOL :=
  {|
    Updater.PROTOCOL.max_block_length := Main.max_block_length;
    Updater.PROTOCOL.max_operation_data_length := Main.max_operation_data_length;
    Updater.PROTOCOL.validation_passes := Main.validation_passes;
    Updater.PROTOCOL.block_header_data_encoding := Main.block_header_data_encoding;
    Updater.PROTOCOL.block_header_metadata_encoding := Main.block_header_metadata_encoding;
    Updater.PROTOCOL.operation_data_encoding := Main.operation_data_encoding;
    Updater.PROTOCOL.operation_receipt_encoding := Main.operation_receipt_encoding;
    Updater.PROTOCOL.operation_data_and_receipt_encoding := Main.operation_data_and_receipt_encoding;
    Updater.PROTOCOL.acceptable_passes := Main.acceptable_passes;
    Updater.PROTOCOL.compare_operations := Main.compare_operations;
    Updater.PROTOCOL.current_context := Main.current_context;
    Updater.PROTOCOL.begin_partial_application := Main.begin_partial_application;
    Updater.PROTOCOL.begin_application := Main.begin_application;
    Updater.PROTOCOL.begin_construction := Main.begin_construction;
    Updater.PROTOCOL.apply_operation := Main.apply_operation;
    Updater.PROTOCOL.finalize_block := Main.finalize_block;
    Updater.PROTOCOL.rpc_services := Main.rpc_services;
    Updater.PROTOCOL.init_value := Main.init_value;
  |}.

(** Run the following command to list all the axioms of the protocol. *)
(* Print Assumptions Protocol. *)

Require Coq.extraction.ExtrOcamlBasic.
Require Coq.extraction.ExtrOcamlNativeString.

(** Run the following command to extract the protocol code to OCaml. *)
(* Separate Extraction Main. *)

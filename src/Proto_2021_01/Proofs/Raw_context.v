Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Raw_context.

Definition with_unlimited_gas (ctxt : Raw_context.t) : Raw_context.t :=
  Raw_context.update_gas_counter_status ctxt
    Raw_context.Unlimited_operation_gas.

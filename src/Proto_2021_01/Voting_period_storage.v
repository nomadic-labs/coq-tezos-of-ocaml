Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Level_storage.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Voting_period_repr.

Definition set_current
  : Raw_context.t -> Voting_period_repr.voting_period -> M=? Raw_context.t :=
  Storage.Vote.Current_period.(Storage_sigs.Single_data_storage.set).

Definition get_current
  : Raw_context.t -> M=? Voting_period_repr.voting_period :=
  Storage.Vote.Current_period.(Storage_sigs.Single_data_storage.get).

Definition init_value
  : Raw_context.t -> Voting_period_repr.voting_period -> M=? Raw_context.t :=
  Storage.Vote.Current_period.(Storage_sigs.Single_data_storage.init_value).

Definition init_first_period (ctxt : Raw_context.t) (start_position : Int32.t)
  : M=? Raw_context.t :=
  let=? ctxt := init_value ctxt (Voting_period_repr.root start_position) in
  Storage.Vote.Pred_period_kind.(Storage_sigs.Single_data_storage.init_value)
    ctxt Voting_period_repr.Proposal.

Definition common (ctxt : Raw_context.t)
  : M=? (Raw_context.t * Voting_period_repr.voting_period * int32) :=
  let=? current_period := get_current ctxt in
  let=? ctxt :=
    Storage.Vote.Pred_period_kind.(Storage_sigs.Single_data_storage.set) ctxt
      current_period.(Voting_period_repr.voting_period.kind) in
  let start_position :=
    Int32.succ (Level_storage.current ctxt).(Level_repr.t.level_position) in
  return=? (ctxt, current_period, start_position).

Definition reset (ctxt : Raw_context.t) : M=? Raw_context.t :=
  let=? '(ctxt, current_period, start_position) := common ctxt in
  set_current ctxt (Voting_period_repr.raw_reset current_period start_position).

Definition succ (ctxt : Raw_context.t) : M=? Raw_context.t :=
  let=? '(ctxt, current_period, start_position) := common ctxt in
  set_current ctxt (Voting_period_repr.raw_succ current_period start_position).

Definition get_current_kind (ctxt : Raw_context.t)
  : M=? Voting_period_repr.kind :=
  let=? '{| Voting_period_repr.voting_period.kind := kind_value |} :=
    get_current ctxt in
  return=? kind_value.

Definition get_current_info (ctxt : Raw_context.t)
  : M=? Voting_period_repr.info :=
  let=? voting_period := get_current ctxt in
  let blocks_per_voting_period :=
    Constants_storage.blocks_per_voting_period ctxt in
  let level := Level_storage.current ctxt in
  let position := Voting_period_repr.position_since level voting_period in
  let remaining :=
    Voting_period_repr.remaining_blocks level voting_period
      blocks_per_voting_period in
  return=?
    {| Voting_period_repr.info.voting_period := voting_period;
      Voting_period_repr.info.position := position;
      Voting_period_repr.info.remaining := remaining |}.

Definition get_current_remaining (ctxt : Raw_context.t) : M=? Int32.t :=
  let=? voting_period := get_current ctxt in
  let blocks_per_voting_period :=
    Constants_storage.blocks_per_voting_period ctxt in
  return=?
    (Voting_period_repr.remaining_blocks (Level_storage.current ctxt)
      voting_period blocks_per_voting_period).

Definition is_last_block (ctxt : Raw_context.t) : M=? bool :=
  let=? remaining := get_current_remaining ctxt in
  return=? (remaining =i32 0).

Definition get_rpc_fixed_current_info (ctxt : Raw_context.t)
  : M=? Voting_period_repr.info :=
  let=?
    '{|
      Voting_period_repr.info.voting_period := voting_period;
        Voting_period_repr.info.position := position
        |} as voting_period_info := get_current_info ctxt in
  if position =i32 Int32.minus_one then
    let level := Level_storage.current ctxt in
    let blocks_per_voting_period :=
      Constants_storage.blocks_per_voting_period ctxt in
    let=? pred_kind :=
      Storage.Vote.Pred_period_kind.(Storage_sigs.Single_data_storage.get) ctxt
      in
    let voting_period :=
      {|
        Voting_period_repr.voting_period.index :=
          Int32.pred voting_period.(Voting_period_repr.voting_period.index);
        Voting_period_repr.voting_period.kind := pred_kind;
        Voting_period_repr.voting_period.start_position :=
          voting_period.(Voting_period_repr.voting_period.start_position) -i32
          blocks_per_voting_period |} in
    let position := Voting_period_repr.position_since level voting_period in
    let remaining :=
      Voting_period_repr.remaining_blocks level voting_period
        blocks_per_voting_period in
    return=?
      {| Voting_period_repr.info.voting_period := voting_period;
        Voting_period_repr.info.position := position;
        Voting_period_repr.info.remaining := remaining |}
  else
    Error_monad._return voting_period_info.

Definition get_rpc_fixed_succ_info (ctxt : Raw_context.t)
  : M=? Voting_period_repr.info :=
  let=? voting_period := get_current ctxt in
  let blocks_per_voting_period :=
    Constants_storage.blocks_per_voting_period ctxt in
  let level :=
    Level_storage.from_raw ctxt (Some 1)
      (Level_storage.current ctxt).(Level_repr.t.level) in
  let position := Voting_period_repr.position_since level voting_period in
  let remaining :=
    Voting_period_repr.remaining_blocks level voting_period
      blocks_per_voting_period in
  return=?
    {| Voting_period_repr.info.voting_period := voting_period;
      Voting_period_repr.info.position := position;
      Voting_period_repr.info.remaining := remaining |}.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_2021_01.Alpha_context.

Inductive kind : Set :=
| Int_kind : kind
| String_kind : kind
| Bytes_kind : kind
| Prim_kind : kind
| Seq_kind : kind.

Definition unparsed_stack_ty : Set :=
  list (Alpha_context.Script.expr * Alpha_context.Script.annot).

Definition type_map : Set :=
  list (int * (unparsed_stack_ty * unparsed_stack_ty)).

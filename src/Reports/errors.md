The idea of this report is to classify the kinds of errors that would
happen in the protocol. The classification is in the sense of making
internal errors distinct from other errors. This has
informative value but also it will be used as input in the verification
of the absence of internal errors.

## Legends

* `USER` -> User error, or external error, may happen.
* `INTR` -> Internal error, should never happen. (These are the errors that we want to verify that are impossible)

## Error list

Next, I will list all the files where were found error definitions, the errors are 
idenfitied by the extension of the `error` type, like `type error += ...`.

## Errors indicating bugs in the protocol

These errors are surrounded by comments (or such) that indicate that
they are expected to not happen.

* INTR `Tx_rollup_already_exists`,
  at https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_errors_repr.ml#L353
  _The registration states that this is internal and occurrence of this implies a bug in the protocol_

* INTR  `Tx_rollup_negative_input_size`
  at https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_gas.ml#L136
  _The registration states that this is internal and occurrence of this implies a bug in the protocol_

* INTR `Liquidity_baking_toggle_ema_out_of_bound`
  at https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/liquidity_baking_repr.ml#L69
  _The comment states that this is expected to not be raised_

* INTR `Tx_rollup_errors_repr.Internal_error`
  at https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_errors_repr.ml#L56
  
## Other interesting cases

* `Cache_repr.CLIENT` invariants [🔗](https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/cache_repr.mli#L170)
  > (** [value_of_identifier id] builds the cached value identified by
  >      [id]. This function is called when the subcache is loaded into
  >      memory from the on-disk representation of its domain.
  >      An error during the execution of this function is fatal as
  >      witnessed by its type: an error embedded in a [tzresult] is not
  >      supposed to be caught by the protocol. *)

## alpha_context.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/alpha_context.ml

* USER ``Gas_limit_too_high = Raw_context.Gas_limit_too_high``
* USER ``Block_quota_exceeded = Raw_context.Block_quota_exceeded``
* USER ``Operation_quota_exceeded = Raw_context.Operation_quota_exceeded``

### alpha_services.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/alpha_services.ml

* USER `No_available_snapshots of {min_cycle : int32}`

### amendment.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/amendment.ml

* USER ``Invalid_proposal``
* USER ``Unexpected_proposal``
* USER ``Unauthorized_proposal``
* USER ``Too_many_proposals``
* USER ``Empty_proposal``
* USER ``Unexpected_ballot``
* USER ``Unauthorized_ballot``
* USER ``Duplicate_ballot``

### apply.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/apply.ml

* USER `Not_enough_endorsements of {required : int; provided : int}`
* USER `Wrong_consensus_operation_branch of Block_hash.t * Block_hash.t`
* USER
  ```ocaml
  Invalid_double_baking_evidence of {
     hash1 : Block_hash.t;
     level1 : Raw_level.t;
     round1 : Round.t;
     hash2 : Block_hash.t;
     level2 : Raw_level.t;
     round2 : Round.t;
   }
  ```
* USER
  ```ocaml
   Wrong_level_for_consensus_operation of {
      expected : Raw_level.t;
      provided : Raw_level.t;
    }
  ```
* USER 
  ```ocaml
   Wrong_round_for_consensus_operation of {
     expected : Round.t;
     provided : Round.t;
    }
  ```
* USER `Preendorsement_round_too_high of {block_round : Round.t; provided : Round.t}`
* USER `Unexpected_endorsement_in_block`
* USER
  ```ocaml
   Wrong_payload_hash_for_consensus_operation of {
     expected : Block_payload_hash.t;
     provided : Block_payload_hash.t;
   }
   ```
* USER `Wrong_slot_used_for_consensus_operation`
* USER 
  ```ocaml
  Consensus_operation_for_future_level of {
    expected : Raw_level.t;
    provided : Raw_level.t;
  }
  ```
* USER
   ```ocaml
   Consensus_operation_for_future_round of {
       expected : Round.t;
       provided : Round.t;
   }
   ```
* USER
  ```ocaml
  Consensus_operation_for_old_level of {
    expected : Raw_level.t;
    provided : Raw_level.t;
  }
  ```
* USER
  ```ocaml
  Consensus_operation_for_old_round of {
    expected : Round.t;
    provided : Round.t;
  }
  ```
* USER
```ocaml
  Consensus_operation_on_competing_proposal of {
    expected : Block_payload_hash.t;
    provided : Block_payload_hash.t;
  }
  ```
* USER`Set_deposits_limit_on_unregistered_delegate of Signature.Public_key_hash.t`
* USER `Set_deposits_limit_too_high of {limit : Tez.t; max_limit : Tez.t}`
* USER `Empty_transaction of Contract.t`
* USER `Tx_rollup_feature_disabled`
* USER `Tx_rollup_invalid_transaction_amount`
* USER `Tx_rollup_non_internal_transaction`
* USER `Cannot_transfer_ticket_to_implicit`
* USER `Sc_rollup_feature_disabled`
* USER `Inconsistent_counters`
* USER `Wrong_voting_period of {expected : int32; provided : int32}`
* USER `Internal_operation_replay of Apply_results.packed_internal_contents`
* USER `Invalid_denunciation of denunciation_kind`
* USER 
  ```ocaml
  Inconsistent_denunciation of {
    kind : denunciation_kind;
    delegate1 : Signature.Public_key_hash.t;
    delegate2 : Signature.Public_key_hash.t;
  }
  ```
* USER `Unrequired_denunciation`
* USER 
  ```ocaml
  Too_early_denunciation of {
    kind : denunciation_kind;
    level : Raw_level.t;
    current : Raw_level.t;
  }
  ```
* USER 
  ```ocaml
  Outdated_denunciation of {
    kind : denunciation_kind;
    level : Raw_level.t;
    last_cycle : Cycle.t;
  }
  ```
* USER `Invalid_activation of {pkh : Ed25519.Public_key_hash.t}`
* USER `Multiple_revelation`
* USER `Gas_quota_exceeded_init_deserialize`
* USER `Inconsistent_sources`
* USER`Failing_noop_error`
* USER `Zero_frozen_deposits of Signature.Public_key_hash.t`

### baking.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/baking.ml

* USER
 ```ocaml
  (* `Permanent *)
    Insufficient_endorsing_power of {
    endorsing_power : int;
    consensus_threshold : int;
  }
  ```
  
### bitset.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/bitset.ml

* USER `Invalid_position of int`

### block_header_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/block_header_repr.ml

* USER
  ```ocaml
  (* Permanent *)
    Invalid_block_signature of
    Block_hash.t * Signature.Public_key_hash.t
  ```
* USER `(* Permanent *) Invalid_stamp`
 ```ocaml
  (* Permanent *)
    Invalid_payload_hash of {
    expected : Block_payload_hash.t;
    provided : Block_payload_hash.t;
  }
  ```
* USER
  ```ocaml
  (* Permanent *)
    Locked_round_after_block_round of {
    locked_round : Round_repr.t;
    round : Round_repr.t;
  }
  ```
* USER
  ```ocaml
  (* Permanent *)
    Invalid_payload_round of {
    payload_round : Round_repr.t;
    round : Round_repr.t;
  }
  ```
* USER
   ```ocaml
  (* Permanent *)
    Insufficient_locked_round_evidence of {
    voting_power : int;
    consensus_threshold : int;
  }
  ```
* USER `(* Permanent *) Invalid_commitment of {expected : bool}`
* USER `(* Permanent *) Wrong_timestamp of Time.t * Time.t`

### constants_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/constants_repr.ml

* USER ``Invalid_protocol_constants of string (* `Permanent *)``

### contract_manager_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/contract_manager_storage.ml

* USER ``(* `Branch *) Unrevealed_manager_key of Contract_repr.t``
* USER
  ```ocaml
  (* `Permanent *)
    Inconsistent_hash of {
    public_key : Signature.Public_key.t;
    expected_hash : Signature.Public_key_hash.t;
    provided_hash : Signature.Public_key_hash.t;
  }
  ```
* USER ``(* `Branch *) Previously_revealed_key of Contract_repr.t``
* USER ``(* `Branch *) Missing_manager_contract of Contract_repr.t``

### contract_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/contract_repr.ml

* USER ``Invalid_contract_notation of string (* `Permanent *)``

### contract_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/contract_storage.ml

* USER 
  ```ocaml
  (* `Temporary *)
    Balance_too_low of
    Contract_repr.contract * Tez_repr.t * Tez_repr.t
  ```
* USER 
  ```ocaml
  (* `Temporary *)
  Counter_in_the_past of Contract_repr.contract * Z.t * Z.t
  ```
* USER
  ```ocaml
  (* `Branch *)
    Counter_in_the_future of Contract_repr.contract * Z.t * Z.t
  ```
* USER
  ```ocaml
    (* `Temporary *)
    Non_existing_contract of Contract_repr.contract
  ```
* USER
  ```ocaml
    (* `Branch *)
    Empty_implicit_contract of Signature.Public_key_hash.t
  ```
* USER
  ```ocaml
    (* `Branch *)
    Empty_implicit_delegated_contract of
    Signature.Public_key_hash.t
  ```
* USER
  ```ocaml
    (* `Permanent *)
    Inconsistent_public_key of
    Signature.Public_key.t * Signature.Public_key.t
  ```
* USER ``(* `Permanent *) Failure of string``
* USER 
  ```ocaml
   (* `Permanent *)
      Frozen_bonds_must_be_spent_at_once of
      Contract_repr.t * Bond_id_repr.t
  ```
  
### delegate_services.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/delegate_services.ml

* USER `Balance_rpc_non_delegate of public_key_hash`

### delegate_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/delegate_storage.ml

* USER ``(* `Permanent *) No_deletion of Signature.Public_key_hash.t``
* USER ``(* `Temporary *) Active_delegate``
* USER ``(* `Temporary *) Current_delegate``
* USER ``(* `Permanent *) Empty_delegate_account of Signature.Public_key_hash.t``
* USER ``(* `Permanent *) Unregistered_delegate of Signature.Public_key_hash.t``
* USER  ``(* `Permanent *) Unassigned_validation_slot_for_level of Level_repr.t * int``
* USER ``(* `Permanent *) Cannot_find_active_stake of { cycle : Cycle_repr.t; delegate : Signature.Public_key_hash.t; }``
* USER ``(* `Temporary *) Not_registered of Signature.Public_key_hash.t``

### destination_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/destination_repr.ml

* USER `Invalid_destination_b58check of string`, _Depends on the contract_

### entrypoint_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/entrypoint_repr.ml

* USER `Name_too_long of string`
* `Unexpected_default of Script_repr.location`

### fees_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/fees_storage.ml

* USER ``Cannot_pay_storage_fee (* `Temporary *)``, _Depends on contract balance_
* USER ``Operation_quota_exceeded (* `Temporary *)`` _Depends on contract quota_
* USER ``Storage_limit_too_high (* `Permanent *)`` _Depends on an user operation!?_

### fitness_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/fitness_repr.ml

* USER ``(* `Permanent *) Invalid_fitness``
* USER ``(* `Permanent *) Wrong_fitness``
* USER ``(* `Permanent *) Outdated_fitness``
* USER ``(* `Permanent *) Locked_round_not_less_than_round of { round : Round_repr.t; locked_round : Round_repr.t; }``

### global_constants_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/global_constants_storage.ml

* USER `Expression_too_deep`, _Depends on the constant being registered_.
* USER `Expression_already_registered`
* USER `Badly_formed_constant_expression`
* USER `Nonexistent_global`
* USER `Expression_too_large`

### indexable.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/indexable.ml

* USER `Index_cannot_be_negative of int32`

### level_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/level_repr.ml

* USER `Invalid_cycle_eras`
* USER `Level_not_in_alpha of Raw_level_repr.t`
* USER `Negative_level_and_offset_sum of int32 * int32`

### liquidity_baking_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/liquidity_baking_repr.ml

* INTR ``Liquidity_baking_toggle_ema_out_of_bound of Int32.t``
  * From the code
    ```ocaml
    (* This error is not registered because we don't expect it to be raised. *)
    ```
### main.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/main.ml

* USER `Missing_shell_header`
  ** From code
     ```ocaml
      ~description:
        "During finalisation of a block header in Application mode or Full \
         construction mode, a shell header should be provided so that a cache \
         nonce can be computed."
         ```
### merkle_list.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/merkle_list.ml

* USER `Merkle_list_invalid_position`

### michelson_v1_primitives.ml

* USER `Invalid_primitive_name of string Micheline.canonical * Micheline.canonical_location`
* USER `Unknown_primitive_name of string`
* USER `Invalid_case of string`
* USER `Invalid_primitive_name of string Micheline.canonical * Micheline.canonical_location`

### nonce_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/nonce_storage.ml

* USER `Too_late_revelation`
* USER `Too_early_revelation`
* USER `Previously_revealed_nonce`
* USER `Inconsistent_nonce`

### operation_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/operation_repr.ml

* USER ``Contents_list_error of string (* `Permanent *)``, _I'm assuming that operation lists are constructed internally by the protocol so we can reason about their validity_
* USER ``Invalid_signature (* `Permanent *)``, _Depends on the public key used during the signing_
* USER  ``Missing_signature (* `Permanent *)``, _Depends on how operations are built, are raised when an operation that should be signed is not signed_

### period_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/period_repr.ml

* USER `Malformed_period of int64`
* USER `Invalid_arg`
* USER `Period_overflow`

### raw_context.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/raw_context.ml

* USER ``Double_inclusion_of_consensus_operation``
* USER ``Too_many_internal_operations (* `Permanent *)``
* USER ``Block_quota_exceeded (* `Temporary *)``
* USER ``Operation_quota_exceeded (* `Temporary *)``
* USER ``Stake_distribution_not_set (* `Branch *)``
* USER  ``Sampler_already_set of Cycle_repr.t (* `Permanent *)``
* USER ``Undefined_operation_nonce (* `Permanent *)``
* USER ``Gas_limit_too_high (* `Permanent *)``
* USER ``Storage_error of storage_error``
* USER ``Failed_to_parse_parameter of bytes``
* USER ``Failed_to_decode_parameter of Data_encoding.json * string``
* USER ``Block_quota_exceeded = Block_quota_exceeded``
* USER ``Operation_quota_exceeded = Operation_quota_exceeded``

### raw_context_intf.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/raw_context_intf.ml

* USER `Block_quota_exceeded`
* USER `Operation_quota_exceeded`

### raw_level_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/raw_level_repr.ml

* USER ``Unexpected_level of Int32.t (* `Permanent *)``

### round_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/round_repr.ml

* USER ``Non_increasing_rounds of {increment : Period_repr.t}``
* USER ``Round_durations_must_be_at_least_one_second of {round : Period_repr.t}``
* USER
  ```ocaml
  Round_of_past_timestamp of {
    provided_timestamp : Time.t;
    predecessor_timestamp : Time.t;
    predecessor_round : t;
  }
  ```
* USER `Negative_round of int`
* USER `Round_overflow of int`
* USER `Round_too_high of int32`
* USER `Level_offset_too_high of Period_repr.t`

### sc_rollup_inbox_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/sc_rollup_inbox_repr.ml

* USER `Invalid_level_add_messages of Raw_level_repr.t`
* USER `Invalid_number_of_messages_to_consume of int64`

###  sc_rollup_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/sc_rollup_storage.ml

* USER ``(* `Temporary *) Sc_rollup_already_staked``
* USER ``(* `Temporary *) Sc_rollup_disputed``
* USER ``(* `Temporary *) Sc_rollup_does_not_exist of Sc_rollup_repr.t``
* USER ``(* `Temporary *) Sc_rollup_no_conflict``
* USER ``(* `Temporary *) Sc_rollup_no_stakers``
* USER ``(* `Temporary *) Sc_rollup_not_staked``
* USER ``(* `Temporary *) Sc_rollup_not_staked_on_lcc``
* USER ``(* `Temporary *) Sc_rollup_parent_not_lcc``
* USER ``(* `Temporary *) Sc_rollup_remove_lcc``
* USER ``(* `Temporary *) Sc_rollup_staker_backtracked``
* USER ``(* `Temporary *) Sc_rollup_too_far_ahead``
* USER ``(* `Temporary *) Sc_rollup_too_recent``
* USER 
  ```ocaml
  (* `Temporary *)
    Sc_rollup_unknown_commitment of
    Sc_rollup_repr.Commitment_hash.t
  ```
* USER ``(* `Temporary *) Sc_rollup_bad_inbox_level``
* USER ``(* `Temporary *) Sc_rollup_max_number_of_available_messages_reached``

### script_interpreter.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/script_interpreter.ml

* USER `Reject of Script.location * Script.expr * execution_trace option`
* USER `Overflow of Script.location * execution_trace option`
* USER ``Runtime_contract_error of Contract.t``
* USER ``Bad_contract_parameter of Contract.t (* `Permanent *)``
* USER ``Cannot_serialize_failure``
* USER `Cannot_serialize_storage`
* USER ``Michelson_too_many_recursive_calls``

### script_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/script_repr.ml

* USER ``Lazy_script_decode (* `Permanent *)``

### script_string_repr.ml (missing in b9b6f296c32a3364b64992fe41f4454d14936dfd)

_PS : This file is missing in b9b6f296c32a3364b64992fe41f4454d14936dfd but I found this one which is the same file renamed_ 

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/script_string.ml
https://gitlab.com/clarus1/tezos/-/blob/guillaume-claret@proto_alpha-coq-of-ocaml/src/proto_alpha/lib_protocol/script_string_repr.ml

* USER `Non_printable_character of (int * string)`

### script_tc_errors.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/script_tc_errors.ml

_I understand that these depend on the script, so I'm marking them as USER errors._

* USER `Invalid_arity of Script.location * prim * int * int`
* USER `Invalid_seq_arity of Script.location * int * int`
* USER `Invalid_primitive of Script.location * prim list * prim`
* USER `Invalid_kind of Script.location * kind list * kind`
* USER `Invalid_never_expr of Script.location`
* USER `Missing_field of prim`
* USER `Duplicate_field of Script.location * prim`
* USER `Unexpected_lazy_storage of Script.location`
* USER `Unexpected_operation of Script.location`
* USER `Unexpected_contract of Script.location`
* USER `No_such_entrypoint of Entrypoint.t`
* USER `Duplicate_entrypoint of Entrypoint.t`
* USER `Unreachable_entrypoint of prim list`
* USER `Tx_rollup_bad_deposit_parameter of Script.location * Script.expr`
* USER `Tx_rollup_invalid_ticket_amount of Z.t`
* USER `Tx_rollup_addresses_disabled of Script.location`
* USER `Fail_not_in_tail_position of Script.location`
* USER `Undefined_unop : Script.location * prim * Script.expr -> error`
* USER `View_name_too_long of string`
* USER `Bad_view_name of Script.location`
* USER `Duplicated_view_name of Script.location`
* USER `Bad_stack_length`
* USER `Bad_stack_item of int`
* USER `Unexpected_annotation of Script.location`
* USER `Ungrouped_annotations of Script.location`
* USER `Invalid_map_body : Script.location * unparsed_stack_ty -> error`
* USER `Invalid_map_block_fail of Script.location`
* USER `Type_too_large : Script.location * int -> error`
* USER `Pair_bad_argument of Script.location`
* USER `Unpair_bad_argument of Script.location`
* USER `Dup_n_bad_argument of Script.location`
* USER `Dup_n_bad_stack of Script.location`
* USER `Invalid_contract of Script.location * Contract.t`
* USER `Invalid_big_map of Script.location * Big_map.Id.t`
* USER `Comparable_type_expected : Script.location * Script.expr -> error`
* USER `Inconsistent_type_sizes : int * int -> error`
* USER `Unordered_map_keys of Script.location * Script.expr`
* USER `Unordered_set_values of Script.location * Script.expr`
* USER `Duplicate_map_keys of Script.location * Script.expr`
* USER `Duplicate_set_values of Script.location * Script.expr`
* USER `Ill_formed_type of string option * Script.expr * Script.location`
* USER `Ill_typed_contract : Script.expr * type_map -> error`
* USER `Deprecated_instruction of prim`
* USER `Typechecking_too_many_recursive_calls`
* USER `Unparsing_too_many_recursive_calls`
* USER `Unexpected_ticket of Script.location`
* USER `Unexpected_forged_value of Script.location`
* USER `Non_dupable_type of Script.location * Script.expr`
* USER `Unexpected_ticket_owner of Destination.t`
* USER 
  ```ocaml
  Invalid_namespace of
    Script.location
    * prim
    * Michelson_v1_primitives.namespace
    * Michelson_v1_primitives.namespace
  ```
* USER ``Invalid_primitive of Script.location * prim list * prim``
* USER ``Invalid_kind of Script.location * kind list * kind``
* USER ``Invalid_never_expr of Script.location``
* USER ``Missing_field of prim``
* USER ``Duplicate_field of Script.location * prim``
* USER ``Unexpected_lazy_storage of Script.location``
* USER ``Unexpected_operation of Script.location``
* USER 
  ```ocaml
  Undefined_binop :
    Script.location * prim * Script.expr * Script.expr
    -> error
  ```
* USER ``Undefined_unop : Script.location * prim * Script.expr -> error``
* USER ``Bad_return : Script.location * unparsed_stack_ty * Script.expr -> error``
* USER ``Bad_stack : Script.location * prim * int * unparsed_stack_ty -> error``
* USER
  ```ocaml
  Unmatched_branches :
    Script.location * unparsed_stack_ty * unparsed_stack_ty
    -> error
  ```
* USER ``View_name_too_long of string``
* USER ``Bad_view_name of Script.location``
* USER
  ```ocaml
  Ill_typed_view of {
    loc : Script.location;
    actual : unparsed_stack_ty;
    expected : unparsed_stack_ty;
  }
  ```
* USER ``Duplicated_view_name of Script.location``
* USER ``Forbidden_instr_in_context of Script.location * context_desc * prim``
* USER ``Bad_stack_length``
* USER ``Bad_stack_item of int``
* USER ``Unexpected_annotation of Script.location``
* USER ``Ungrouped_annotations of Script.location``
* USER ``Invalid_map_body : Script.location * unparsed_stack_ty -> error``
* USER ``Invalid_map_block_fail of Script.location``
* USER
  ```ocaml
  Invalid_iter_body :
    Script.location * unparsed_stack_ty * unparsed_stack_ty
    -> error
  ```
* USER ``Type_too_large : Script.location * int -> error``
* USER ``Pair_bad_argument of Script.location``
* USER ``Unpair_bad_argument of Script.location``
* USER ``Dup_n_bad_argument of Script.location``
* USER ``Dup_n_bad_stack of Script.location``
* USER ``Invalid_constant : Script.location * Script.expr * Script.expr -> error``
* USER ``Invalid_syntactic_constant : Script.location * Script.expr * string -> error``
* USER ``Invalid_contract of Script.location * Contract.t``
* USER ``Invalid_big_map of Script.location * Big_map.Id.t``
* USER ``Comparable_type_expected : Script.location * Script.expr -> error``
* USER ``Inconsistent_type_sizes : int * int -> error``
* USER 
  ```ocaml
  Inconsistent_types :
    Script.location option * Script.expr * Script.expr
    -> error
  ```
* USER ``Inconsistent_memo_sizes : Sapling.Memo_size.t * Sapling.Memo_size.t -> error``
* USER ``Unordered_map_keys of Script.location * Script.expr``
* USER ``Unordered_set_values of Script.location * Script.expr``
* USER ``Duplicate_map_keys of Script.location * Script.expr``
* USER ``Duplicate_set_values of Script.location * Script.expr``
* USER ``Ill_typed_data : string option * Script.expr * Script.expr -> error``
* USER ``Ill_formed_type of string option * Script.expr * Script.location``
* USER ``Ill_typed_contract : Script.expr * type_map -> error``
* USER ``Deprecated_instruction of prim``
* USER ``Typechecking_too_many_recursive_calls``
* USER ``Unparsing_too_many_recursive_calls``
* USER ``Unexpected_ticket of Script.location``
* USER ``Unexpected_forged_value of Script.location``
* USER ``Non_dupable_type of Script.location * Script.expr``

### seed_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/seed_repr.ml

* USER ``Unexpected_nonce_length (* `Permanent *)``

### seed_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/seed_storage.ml

* USER
  ```ocaml
  Unknown of {
    oldest : Cycle_repr.t;
    cycle : Cycle_repr.t;
    latest : Cycle_repr.t;
  }
  ```
### slot_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/slot_repr.ml

* USER `Invalid_slot of int`

### storage_functors.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/storage_functors.ml

* USER `Block_quota_exceeded = C.Block_quota_exceeded`
* USER `Operation_quota_exceeded = C.Operation_quota_exceeded`

### tez_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tez_repr.ml

* USER ``Addition_overflow of t * t (* `Temporary *)``
* USER ``Subtraction_underflow of t * t (* `Temporary *)``
* USER ``Multiplication_overflow of t * int64 (* `Temporary *)``
* USER ``Negative_multiplicator of t * int64 (* `Temporary *)``
* USER ``Invalid_divisor of t * int64``

### ticket_accounting.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_accounting.ml

* USER `Invalid_ticket_transfer of {ticketer : string; amount : Z.t}`

### ticket_hash_builder.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_hash_builder.ml

* USER `Failed_to_hash_node`

### ticket_lazy_storage_diff.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_lazy_storage_diff.ml

* USER `Failed_to_load_big_map_value_type of Big_map.Id.t` it depends on the existence of the big map [in disk?]

### ticket_operations.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_operations_diff.ml

* USER ``Failed_to_get_script of Contract.t``
* USER ``Contract_not_originated``

### ticket_scanner.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_scanner.ml

* USER `Unsupported_non_empty_overlay`
* USER `Unsupported_type_operation`

### ticket_storage.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/ticket_storage.ml

* USER ``Negative_ticket_balance of {key : Ticket_hash_repr.t; balance : Z.t}``
* USER ``Used_storage_space_underflow``

### time_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/time_repr.ml

_These are about overflows so I think is interesting trying to proof that they are impossible_

* USER ``Timestamp_add (* `Permanent *)``
* USER ``Timestamp_sub (* `Permanent *)``

### tx_rollup_errors_repr.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_errors_repr.ml

* INTR `Tx_rollup_already_exists of Tx_rollup_repr.t`
* USER `Tx_rollup_does_not_exist of Tx_rollup_repr.t`
* USER `Submit_batch_burn_exceeded of {burn : Tez_repr.t; limit : Tez_repr.t}`
* USER `Inbox_does_not_exist of Tx_rollup_repr.t * Tx_rollup_level_repr.t`
* USER `Inbox_size_would_exceed_limit of Tx_rollup_repr.t`
* USER `Inbox_count_would_exceed_limit of Tx_rollup_repr.t`
* USER `No_uncommitted_inbox`
* USER `Message_size_exceeds_limit`
* USER `Too_many_inboxes`
* USER `Too_many_commitments`
* USER `Too_many_withdrawals`
* USER `Wrong_batch_count`
* USER 
  ```ocaml
  Commitment_too_early of {
    provided : Tx_rollup_level_repr.t;
    expected : Tx_rollup_level_repr.t;
  }
  ```
* USER `Level_already_has_commitment of Tx_rollup_level_repr.t`
* USER `Wrong_inbox_hash`
* USER `Bond_does_not_exist of Signature.public_key_hash`
* USER `Bond_in_use of Signature.public_key_hash`
* USER ``No_commitment_to_finalize``
* USER `No_commitment_to_remove`
* USER `Invalid_rejection_level_argument`
* USER `Commitment_does_not_exist of Tx_rollup_level_repr.t`
* USER 
  ``` ocaml
  Wrong_predecessor_hash of {` 
      provided : Tx_rollup_commitment_repr.Commitment_hash.t option;
      expected : Tx_rollup_commitment_repr.Commitment_hash.t option;
    }
  ```
* INTR ``Internal_error of string``
* USER
  ```ocaml
   Wrong_message_position of {
      level : Tx_rollup_level_repr.t;
      position : int;
      length : int;
  }
  ```
* USER `Wrong_message_path_depth of {provided : int; limit : int}`
* USER ``Wrong_withdraw_path_depth of {provided : int; limit : int}`
* USER ``Wrong_message_path of {expected : Tx_rollup_inbox_repr.Merkle.root}``
* USER 
  ```ocaml
    No_finalized_commitment_for_level of {
      level : Tx_rollup_level_repr.t;
      window : (Tx_rollup_level_repr.t * Tx_rollup_level_repr.t) option;
    }
   ```
* USER ``Withdraw_invalid_path``
* USER ``Withdraw_already_consumed``
* USER ``Commitment_bond_negative of int``
* USER 
  ```ocaml
  Cannot_reject_level of {
      provided : Tx_rollup_level_repr.t;
      accepted_range : (Tx_rollup_level_repr.t * Tx_rollup_level_repr.t) option;
  }
 ```
* USER 
  ```ocaml
  | Wrong_rejection_hashes of {
      provided : Tx_rollup_commitment_repr.message_result;
      computed : Tx_rollup_commitment_repr.Message_result_hash.t;
      expected : Tx_rollup_commitment_repr.Message_result_hash.t;
    }
  ```
* USER ``Ticket_payload_size_limit_exceeded of {payload_size : int; limit : int}``
* USER ``Deposit_wrong_ticketer of Tx_rollup_repr.t``
* USER ``Wrong_deposit_parameters``
* USER ``Proof_failed_to_reject``
* USER ``Proof_produced_rejected_state``
* USER ``Proof_invalid_before of {agreed : Context_hash.t; provided : Context_hash.t}``

### tx_rollup_gas.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_gas.ml

* INTR `Tx_rollup_negative_input_size`

### tx_rollup_l2_apply.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_l2_apply.ml

* USER 
  ```ocaml
  Counter_mismatch of {
    account : Tx_rollup_l2_address.t;
    expected : int64;
    provided : int64;
  }
  ```
* USER `Incorrect_aggregated_signature`
* USER `Unallocated_metadata of int32`
* USER `Multiple_operations_for_signer of Bls_signature.pk`
* USER `Invalid_transaction_encoding`, _Maybe I should classify these as I/O error?_
* USER `Invalid_batch_encoding`
* USER `Unexpectedly_indexed_ticket`
* USER `Missing_ticket of Ticket_hash.t`
* USER `Unknown_address of Tx_rollup_l2_address.t`
* USER `Invalid_self_transfer`
* USER `Invalid_zero_transfer`
* USER `Maximum_withdraws_per_message_exceeded of {current : int; maximum : int}`

### tx_rollup_l2_context.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_l2_context.ml

_A better classification for these would be IO_

* USER `Key_cannot_be_serialized`
* USER `Value_cannot_be_serialized`
* USER `Value_cannot_be_deserialized`

### tx_rollup_l2_context_sig.ml

https://gitlab.com/tezos/tezos/-/blob/b9b6f296c32a3364b64992fe41f4454d14936dfd/src/proto_alpha/lib_protocol/tx_rollup_l2_context_sig.ml

* USER `Balance_too_low`
* USER `Balance_overflow`
* USER `Invalid_quantity`
* USER `Unknown_address_index of address_index`
* USER `Metadata_already_initialized of address_index`
* USER `Too_many_l2_addresses`
* USER `Too_many_l2_tickets`
* USER `Counter_overflow`

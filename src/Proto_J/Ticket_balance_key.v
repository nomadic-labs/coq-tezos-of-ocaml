(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Alpha_context.
Require TezosOfOCaml.Proto_J.Script_ir_translator.
Require TezosOfOCaml.Proto_J.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Ticket_token.

Definition of_ex_token
  (ctxt : Alpha_context.context) (owner : Alpha_context.Destination.t)
  (function_parameter : Ticket_token.ex_token)
  : M? (Alpha_context.Ticket_hash.t * Alpha_context.context) :=
  let
    'Ticket_token.Ex_token {|
      Ticket_token.ex_token.Ex_token.ticketer := ticketer;
        Ticket_token.ex_token.Ex_token.contents_type := contents_type;
        Ticket_token.ex_token.Ex_token.contents := contents
        |} := function_parameter in
  let loc_value := Micheline.dummy_location in
  let? '(cont_ty_unstripped, ctxt) :=
    Script_ir_translator.unparse_comparable_ty loc_value ctxt contents_type in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.strip_annotations_cost cont_ty_unstripped) in
  let ty := Alpha_context.Script.strip_annotations cont_ty_unstripped in
  let? '(contents, ctxt) :=
    Script_ir_translator.unparse_comparable_data loc_value ctxt
      Script_ir_translator.Optimized_legacy contents_type contents in
  let ticketer_address :=
    {|
      Script_typed_ir.address.destination :=
        Alpha_context.Destination.Contract ticketer;
      Script_typed_ir.address.entrypoint := Alpha_context.Entrypoint.default; |}
    in
  let owner_address :=
    {| Script_typed_ir.address.destination := owner;
      Script_typed_ir.address.entrypoint := Alpha_context.Entrypoint.default; |}
    in
  let? '(ticketer, ctxt) :=
    Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized_legacy
      Script_typed_ir.address_t ticketer_address in
  let? '(owner, ctxt) :=
    Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized_legacy
      Script_typed_ir.address_t owner_address in
  Alpha_context.Ticket_hash.make ctxt ticketer ty contents owner.

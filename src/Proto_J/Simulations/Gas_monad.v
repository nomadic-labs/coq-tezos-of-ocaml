Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require Import TezosOfOCaml.Proto_J.Gas_monad.
Require TezosOfOCaml.Proto_J.Simulations.Script_tc_errors.

Definition dep_record_trace_eval {a : Set} {error_trace}
  (error_details : Script_tc_errors.dep_error_details error_trace)
  (f_value : unit -> Error_monad._error)
  (m_value : t a (Script_tc_errors.Error_trace_family.to_Set error_trace))
  : t a (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
  match error_details, m_value with
  | Script_tc_errors.Dep_fast, _ => m_value
  | Script_tc_errors.Dep_informative, m_value =>
    (fun (gas : Local_gas_counter.local_gas_counter) =>
      op_gtgtquestionquestion (m_value gas)
        (fun (function_parameter : M? a * Local_gas_counter.local_gas_counter)
          =>
          let '(x_value, gas) := function_parameter in
          of_result (Error_monad.record_trace_eval f_value x_value) gas))
  end.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_tc_context.

Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

(** Dependent version of [callsite]. *)
Inductive dep_callsite : Set :=
| Dep_toplevel {storage param} :
  With_family.ty storage -> With_family.ty param ->
  Script_typed_ir.entrypoints -> dep_callsite
| Dep_view : dep_callsite
| Dep_data : dep_callsite.

(** Conversion back to [callsite]. *)
Definition to_callsite (x : dep_callsite) : Script_tc_context.callsite :=
  match x with
  | Dep_toplevel storage_type param_type entrypoints =>
    Script_tc_context.Toplevel {|
      Script_tc_context.callsite.Toplevel.storage_type :=
        With_family.to_ty storage_type;
      Script_tc_context.callsite.Toplevel.param_type :=
        With_family.to_ty param_type;
      Script_tc_context.callsite.Toplevel.entrypoints :=
        entrypoints;
    |}
  | Dep_view => Script_tc_context.View
  | Dep_data => Script_tc_context.Data
  end.

(** Dependent version of [t]. *)
Module dep_t.
  Record record : Set := Build {
    callsite : dep_callsite;
    in_lambda : Script_tc_context.in_lambda }.

    Definition with_in_lambda in_lambda (r : record) :=
      Build r.(callsite) in_lambda.
End dep_t.
Definition dep_t := dep_t.record.

(** Conversion back to [t]. *)
Definition to_t (x : dep_t) : Script_tc_context.t := {|
  Script_tc_context.t.callsite := to_callsite x.(dep_t.callsite);
  Script_tc_context.t.in_lambda := x.(dep_t.in_lambda);
|}.

(** Simulation of [init_value]. *)
Definition dep_init_value (callsite : dep_callsite) : dep_t :=
  {| dep_t.callsite := callsite; dep_t.in_lambda := false |}.

(** Simulation of [toplevel_value]. *)
Definition dep_toplevel_value {storage param}
  (storage_type : With_family.ty storage) (param_type : With_family.ty param)
  (entrypoints : Script_typed_ir.entrypoints) : dep_t :=
  dep_init_value (Dep_toplevel storage_type param_type entrypoints).

(** Simulation of [view]. *)
Definition dep_view : dep_t := dep_init_value Dep_view.

(** Simulation of [data]. *)
Definition dep_data : dep_t := dep_init_value Dep_data.

Definition dep_add_lambda (tc_context_value : dep_t) : dep_t :=
  dep_t.with_in_lambda true tc_context_value.

Definition dep_is_in_lambda (function_parameter : dep_t) : Script_tc_context.in_lambda :=
  let '{| dep_t.callsite := _; dep_t.in_lambda := in_lambda |}
    := function_parameter in
  in_lambda.

Definition dep_check_not_in_view
  (loc_value : Alpha_context.Script.location) (legacy : bool)
  (tc_context_value : dep_t) (prim : Alpha_context.Script.prim) : M? unit :=
  match
    (tc_context_value.(dep_t.callsite),
      match tc_context_value.(dep_t.callsite) with
      | Dep_view => (dep_is_in_lambda tc_context_value) || legacy
      | _ => false
      end) with
  | ((Dep_toplevel _ _ _ | Dep_data), _) => Result.return_unit
  | (Dep_view, true) => Result.return_unit
  | (Dep_view, _) =>
    Error_monad.error_value
      (Build_extensible "Forbidden_instr_in_context"
        (Alpha_context.Script.location * Script_tc_errors.context_desc *
          Alpha_context.Script.prim) (loc_value, Script_tc_errors.View, prim))
  end.

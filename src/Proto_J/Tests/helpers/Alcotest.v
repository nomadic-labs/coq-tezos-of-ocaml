Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

(** Axiomatization done from the library Alcotest found on
    https://github.com/mirage/alcotest *)

Definition _return : Set := unit.

Definition speed_level : Set := Variant.t.

Definition test_case (a : Set) : Set := string * speed_level * (a -> _return).

Definition test (a : Set) : Set := string * list (test_case a).

Definition with_options (a : Set) : Set :=
  (* and_exit *) option bool ->
  (* verbose *) option bool ->
  (* compact *) option bool ->
  (* tail_errors *) option Variant.t ->
  (* quick_only *) option bool ->
  (* show_errors *) option bool ->
  (* json *) option bool ->
  (* filter *) option (string -> int -> Variant.t) ->
  (* log_dir *) option string ->
  (* bail *) option bool ->
  (* record_backtrace *) option bool ->
  (* argv *) option (array string) ->
  a.

(** Main entrypoint for [Alcotest]. *)
Parameter run : with_options (string -> list (test unit) -> _return).

Module Source_code_position.
  Parameter here : Set.

  Definition pos : Set := string * int * int * int.
End Source_code_position.

Definition extra_info (a : Set) : Set :=
  (* here *) option Source_code_position.here ->
  (* pos *) option Source_code_position.pos ->
  a.

Parameter failf : forall {a b : Set},
  extra_info (Pervasives.format4 a Format.formatter unit b -> a).

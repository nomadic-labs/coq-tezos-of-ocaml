Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tests.helpers.Alcotest.
Require TezosOfOCaml.Proto_J.Tests.helpers.QCheck.
Require TezosOfOCaml.Proto_J.Tests.helpers.QCheck2.

Module Qcheck_helpers.
  (** Transforms a [QCheck] test to [Alcotest]. *)
  Parameter qcheck_wrap :
    (* verbose *) option bool ->
    (* long *) option bool ->
    (* rand *) option Random.State.t ->
    list QCheck.Test.t ->
    list (Alcotest.test_case unit).

  (** Check for the equality. *)
  Definition qcheck_eq {a : Set}
    (pp : option (Format.formatter -> a -> unit))
    (cmp : option (a -> a -> int))
    (eq : option (a -> a -> bool))
    (expected actual : a) :
    bool :=
    Compare.polymorphic_eq expected actual.

  (** Check for the equality. *)
  Definition qcheck_eq' {a : Set}
    (pp : option (Format.formatter -> a -> unit))
    (cmp : option (a -> a -> int))
    (eq : option (a -> a -> bool))
    (expected actual : a)
    (_ : unit) :
    bool :=
    qcheck_eq pp cmp eq expected actual.

  (** Generator from an optional generator, for the case of values not equal
      to [None]. *)
  Definition of_option_arb {a : Set} (gen : QCheck.arbitrary (option a)) :
    QCheck.arbitrary a :=
    fun v =>
    gen (Some v).
End Qcheck_helpers.

Module Qcheck2_helpers.
  Parameter qcheck_wrap :
    (* verbose *) option bool ->
    (* long *) option bool ->
    (* rand *) option Random.State.t ->
    list QCheck2.Test.t ->
    list (Alcotest.test_case unit).
End Qcheck2_helpers.

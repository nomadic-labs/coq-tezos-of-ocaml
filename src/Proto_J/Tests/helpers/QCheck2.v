Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.List.
Require TezosOfOCaml.Environment.V5.Proofs.Pervasives.

Module Gen.
  Definition t (a : Set) : Set := a -> bool.

  Parameter unit_value : t unit.

  Parameter bool_value : t bool.

  Parameter int_value : t int.

  Axiom int_value_spec :
    forall (v : int),
    int_value v = true ->
    Pervasives.Int.Valid.t v.
  
  Parameter int_range : int -> int -> t int.

  Axiom int_range_spec :
    forall (min max : int) (v : int),
    int_range min max v = true ->
    min <= v <= max.

  Parameter nat : t int.

  Axiom nat_spec :
    forall (v : int),
    nat v = true -> 
    Pervasives.Int.Valid.non_negative v.

  Definition small_nat : t int := nat.

  Definition big_nat : t int := nat.

  Parameter int_bound : int -> t int.

  Axiom int_bound_spec :
    forall (max v : int),
    int_bound max v = true->
    0 <= v <= max.

  Parameter list_value : forall {a : Set}, t a -> t (list a).

  Axiom list_value_spec :
    forall {a : Set} (gen : t a) (v : list a),
    list_value gen v = true ->
    List.Forall (fun x => gen x = true) v /\
    List.Int_length.t v.

  Definition small_list : forall {a : Set}, t a -> t (list a) := @list_value.

  Parameter op_letstar : forall {a b : Set}, t a -> (a -> t b) -> t b.

  Axiom op_letstar_spec :
    forall {a b : Set} (gen : t a) (f : a -> t b) (v : b),
    op_letstar gen f v = true ->
    exists x, gen x = true /\ f x v = true.

  Parameter map : forall {a b : Set}, (a -> b) -> t a -> t b.

  Axiom map_spec :
    forall {a b : Set} (gen : t a) (f : a -> b) (v : b),
    map f gen v = true ->
    exists x, gen x = true /\ f x = v.

  (** Randomly selects a generator from several generators. As Coq does not have 
    random values, we instead return any of the values. *)
  Parameter oneof : forall {a : Set}, (list (t a)) -> t a.

  Axiom oneof_spec :
    forall {a : Set} (gens : list (t a)) (v : a),
    oneof gens v = true ->
    List.Exists (fun gen => gen v = true) gens.

  Parameter _return : forall {a : Set}, a -> t a.

  Axiom _return_spec :
    forall {a : Set} (x v : a),
    _return x v = true ->
    v = x.

  Parameter pair_value : forall {a b : Set}, t a -> t b -> t (a * b).

  Axiom pair_value_spec :
    forall {a b : Set} (gen_a : t a) (gen_b : t b) (v : (a * b)),
    pair_value gen_a gen_b v = true ->
    let '(v_a, v_b) := v in
    gen_a v_a = true /\ gen_b v_b = true.

  Parameter triple_value : forall {a b c : Set}, 
    t a -> t b -> t c -> t (a * b * c).

  Axiom triple_value_spec :
    forall {a b c : Set} (gen_a : t a) (gen_b : t b) (gen_c : t c) 
    (v : (a * b * c)),
    triple_value gen_a gen_b gen_c v = true ->
    let '(v_a, v_b, v_c) := v in
    gen_a v_a = true /\ gen_b v_b = true /\ gen_c v_c = true.
End Gen.

Module Print.
  Definition t (a : Set) : Set :=
    a -> string.
End Print.

Parameter stat : Set -> Set.

Module Test.
  Definition t : Type := Prop.

  Definition make {a : Set}
    (if_assumptions_fail : option (Variant.t * float))
    (count : option int)
    (long_factor : option int)
    (max_gen : option int)
    (max_fail : option int)
    (name : option string)
    (print : option (Print.t a))
    (collect : option (a -> string))
    (stats : option (list (stat a)))
    (gen : Gen.t a)
    (test_function : a -> bool) : t :=
    forall (v : a),
      gen v = true ->
      test_function v = true.
End Test.

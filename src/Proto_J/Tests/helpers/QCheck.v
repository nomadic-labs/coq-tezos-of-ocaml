Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.Pervasives.

(** We represent a generator by a predicate over a type. This predicate should
    be true for all the values we can generate. In contrast, in OCaml,
    a generator is a random function generating values verifying a certain
    predicate. *)
Definition arbitrary (a : Set) : Type := a -> Prop.

(** Function for mapping OCaml integer range to Coq *)
Definition range (min max : int) : arbitrary int :=
  fun z =>
  min <= z <= max.

(** The constant generator of the single value [value]. *)
Definition always {a : Set} (_ : option string) (value : a) : arbitrary a :=
  fun v =>
  v = value.

(** Create a generator for a pair from two generators. *)
Definition pair_value {a b : Set} (gen1 : arbitrary a) (gen2 : arbitrary b) :
  arbitrary (a * b) :=
  fun '(x, y) =>
  gen1 x /\ gen2 y.

(** Create a generator for a triple (3-tuples) from three generators. *)
Definition triple_value {a b c : Set} (gen1 : arbitrary a) (gen2 : arbitrary b) (gen3 : arbitrary c) :
  arbitrary (a * b * c) :=
  fun '(x, y, z) =>
  gen1 x /\ gen2 y /\ gen3 z.

(** The generator for the images of a generator once we apply a certain
    function [f] on the generated values. *)
Definition map {a b : Set} (_ : option string) (f : a -> b)
  (gen : arbitrary a) : arbitrary b :=
  fun v =>
  exists v', gen v' /\ f v' = v.

(** Randomly selects a generator from several generators. As Coq does not have 
    random values, we instead return any of the values. *)
Fixpoint oneof {a : Set} 
  (gens : list (arbitrary a)) : arbitrary a :=
  fun v =>
  match gens with
  | [] => False
  | [gen] => gen v
  | gen :: gens => gen v \/ oneof gens v
  end.

(** A combinator of several generators. Since we do not randomly generate the
    values in Coq, we ignore the [frequency] argument of each generator. *)
Fixpoint frequency {a : Set} (_ _ _ _ : option string)
  (gens : list (int * arbitrary a)) : arbitrary a :=
  fun v =>
  match gens with
  | [] => False
  | [(_, gen)] => gen v
  | (_, gen) :: gens => gen v \/ frequency None None None None gens v
  end.

(** A generator for all the OCaml [int]. *)
Definition int_value : arbitrary int :=
  fun v =>
  Pervasives.Int.Valid.t v.

Definition make {a : Set}
  (_ _ _ _ _ : option string)
  (generator : arbitrary a) :
  arbitrary a :=
  generator.

Module Gen.
  (** An operator for [map]. *)
  Definition op_ltdollargt {a b : Set} (f : a -> b) (gen : arbitrary a)
    : arbitrary b :=
    map None f gen.

  (** Like [map] but without the optional parameter. *)
  Definition map {a b : Set} (f : a -> b) (gen : arbitrary a) : arbitrary b :=
    map None f gen.

  (** A generator for the OCaml [int] type. *)
  Definition nat : arbitrary int :=
    int_value.
End Gen.

Module Test.
  (** We represent a test as a proposition. Next we will be able to prove these
      propositions in Coq, hence ensuring that the tests are valid for all
      possible inputs. *)
  Definition t : Type := Prop.

  (** To create a test, we take as inputs a [generator] and a [test_function].
      Then we verify that for any values that can be generated then
      the [test_function] is equals to [true]. *)
  Definition make {a : Set}
    (_ _ _ _ _ _ _ : option string)
    (generator : QCheck.arbitrary a)
    (test_function : a -> bool) : t :=
    forall x,
      generator x ->
      test_function x = true.
End Test.

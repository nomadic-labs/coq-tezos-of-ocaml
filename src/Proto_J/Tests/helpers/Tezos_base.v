Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Misc.

Module TzPervasives.
  Module Error_monad.
    Module Legacy_monad_globals := Environment.V5.Error_monad.
  End Error_monad.

  Module List := Environment.V5.List.

  Module Tzresult_syntax := Error_monad.Result_syntax.

  Definition op_minusminus := Misc.op_minusminusgt.
End TzPervasives.

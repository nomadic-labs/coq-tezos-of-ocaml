Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Saturation_repr.
Require TezosOfOCaml.Proto_J.Proofs.Saturation_repr.

Lemma test_add_commutes (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.add t1 t2 = Saturation_repr.add t2 t1.
Proof.
  intros.
  do 2 (rewrite Saturation_repr.add_eq; trivial).
  replace ((t1  + t2)%Z) with ((t2 + t1)%Z).
  { reflexivity. }
  { lia. }
Qed.

Lemma test_mul_commutes (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.mul t1 t2 = Saturation_repr.mul t2 t1.
Proof.
  intros.
  do 2 (rewrite Saturation_repr.mul_eq; trivial).
  replace ((t1 * t2)%Z) with ((t2 * t1)%Z).
  { reflexivity. }
  { lia. }
Qed.

Lemma test_add_zero (t : int) :
  Saturation_repr.Valid.t t ->
  Saturation_repr.add t 0 = t. 
Proof.
  intros.
  rewrite Saturation_repr.add_eq.
  replace ((t+0)%Z) with t. 
  { 
    apply Saturation_repr.saturate_eq.
    trivial. 
  }
  { lia. }
  { trivial. }
  {
    unfold Saturation_repr.Valid.t.
    lia.
  }
Qed.

Lemma test_add_neq (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.add t1 t2 >= t1.
Proof.
  intros.
  rewrite Saturation_repr.add_eq.
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  { trivial. }
  { trivial. }
Qed.

Lemma test_mul_one (t : int) : 
  Saturation_repr.Valid.t t ->
  Saturation_repr.mul t 1 = t. 
Proof. 
  intros.
  rewrite Saturation_repr.mul_eq.
  {
    replace ((t * 1)%Z) with t.
    { 
      apply Saturation_repr.saturate_eq.
      lia.
    }
    { lia. }
  }
  { trivial. }
  { lia. }
Qed.

Lemma test_mul_zero (t : int) :
  Saturation_repr.Valid.t t ->
  Saturation_repr.mul t 0 = 0.
Proof.
  intros.
  rewrite Saturation_repr.mul_eq.
  { 
    replace ((t * 0)%Z) with 0.
    {
      apply Saturation_repr.saturate_eq.
      lia.
    }
    lia.
  }
  { lia. }
  { lia. }
Qed.

Lemma test_sub_zero (t : int) :
  Saturation_repr.Valid.t t ->
  Saturation_repr.sub t 0 = t.
Proof.
  intros.
  rewrite Saturation_repr.sub_eq.
  {
    replace ((t-0)%Z) with t. 
    { 
      apply Saturation_repr.saturate_eq. 
      trivial.
    }
    lia.
  }
  { trivial. }
  {
    unfold Saturation_repr.Valid.t.
    lia.
  }
Qed.

Lemma test_sub_itself (t: int) :
  Saturation_repr.Valid.t t ->
  Saturation_repr.sub t t = 0.
Proof.
  intros.
  rewrite Saturation_repr.sub_eq.
  {
    replace ((t-t)%Z) with 0.
    { 
      apply Saturation_repr.saturate_eq.
      lia.
    }
    { lia. }
  }
  { trivial. }
  { trivial. }
Qed.

Lemma test_sub_neq (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.sub t1 t2 <= t1.
Proof.
  intros.
  rewrite Saturation_repr.sub_eq.
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  { trivial. }
  { trivial. }
Qed.

Lemma test_add_sub (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.sub (Saturation_repr.add t1 t2) t2 <= t1. 
Proof.
  intros.
  rewrite Saturation_repr.add_eq.
  rewrite Saturation_repr.sub_eq.
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  { trivial. }
  { trivial. }
  { trivial. }
Qed.

Lemma test_sub_add (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.add (Saturation_repr.sub t1 t2) t2 >= t1.
Proof.
  intros.
  rewrite Saturation_repr.sub_eq.
  rewrite Saturation_repr.add_eq.
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  {
    unfold Saturation_repr.saturate.
    lia.
  }
  { trivial. }
  { trivial. }
  { trivial. }
Qed.

Lemma test_leq_saturated (t : int) :
  Saturation_repr.Valid.t t ->
  t <= Saturation_repr.saturated.
Proof.
  intros.
  unfold Saturation_repr.saturated.
  lia.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V5.Proofs.Int64.
Require TezosOfOCaml.Environment.V5.Proofs.Option.
Require TezosOfOCaml.Environment.V5.Proofs.Time.
Require TezosOfOCaml.Environment.V5.Proofs.Z.

Require TezosOfOCaml.Proto_J.Script_timestamp_repr.

#[global] Hint Unfold
  Script_timestamp_repr.of_int64
  Script_timestamp_repr.diff_value
  Script_timestamp_repr.sub_delta
  Script_timestamp_repr.add_delta
  Script_timestamp_repr.to_zint
  Script_timestamp_repr.of_zint
  : tezos_z.

#[local] Hint Unfold
  Script_timestamp_repr.of_string
  Script_timestamp_repr.to_string
  Script_timestamp_repr.of_int64 
  Script_timestamp_repr.to_num_str
  Script_timestamp_repr.to_notation 
  Script_timestamp_repr.to_zint
  Time_repr.of_notation
  Time_repr.to_seconds
  Time_repr.to_notation
  of_seconds
  of_int64
  : script_timestamp_repr.

Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Script_timestamp_repr.compare.
Proof.
  apply (Compare.equality (
    let proj '(Script_timestamp_repr.Timestamp_tag x) := x in
    Compare.projection proj Z.compare
  )); [sauto q: on|].
  eapply Compare.implies.
  { eapply Compare.projection_is_valid.
    apply Compare.z_is_valid.
  }
  all: sauto q: on.
Qed.

(** of_string (to_string t) preserves t *)
Lemma of_string_to_string_is_inverse : forall t,
  Int64.Valid.t (Script_timestamp_repr.to_zint t) ->
  Script_timestamp_repr.of_string
    (Script_timestamp_repr.to_string t) = Some t.
Admitted.

(** to_string and of_string are inverse *)
Lemma to_string_of_string_is_inverse : forall t s,
  Int64.Valid.t (Script_timestamp_repr.to_zint t)->
  Script_timestamp_repr.of_string s = Some t ->
  Script_timestamp_repr.to_string t = s.
  autounfold with script_timestamp_repr.
Admitted.

Lemma encoding_is_valid : 
  Data_encoding.Valid.t (fun _ => True) Script_timestamp_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

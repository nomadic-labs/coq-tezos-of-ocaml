Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tx_rollup_message_result_repr.

Require TezosOfOCaml.Environment.V5.Proofs.Context_hash.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_J.Proofs.Tx_rollup_withdraw_list_hash_repr.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Tx_rollup_message_result_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
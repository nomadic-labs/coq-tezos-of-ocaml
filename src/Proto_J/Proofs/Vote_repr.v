Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Vote_repr.

Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Lemma ballot_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Vote_repr.ballot_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros []; hfcrush.
Qed.
#[global] Hint Resolve ballot_encoding_is_valid : Data_encoding_db.

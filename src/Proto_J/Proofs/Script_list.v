Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_list.
Require TezosOfOCaml.Environment.V5.Proofs.Utils.

Module Valid.
  Definition t {a : Set} (l : Script_typed_ir.boxed_list a) : Prop :=
    List.length l.(Script_typed_ir.boxed_list.elements) =
    l.(Script_typed_ir.boxed_list.length).
End Valid.

Lemma empty_is_valid {a : Set} : Valid.t (Script_list.empty (a := a)).
  reflexivity.
Qed.

Lemma cons_is_valid {a : Set} {v : a} {l} 
  : Valid.t l -> Valid.t (Script_list.cons_value v l).
  unfold Valid.t; intro H; cbn - ["+i"].
  lia.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tx_rollup_l2_proof.

Require TezosOfOCaml.Environment.V5.Proofs.Context.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Tx_rollup_l2_proof.encoding.
Proof.
  apply Context.Proof_encoding.V2.Tree2_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

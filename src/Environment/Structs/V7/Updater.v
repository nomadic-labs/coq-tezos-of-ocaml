Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Environment.Structs.V6.Updater.
Require TezosOfOCaml.Environment.Structs.V7.RPC_directory.

Module validation_result := Updater.validation_result.
Definition validation_result := Updater.validation_result.

Module quota := Updater.quota.
Definition quota := Updater.quota.

Module rpc_context := Updater.rpc_context.
Definition rpc_context := Updater.rpc_context.

Module PROTOCOL.
  Record signature
    {block_header_data block_header block_header_metadata operation_data
      operation_receipt operation mode validation_state application_state
      Mempool_t Mempool_validation_info Mempool_keep_or_replace
      Mempool_operation_conflict Mempool_add_result Mempool_add_error
      Mempool_merge_error : Set} : Set := {
    (** The maximum size of a block header in bytes. *)
    max_block_length : int;
    (** The maximum size of an {!operation} in bytes. This value is bigger than the size
    of the bytes required for {!operation_data}, because this value accounts
    for the shell header. *)
    max_operation_data_length : int;
    (** Operations quota for each validation pass. The length of the
    list denotes the number of validation passes. *)
    validation_passes : list quota;
    (** The economic protocol-specific type of blocks. *)
    block_header_data := block_header_data;
    (** Encoding for economic protocol-specific part of block headers. *)
    block_header_data_encoding : Data_encoding.t block_header_data;
    (** A fully parsed block header. *)
    block_header := block_header;
    (** Economic protocol-specific side information computed by the
    protocol during the validation of a block. Should not include
    information about the evaluation of operations which is handled
    separately by {!operation_metadata}. To be used as an execution
    trace by tools (client, indexer). Not necessary for
    validation. *)
    block_header_metadata := block_header_metadata;
    (** Encoding for economic protocol-specific block metadata. *)
    block_header_metadata_encoding : Data_encoding.t block_header_metadata;
    (** The economic protocol-specific type of operations. *)
    operation_data := operation_data;
    (** Economic protocol-specific side information computed by the
    protocol during the validation of each operation, to be used
    conjointly with {!block_header_metadata}. *)
    operation_receipt := operation_receipt;
    (** A fully parsed operation. *)
    operation := operation;
    (** Encoding for economoic protocol-specific operation data. *)
    operation_data_encoding : Data_encoding.t operation_data;
    (** Encoding for eonomic protocol-specific operation receipts. *)
    operation_receipt_encoding : Data_encoding.t operation_receipt;
    (** Encoding that mixes an operation data and its receipt. *)
    operation_data_and_receipt_encoding :
      Data_encoding.t (operation_data * operation_receipt);
    (** [acceptable_pass op] gives the validation pass in which the
    input operation [op] can appear. For instance, it results in
    [Some 0] if [op] only belongs to the first pass. When [op] is
    ill-formed, [acceptable_pass op] returns [None]. *)
    acceptable_pass : operation -> option int;
    (** [compare_operations (oph1,op1) (oph2,op2)] defines a total
    ordering relation on valid operations.

    The following requirements must be satisfied: [oph1] is the
    [Operation.hash.p1], [oph2] is [Operation.hash op2] and that
    [op1] and [op2] are valid in the same context.

    [compare_operations (oph1,op1) (oph2,op2) = 0] happens only if
    [Operation_hash.compare oph1 oph2 = 0], meaning [op1 = op2] only
    when [op1] and [op2] are structurally identical.

    Two operations of different validation_passes are compared in the
    reverse order of their [validation_pass]: the one with the
    smaller [validation_pass] is compared as being the greater.

    When belonging to the same validation_pass, two operations
    comparison depends on their static parameters. An abstract weight
    is computed for each operation based on its static parameters.
    When two operations' weights are compared as equal,
    [compare_operation (oph1,op1) (oph2,op2)] is
    [Operation_hash.compare oph1 oph2].

    [compare_operations] can be used as a [compare] component of an
    {!Stdlib.Map.OrderedType}, or any such collection which relies on
    a total comparison function. *)
    compare_operations :
      Operation_hash.t * operation -> Operation_hash.t * operation -> int;
    (** The mode indicates the circumstances in which a block and/or
    operations are validated or applied, and contains specific
    information. It must be provided as an argument to
    [begin_validation] and [begin_application]. *)
    mode := mode;
    (** A functional state that is transmitted throughout the validation
    of a block (or during the lifetime of a mempool or RPC). It is
    created by [begin_validation] below, updated by
    [validate_operation], and required by [finalize_validation].
    This state is immutable thus validator or baker implementations
    are allowed to pause, replay or backtrack throughout validation
    steps. *)
    validation_state := validation_state;
    (** Similar to {!validation_state}, but for the application process. *)
    application_state := application_state;
    (** [begin_validation predecessor_context chain_id mode
    ~predecessor] initializes the {!validation_state} for the
    validation process of an existing or new block.

    [predecessor_context] and [predecessor] are the resulting
    context and shell header of the predecessor block. Exceptionally
    in {!Partial_validation} mode, they may instead come from any
    ancestor block that is more recent (i.e. has a greater level)
    than the current head's "last_allowed_fork_level".

    [mode] specifies the circumstances of validation and also
    carries additional information: see {!mode}.

    Note that for protocol versions <= K where [begin_validation]
    does not exist yet, this calls the old [begin_application] by
    necessity. However, in [Application] mode, this calls the old
    [begin_application] in [Partial_validation] mode in order to run
    more quickly. This preserves the behavior of [precheck] in
    [lib_validation/block_validation.ml] for old protocols. It does
    mean that the application of a validated block may fail in these
    protocols. *)
    begin_validation :
      Context.t -> Chain_id.t -> mode -> Block_header.shell_header ->
      Error_monad.tzresult validation_state;
    (** Validate an operation. If successful, return the updated
    {!validation_state}.

    [check_signature] indicates whether the signature should be
    checked. It defaults to [true] because the signature needs to be
    correct for the operation to be valid. This argument exists for
    special cases where it is acceptable to bypass this check,
    e.g. if we know that the operation has already been successfully
    validated in another context. *)
    validate_operation :
      option bool -> validation_state -> Operation_hash.t -> operation ->
      Error_monad.tzresult validation_state;
    (** Run final and global checks on the block that must come after
    the validation of all its operations to establish its
    validity. *)
    finalize_validation : validation_state -> Error_monad.tzresult unit;
    (** Initialize the {!application_state} for the application process
    of an existing or new block. See {!begin_validation} for details
    on the arguments.

    In protocol versions > K, calling this function with the
    {!Partial_validation} mode returns an error. *)
    begin_application :
      Context.t -> Chain_id.t -> mode -> Block_header.shell_header ->
      Error_monad.tzresult application_state;
    (** Apply an operation. If successful, return the updated
    {!application_state} and the corresponding {!operation_receipt}.

    This should be called for all operations in a block, after
    {!begin_application} and before
    {!finalize_application}. Moreover, the operation should have
    already been validated by {!validate_operation}. *)
    apply_operation :
      application_state -> Operation_hash.t -> operation ->
      Error_monad.tzresult (application_state * operation_receipt);
    (** Finalize the context resulting from the application of the
    contents of the block.

    If there is no protocol migration, i.e. if the block being
    applied is not the last block of the current economic protocol,
    then the resulting context can be used in the future as input for
    the validation and application of its successor blocks.

    In {!Construction} mode, the [Block_header.shell_header option]
    argument must contain a value, which will be used to compute the
    [cache_nonce]. In other modes, it can as well be [None] since it
    will not be used. *)
    finalize_application :
      application_state -> option Block_header.shell_header ->
      Error_monad.tzresult (validation_result * block_header_metadata);
    (** [rpc_services] provides the list of remote procedures exported
    by this protocol implementation. *)
    rpc_services : RPC_directory.t rpc_context;
    (** [init chain_id ctxt hd] initializes the context, or upgrades the
    context after a protocol amendment. This function receives as
    arguments the [chain_id] of the current chain and the context
    [ctxt] resulting from the application of the block that triggered
    the amendment, as well as its header [hd]. This function should
    fail if the "protocol stitching", i.e., the transition from a
    valid previous protocol to the one being activated, has not been
    implemented. *)
    init_value :
      Chain_id.t -> Context.t -> Block_header.shell_header ->
      Error_monad.tzresult validation_result;
    (** [value_of_key chain_id predecessor_context
    predecessor_timestamp predecessor_level predecessor_fitness
    predecessor timestamp] returns a function to build one value of
    the cache from its key.

    This function is used to restore all or part of the cache, for
    instance when booting a validator to preheat the cache, or when a
    reorganization happens. This function should never fail, returned
    errors are fatal.

    The generated function is passed to [Context.Cache.load_caches]
    which will use it either immediately a cache-loading time or
    on-demand, when a given cached value is accessed. *)
    value_of_key :
      Chain_id.t -> Context.t -> Time.t -> Int32.t -> Fitness.t ->
      Block_hash.t -> Time.t ->
      Error_monad.tzresult
        (Context.cache_key -> Error_monad.tzresult Context.cache_value);
    (** Mempool type. This immutable functional state keeps track of
      operations added to the mempool, and allows to detect conflicts
      between them and a new candidate operation. *)
    Mempool_t := Mempool_t;
    (** Validation info type required to validate and add operations to a
      mempool. *)
    Mempool_validation_info := Mempool_validation_info;
    Mempool_keep_or_replace := Mempool_keep_or_replace;
    (** Type of the function that may be provided in order to resolve a
      potential conflict when adding an operation to an existing mempool
      or when merging two mempools. This handler may be defined as a
      simple order relation over operations (e.g. prioritize the most
      profitable operations) or an arbitrary one (e.g. prioritize
      operations where the source is a specific manager).

      Returning [`Keep] will leave the mempool unchanged and retain the
      [existing_operation] while returning [`Replace] will remove
      [existing_operation] and add [new_operation] instead. *)
    Mempool_conflict_handler :=
      Operation_hash.t * operation -> Operation_hash.t * operation ->
      Mempool_keep_or_replace;
    Mempool_operation_conflict := Mempool_operation_conflict;
    (** Return type when adding an operation to the mempool *)
    Mempool_add_result := Mempool_add_result;
    (** Error type returned when adding an operation to the mempool fails. *)
    Mempool_add_error := Mempool_add_error;
    (** Error type returned when the merge of two mempools fails. *)
    Mempool_merge_error := Mempool_merge_error;
    (** Initialize a static [validation_info] and [mempool], required
      to validate and add operations, and an incremental and
      serializable {!mempool}. *)
    Mempool_init :
      Context.t -> Chain_id.t -> Block_hash.t -> Block_header.shell_header ->
      Error_monad.tzresult (Mempool_validation_info * Mempool_t);
    (** Mempool encoding *)
    Mempool_encoding : Data_encoding.t Mempool_t;
    (** Adds an operation to a [mempool] if and only if it is valid and
      does not conflict with previously added operation.

      This function checks the validity of an operation and tries to
      add it to the mempool.

      If a validation error is triggered, the result will be a
      [Validation_error].  If a conflict with a previous operation
      exists, the result will be [Add_conflict] is then checked.
      Important: no [Add_conflict] will be raised if a
      [conflict_handler] is provided (see [add_result]).

      If no error is raised the operation is potentially added to the
      [mempool] depending on the [add_result] value. *)
    Mempool_add_operation :
      option bool -> option Mempool_conflict_handler ->
      Mempool_validation_info -> Mempool_t -> Operation_hash.t * operation ->
      Pervasives.result (Mempool_t * Mempool_add_result) Mempool_add_error;
    (** [remove_operation mempool oph] removes the operation [oph] from
      the [mempool]. The [mempool] remains unchanged when [oph] is not
      present in the [mempool] *)
    Mempool_remove_operation : Mempool_t -> Operation_hash.t -> Mempool_t;
    (** [merge ?conflict_handler mempool mempool'] merges [mempool']
      {b into} [mempool].

      Mempools may only be merged if they are compatible: i.e. both have
      been initialised with the same predecessor block. Otherwise, the
      [Incompatible_mempool] error is returned.

      Conflicts between operations from the two mempools can
      occur. Similarly as [add_operation], a [Merge_conflict] error
      may be raised when no [conflict_handler] is provided.

      [existing_operation] in [conflict_handler ~existing_operation ~new_operation]
      references operations present in [mempool] while
      [new_operation] will reference operations present in
      [mempool']. *)
    Mempool_merge :
      option Mempool_conflict_handler -> Mempool_t -> Mempool_t ->
      Pervasives.result Mempool_t Mempool_merge_error;
    (** [operations mempool] returns the map of operations present in
      [mempool]. *)
    Mempool_operations :
      Mempool_t -> Operation_hash.Map.(S.INDEXES_MAP.t) operation;
  }.
End PROTOCOL.
Definition PROTOCOL := @PROTOCOL.signature.
Arguments PROTOCOL {_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _}.

Parameter activate : Context.t -> Protocol_hash.t -> Context.t.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Environment.Structs.V7.Bls.
Require TezosOfOCaml.Environment.Structs.V0.Data_encoding.
Require TezosOfOCaml.Environment.Structs.V0.S.

Definition scalar : Set := Bls.Primitive.Fr.(S.PRIME_FIELD.t).

Parameter public_parameters : Set.

Parameter proof : Set.

Parameter public_parameters_encoding : Data_encoding.t public_parameters.

Parameter proof_encoding : Data_encoding.t proof.

Parameter scalar_encoding : Data_encoding.t scalar.

Parameter scalar_array_encoding : Data_encoding.t (array scalar).

Parameter verify : public_parameters -> array scalar -> proof -> bool.

Parameter verify_multi_circuits :
  public_parameters -> list (string * list (array scalar)) -> proof -> bool.

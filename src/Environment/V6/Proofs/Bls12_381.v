Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Bls12_381.

Axiom pairing_check_cons :
  forall x xs xs',
    pairing_check xs = pairing_check xs' ->
    pairing_check (x::xs) = pairing_check (x::xs').

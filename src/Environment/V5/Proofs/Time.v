Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Axiom of_notation_to_notation
  : forall time,
    let notation := Time.to_notation time in
    Time.of_notation notation =
    if String.equal notation "out_of_range" then
      None
    else
      Some time.

Axiom of_notation_to_notation_eq : forall t,
  of_notation (to_notation t) = Some t.

Axiom of_notation_to_string_eq : forall t,
  of_notation (to_string t) = Some t.

Axiom to_notation_of_string_eq : forall s,
  to_notation (of_string s) = s.

Axiom of_notation_some_implies : forall s t,
  of_notation s = Some t -> 
  to_notation t = s /\ to_string t = s.

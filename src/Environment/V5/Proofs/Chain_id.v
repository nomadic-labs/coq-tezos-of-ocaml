Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.Compare.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Axiom compare_is_valid : Compare.Valid.t (fun _ => True) id Chain_id.compare.

Axiom encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Chain_id.encoding.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Constants_parametric_repr.
Require TezosOfOCaml.Proto_K.Dal_endorsement_repr.
Require TezosOfOCaml.Proto_K.Dal_slot_repr.
Require TezosOfOCaml.Proto_K.Level_repr.
Require TezosOfOCaml.Proto_K.Raw_context.
Require TezosOfOCaml.Proto_K.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Storage.
Require TezosOfOCaml.Proto_K.Storage_sigs.

Definition find (ctxt : Raw_context.t) (level : Raw_level_repr.t)
  : M?
    (option
      Storage.Dal.Slot_headers.(Storage_sigs.Non_iterable_indexed_data_storage.value)) :=
  Storage.Dal.Slot_headers.(Storage_sigs.Non_iterable_indexed_data_storage.find)
    ctxt level.

Definition finalize_current_slots (ctxt : Raw_context.t) : Raw_context.t :=
  let current_level := Raw_context.current_level ctxt in
  let slots := Raw_context.Dal.candidates ctxt in
  match slots with
  | [] => ctxt
  | cons _ _ =>
    Storage.Dal.Slot_headers.(Storage_sigs.Non_iterable_indexed_data_storage.add)
      ctxt current_level.(Level_repr.t.level) slots
  end.

Definition compute_available_slots
  (ctxt : Raw_context.t) (slots : list Dal_slot_repr.t)
  : Dal_endorsement_repr.t :=
  let fold_available_slots
    (available_slots : Dal_endorsement_repr.t) (slot : Dal_slot_repr.t)
    : Dal_endorsement_repr.t :=
    if
      Raw_context.Dal.is_slot_available ctxt slot.(Dal_slot_repr.t.index)
    then
      Dal_endorsement_repr.commit available_slots slot.(Dal_slot_repr.t.index)
    else
      available_slots in
  List.fold_left fold_available_slots Dal_endorsement_repr.empty slots.

Definition finalize_pending_slots (ctxt : Raw_context.t)
  : M? (Raw_context.t * Dal_endorsement_repr.t) :=
  let current_level := Raw_context.current_level ctxt in
  let '{| Constants_parametric_repr.t.dal := dal |} :=
    Raw_context.constants ctxt in
  match
    Raw_level_repr.sub current_level.(Level_repr.t.level)
      dal.(Constants_parametric_repr.dal.endorsement_lag) with
  | None => return? (ctxt, Dal_endorsement_repr.empty)
  | Some level_endorsed =>
    let? function_parameter :=
      Storage.Dal.Slot_headers.(Storage_sigs.Non_iterable_indexed_data_storage.find)
        ctxt level_endorsed in
    match function_parameter with
    | None => return? (ctxt, Dal_endorsement_repr.empty)
    | Some slots =>
      let available_slots := compute_available_slots ctxt slots in
      let ctxt :=
        Storage.Dal.Slot_headers.(Storage_sigs.Non_iterable_indexed_data_storage.remove)
          ctxt level_endorsed in
      return? (ctxt, available_slots)
    end
  end.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_map.

Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K.Script_int.

(** Simulation of [empty_big_map]. *)
Definition dep_empty_big_map {a b : Ty.t}
  (key_type : With_family.ty a) (value_type : With_family.ty b) :
  With_family.big_map a b :=
  {|
    With_family.big_map.id := None;
    With_family.big_map.diff :=
      {|
        Script_typed_ir.big_map_overlay.map :=
          Script_typed_ir.Big_map_overlay.(Map.S.empty);
        Script_typed_ir.big_map_overlay.size := 0;
      |};
    With_family.big_map.key_type := key_type;
    With_family.big_map.value_type := value_type;
  |}.

(** Simulation of [big_map_mem]. *)
Definition dep_big_map_mem {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (big_map : With_family.big_map a b) :
  M? (bool * Alpha_context.context) :=
  let '{|
    With_family.big_map.id := id;
    With_family.big_map.diff := diff_value;
    With_family.big_map.key_type := key_type
  |} := big_map in
  let? '(key_value, ctxt) := dep_hash_comparable_data ctxt key_type key_value in
  match
    ((Script_typed_ir.Big_map_overlay.(Map.S.find) key_value
      diff_value.(Script_typed_ir.big_map_overlay.map)), id) with
  | (None, None) => return? (false, ctxt)
  | (None, Some id) =>
    let? '(ctxt, res) := Alpha_context.Big_map.mem ctxt id key_value in
    return? (res, ctxt)
  | (Some (_, None), _) => return? (false, ctxt)
  | (Some (_, Some _), _) => return? (true, ctxt)
  end.

(** Simulation of [big_map_get_by_hash]. *)
Definition dep_big_map_get_by_hash {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : Script_expr_hash.t)
  (big_map : With_family.big_map a b) :
  M? (option (With_family.ty_to_dep_Set b) * Alpha_context.context) :=
  let '{|
    With_family.big_map.id := id;
    With_family.big_map.diff := diff_value;
    With_family.big_map.value_type := value_type
  |} := big_map in
  match
    Script_typed_ir.Big_map_overlay.(Map.S.find) key_value
      diff_value.(Script_typed_ir.big_map_overlay.map),
    id
  with
  | Some (_, x_value), _ => return? (x_value, ctxt)
  | None, None => return? (None, ctxt)
  | None, Some id =>
    let? function_parameter :=
      Alpha_context.Big_map.get_opt ctxt id key_value in
    match function_parameter with
    | (ctxt, None) => return? (None, ctxt)
    | (ctxt, Some value_value) =>
      let? '(x_value, ctxt) :=
        dep_parse_data_aux None 0 ctxt true true value_type
          (Micheline.root_value value_value) in
      return? ((Some x_value), ctxt)
    end
  end.

(** Simulation of [big_map_get]. *)
Definition dep_big_map_get {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (function_parameter : With_family.big_map a b) :
  M? (option (With_family.ty_to_dep_Set b) * Alpha_context.context) :=
  let
  '{|
    With_family.big_map.key_type := key_type |} as map :=
  function_parameter in
  let? '(key_hash, ctxt) := dep_hash_comparable_data ctxt key_type key_value in
  dep_big_map_get_by_hash ctxt key_hash map.

(** Simulation of [big_map_update_by_hash]. *)
Definition dep_big_map_update_by_hash {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_hash : Script_expr_hash.t)
  (key_value : With_family.ty_to_dep_Set a)
  (value_value : option (With_family.ty_to_dep_Set b))
  (map : With_family.big_map a b) :
  M? (With_family.big_map a b * Alpha_context.context) :=
  let contains :=
    Script_typed_ir.Big_map_overlay.(Map.S.mem) key_hash
      map.(With_family.big_map.diff).(Script_typed_ir.big_map_overlay.map)
    in
  return? (
    With_family.big_map.with_diff {|
      Script_typed_ir.big_map_overlay.map :=
        Script_typed_ir.Big_map_overlay.(Map.S.add) key_hash
          (key_value, value_value)
          map.(With_family.big_map.diff).(Script_typed_ir.big_map_overlay.map);
      Script_typed_ir.big_map_overlay.size :=
        if contains then
          map.(With_family.big_map.diff).(Script_typed_ir.big_map_overlay.size)
        else
          map.(With_family.big_map.diff).(Script_typed_ir.big_map_overlay.size)
          +i 1
    |} map,
    ctxt
  ).

Definition dep_big_map_update {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (value_value : option (With_family.ty_to_dep_Set b))
  (function_parameter : With_family.big_map a b) :
  M? (With_family.big_map a b * Alpha_context.context) :=
  let '{| With_family.big_map.key_type := key_type |} as map :=
    function_parameter in
  let? '(key_hash, ctxt) := dep_hash_comparable_data ctxt key_type key_value in
  dep_big_map_update_by_hash ctxt key_hash key_value value_value map.

(** Simulation of [big_map_get_and_update]. *)
Definition dep_big_map_get_and_update {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (value_value : option (With_family.ty_to_dep_Set b))
  (function_parameter : With_family.big_map a b) :
  M? (
    (option (With_family.ty_to_dep_Set b) * With_family.big_map a b) *
    Alpha_context.context) :=
  let
    '{| With_family.big_map.key_type := key_type |} as map :=
    function_parameter in
  let? '(key_hash, ctxt) := dep_hash_comparable_data ctxt key_type key_value in
  let? '(map', ctxt) :=
    dep_big_map_update_by_hash ctxt key_hash key_value value_value map in
  let? '(old_value, ctxt) := dep_big_map_get_by_hash ctxt key_hash map in
  return? ((old_value, map'), ctxt).

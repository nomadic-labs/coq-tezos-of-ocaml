Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_interpreter_defs.

Require TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

Module Interp_costs := Proto_K.Michelson_v1_gas.Cost_of.Interpreter.

(** Simulation of [cost_of_instr]. *)
Definition dep_cost_of_instr {s f}
  (i : With_family.kinstr s f) (accu_stack : With_family.stack_ty_to_dep_Set s) :
  Alpha_context.Gas.cost :=
  match i, accu_stack with
  | With_family.IList_map _ _ _ _, (list_value, _) =>
    Interp_costs.list_map list_value
  | With_family.IList_iter _ _ _ _, (list_value, _) =>
    Interp_costs.list_iter list_value
  | With_family.ISet_iter _ _ _ _, (set_v, _) =>
    Interp_costs.set_iter (With_family.to_set set_v)
  | With_family.ISet_mem _ _, (v, (set_v, _)) =>
    Interp_costs.set_mem (With_family.to_value v) (With_family.to_set set_v)
  | With_family.ISet_update _ _, (v, (bool_v, (set_v, _))) =>
    Interp_costs.set_update (With_family.to_value v) (With_family.to_set set_v)
  | With_family.IMap_map _ _ _ _, (map, _) =>
    Interp_costs.map_map (With_family.to_map map)
  | With_family.IMap_iter _ _ _ _, (map, _) =>
    Interp_costs.map_iter (With_family.to_map map)
  | With_family.IMap_mem _ _, (k, (map_kv, _)) =>
    Interp_costs.map_mem (With_family.to_value k) (With_family.to_map map_kv)
  | With_family.IMap_get _ _, (k, (map_kv, _)) =>
    Interp_costs.map_get (With_family.to_value k) (With_family.to_map map_kv)
  | With_family.IMap_update _ _, (k, (opt_v, (map_kv, _))) =>
    Interp_costs.map_update (With_family.to_value k) (With_family.to_map map_kv)
  | With_family.IMap_get_and_update _ _, (k, (opt_v, (map_kv, _))) =>
    Interp_costs.map_get_and_update
      (With_family.to_value k) (With_family.to_map map_kv)
  | With_family.IBig_map_mem _ _, (_, (big_map, _)) =>
    Interp_costs.big_map_mem big_map.(With_family.big_map.diff)
  | With_family.IBig_map_get _ _, (_, (big_map, _)) =>
    Interp_costs.big_map_get big_map.(With_family.big_map.diff)
  | With_family.IBig_map_update _ _, (_, (_, (big_map, _))) =>
    Interp_costs.big_map_update big_map.(With_family.big_map.diff)
  | With_family.IBig_map_get_and_update _ _, (_, (_, (big_map, _))) =>
    Interp_costs.big_map_get_and_update big_map.(With_family.big_map.diff)
  | With_family.IAdd_seconds_to_timestamp _ _, (n, (t, _)) =>
    Interp_costs.add_seconds_timestamp n t
  | With_family.IAdd_timestamp_to_seconds _ _, (t, (n, _)) =>
    Interp_costs.add_timestamp_seconds t n
  | With_family.ISub_timestamp_seconds _ _, (t, (n, _)) =>
    Interp_costs.sub_timestamp_seconds t n
  | With_family.IDiff_timestamps _ _, (t1, (t2, _)) =>
    Interp_costs.diff_timestamps t1 t2
  | With_family.IConcat_string_pair _ _, (s1, (s2, _)) =>
    Interp_costs.concat_string_pair s1 s2
  | With_family.IConcat_string _ _, (ss, _) =>
    Interp_costs.concat_string_precheck ss
  | With_family.ISlice_string _ _, (offset, (length, (s, _))) =>
    Interp_costs.slice_string s
  | With_family.IConcat_bytes_pair _ _, (x, (y, _)) =>
    Interp_costs.concat_bytes_pair x y
  | With_family.IConcat_bytes _ _, (ss, _) =>
    Interp_costs.concat_string_precheck ss
  | With_family.ISlice_bytes _ _, (_, (_, (s, _))) => Interp_costs.slice_bytes s
  | With_family.IMul_teznat _ _, _ => Interp_costs.mul_teznat
  | With_family.IMul_nattez _ _, _ => Interp_costs.mul_nattez
  | With_family.IAbs_int _ _, (x, _) => Interp_costs.abs_int x
  | With_family.INeg _ _, (x, _) => Interp_costs.neg x
  | With_family.IAdd_int _ _, (x, (y, _)) => Interp_costs.add_int x y
  | With_family.IAdd_nat _ _, (x, (y, _)) => Interp_costs.add_nat x y
  | With_family.ISub_int _ _, (x, (y, _)) => Interp_costs.sub_int x y
  | With_family.IMul_int _ _, (x, (y, _)) => Interp_costs.mul_int x y
  | With_family.IMul_nat _ _, (x, (y, _)) => Interp_costs.mul_nat x y
  | With_family.IEdiv_teznat _ _, (mz, (n, _)) => Interp_costs.ediv_teznat mz n
  | With_family.IEdiv_int _ _, (x, (y, _)) => Interp_costs.ediv_int x y
  | With_family.IEdiv_nat _ _, (x, (y, _)) => Interp_costs.ediv_nat x y
  | With_family.ILsl_nat _ _, (x, _) => Interp_costs.lsl_nat x
  | With_family.ILsr_nat _ _, (x, _) => Interp_costs.lsr_nat x
  | With_family.IOr_nat _ _, (x, (y, _)) => Interp_costs.or_nat x y
  | With_family.IAnd_nat _ _, (x, (y, _)) => Interp_costs.and_nat x y
  | With_family.IAnd_int_nat _ _, (x, (y, _)) => Interp_costs.and_int_nat x y
  | With_family.IXor_nat _ _, (x, (y, _)) => Interp_costs.xor_nat x y
  | With_family.INot_int _ _, (x, _) => Interp_costs.not_int x
  | With_family.ICompare _ ty _, (x, (y, _)) =>
    Interp_costs.compare
      (With_family.to_ty ty) (With_family.to_value x) (With_family.to_value y)
  | With_family.ICheck_signature _ _, (k, (_, (b, _))) =>
    Interp_costs.check_signature k b
  | With_family.IHash_key _ _, (pk, _) =>
    Interp_costs.hash_key pk
  | With_family.IBlake2b _ _, (bytes_value, _) =>
    Interp_costs.blake2b bytes_value
  | With_family.ISha256 _ _, (bytes_value, _) =>
    Interp_costs.sha256 bytes_value
  | With_family.ISha512 _ _, (bytes_value, _) =>
    Interp_costs.sha512 bytes_value
  | With_family.IKeccak _ _, (bytes_value, _) =>
    Interp_costs.keccak bytes_value
  | With_family.ISha3 _ _, (bytes_value, _) =>
    Interp_costs.sha3 bytes_value
  | With_family.IPairing_check_bls12_381 _ _, (pairs, _) =>
    Interp_costs.pairing_check_bls12_381 pairs
  | With_family.ISapling_verify_update _ _, (tx, _) =>
    let inputs := Gas_input_size.sapling_transaction_inputs tx in
    let outputs := Gas_input_size.sapling_transaction_outputs tx in
    let bound_data := Gas_input_size.sapling_transaction_bound_data tx in
    Interp_costs.sapling_verify_update inputs outputs bound_data
  | With_family.ISapling_verify_update_deprecated _ _, (tx, _) =>
    let inputs := List.length tx.(Sapling.UTXO.Legacy.transaction.inputs) in
    let outputs := List.length tx.(Sapling.UTXO.Legacy.transaction.outputs) in
    Interp_costs.sapling_verify_update_deprecated inputs outputs
  | With_family.ISplit_ticket _ _, (ticket, ((amount_a, amount_b), _)) =>
    Interp_costs.split_ticket ticket.(Script_typed_ir.ticket.amount) amount_a
      amount_b
  | With_family.IJoin_tickets _ ty _, ((ticket_a, ticket_b), _) =>
    Interp_costs.join_tickets
      (With_family.to_ty ty)
      (With_family.to_ticket ticket_a)
      (With_family.to_ticket ticket_b)
  | With_family.IHalt _, _ => Interp_costs.halt
  | With_family.IDrop _ _, _ => Interp_costs.drop
  | With_family.IDup _ _, _ => Interp_costs.dup
  | With_family.ISwap _ _, _ => Interp_costs.swap
  | With_family.IConst _ _ _ _, _ => Interp_costs.const
  | With_family.ICons_some _ _, _ => Interp_costs.cons_some
  | With_family.ICons_none _ _ _, _ => Interp_costs.cons_none
  | With_family.IIf_none _ _ _ _, _ => Interp_costs.if_none
  | With_family.IOpt_map _ _ _, _ => Interp_costs.opt_map
  | With_family.ICons_pair _ _, _ => Interp_costs.cons_pair
  | With_family.IUnpair _ _, _ => Interp_costs.unpair
  | With_family.ICar _ _, _ => Interp_costs.car
  | With_family.ICdr _ _, _ => Interp_costs.cdr
  | With_family.ICons_left _ _ _, _ => Interp_costs.cons_left
  | With_family.ICons_right _ _ _, _ => Interp_costs.cons_right
  | With_family.IIf_left _ _ _ _, _ => Interp_costs.if_left
  | With_family.ICons_list _ _, _ => Interp_costs.cons_list
  | With_family.INil _ _ _, _ => Interp_costs.nil
  | With_family.IIf_cons _ _ _ _, _ => Interp_costs.if_cons
  | With_family.IList_size _ _, _ => Interp_costs.list_size
  | With_family.IEmpty_set _ _ _, _ => Interp_costs.empty_set
  | With_family.ISet_size _ _, _ => Interp_costs.set_size
  | With_family.IEmpty_map _ _ _ _, _ => Interp_costs.empty_map
  | With_family.IMap_size _ _, _ => Interp_costs.map_size
  | With_family.IEmpty_big_map _ _ _ _, _ => Interp_costs.empty_big_map
  | With_family.IString_size _ _, _ => Interp_costs.string_size
  | With_family.IBytes_size _ _, _ => Interp_costs.bytes_size
  | With_family.IAdd_tez _ _, _ => Interp_costs.add_tez
  | With_family.ISub_tez _ _, _ => Interp_costs.sub_tez
  | With_family.ISub_tez_legacy _ _, _ => Interp_costs.sub_tez_legacy
  | With_family.IOr _ _, _ => Interp_costs.bool_or
  | With_family.IAnd _ _, _ => Interp_costs.bool_and
  | With_family.IXor _ _, _ => Interp_costs.bool_xor
  | With_family.INot _ _, _ => Interp_costs.bool_not
  | With_family.IIs_nat _ _, _ => Interp_costs.is_nat
  | With_family.IInt_nat _ _, _ => Interp_costs.int_nat
  | With_family.IInt_bls12_381_fr _ _, _ =>
    Interp_costs.int_bls12_381_fr
  | With_family.IEdiv_tez _ _, _ => Interp_costs.ediv_tez
  | With_family.IIf _ _ _ _, _ => Interp_costs.if_
  | With_family.ILoop _ _ _, _ => Interp_costs.loop
  | With_family.ILoop_left _ _ _, _ => Interp_costs.loop_left
  | With_family.IDip _ _ _ _, _ => Interp_costs.dip
  | With_family.IExec _ _ _, _ => Interp_costs.exec
  | With_family.IApply _ _ _, _ => Interp_costs.apply
  | With_family.ILambda _ _ _, _ => Interp_costs.lambda
  | With_family.IFailwith _ _, _ => Alpha_context.Gas.free
  | With_family.IEq _ _, _ => Interp_costs.eq_value
  | With_family.INeq _ _, _ => Interp_costs.neq
  | With_family.ILt _ _, _ => Interp_costs.lt
  | With_family.ILe _ _, _ => Interp_costs.le
  | With_family.IGt _ _, _ => Interp_costs.gt
  | With_family.IGe _ _, _ => Interp_costs.ge
  | With_family.IPack _ _ _, _ => Alpha_context.Gas.free
  | With_family.IUnpack _ _ _, (b, _) => Interp_costs.unpack b
  | With_family.IAddress _ _, _ => Interp_costs.address
  | With_family.IContract _ _ _ _, _ => Interp_costs.contract
  | With_family.ITransfer_tokens _ _, _ => Interp_costs.transfer_tokens
  | With_family.IView _ _ _ _, _ => Interp_costs.view
  | With_family.IImplicit_account _ _, _ => Interp_costs.implicit_account
  | With_family.ISet_delegate _ _, _ => Interp_costs.set_delegate
  | With_family.IBalance _ _, _ => Interp_costs.balance
  | With_family.ILevel _ _, _ => Interp_costs.level
  | With_family.INow _ _, _ => Interp_costs.now
  | With_family.IMin_block_time _ _, _ => Interp_costs.min_block_time
  | With_family.ISapling_empty_state _ _ _, _ =>
    Interp_costs.sapling_empty_state
  | With_family.ISource _ _, _ => Interp_costs.source
  | With_family.ISender _ _, _ => Interp_costs.sender
  | With_family.ISelf _ _ _ _, _ => Interp_costs.self
  | With_family.ISelf_address _ _, _ => Interp_costs.self_address
  | With_family.IAmount _ _, _ => Interp_costs.amount
  | With_family.IDig _ n_value _ _, _ => Interp_costs.dign n_value
  | With_family.IDug _ n_value _ _, _ => Interp_costs.dugn n_value
  | With_family.IDipn _ n_value _ _ _, _ => Interp_costs.dipn n_value
  | With_family.IDropn _ n_value _ _, _ => Interp_costs.dropn n_value
  | With_family.IChainId _ _, _ => Interp_costs.chain_id
  | With_family.ICreate_contract _ _ _ _, _ =>
    Interp_costs.create_contract
  | With_family.IVoting_power _ _, _ => Interp_costs.voting_power
  | With_family.ITotal_voting_power _ _, _ => Interp_costs.total_voting_power
  | With_family.IAdd_bls12_381_g1 _ _, _ => Interp_costs.add_bls12_381_g1
  | With_family.IAdd_bls12_381_g2 _ _, _ => Interp_costs.add_bls12_381_g2
  | With_family.IAdd_bls12_381_fr _ _, _ => Interp_costs.add_bls12_381_fr
  | With_family.IMul_bls12_381_g1 _ _, _ => Interp_costs.mul_bls12_381_g1
  | With_family.IMul_bls12_381_g2 _ _, _ => Interp_costs.mul_bls12_381_g2
  | With_family.IMul_bls12_381_fr _ _, _ => Interp_costs.mul_bls12_381_fr
  | With_family.INeg_bls12_381_g1 _ _, _ => Interp_costs.neg_bls12_381_g1
  | With_family.INeg_bls12_381_g2 _ _, _ => Interp_costs.neg_bls12_381_g2
  | With_family.INeg_bls12_381_fr _ _, _ => Interp_costs.neg_bls12_381_fr
  | With_family.IMul_bls12_381_fr_z _ _, (x, _) =>
    Interp_costs.mul_bls12_381_fr_z x
  | With_family.IMul_bls12_381_z_fr _ _, (_, (x, _)) =>
    Interp_costs.mul_bls12_381_z_fr x
  | With_family.IDup_n _ n_value _ _, _ => Interp_costs.dupn n_value
  | With_family.IComb _ n_value _ _, _ => Interp_costs.comb n_value
  | With_family.IUncomb _ n_value _ _, _ => Interp_costs.uncomb n_value
  | With_family.IComb_get _ n_value _ _, _ => Interp_costs.comb_get n_value
  | With_family.IComb_set _ n_value _ _, _ => Interp_costs.comb_set n_value
  | With_family.ITicket _ _ _, _ => Interp_costs.ticket
  | With_family.IRead_ticket _ _ _, _ => Interp_costs.read_ticket
  | With_family.IOpen_chest _ _, (_, (chest, (time, _))) =>
    Interp_costs.open_chest chest (Script_int.to_zint time)
  | With_family.INever _, (v, _) => match v with end
  end.

Definition dep_consume_instr {s f}
  (local_gas_counter : Local_gas_counter.local_gas_counter)
  (i : With_family.kinstr s f)
  (accu_stack : With_family.stack_ty_to_dep_Set s) :
  option Local_gas_counter.local_gas_counter :=
  let cost := dep_cost_of_instr i accu_stack in
  Local_gas_counter.consume_opt local_gas_counter cost.

(** Simulation of [kundip]. *)
Fixpoint dep_kundip {s z u w t}
  (w_value : With_family.stack_prefix_preservation_witness s z u w)
  (accu_stack : With_family.stack_ty_to_dep_Set u)
  (k_value : With_family.kinstr w t) :
  With_family.stack_ty_to_dep_Set s * With_family.kinstr z t :=
  match w_value, accu_stack, k_value with
  | With_family.KPrefix loc_value ty_value w_value, (accu_value, stack_value), _ =>
    let k_value := With_family.IConst loc_value ty_value accu_value k_value in
    let accu_stack := stack_value in
    dep_kundip w_value accu_stack k_value
  | With_family.KRest, _, _ =>
    (accu_stack, k_value)
  end.

(* TODO: define *)
(** Simulation of [apply]. *)
Definition dep_apply {a b c : Ty.t}
  (ctxt : Local_gas_counter.outdated_context)
  (gas : Local_gas_counter.local_gas_counter)
  (capture_ty : With_family.ty a) (capture : With_family.ty_to_dep_Set a)
  (lam : With_family.lambda (Ty.Pair a b) c) :
  M? (
    With_family.lambda b c *
    Local_gas_counter.outdated_context *
    Local_gas_counter.local_gas_counter
  ).
Admitted.

(** Simulation of [transfer]. *)
Definition dep_transfer {a : Ty.t}
  (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (amount : Alpha_context.Tez.tez)
  (location : Alpha_context.Script.location)
  (parameters_ty : With_family.ty a)
  (parameters : With_family.ty_to_dep_Set a)
  (destination : Alpha_context.Destination.t)
  (entrypoints : Alpha_context.Entrypoint.t) :
  M?
    (Script_typed_ir.operation * Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter).
Admitted.

(** Simulation of [unpack] *)
Definition dep_unpack {a : Ty.t}
  (ctxt : Alpha_context.context) (ty : With_family.ty a) (bytes_value : bytes)
  : M? (option _ * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.deserialization_cost_estimated_from_bytes
        (Bytes.length bytes_value)) in
  if
    ((Bytes.length bytes_value) >=i 1) &&
    ((TzEndian.get_uint8 bytes_value 0) =i 5)
  then
    let str := Bytes.sub_string bytes_value 1 ((Bytes.length bytes_value) -i 1)
      in
    match
      Data_encoding.Binary.of_string_opt Alpha_context.Script.expr_encoding str
      with
    | None =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt (Interp_costs.unpack_failed str) in
      return? (None, ctxt)
    | Some expr =>
      let function_parameter :=
        Script_ir_translator.dep_parse_data None ctxt false false ty
          (Micheline.root_value expr) in
       match function_parameter with
      | Pervasives.Ok (value_value, ctxt) => return? ((Some value_value), ctxt)
      | Pervasives.Error _ignored =>
        let? ctxt :=
          Alpha_context.Gas.consume ctxt (Interp_costs.unpack_failed str) in
        return? (None, ctxt)
      end
    end
  else
    return? (None, ctxt).


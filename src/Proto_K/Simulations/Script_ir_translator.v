Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Coq.Program.Equality.

Require Import TezosOfOCaml.Environment.V6.
Require Import TezosOfOCaml.Proto_K.Script_ir_translator.

Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_K.Simulations.Script_map.
Require TezosOfOCaml.Proto_K.Simulations.Script_set.
Require TezosOfOCaml.Proto_K.Simulations.Script_tc_context.
Require TezosOfOCaml.Proto_K.Simulations.Script_tc_errors.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

(** Dependent version of [ex_stack_ty]. *)
Inductive dep_ex_stack_ty : Set :=
| Dep_ex_stack_ty {s} : With_family.stack_ty s -> dep_ex_stack_ty.

(** Conversion from [dep_ex_stack_ty] to [ex_stack_ty]. *)
Definition to_ex_stack_ty (x : dep_ex_stack_ty) : ex_stack_ty :=
  match x with
  | Dep_ex_stack_ty stack => Ex_stack_ty (With_family.to_stack_ty stack)
  end.

(** Dependent version of [cinstr]. *)
Module dep_cinstr.
  Record record {s u : Stack_ty.t} : Set := Build {
    apply {f} : With_family.kinstr u f -> With_family.kinstr s f;
  }.
  Arguments record : clear implicits.
End dep_cinstr.
Definition dep_cinstr := dep_cinstr.record.

(** Dependent version of [descr]. *)
Module dep_descr.
  Record record {s u : Stack_ty.t} : Set := Build {
    loc : Alpha_context.Script.location;
    bef : With_family.stack_ty s;
    aft : With_family.stack_ty u;
    instr : dep_cinstr s u;
  }.
  Arguments record : clear implicits.
End dep_descr.
Definition dep_descr := dep_descr.record.

(** Dependent version of [close_descr]. *)
Definition dep_close_descr {s u} (function_parameter : dep_descr s u) :
  With_family.kdescr s u :=
  let '{|
    dep_descr.loc := loc_value;
    dep_descr.bef := bef;
    dep_descr.aft := aft;
    dep_descr.instr := instr
  |} := function_parameter in
  let kinstr := instr.(dep_cinstr.apply) (With_family.IHalt loc_value) in
  {|
    With_family.kdescr.kloc := loc_value;
    With_family.kdescr.kbef := bef;
    With_family.kdescr.kaft := aft;
    With_family.kdescr.kinstr := kinstr;
  |}.

(** Dependent version of [compose_descr]. *)
Definition dep_compose_descr {s u v}
  (loc_value : Alpha_context.Script.location)
  (d1 : dep_descr s u) (d2 : dep_descr u v)
  : dep_descr s v :=
  {|
    dep_descr.loc := loc_value;
    dep_descr.bef := d1.(dep_descr.bef);
    dep_descr.aft := d2.(dep_descr.aft);
    dep_descr.instr :=
      {|
        dep_cinstr.apply :=
          fun f (k_value : With_family.kinstr v f) =>
            d1.(dep_descr.instr).(dep_cinstr.apply)
              (d2.(dep_descr.instr).(dep_cinstr.apply) k_value)
      |}
  |}.

(** Dependent version of [tc_context]. *)
Definition dep_tc_context : Set := Script_tc_context.dep_t.

(** Conversion back to [tc_context]. *)
Definition to_tc_context (x : dep_tc_context) : Script_tc_context.t :=
  Script_tc_context.to_t x.

(** Simulation of [ty_of_comparable_ty]. *)
Definition dep_ty_of_comparable_ty {a} (ty : With_family.ty a)
  : With_family.ty a := ty.

(** Simulation of [unparse_comparable_ty_uncarbonated]. *)
Fixpoint dep_unparse_comparable_ty_uncarbonated
  {loc : Set} {t}
  (loc_value : loc) (ty : With_family.ty t) :
  Alpha_context.Script.michelson_node loc :=
  match ty with
  | With_family.Unit_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_unit nil nil
  | With_family.Never_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_never nil nil
  | With_family.Int_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_int nil nil
  | With_family.Nat_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_nat nil nil
  | With_family.Signature_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_signature nil nil
  | With_family.String_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_string nil nil
  | With_family.Bytes_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_bytes nil nil
  | With_family.Mutez_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_mutez nil nil
  | With_family.Bool_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_bool nil nil
  | With_family.Key_hash_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_key_hash nil nil
  | With_family.Key_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_key nil nil
  | With_family.Timestamp_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_timestamp nil nil
  | With_family.Address_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_address nil nil
  | With_family.Tx_rollup_l2_address_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_tx_rollup_l2_address nil
      nil
  | With_family.Chain_id_t =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_chain_id nil nil
  | With_family.Pair_t l_value r_value _meta Dependent_bool.YesYes =>
        let tl := dep_unparse_comparable_ty_uncarbonated loc_value l_value in
        let tr := dep_unparse_comparable_ty_uncarbonated loc_value r_value in
        match tr with
        | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
            Micheline.Prim loc_value Michelson_v1_primitives.T_pair
                           (cons tl ts) nil
        | _ =>
            Micheline.Prim loc_value Michelson_v1_primitives.T_pair
                           [ tl; tr ] nil
        end
  | With_family.Union_t l_value r_value _meta Dependent_bool.YesYes =>
        let tl := dep_unparse_comparable_ty_uncarbonated loc_value l_value in
        let tr := dep_unparse_comparable_ty_uncarbonated loc_value r_value in
        Micheline.Prim loc_value Michelson_v1_primitives.T_or [ tl; tr ] nil
  | With_family.Option_t t_value _meta Dependent_bool.Yes =>
        Micheline.Prim loc_value Michelson_v1_primitives.T_option
          [ dep_unparse_comparable_ty_uncarbonated loc_value t_value ] nil
  | _ => Micheline.Prim loc_value Michelson_v1_primitives.T_unit nil nil
  end.

(** Simulation of [unparse_ty_entrypoints_uncarbonated]. *)
Fixpoint dep_unparse_ty_entrypoints_uncarbonated {loc : Set} {t}
  (loc_value : loc) (ty : With_family.ty t)
  (function_parameter : Script_typed_ir.entrypoints_node)
  : Alpha_context.Script.michelson_node loc :=
  let '{|
        Script_typed_ir.entrypoints_node.at_node := entrypoint_name;
        Script_typed_ir.entrypoints_node.nested := nested_entrypoints
      |} := function_parameter in

  let '(name, args) :=
    match ty with
    | With_family.Unit_t => (Michelson_v1_primitives.T_unit, nil)
    | With_family.Int_t => (Michelson_v1_primitives.T_int, nil)
    | With_family.Nat_t => (Michelson_v1_primitives.T_nat, nil)
    | With_family.Signature_t => (Michelson_v1_primitives.T_signature, nil)
    | With_family.String_t => (Michelson_v1_primitives.T_string, nil)
    | With_family.Bytes_t => (Michelson_v1_primitives.T_bytes, nil)
    | With_family.Mutez_t => (Michelson_v1_primitives.T_mutez, nil)
    | With_family.Bool_t => (Michelson_v1_primitives.T_bool, nil)
    | With_family.Key_hash_t => (Michelson_v1_primitives.T_key_hash, nil)
    | With_family.Key_t => (Michelson_v1_primitives.T_key, nil)
    | With_family.Timestamp_t => (Michelson_v1_primitives.T_timestamp, nil)
    | With_family.Address_t => (Michelson_v1_primitives.T_address, nil)
    | With_family.Tx_rollup_l2_address_t =>
      (Michelson_v1_primitives.T_tx_rollup_l2_address, nil)
    | With_family.Operation_t => (Michelson_v1_primitives.T_operation, nil)
    | With_family.Chain_id_t => (Michelson_v1_primitives.T_chain_id, nil)
    | With_family.Never_t => (Michelson_v1_primitives.T_never, nil)
    | With_family.Bls12_381_g1_t =>
      (Michelson_v1_primitives.T_bls12_381_g1, nil)
    | With_family.Bls12_381_g2_t =>
      (Michelson_v1_primitives.T_bls12_381_g2, nil)
    | With_family.Bls12_381_fr_t =>
      (Michelson_v1_primitives.T_bls12_381_fr, nil)
    | With_family.Contract_t ut _meta =>
      let t_value :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value ut
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_contract, [ t_value ])
    | With_family.Pair_t utl utr _meta _ =>
      let tl :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value (utl)
          Script_typed_ir.no_entrypoints in
      let tr :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value (utr)
          Script_typed_ir.no_entrypoints in
      match tr with
      | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
        (Michelson_v1_primitives.T_pair, (cons tl ts))
      | _ => (Michelson_v1_primitives.T_pair, [ tl; tr ])
      end
    | With_family.Union_t utl utr _meta _ =>
      let '(entrypoints_l, entrypoints_r) :=
        match nested_entrypoints with
        | Script_typed_ir.Entrypoints_None =>
          (Script_typed_ir.no_entrypoints, Script_typed_ir.no_entrypoints)
        |
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right :=
                _right
              |} => (_left, _right)
        end in
      let tl := dep_unparse_ty_entrypoints_uncarbonated
                  loc_value utl entrypoints_l in
      let tr := dep_unparse_ty_entrypoints_uncarbonated
                  loc_value utr entrypoints_r in
      (Michelson_v1_primitives.T_or, [ tl; tr ])
    | With_family.Lambda_t uta utr _meta =>
      let ta :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value uta
          Script_typed_ir.no_entrypoints in
      let tr :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value utr
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_lambda, [ ta; tr ])
    | With_family.Option_t ut _meta _ =>
      let ut :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value ut
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_option, [ ut ])
    | With_family.List_t ut _meta =>
      let t_value :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value ut
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_list, [ t_value ])
    | With_family.Ticket_t ut _meta =>
      let t_value := dep_unparse_comparable_ty_uncarbonated loc_value ut in
      (Michelson_v1_primitives.T_ticket, [ t_value ])
    | With_family.Set_t ut _meta =>
      let t_value := dep_unparse_comparable_ty_uncarbonated loc_value ut in
      (Michelson_v1_primitives.T_set, [ t_value ])
    | With_family.Map_t uta utr _meta =>
      let ta := dep_unparse_comparable_ty_uncarbonated loc_value uta in
      let tr :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value utr
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_map, [ ta; tr ])
    | With_family.Big_map_t uta utr _meta =>
      let ta := dep_unparse_comparable_ty_uncarbonated loc_value uta in
      let tr :=
        dep_unparse_ty_entrypoints_uncarbonated loc_value utr
          Script_typed_ir.no_entrypoints in
      (Michelson_v1_primitives.T_big_map, [ ta; tr ])
    | With_family.Sapling_transaction_t memo_size =>
      (Michelson_v1_primitives.T_sapling_transaction,
        [ unparse_memo_size loc_value memo_size ])
    | With_family.Sapling_transaction_deprecated_t memo_size =>
      (Michelson_v1_primitives.T_sapling_transaction_deprecated,
        [ unparse_memo_size loc_value memo_size ])
    | With_family.Sapling_state_t memo_size =>
      (Michelson_v1_primitives.T_sapling_state,
        [ unparse_memo_size loc_value memo_size ])
    | With_family.Chest_key_t => (Michelson_v1_primitives.T_chest_key, nil)
    | With_family.Chest_t => (Michelson_v1_primitives.T_chest, nil)
    end in
  let annot :=
    match entrypoint_name with
    | None => nil
    | Some info_ => [ Alpha_context.Entrypoint.unparse_as_field_annot
       info_.(Script_typed_ir.entrypoint_info.name) ]
    end in
  Micheline.Prim loc_value name args annot.

(** Simulation of [unparse_ty_uncarbonated]. *)
Definition dep_unparse_ty_uncarbonated {A : Set} {t}
  (loc_value : A) (ty : With_family.ty t)
  : Alpha_context.Script.michelson_node A :=
  dep_unparse_ty_entrypoints_uncarbonated loc_value ty
    Script_typed_ir.no_entrypoints.

(** Simulation of [unparse_ty]. *)
Definition dep_unparse_ty {A : Set} {t}
  (loc_value : A) (ctxt : Alpha_context.context) (ty : With_family.ty t)
  : M? (Alpha_context.Script.michelson_node A * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume
                 ctxt (Unparse_costs.unparse_type (With_family.to_ty ty)) in
  return? (dep_unparse_ty_uncarbonated loc_value ty, ctxt).

(** Simulation of [unparse_comparable_ty]. *)
Definition dep_unparse_comparable_ty {A : Set} {t}
  (loc_value : A) (ctxt : Alpha_context.context)
  (comp_ty : With_family.ty t)
  : M? (Alpha_context.Script.michelson_node A * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Unparse_costs.unparse_type (With_family.to_ty comp_ty)) in
  return? (dep_unparse_comparable_ty_uncarbonated loc_value comp_ty, ctxt).

(** Simulation of [unparse_parameter_ty]. *)
Definition dep_unparse_parameter_ty {A : Set} {t}
  (loc_value : A) (ctxt : Alpha_context.context) (ty : With_family.ty t)
  (entrypoints : Script_typed_ir.entrypoints)
  : M? (Alpha_context.Script.michelson_node A * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume
                 ctxt (Unparse_costs.unparse_type (With_family.to_ty ty)) in
  return? (dep_unparse_ty_entrypoints_uncarbonated
             loc_value ty entrypoints.(Script_typed_ir.entrypoints.root), ctxt).

(** Simulation of [serialize_ty_for_error]. *)
Definition dep_serialize_ty_for_error {t} (ty : With_family.ty t) :
  Micheline.canonical Alpha_context.Script.prim :=
  Micheline.strip_locations (dep_unparse_ty_uncarbonated tt ty).

(** Simulation of [check_comparable]. *)
Definition dep_check_comparable {a}
  (loc_value : Alpha_context.Script.location) (ty_value : With_family.ty a)
  : M? eq.
Admitted.

(** Simulation of [unparse_stack_uncarbonated]. *)
Fixpoint dep_unparse_stack_uncarbonated {t}
  (function_parameter : With_family.stack_ty t)
  : list Alpha_context.Script.expr :=
  match function_parameter with
  | With_family.Bot_t => nil
  | With_family.Item_t ty rest =>
    let uty := dep_unparse_ty_uncarbonated tt ty in
    let urest := dep_unparse_stack_uncarbonated rest in
    cons (Micheline.strip_locations uty) urest
  end.

(** Simulation of [serialize_stack_for_error]. *)
Definition dep_serialize_stack_for_error {t}
  (ctxt : Alpha_context.context) (stack_ty : With_family.stack_ty t)
  : list Alpha_context.Script.expr :=
  match Alpha_context.Gas.level ctxt with
  | Alpha_context.Gas.Unaccounted => dep_unparse_stack_uncarbonated stack_ty
  | Alpha_context.Gas.Limited _ => nil
  end.

(** Simulation of [unparse_contract]. *)
Definition dep_unparse_contract {A B : Set} {arg : Ty.t}
  (loc_value : A) (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : With_family.typed_contract arg)
  : M? (Micheline.node A B * Alpha_context.context) :=
  let 'With_family.Typed_contract _ address := function_parameter in
  unparse_address loc_value ctxt mode address.

(** Simulation of [comb_witness2]. *)
Definition dep_comb_witness2 {a} (function_parameter : With_family.ty a)
  : comb_witness :=
  match function_parameter with
  | With_family.Pair_t _ (With_family.Pair_t _ _ _ _) _ _ =>
    Comb_Pair (Comb_Pair Comb_Any)
  | With_family.Pair_t _ _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

(** Simulation of [unparse_comparable_data]. *)
Fixpoint dep_unparse_comparable_data {loc : Set} {a}
  (loc_value : loc) (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (ty : With_family.ty a) (a_value : With_family.ty_to_dep_Set a) {struct ty}
  : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.unparse_data_cycle
    in
  match (ty, a_value) with
  | (With_family.Unit_t, v_value) =>
    unparse_unit loc_value ctxt v_value
  | (With_family.Int_t, v_value) =>
    unparse_int loc_value ctxt v_value
  | (With_family.Nat_t, v_value) =>
    unparse_nat loc_value ctxt v_value
  | (With_family.String_t, s_value) =>
    unparse_string loc_value ctxt s_value
  | (With_family.Bytes_t, s_value) =>
    unparse_bytes loc_value ctxt s_value
  | (With_family.Bool_t, b_value) =>
    unparse_bool loc_value ctxt b_value
  | (With_family.Timestamp_t, t_value) =>
    unparse_timestamp loc_value ctxt mode t_value
  | (With_family.Address_t, address) =>
    unparse_address loc_value ctxt mode address
  | (With_family.Tx_rollup_l2_address_t, address) =>
    unparse_tx_rollup_l2_address loc_value ctxt mode address
  | (With_family.Signature_t, s_value) =>
    unparse_signature loc_value ctxt mode s_value
  | (With_family.Mutez_t, v_value) =>
    unparse_mutez loc_value ctxt v_value
  | (With_family.Key_t, k_value) =>
    unparse_key loc_value ctxt mode k_value
  | (With_family.Key_hash_t, k_value) =>
    unparse_key_hash loc_value ctxt mode k_value
  | (With_family.Chain_id_t, chain_id) =>
    unparse_chain_id loc_value ctxt mode chain_id
  | (With_family.Pair_t tl tr _ Dependent_bool.YesYes, pair_value) =>
    let r_witness := dep_comb_witness2 tr in
    let unparse_l (ctxt : Alpha_context.context) (v_value : _)
      : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
      dep_unparse_comparable_data loc_value ctxt mode tl v_value in
    let unparse_r (ctxt : Alpha_context.context) (v_value : _)
      : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
      dep_unparse_comparable_data loc_value ctxt mode tr v_value in
    unparse_pair loc_value unparse_l unparse_r ctxt mode r_witness pair_value
  | (With_family.Union_t tl tr _ Dependent_bool.YesYes, v_value) =>
    let unparse_l (ctxt : Alpha_context.context) (v_value : _)
      : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
      dep_unparse_comparable_data loc_value ctxt mode tl v_value in
    let unparse_r (ctxt : Alpha_context.context) (v_value : _)
      : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
      dep_unparse_comparable_data loc_value ctxt mode tr v_value in
    unparse_union loc_value unparse_l unparse_r ctxt v_value
  | (With_family.Option_t t_value _ Dependent_bool.Yes, v_value) =>
    let unparse_v (ctxt : Alpha_context.context) (v_value : _)
      : M? (Alpha_context.Script.michelson_node loc * Alpha_context.context) :=
      dep_unparse_comparable_data loc_value ctxt mode t_value v_value in
    unparse_option loc_value unparse_v ctxt v_value
  | _ => return?
         (Micheline.Prim loc_value Michelson_v1_primitives.T_unit nil nil, ctxt)
  end.

(** Simulation of [pack_comparable_data]. *)
Definition dep_pack_comparable_data {t}
  (ctxt : Alpha_context.context) (ty : With_family.ty t) (data : With_family.ty_to_dep_Set t)
   : M? (bytes * Alpha_context.context) :=
  let? '(unparsed, ctxt) := dep_unparse_comparable_data tt ctxt Optimized_legacy ty data in
  pack_node unparsed ctxt.

(** Simulation of [hash_comparable_data]. *)
Definition dep_hash_comparable_data {t}
  (ctxt : Alpha_context.context) (ty : With_family.ty t) (data : With_family.ty_to_dep_Set t)
  : M? (Script_expr_hash.t * Alpha_context.context) :=
  let? '(bytes_value, ctxt) :=
    dep_pack_comparable_data ctxt ty data in
  hash_bytes ctxt bytes_value.

(** Simulation of [check_dupable_comparable_ty]. *)
Definition dep_check_dupable_comparable_ty {t}
  (_ : With_family.ty t) : unit :=
  tt.

(** Simulation of [check_dupable_ty]. *)
Definition dep_check_dupable_ty {t}
  (ctxt : Alpha_context.context) (loc_value : Alpha_context.Script.location)
  (ty : With_family.ty t) : M? Alpha_context.context :=
  let fix aux {t1}
    (loc_value : Alpha_context.Script.location) (ty : With_family.ty t1)
    : Gas_monad.t unit Error_monad._error :=
    Gas_monad.Syntax.op_letstar
      (Gas_monad.consume_gas Typecheck_costs.check_dupable_cycle)
      (fun function_parameter =>
        let '_ := function_parameter in
        match ty with
        | With_family.Unit_t => Gas_monad.Syntax.return_unit
        | With_family.Int_t => Gas_monad.Syntax.return_unit
        | With_family.Nat_t => Gas_monad.Syntax.return_unit
        | With_family.Signature_t => Gas_monad.Syntax.return_unit
        | With_family.String_t => Gas_monad.Syntax.return_unit
        | With_family.Bytes_t => Gas_monad.Syntax.return_unit
        | With_family.Mutez_t => Gas_monad.Syntax.return_unit
        | With_family.Key_hash_t => Gas_monad.Syntax.return_unit
        | With_family.Key_t => Gas_monad.Syntax.return_unit
        | With_family.Timestamp_t => Gas_monad.Syntax.return_unit
        | With_family.Address_t => Gas_monad.Syntax.return_unit
        | With_family.Tx_rollup_l2_address_t => Gas_monad.Syntax.return_unit
        | With_family.Bool_t => Gas_monad.Syntax.return_unit
        | With_family.Contract_t _ _ => Gas_monad.Syntax.return_unit
        | With_family.Operation_t => Gas_monad.Syntax.return_unit
        | With_family.Chain_id_t => Gas_monad.Syntax.return_unit
        | With_family.Never_t => Gas_monad.Syntax.return_unit
        | With_family.Bls12_381_g1_t => Gas_monad.Syntax.return_unit
        | With_family.Bls12_381_g2_t => Gas_monad.Syntax.return_unit
        | With_family.Bls12_381_fr_t => Gas_monad.Syntax.return_unit
        | With_family.Sapling_state_t _ => Gas_monad.Syntax.return_unit
        | With_family.Sapling_transaction_t _ =>
          Gas_monad.Syntax.return_unit
        | With_family.Sapling_transaction_deprecated_t _ =>
          Gas_monad.Syntax.return_unit
        | With_family.Chest_t => Gas_monad.Syntax.return_unit
        | With_family.Chest_key_t => Gas_monad.Syntax.return_unit
        | With_family.Ticket_t _ _ =>
          Gas_monad.Syntax.fail
            (Build_extensible "Unexpected_ticket" Alpha_context.Script.location
              loc_value)
        | With_family.Pair_t ty_a ty_b _ _ =>
          Gas_monad.Syntax.op_letstar (aux loc_value ty_a)
            (fun function_parameter =>
              let '_ := function_parameter in
              aux loc_value ty_b)
        | With_family.Union_t ty_a ty_b _ _ =>
          Gas_monad.Syntax.op_letstar (aux loc_value ty_a)
            (fun function_parameter =>
              let '_ := function_parameter in
              aux loc_value ty_b)
        | With_family.Lambda_t _ _ _ => Gas_monad.Syntax.return_unit
        | With_family.Option_t ty _ _ => aux loc_value ty
        | With_family.List_t ty _ => aux loc_value ty
        | With_family.Set_t key_ty _ =>
          let '_ := dep_check_dupable_comparable_ty key_ty in
          Gas_monad.Syntax.return_unit
        | With_family.Map_t key_ty val_ty _ =>
          let '_ := dep_check_dupable_comparable_ty key_ty in
          aux loc_value val_ty
        | With_family.Big_map_t key_ty val_ty _ =>
          let '_ := dep_check_dupable_comparable_ty key_ty in
          aux loc_value val_ty
        end) in
  let gas := aux loc_value ty in
  let? '(res, ctxt) := Gas_monad.run ctxt gas in
  match res with
  | Pervasives.Ok _ => return? ctxt
  | Pervasives.Error e_value => Error_monad.error_value e_value
  end.

(** Simulation of [type_metadata_eq]. *)
Definition dep_type_metadata_eq {A error_trace}
  (error_details : Script_tc_errors.dep_error_details A error_trace)
  (function_parameter : Script_typed_ir.ty_metadata) :
  Script_typed_ir.ty_metadata ->
  Pervasives.result unit
    (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
  let '{| Script_typed_ir.ty_metadata.size := size_a |} := function_parameter in
  fun (function_parameter : Script_typed_ir.ty_metadata) =>
    let '{| Script_typed_ir.ty_metadata.size := size_b |} := function_parameter
      in
    Script_typed_ir.Type_size.dep_check_eq error_details size_a size_b.

(** Simulation of [default_ty_eq_error]. *)
Definition dep_default_ty_eq_error {a b}
  (loc_value : Alpha_context.Script.location)
  (ty1 : With_family.ty a) (ty2 : With_family.ty b) : Error_monad._error :=
  let ty1 := serialize_ty_for_error (With_family.to_ty ty1) in
  let ty2 := serialize_ty_for_error (With_family.to_ty ty2) in
  Build_extensible "Inconsistent_types"
    (Alpha_context.Script.location *
      Micheline.canonical Alpha_context.Script.prim *
      Micheline.canonical Alpha_context.Script.prim) (loc_value, ty1, ty2).

(** Simulation of [ty_eq]. *)
Definition dep_ty_eq {error_trace} {a b}
  (error_details :
    Script_tc_errors.dep_error_details Alpha_context.Script.location
      error_trace)
  (ty1 : With_family.ty a) (ty2 : With_family.ty b) :
  Gas_monad.t
    (a = b)
    (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
  let type_metadata_eq
    (meta1 : Script_typed_ir.ty_metadata) (meta2 : Script_typed_ir.ty_metadata)
    : Gas_monad.t unit (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
    Gas_monad.record_trace_eval (Script_tc_errors.to_error_details error_details)
    (fun (loc_value : Alpha_context.Script.location) =>
      dep_default_ty_eq_error loc_value ty1 ty2)
      (Gas_monad.of_result (type_metadata_eq
        (Script_tc_errors.to_error_details error_details) meta1 meta2)) in
  let memo_size_eq
    (ms1 : Alpha_context.Sapling.Memo_size.t)
    (ms2 : Alpha_context.Sapling.Memo_size.t) : Gas_monad.t unit
      (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
    Gas_monad.of_result (memo_size_eq
      (Script_tc_errors.to_error_details error_details) ms1 ms2) in
  let fix help {a b : Ty.t} (ty1 : With_family.ty a) (ty2 : With_family.ty b) :
    Gas_monad.t (a = b)
      (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
    Gas_monad.Syntax.op_letstar
      (Gas_monad.consume_gas Typecheck_costs.merge_cycle)
      (fun function_parameter =>
        let '_ := function_parameter in
        let not_equal {B : Set} (function_parameter : unit)
          : Gas_monad.t B
            (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
          let '_ := function_parameter in
          Gas_monad.of_result
            (Pervasives.Error
              match error_details
                in Script_tc_errors.dep_error_details _ error_trace
                return Script_tc_errors.Error_trace_family.to_Set error_trace
              with
              | Script_tc_errors.Dep_fast =>
                Script_tc_errors.Inconsistent_types_fast
              | Script_tc_errors.Dep_informative loc_value =>
                Error_monad.trace_of_error
                  (dep_default_ty_eq_error loc_value ty1 ty2)
              end) in
        match
          ty1 in With_family.ty a,
          ty2 in With_family.ty b
          return Gas_monad.t (a = b) _
        with
        | With_family.Unit_t, With_family.Unit_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Unit_t, _ => not_equal tt
        | With_family.Int_t, With_family.Int_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Int_t, _ => not_equal tt
        | With_family.Nat_t, With_family.Nat_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Nat_t, _ => not_equal tt
        | With_family.Key_t, With_family.Key_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Key_t, _ => not_equal tt
        | With_family.Key_hash_t, With_family.Key_hash_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Key_hash_t, _ => not_equal tt
        | With_family.String_t, With_family.String_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.String_t, _ => not_equal tt
        | With_family.Bytes_t, With_family.Bytes_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Bytes_t, _ => not_equal tt
        | With_family.Signature_t, With_family.Signature_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Signature_t, _ => not_equal tt
        | With_family.Mutez_t, With_family.Mutez_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Mutez_t, _ => not_equal tt
        | With_family.Timestamp_t, With_family.Timestamp_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Timestamp_t, _ => not_equal tt
        | With_family.Address_t, With_family.Address_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Address_t, _ => not_equal tt
        | With_family.Tx_rollup_l2_address_t,
            With_family.Tx_rollup_l2_address_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Tx_rollup_l2_address_t, _ => not_equal tt
        | With_family.Bool_t, With_family.Bool_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Bool_t, _ => not_equal tt
        | With_family.Chain_id_t, With_family.Chain_id_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Chain_id_t, _ => not_equal tt
        | With_family.Never_t, With_family.Never_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Never_t, _ => not_equal tt
        | With_family.Operation_t, With_family.Operation_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Operation_t, _ => not_equal tt
        | With_family.Bls12_381_g1_t, With_family.Bls12_381_g1_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Bls12_381_g1_t, _ => not_equal tt
        | With_family.Bls12_381_g2_t, With_family.Bls12_381_g2_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Bls12_381_g2_t, _ => not_equal tt
        | With_family.Bls12_381_fr_t, With_family.Bls12_381_fr_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Bls12_381_fr_t, _ => not_equal tt
        | With_family.Map_t tal tar meta1,
            With_family.Map_t tbl tbr meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun _ =>
              Gas_monad.Syntax.op_letstar
                (help tar tbr)
                (fun eq_left =>
                  Gas_monad.Syntax.op_letplus
                    (help tal tbl)
                    (fun eq_right =>
                      let 'eq_refl := eq_left in
                      let 'eq_refl := eq_right in
                      eq_refl)))
        | With_family.Map_t _ _ _, _ => not_equal tt
        | With_family.Big_map_t tal tar meta1,
            With_family.Big_map_t tbl tbr meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letstar (help tar tbr)
                (fun eq_left =>
                  Gas_monad.Syntax.op_letplus
                    (help tal tbl)
                    (fun eq_right =>
                      let 'eq_refl := eq_left in
                      let 'eq_refl := eq_right in
                      eq_refl)))
        | With_family.Big_map_t _ _ _, _ => not_equal tt
        | With_family.Set_t ea meta1, With_family.Set_t eb meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letplus
                (help ea eb)
                (fun function_parameter =>
                  let 'eq_refl := function_parameter in
                  eq_refl))
        | With_family.Set_t _ _, _ => not_equal tt
        | With_family.Ticket_t ea meta1,
            With_family.Ticket_t eb meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letplus
                (help ea eb)
                (fun function_parameter =>
                  let 'eq_refl := function_parameter in
                  eq_refl))
        | With_family.Ticket_t _ _, _ => not_equal tt
        | With_family.Pair_t tal tar meta1 cmp1,
            With_family.Pair_t tbl tbr meta2 cmp2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun _ =>
              Gas_monad.Syntax.op_letstar (help tal tbl)
                (fun eq_left =>
                  Gas_monad.Syntax.op_letplus (help tar tbr)
                    (fun eq_right =>
                      let 'eq_refl := eq_left in
                      let 'eq_refl := eq_right in
                      let 'Dependent_bool.Eq :=
                        Dependent_bool.merge_dand cmp1 cmp2 in
                      eq_refl)))
        | With_family.Pair_t _ _ _ _, _ => not_equal tt
        | With_family.Union_t tal tar meta1 cmp1,
            With_family.Union_t tbl tbr meta2 cmp2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun _ =>
              Gas_monad.Syntax.op_letstar (help tal tbl)
                (fun eq_left =>
                  Gas_monad.Syntax.op_letplus (help tar tbr)
                    (fun eq_right =>
                      let 'eq_refl := eq_left in
                      let 'eq_refl := eq_right in
                      let 'Dependent_bool.Eq :=
                        Dependent_bool.merge_dand cmp1 cmp2 in
                      eq_refl)))
        | With_family.Union_t _ _ _ _, _ => not_equal tt
        | With_family.Lambda_t tal tar meta1,
            With_family.Lambda_t tbl tbr meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun _ =>
              Gas_monad.Syntax.op_letstar (help tal tbl)
                (fun eq_left =>
                  Gas_monad.Syntax.op_letplus (help tar tbr)
                    (fun eq_right =>
                      let 'eq_refl := eq_left in
                      let 'eq_refl := eq_right in
                      eq_refl)))
        | With_family.Lambda_t _ _ _, _ => not_equal tt
        | With_family.Contract_t tal meta1,
            With_family.Contract_t tbl meta2 =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letplus (help tal tbl)
                (fun function_parameter =>
                  let 'eq_refl := function_parameter in
                  eq_refl))
        | With_family.Contract_t _ _, _ => not_equal tt
        | With_family.Option_t tva meta1 _,
            With_family.Option_t tvb meta2 _ =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letplus (help tva tvb)
                (fun function_parameter =>
                  let 'eq_refl := function_parameter in
                  eq_refl))
        | With_family.Option_t _ _ _, _ => not_equal tt
        | With_family.List_t tva meta1, With_family.List_t tvb meta2
          =>
          Gas_monad.Syntax.op_letstar (type_metadata_eq meta1 meta2)
            (fun function_parameter =>
              let '_ := function_parameter in
              Gas_monad.Syntax.op_letplus (help tva tvb)
                (fun function_parameter =>
                  let 'eq_refl := function_parameter in
                  eq_refl))
        | With_family.List_t _ _, _ => not_equal tt
        | With_family.Sapling_state_t ms1,
            With_family.Sapling_state_t ms2 =>
          Gas_monad.Syntax.op_letplus (memo_size_eq ms1 ms2)
            (fun function_parameter =>
              let '_ := function_parameter in
              eq_refl)
        | With_family.Sapling_state_t _, _ => not_equal tt
        | With_family.Sapling_transaction_t ms1,
            With_family.Sapling_transaction_t ms2 =>
          Gas_monad.Syntax.op_letplus (memo_size_eq ms1 ms2)
            (fun function_parameter =>
              let '_ := function_parameter in
              eq_refl)
        | With_family.Sapling_transaction_t _, _ => not_equal tt
        | With_family.Sapling_transaction_deprecated_t ms1,
            With_family.Sapling_transaction_deprecated_t ms2 =>
          Gas_monad.Syntax.op_letplus (memo_size_eq ms1 ms2)
            (fun function_parameter => eq_refl)
        | With_family.Sapling_transaction_deprecated_t _, _ =>
          not_equal tt
        | With_family.Chest_t, With_family.Chest_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Chest_t, _ => not_equal tt
        | With_family.Chest_key_t, With_family.Chest_key_t =>
          Gas_monad.Syntax._return eq_refl
        | With_family.Chest_key_t, _ => not_equal tt
        end) in
  Gas_monad.record_trace_eval (Script_tc_errors.to_error_details error_details)
    (fun (loc_value : Alpha_context.Script.location) =>
      dep_default_ty_eq_error loc_value ty1 ty2) (help ty1 ty2).

(** Simulation of [stack_eq]. *)
Fixpoint dep_stack_eq {t k}
  (loc_value : Alpha_context.Script.location) (ctxt : Alpha_context.context)
  (lvl : int) (stack1 : With_family.stack_ty t)
  (stack2 : With_family.stack_ty k) :
  M? ((t = k) * Alpha_context.context) :=
  match stack1, stack2 with
  | With_family.Bot_t, With_family.Bot_t => return? (eq_refl, ctxt)
  | With_family.Item_t ty1 rest1, With_family.Item_t ty2 rest2 =>
    let? '(eq_value, ctxt) :=
      Error_monad.record_trace (Build_extensible "Bad_stack_item" int lvl)
        (Gas_monad.run ctxt
           (dep_ty_eq (Script_tc_errors.Dep_informative loc_value) ty1 ty2)) in
    let? 'eq_refl := eq_value in
    let? '(eq_value, ctxt) :=
      dep_stack_eq loc_value ctxt (lvl +i 1) rest1 rest2 in
    let 'eq_refl := eq_value in
    return? (eq_refl, ctxt)
  | _, _ =>
    Error_monad.error_value (Build_extensible "Bad_stack_length" unit tt)
  end.

(** Dependent version of [judgement]. *)
Inductive dep_judgement (s : Stack_ty.t) : Set :=
| Dep_typed {u} : dep_descr s u -> dep_judgement s
| Dep_failed :
  (forall {u}, With_family.stack_ty u -> dep_descr s u) ->
  dep_judgement s.
Arguments Dep_typed {_ _}.
Arguments Dep_failed {_}.

(** Dependent version of [branch]. *)
Module dep_branch.
  Record record {s u v : Stack_ty.t} : Set := {
    branch {f} : dep_descr s f -> dep_descr u f -> dep_descr v f;
  }.
  Arguments record : clear implicits.
End dep_branch.
Definition dep_branch := dep_branch.record.

(** Simulation of [merge_branches]. *)
Definition dep_merge_branches {s u v}
  (ctxt : Alpha_context.context) (loc_value : Alpha_context.Script.location)
  (btr : dep_judgement s) (bfr : dep_judgement u)
  (function_parameter : dep_branch s u v)
  : M? (dep_judgement v * Alpha_context.context) :=
  let '{| dep_branch.branch := dep_branch |} := function_parameter in
  match (btr, bfr) with
  |
    (Dep_typed ({| dep_descr.aft := aftbt |} as dbt),
      Dep_typed ({| dep_descr.aft := aftbf |} as dbf)) =>
      let unmatched_branches (function_parameter : unit) : Error_monad._error :=
        let '_ := function_parameter in
        let aftbt := dep_serialize_stack_for_error ctxt aftbt in
        let aftbf := dep_serialize_stack_for_error ctxt aftbf in
        Build_extensible "Unmatched_branches"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
             Script_tc_errors.unparsed_stack_ty) (loc_value, aftbt, aftbf) in
      Error_monad.record_trace_eval unmatched_branches
        (let? '(eq, ctxt) := dep_stack_eq loc_value ctxt 1 aftbt aftbf in
        let dbt := let 'eq_refl := eq in dbt in
         return?
           (Dep_typed (dep_branch _ dbt dbf), ctxt))
  |
    (Dep_failed descrt, Dep_failed descrf) =>
      let descr_value b (ret_value : With_family.stack_ty b) : dep_descr v b :=
        dep_branch _ (descrt _ ret_value) (descrf _ ret_value) in
      return? (Dep_failed descr_value, ctxt)
  | (Dep_typed dbt, Dep_failed descrf) =>
      return?
        ((Dep_typed (dep_branch _ dbt (descrf _ dbt.(dep_descr.aft)))), ctxt)
  | (Dep_failed descrt, Dep_typed dbf) =>
      return?
        ((Dep_typed (dep_branch _ (descrt _ dbf.(dep_descr.aft)) dbf)), ctxt)
  end.

(** Dependent version of [ex_comparable_ty]. *)
Inductive dep_ex_comparable_ty : Set :=
| Dep_ex_comparable_ty {t : Ty.t} (ty : With_family.ty t) :
  dep_ex_comparable_ty.

(** Conversion to [ex_comparable_ty]. *)
Definition to_ex_comparable_ty (v : dep_ex_comparable_ty) : ex_comparable_ty :=
  match v with
  | Dep_ex_comparable_ty ty => Ex_comparable_ty (With_family.to_ty ty)
  end.

(** We define this number as opaque to prevent unwanted evaluations. *)
Definition z_number_10001 : Z.t := 10001.
#[global] Opaque z_number_10001.

(** Translation of a [stack_depth] to a corresponding [fuel] for termination. *)
Definition stack_depth_to_fuel (stack_depth : int) : nat :=
  Z.to_nat (z_number_10001 - stack_depth).

(** Translation of a [fuel] to a corresponding [stack_depth] for termination. *)
Definition fuel_to_stack_depth (fuel : nat) : int :=
  z_number_10001 - (Z.of_nat fuel).

#[global] Hint Unfold stack_depth_to_fuel fuel_to_stack_depth : tezos_z.

(** Dependent version of [ex_ty]. *)
Inductive dep_ex_ty : Set :=
| Dep_ex_ty {t} : With_family.ty t -> dep_ex_ty.

(** Conversion back to [ex_ty] from [dep_ex_ty]. *)
Definition to_ex_ty (v : dep_ex_ty) : ex_ty :=
  match v with
  | Dep_ex_ty ty => Ex_ty (With_family.to_ty ty)
  end.

(** Dependent version of [ex_parameter_ty_and_entrypoints_node]. *)
Inductive dep_ex_parameter_ty_and_entrypoints_node : Set :=
| Dep_ex_parameter_ty_and_entrypoints_node {t} :
  With_family.ty t -> Script_typed_ir.entrypoints_node ->
  dep_ex_parameter_ty_and_entrypoints_node.

(** Conversion back to [ex_parameter_ty_and_entrypoints_node]. *)
Definition to_ex_parameter_ty_and_entrypoints_node
  (v : dep_ex_parameter_ty_and_entrypoints_node) :
  ex_parameter_ty_and_entrypoints_node :=
  match v with
  | Dep_ex_parameter_ty_and_entrypoints_node ty node =>
    Ex_parameter_ty_and_entrypoints_node
      {|
        ex_parameter_ty_and_entrypoints_node
          .Ex_parameter_ty_and_entrypoints_node.arg_type := With_family.to_ty ty;
        ex_parameter_ty_and_entrypoints_node
          .Ex_parameter_ty_and_entrypoints_node.entrypoints := node;
      |}
  end.

Module Parse_ty_ret_family.
  (** A type family for the GADT [parse_ty_ret]. *)
  Inductive t : Set :=
  | Don't_parse_entrypoints
  | Parse_entrypoints.

  (** The [ret] parameter of the GADT [parse_ty_ret] using dependent types. *)
  Definition to_ret_dep_Set (f : t) : Set :=
    match f with
    | Don't_parse_entrypoints => dep_ex_ty
    | Parse_entrypoints => dep_ex_parameter_ty_and_entrypoints_node
    end.

  (** The [ret] parameter of the GADT [parse_ty_ret] using OCaml types. *)
  Definition to_ret_Set (f : t) : Set :=
    match f with
    | Don't_parse_entrypoints => ex_ty
    | Parse_entrypoints => ex_parameter_ty_and_entrypoints_node
    end.

  (** The [name] parameter of the GADT [parse_ty_ret]. *)
  Definition to_name_Set (f : t) : Set :=
    match f with
    | Don't_parse_entrypoints => unit
    | Parse_entrypoints => option Alpha_context.Entrypoint.t
    end.

  (** Conversion back to [parse_ty_ret]. *)
  Definition to_parse_ty_ret (f : t) : parse_ty_ret :=
    match f with
    | Don't_parse_entrypoints => Script_ir_translator.Don't_parse_entrypoints
    | Parse_entrypoints => Script_ir_translator.Parse_entrypoints
    end.

  (** Conversion of the [ret] GADT parameter back to the OCaml version. *)
  Definition to_ret (f : t) (v : to_ret_dep_Set f) : to_ret_Set f :=
    match f, v with
    | Don't_parse_entrypoints, _ => to_ex_ty v
    | Parse_entrypoints, _ => to_ex_parameter_ty_and_entrypoints_node v
    end.
End Parse_ty_ret_family.

Reserved Notation "'dep_parse_passable_ty_aux_with_ret_fuel".
Reserved Notation "'dep_parse_any_ty_aux_fuel".
Reserved Notation "'dep_parse_big_map_value_ty_aux_fuel".
Reserved Notation "'dep_parse_big_map_ty_fuel".

(** Simulation of [parse_ty_aux] with fuel. *)
Fixpoint dep_parse_ty_aux_fuel
  (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool)
  (allow_lazy_storage : bool) (allow_operation : bool) (allow_contract : bool)
  (allow_ticket : bool) (ret_value : Parse_ty_ret_family.t)
  (node_value : Alpha_context.Script.node) {struct fuel}
  : M? (Parse_ty_ret_family.to_ret_dep_Set ret_value * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
  match fuel with
  | Datatypes.O =>
    Error_monad.error_value
      (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
  | Datatypes.S fuel =>
    let? '(node_value, name) :=
      match ret_value with
      | Parse_ty_ret_family.Don't_parse_entrypoints =>
        return? (node_value, None)
      | Parse_ty_ret_family.Parse_entrypoints =>
        Script_ir_annot.extract_entrypoint_annot node_value
      end in
    let _return {t} (ctxt : Alpha_context.context) (ty : With_family.ty t)
      : Parse_ty_ret_family.to_ret_dep_Set ret_value * Alpha_context.context :=
      match ret_value with
      | Parse_ty_ret_family.Don't_parse_entrypoints => (Dep_ex_ty ty, ctxt)
      | Parse_ty_ret_family.Parse_entrypoints =>
         let node_info :=
            match name with
            | Some name' => Some
                {|
                  Script_typed_ir.entrypoint_info.name := name';
                  Script_typed_ir.entrypoint_info.original_type_expr :=
                  node_value;
                |}
            | None => None
            end
          in
          (Dep_ex_parameter_ty_and_entrypoints_node
             ty
               {|
                 Script_typed_ir.entrypoints_node.at_node := node_info;
                 Script_typed_ir.entrypoints_node.nested :=
                 Script_typed_ir.Entrypoints_None
               |}
           , ctxt)
      end in
    match
      (node_value,
        match node_value with
        | Micheline.Prim loc_value Michelson_v1_primitives.T_big_map
                         args annot
          => allow_lazy_storage
        | _ => false
        end,
        match node_value with
        |
          Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_state
                         (cons memo_size []) annot => allow_lazy_storage
        | _ => false
        end) with
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_unit [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Unit_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_int [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Int_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_nat [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Nat_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_string [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.String_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bytes [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Bytes_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_mutez [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Mutez_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bool [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Bool_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_key [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Key_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_key_hash [] annot, _,
        _) =>
       let? '_ := Script_ir_annot.check_type_annot loc_value annot in
       return? (_return ctxt With_family.Key_hash_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chest_key [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt With_family.Chest_key_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chest [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Chest_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_timestamp [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt With_family.Timestamp_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_address [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Address_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_tx_rollup_l2_address
        [] annot, _, _) =>
      if Alpha_context.Constants.tx_rollup_enable ctxt then
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Tx_rollup_l2_address_t)
      else
        Error_monad.error_value
          (Build_extensible "Tx_rollup_addresses_disabled"
            Alpha_context.Script.location loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_signature [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt With_family.Signature_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_operation [] annot, _,
        _) =>
        if allow_operation then
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          return?
            (_return ctxt With_family.Operation_t)
        else Error_monad.error_value
          (Build_extensible "Unexpected_operation" Alpha_context.Script.location
            loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chain_id [] annot, _,
        _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
       return? (_return ctxt With_family.Chain_id_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_never [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt With_family.Never_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_g1 [] annot,
        _, _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return?
         (_return ctxt Simulations.Script_typed_ir.With_family.Bls12_381_g1_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_g2 [] annot,
        _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return?
         (_return ctxt With_family.Bls12_381_g2_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_fr [] annot,
        _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return?
          (_return ctxt With_family.Bls12_381_fr_t)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_contract (cons utl [])
        annot, _, _) =>
        if allow_contract then
          let? '(res,ctxt) := 'dep_parse_passable_ty_aux_with_ret_fuel ctxt
                               fuel legacy
                               Parse_ty_ret_family.Don't_parse_entrypoints
                               utl in
          let '(Dep_ex_ty tl) := res in
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          let? cont_check := Script_typed_ir.contract_t
              loc_value (Simulations.Script_typed_ir.With_family.to_ty tl) in
          let md := Script_typed_ir.ty_metadata_value (cont_check) in
          return? (_return ctxt
              (Simulations.Script_typed_ir.With_family.Contract_t
                tl md))
        else Error_monad.error_value
           (Build_extensible "Unexpected_contract" Alpha_context.Script.location
                                 loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_pair (cons utl utr)
                      annot, _, _) =>
        let? utl := Script_ir_annot.remove_field_annot utl in
        let? '(res1,ctxt) :=
          dep_parse_ty_aux_fuel ctxt fuel
            legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket
            Parse_ty_ret_family.Don't_parse_entrypoints utl in
        let? utr :=
          match utr with
          | cons utr [] => Script_ir_annot.remove_field_annot utr
          | utr => return?
            (Micheline.Prim loc_value Michelson_v1_primitives.T_pair utr nil)
          end in
        let? '(res2,ctxt) :=
          dep_parse_ty_aux_fuel ctxt fuel
            legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket
            Parse_ty_ret_family.Don't_parse_entrypoints utr in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        match res1, res2 with
        | Dep_ex_ty tl, Dep_ex_ty tr => let? '_ :=
            Script_ir_annot.check_type_annot loc_value annot in
            let? 'Script_typed_ir.Ty_ex_c res_ty := Script_typed_ir.pair_t
              loc_value (With_family.to_ty tl)
                        (With_family.to_ty tr) in
            let md := Script_typed_ir.ty_metadata_value (res_ty) in
            let 'Dependent_bool.Ex_dand cmp :=
                  Dependent_bool.dand_value
                    (Script_typed_ir.is_comparable (With_family.to_ty tl))
                    (Script_typed_ir.is_comparable (With_family.to_ty tr)) in
           return? (_return ctxt (With_family.Pair_t
           tl tr md cmp))
        end
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_or
                      (cons utl (cons utr [])) annot, _, _) =>
        let? '(utl, utr) :=
          match ret_value with
          | Parse_ty_ret_family.Don't_parse_entrypoints =>
              let? utl := Script_ir_annot.remove_field_annot utl in
              let? utr := Script_ir_annot.remove_field_annot utr in
              return? (utl, utr)
          | Parse_ty_ret_family.Parse_entrypoints => return? (utl, utr)
          end in
        let? '(parsed_l, ctxt) :=
          dep_parse_ty_aux_fuel ctxt fuel
            legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket ret_value utl in
        let? '(parsed_r, ctxt) :=
          dep_parse_ty_aux_fuel ctxt fuel legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket
            ret_value utr in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        (match ret_value as rv
          return
            Parse_ty_ret_family.to_ret_dep_Set rv ->
            Parse_ty_ret_family.to_ret_dep_Set rv ->
            M? (Parse_ty_ret_family.to_ret_dep_Set rv * _)
        with
        | Parse_ty_ret_family.Don't_parse_entrypoints =>
             (fun parsed_l parsed_r =>
                let 'Dep_ex_ty tl := parsed_l in
                let 'Dep_ex_ty tr := parsed_r in
                let? 'Script_typed_ir.Ty_ex_c res_ty :=
                  Script_typed_ir.union_t
                    loc_value (With_family.to_ty tl)
                              (With_family.to_ty tr)
                in
                let md := Script_typed_ir.ty_metadata_value (res_ty) in
                let 'Dependent_bool.Ex_dand cmp :=
                  Dependent_bool.dand_value
                    (Script_typed_ir.is_comparable (With_family.to_ty tl))
                    (Script_typed_ir.is_comparable (With_family.to_ty tr)) in
                return?
                 ((Dep_ex_ty
                 (With_family.Union_t tl tr
                 md cmp)), ctxt))
        | Parse_ty_ret_family.Parse_entrypoints =>
             (fun parsed_l parsed_r =>
                let 'Dep_ex_parameter_ty_and_entrypoints_node tl ep_l := parsed_l in
                let 'Dep_ex_parameter_ty_and_entrypoints_node tr ep_r := parsed_r in
                let? 'Script_typed_ir.Ty_ex_c res_ty :=
                  Script_typed_ir.union_t
                    loc_value (Simulations.Script_typed_ir.With_family.to_ty tl)
                              (Simulations.Script_typed_ir.With_family.to_ty tr)
                in
                let md := Script_typed_ir.ty_metadata_value (res_ty) in
                let 'Dependent_bool.Ex_dand cmp :=
                  Dependent_bool.dand_value
                    (Script_typed_ir.is_comparable (With_family.to_ty tl))
                    (Script_typed_ir.is_comparable (With_family.to_ty tr)) in
                 (let arg_type :=
                    (With_family.Union_t tl tr md cmp) in
                  let node_info :=
                    match name with
                    | Some name' => Some
                      {| Script_typed_ir.entrypoint_info.name := name';
                        Script_typed_ir.entrypoint_info.original_type_expr :=
                        node_value; |}
                    | None => None
                    end in
                  let entrypoints :=
                    {| Script_typed_ir.entrypoints_node.at_node := node_info;
                      Script_typed_ir.entrypoints_node.nested :=
                      Script_typed_ir.Entrypoints_Union
                        {|
                          Script_typed_ir.nested_entrypoints
                            .Entrypoints_Union._left := ep_l;
                          Script_typed_ir.nested_entrypoints
                            .Entrypoints_Union._right := ep_r; |} |} in
                   return?
               (Dep_ex_parameter_ty_and_entrypoints_node arg_type entrypoints, ctxt)))
         end parsed_l parsed_r)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_lambda
                      (cons uta (cons utr [])) annot, _, _) =>
        let? '(res1, ctxt) :=
          'dep_parse_any_ty_aux_fuel ctxt fuel legacy uta in
        let '(Dep_ex_ty ta) := res1 in
        let? '(res2, ctxt) :=
          'dep_parse_any_ty_aux_fuel ctxt fuel legacy utr in
        let '(Dep_ex_ty tr) := res2 in
        let? '_ :=
          Script_ir_annot.check_type_annot loc_value annot in
        let? lambda_check := Script_typed_ir.lambda_t
                               loc_value (With_family.to_ty ta)
                                (With_family.to_ty tr)
        in
        let md := Script_typed_ir.ty_metadata_value (lambda_check) in
        return? (_return ctxt
          (Simulations.Script_typed_ir.With_family.Lambda_t ta tr md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_option (cons ut [])
                      annot, _, _) =>
        let? ut :=
          if legacy then
            let? ut := Script_ir_annot.remove_field_annot ut in
            let? '_ := Script_ir_annot.check_composed_type_annot loc_value annot
            in return? ut
          else
            let? '_ := Script_ir_annot.check_type_annot loc_value annot in
            return? ut in
        let? '(res,ctxt) := dep_parse_ty_aux_fuel ctxt
                              fuel legacy allow_lazy_storage
                              allow_operation allow_contract allow_ticket
                              Parse_ty_ret_family.Don't_parse_entrypoints ut in
        let '(Dep_ex_ty tl) := res in
        let? option_check := Script_typed_ir.option_t
            loc_value (With_family.to_ty tl) in
        let md := Script_typed_ir.ty_metadata_value (option_check) in
        return? (_return ctxt (With_family.Option_t
                                 tl md (Script_typed_ir.is_comparable
                                          (With_family.to_ty tl))))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_list (cons ut [])
                      annot, _, _) =>
        let? '(res,ctxt) :=  dep_parse_ty_aux_fuel ctxt fuel legacy
                             allow_lazy_storage
                             allow_operation allow_contract allow_ticket
                             Parse_ty_ret_family.Don't_parse_entrypoints ut in
        let '(Dep_ex_ty tl) := res in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          let? list_check := Script_typed_ir.list_t
              loc_value (Simulations.Script_typed_ir.With_family.to_ty tl) in
          let md := Script_typed_ir.ty_metadata_value (list_check) in
        return? (_return ctxt
                  (Simulations.Script_typed_ir.With_family.List_t tl md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_ticket (cons ut [])
                      annot, _, _) =>
        if allow_ticket then
          let? '(Dep_ex_comparable_ty t_value, ctxt) :=
            dep_parse_comparable_ty_aux_fuel fuel ctxt ut in
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          let? res_ty := Script_typed_ir.ticket_t
              loc_value (With_family.to_ty t_value) in
          let md := Script_typed_ir.ty_metadata_value res_ty in
          return? (_return ctxt
                     (With_family.Ticket_t
                     t_value md))
        else
          Error_monad.error_value
            (Build_extensible "Unexpected_ticket" Alpha_context.Script.location
                              loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_set
                      (cons ut []) annot, _, _) =>
        let? '(Dep_ex_comparable_ty t_value, ctxt) :=
          dep_parse_comparable_ty_aux_fuel fuel ctxt ut in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? unf_check := Script_typed_ir.set_t
        loc_value (With_family.to_ty t_value) in
        let md := Script_typed_ir.ty_metadata_value unf_check in
        return? (_return ctxt (With_family.Set_t
                   t_value md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_map
                      (cons uta (cons utr [])) annot, _, _) =>
        let? '(Dep_ex_comparable_ty ta, ctxt) :=
          dep_parse_comparable_ty_aux_fuel fuel ctxt uta in
        let? '(ret, ctxt) :=
          dep_parse_ty_aux_fuel ctxt fuel legacy allow_lazy_storage
                           allow_operation allow_contract allow_ticket
                           Parse_ty_ret_family.Don't_parse_entrypoints
                           utr in
        let '(Dep_ex_ty tr) := ret in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in

        let? unf_check := Script_typed_ir.map_t
                            loc_value
                            (With_family.to_ty ta)
                            (With_family.to_ty tr) in
        let md := Script_typed_ir.ty_metadata_value (unf_check) in
        return?
          (_return ctxt
             (Simulations.Script_typed_ir.With_family.Map_t ta tr md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_transaction
                      (cons memo_size []) annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? memo_size := parse_memo_size memo_size in
        return? (_return ctxt
                         (With_family.Sapling_transaction_t memo_size))
    | (Micheline.Prim loc_value
        Michelson_v1_primitives.T_sapling_transaction_deprecated
        (cons memo_size []) annot, _, _) =>
      if legacy then
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? memo_size := parse_memo_size memo_size in
        return?
          (_return ctxt
            (With_family.Sapling_transaction_deprecated_t memo_size))
      else
        Error_monad.error_value
          (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
            Michelson_v1_primitives.T_sapling_transaction_deprecated)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_big_map args annot,
        true, _) =>
        let? '(res, ctxt) :=
          dep_parse_big_map_ty_fuel ctxt fuel legacy loc_value args annot in
        let '(Dep_ex_ty ty) := res in return? (_return ctxt ty)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_state
                      (cons memo_size []) annot, _, true) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? memo_size := parse_memo_size memo_size in
        return? (_return ctxt
          (With_family.Sapling_state_t memo_size ))
    | (Micheline.Prim loc_value
                      (Michelson_v1_primitives.T_big_map |
                      Michelson_v1_primitives.T_sapling_state) _ _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Unexpected_lazy_storage"
                            Alpha_context.Script.location loc_value)
    | (Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_signature |
          Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
          Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
          Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
          Michelson_v1_primitives.T_key | Michelson_v1_primitives.T_key_hash |
          Michelson_v1_primitives.T_timestamp |
           Michelson_v1_primitives.T_address |
           Michelson_v1_primitives.T_tx_rollup_l2_address |
          Michelson_v1_primitives.T_chain_id |
          Michelson_v1_primitives.T_operation | Michelson_v1_primitives.T_never)
            as prim) l_value _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc_value, prim, 0, (List.length l_value)))
    | (Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_list |
          Michelson_v1_primitives.T_option | Michelson_v1_primitives.T_contract |
          Michelson_v1_primitives.T_ticket) as prim) l_value _, _, _) =>
        Error_monad.error_value
        (Build_extensible "Invalid_arity"
         (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
            (loc_value, prim, 1, (List.length l_value)))
    | (Micheline.Prim loc_value
      ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or |
      Michelson_v1_primitives.T_map | Michelson_v1_primitives.T_lambda) as
           prim) l_value _, _, _) =>
        Error_monad.error_value
      (Build_extensible "Invalid_arity"
      (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
      (loc_value, prim, 2, (List.length l_value)))
    | (expr, _, _) =>
        Error_monad.error_value
          (unexpected expr nil Michelson_v1_primitives.Type_namespace
            [Michelson_v1_primitives.T_bls12_381_fr;
             Michelson_v1_primitives.T_bls12_381_g1;
             Michelson_v1_primitives.T_bls12_381_g2;
             Michelson_v1_primitives.T_bool;
             Michelson_v1_primitives.T_bytes;
             Michelson_v1_primitives.T_chain_id;
             Michelson_v1_primitives.T_contract;
             Michelson_v1_primitives.T_int;
             Michelson_v1_primitives.T_key;
             Michelson_v1_primitives.T_key_hash;
             Michelson_v1_primitives.T_lambda;
             Michelson_v1_primitives.T_list;
             Michelson_v1_primitives.T_map;
             Michelson_v1_primitives.T_mutez;
             Michelson_v1_primitives.T_nat;
             Michelson_v1_primitives.T_never;
             Michelson_v1_primitives.T_operation;
             Michelson_v1_primitives.T_option;
             Michelson_v1_primitives.T_or;
             Michelson_v1_primitives.T_pair;
             Michelson_v1_primitives.T_set;
             Michelson_v1_primitives.T_signature;
             Michelson_v1_primitives.T_string;
             Michelson_v1_primitives.T_ticket;
             Michelson_v1_primitives.T_timestamp;
             Michelson_v1_primitives.T_tx_rollup_l2_address;
             Michelson_v1_primitives.T_unit])
    end
end

with dep_parse_comparable_ty_aux_fuel (fuel : nat)
  (ctxt : Alpha_context.context) (node_value : Alpha_context.Script.node) :
  M? (dep_ex_comparable_ty * Alpha_context.context) :=
  match fuel with
  | Datatypes.O =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
    Error_monad.error_value
      (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
  | Datatypes.S fuel =>
    let? '(Dep_ex_ty t_value, ctxt) :=
      dep_parse_ty_aux_fuel ctxt fuel false false false false false
        Parse_ty_ret_family.Don't_parse_entrypoints node_value in
    match Script_typed_ir.dep_is_comparable t_value with
    | Dependent_bool.Yes => return? ((Dep_ex_comparable_ty t_value), ctxt)
    | Dependent_bool.No =>
      Error_monad.error_value
        (Build_extensible "Comparable_type_expected"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim)
          ((location node_value), (Micheline.strip_locations node_value)))
    end
  end

with dep_parse_big_map_ty_fuel (ctxt : Alpha_context.context) (fuel : nat)
  (legacy : bool) (big_map_loc : Alpha_context.Script.location)
  (args : list
    (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  )
  (map_annot : Micheline.annot) {struct fuel} :
  M? (dep_ex_ty * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
  match args with
  | cons key_ty (cons value_ty []) =>
    (* This gas consume is not there in the OCaml code, and just there to check
       that there is enough gas and simplify the simulation proof. *)
    let? _ :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
    match fuel with
    | Datatypes.O =>
      Error_monad.error_value
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
      let? '(Dep_ex_comparable_ty key_ty, ctxt) :=
        dep_parse_comparable_ty_aux_fuel fuel ctxt key_ty in
      let? '(res, ctxt) :=
        'dep_parse_big_map_value_ty_aux_fuel ctxt fuel
          legacy value_ty in
      let ' (Dep_ex_ty value_ty) := res in
      let? '_ := Script_ir_annot.check_type_annot big_map_loc map_annot in
      let? map_check :=
        Script_typed_ir.big_map_t
          big_map_loc
          (Simulations.Script_typed_ir.With_family.to_ty key_ty)
          (Simulations.Script_typed_ir.With_family.to_ty value_ty) in
      let md := Script_typed_ir.ty_metadata_value (map_check) in
      return? (
        (Dep_ex_ty (
          Simulations.Script_typed_ir.With_family.Big_map_t key_ty value_ty md)
        ),
        ctxt
      )
    end
  | args =>
    Error_monad.error_value
      (Build_extensible
        "Invalid_arity"
        (Alpha_context.Script.location *
          Alpha_context.Script.prim * int * int)
        (big_map_loc, Michelson_v1_primitives.T_big_map,
          2, (List.length args)))
  end

where "'dep_parse_passable_ty_aux_with_ret_fuel" :=
  (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool) =>
     ((dep_parse_ty_aux_fuel ctxt fuel legacy true false true true) :
        forall (r : Parse_ty_ret_family.t), Alpha_context.Script.node ->
          M? (Parse_ty_ret_family.to_ret_dep_Set r * Alpha_context.context)))

and "'dep_parse_any_ty_aux_fuel" :=
  (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool) =>
     ((dep_parse_ty_aux_fuel ctxt fuel legacy true true true true
      Parse_ty_ret_family.Don't_parse_entrypoints) :
       Alpha_context.Script.node ->
       M? (dep_ex_ty * Alpha_context.context)))

and "'dep_parse_big_map_value_ty_aux_fuel" :=
      (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool)
         (value_ty :
           Micheline.node
             Alpha_context.Script.location Alpha_context.Script.prim) =>
          ((dep_parse_ty_aux_fuel ctxt fuel legacy false false legacy true
            Parse_ty_ret_family.Don't_parse_entrypoints value_ty) :
            M? (dep_ex_ty * Alpha_context.context))).

(** Simulation of [parse_ty_aux]. *)
Definition dep_parse_ty_aux
  (ctxt : Alpha_context.context) (stack_depth : int) :=
  dep_parse_ty_aux_fuel ctxt (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_comparable_ty_aux]. *)
Definition dep_parse_comparable_ty_aux (ctxt : Alpha_context.context)
  (stack_depth : int) (node_value : Alpha_context.Script.node) :
  M? (dep_ex_comparable_ty * Alpha_context.context) :=
  dep_parse_comparable_ty_aux_fuel (stack_depth_to_fuel stack_depth) ctxt
    node_value.

(** Simulation of [parse_big_map_ty]. *)
Definition dep_parse_big_map_ty
  (ctxt : Alpha_context.context) (stack_depth : int) :=
  dep_parse_big_map_ty_fuel ctxt (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_passable_ty_aux_with_ret]. *)
Definition dep_parse_passable_ty_aux_with_ret
  (ctxt : Alpha_context.context) (stack_depth : int) :=
  'dep_parse_passable_ty_aux_with_ret_fuel
    ctxt (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_any_ty_aux]. *)
Definition dep_parse_any_ty_aux
  (ctxt : Alpha_context.context) (stack_depth : int) :=
  'dep_parse_any_ty_aux_fuel ctxt (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_big_map_value_ty_aux]. *)
Definition dep_parse_big_map_value_ty_aux
  (ctxt : Alpha_context.context) (stack_depth : int) :=
  'dep_parse_big_map_value_ty_aux_fuel ctxt (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_packable_ty_aux]. *)
Definition dep_parse_packable_ty_aux
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_ty * Alpha_context.context) :=
  dep_parse_ty_aux ctxt stack_depth legacy false false legacy false
    Parse_ty_ret_family.Don't_parse_entrypoints node_value.

(** Simulation of [parse_view_input_ty]. *)
Definition dep_parse_view_input_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_ty * Alpha_context.context) :=
  dep_parse_ty_aux ctxt stack_depth legacy false false true false
    Parse_ty_ret_family.Don't_parse_entrypoints node_value.

(** Simulation of [parse_view_output_ty]. *)
Definition dep_parse_view_output_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_ty * Alpha_context.context) :=
  dep_parse_ty_aux ctxt stack_depth legacy false false true false
    Parse_ty_ret_family.Don't_parse_entrypoints node_value.

(** Simulation of [parse_normal_storage_ty]. *)
Definition dep_parse_normal_storage_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_ty * Alpha_context.context) :=
  dep_parse_ty_aux ctxt stack_depth legacy true false legacy true
    Parse_ty_ret_family.Don't_parse_entrypoints node_value.

(** Simulation of [parse_storage_ty]. *)
Definition dep_parse_storage_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_ty * Alpha_context.context) :=
   (match
    node_value,
      match node_value with
      |
        Micheline.Prim loc_value Michelson_v1_primitives.T_pair
          (cons
            (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
              map_annot) (cons remaining_storage [])) storage_annot => legacy
      | _ => false
      end with
  | Micheline.Prim loc_value Michelson_v1_primitives.T_pair
      (cons
        (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
          map_annot) (cons remaining_storage [])) storage_annot, true =>
    match
      storage_annot,
        match storage_annot with
        | cons single [] =>
          ((String.length single) >i 0) &&
          (Compare.Char.(Compare.S.op_eq) (String.get single 0) "%" % char)
        | _ => false
        end with
    | [], _ => dep_parse_normal_storage_ty ctxt stack_depth legacy node_value
    | cons single [], true =>
      dep_parse_normal_storage_ty ctxt stack_depth legacy node_value
    | _, _ =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
      let? '(Dep_ex_ty big_map_ty, ctxt) :=
        dep_parse_big_map_ty ctxt (stack_depth +i 1) legacy big_map_loc args
          map_annot in
      let? '(Dep_ex_ty remaining_storage, ctxt) :=
        dep_parse_normal_storage_ty ctxt (stack_depth +i 1) legacy remaining_storage
        in
      let? '_ :=
        Script_ir_annot.check_composed_type_annot loc_value storage_annot in
      let? 'Script_typed_ir.Dep_ty_ex_c res_ty := Script_typed_ir.dep_pair_t
        loc_value (big_map_ty)
                  (remaining_storage) in
      let md := Script_typed_ir.ty_metadata_value (With_family.to_ty res_ty) in
      let 'Dependent_bool.Ex_dand cmp :=
        Dependent_bool.dand_value
          (Script_typed_ir.dep_is_comparable (big_map_ty))
          (Script_typed_ir.dep_is_comparable (remaining_storage)) in
      return? (Dep_ex_ty
        (With_family.Pair_t big_map_ty remaining_storage md cmp), ctxt)
    end
  | _, _ => dep_parse_normal_storage_ty ctxt stack_depth legacy node_value
  end).

(** Simulation of [check_packable]. *)
Definition dep_check_packable {t}
  (legacy : bool) (loc_value : Alpha_context.Script.location)
  (root_value : With_family.ty t) : M? unit :=
  let fix check {t'} (ty : With_family.ty t') :=
    match ty with
    | With_family.Big_map_t _ _ _ =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc_value)
    | With_family.Sapling_state_t _ =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc_value)
    | With_family.Operation_t =>
      Error_monad.error_value
        (Build_extensible "Unexpected_operation" Alpha_context.Script.location
          loc_value)
    | With_family.Unit_t => Result.return_unit
    | With_family.Int_t => Result.return_unit
    | With_family.Nat_t => Result.return_unit
    | With_family.Signature_t => Result.return_unit
    | With_family.String_t => Result.return_unit
    | With_family.Bytes_t => Result.return_unit
    | With_family.Mutez_t => Result.return_unit
    | With_family.Key_hash_t => Result.return_unit
    | With_family.Key_t => Result.return_unit
    | With_family.Timestamp_t => Result.return_unit
    | With_family.Address_t => Result.return_unit
    | With_family.Tx_rollup_l2_address_t => Result.return_unit
    | With_family.Bool_t => Result.return_unit
    | With_family.Chain_id_t => Result.return_unit
    | With_family.Never_t => Result.return_unit
    | With_family.Set_t _ _ => Result.return_unit
    | With_family.Ticket_t _ _ =>
      Error_monad.error_value
        (Build_extensible "Unexpected_ticket" Alpha_context.Script.location
          loc_value)
    | With_family.Lambda_t _ _ _ => Result.return_unit
    | With_family.Bls12_381_g1_t => Result.return_unit
    | With_family.Bls12_381_g2_t => Result.return_unit
    | With_family.Bls12_381_fr_t => Result.return_unit
    | With_family.Pair_t l_ty r_ty _ _ =>
      let? '_ := check l_ty in
      check r_ty
    | With_family.Union_t l_ty r_ty _ _ =>
      let? '_ := check l_ty in
      check r_ty
    | With_family.Option_t v_ty _ _ =>
      check v_ty
    | With_family.List_t elt_ty _ =>
      check elt_ty
    | With_family.Map_t _ elt_ty _ =>
      check elt_ty
    | With_family.Contract_t _ _ =>
      match legacy with
      | true => Result.return_unit
      | false =>
        Error_monad.error_value
          (Build_extensible "Unexpected_contract" Alpha_context.Script.location
            loc_value)
      end
    | With_family.Sapling_transaction_t _ => return? tt
    | With_family.Sapling_transaction_deprecated_t _ => return? tt
    | With_family.Chest_key_t => Result.return_unit
    | With_family.Chest_t => Result.return_unit
    end in
  check root_value.

(** Dependent version of [toplevel]. *)
Module dep_toplevel.
  Record record: Set := Build {
    code_field : Alpha_context.Script.node;
    arg_type : Alpha_context.Script.node;
    storage_type : Alpha_context.Script.node;
    views: With_family.view_map }.
    Arguments record : clear implicits.
End dep_toplevel.
Definition dep_toplevel := dep_toplevel.record.

(** Conversion back to [toplevel]. *)
Definition to_toplevel (d : dep_toplevel) : toplevel :=
  (let '{|
      dep_toplevel.code_field := code_field;
      dep_toplevel.arg_type := arg_type;
      dep_toplevel.storage_type := storage_type;
      dep_toplevel.views := views
    |} := d in
    {|
      toplevel.code_field := code_field;
      toplevel.arg_type := arg_type;
      toplevel.storage_type := storage_type;
      toplevel.views := With_family.to_view_map views
    |}).

(** Dependent version of [code]. *)
Module dep_code.
  Record record {arg storage : Ty.t} : Set := {
    code :
      With_family.lambda
        (Ty.Pair arg storage) (Ty.Pair (Ty.List Ty.Operation) storage);
    arg_type : With_family.ty arg;
    storage_type : With_family.ty storage;
    views : Script_typed_ir.view_map;
    entrypoints : Script_typed_ir.entrypoints;
    code_size : Cache_memory_helpers.sint;
  }.
  Arguments record : clear implicits.
End dep_code.
Definition dep_code := dep_code.record.

(** Conversion back to [code]. *)
Definition to_code {arg storage} (x : dep_code arg storage) : code :=
  Code {|
    code.Code.code := With_family.to_lambda x.(dep_code.code);
    code.Code.arg_type := With_family.to_ty x.(dep_code.arg_type);
    code.Code.storage_type := With_family.to_ty x.(dep_code.storage_type);
    code.Code.views := x.(dep_code.views);
    code.Code.entrypoints := x.(dep_code.entrypoints);
    code.Code.code_size := x.(dep_code.code_size);
  |}.

(** Dependent version of [ex_script]. *)
Inductive dep_ex_script : Set :=
| Dep_ex_script {arg storage} : With_family.script arg storage -> dep_ex_script.

(** Conversion back to [ex_script]. *)
Definition to_ex_script (v : dep_ex_script) : ex_script :=
  match v with
  | Dep_ex_script script => Ex_script (With_family.to_script script)
  end.

(** Dependent version of [ex_code]. *)
Inductive dep_ex_code : Set :=
| Dep_ex_code {arg storage} : dep_code arg storage -> dep_ex_code.

(** Conversion back to [ex_code]. *)
Definition to_ex_code (x : dep_ex_code) : ex_code :=
  match x with
  | Dep_ex_code x => Ex_code (to_code x)
  end.

(** Dependent version of [typed_view.Typed_view]. *)
Module dep_typed_view.
  Module Dep_typed_view.
    Record record {storage input output} : Set := {
      input_ty : With_family.ty input;
      output_ty : With_family.ty output;
      kinstr : With_family.kinstr [Ty.Pair input storage] [output];
      original_code_expr : Alpha_context.Script.node;
    }.
    Arguments record : clear implicits.
  End Dep_typed_view.
  Definition Dep_typed_view := Dep_typed_view.record.
End dep_typed_view.

(** Dependent version of [typed_view]. *)
Inductive dep_typed_view (storage : Ty.t) : Set :=
| Dep_typed_view {input output} :
  dep_typed_view.Dep_typed_view storage input output ->
  dep_typed_view storage.
Arguments Dep_typed_view {_ _ _}.

(** Conversion back to [typed_view]. *)
Definition to_typed_view {storage} (x : dep_typed_view storage) : typed_view :=
  let 'Dep_typed_view {|
    dep_typed_view.Dep_typed_view.input_ty := input_ty;
    dep_typed_view.Dep_typed_view.output_ty := output_ty;
    dep_typed_view.Dep_typed_view.kinstr := kinstr;
    dep_typed_view.Dep_typed_view.original_code_expr := original_code_expr;
  |} := x in
  Typed_view {|
    typed_view.Typed_view.input_ty := With_family.to_ty input_ty;
    typed_view.Typed_view.output_ty := With_family.to_ty output_ty;
    typed_view.Typed_view.kinstr := With_family.to_kinstr kinstr;
    typed_view.Typed_view.original_code_expr := original_code_expr;
  |}.

(** Dependent version of [typed_view_map]. *)
Definition dep_typed_view_map (storage : Ty.t) : Set :=
  With_family.map Ty.String (dep_typed_view storage).

(** Conversion back to [typed_view_map]. *)
Definition to_typed_view_map {storage} (x : dep_typed_view_map storage) :
  typed_view_map :=
  With_family.to_map_aux to_typed_view x.

(** Dependent version of [dig_proof_argument]. *)
Inductive dep_dig_proof_argument (t : Stack_ty.t) : Set :=
| Dep_dig_proof_argument {x s u} :
  With_family.stack_prefix_preservation_witness (x :: s) s t u->
  With_family.ty x ->
  With_family.stack_ty u ->
  dep_dig_proof_argument t.
Arguments Dep_dig_proof_argument {_ _ _ _}.

(** Dependent version of [dug_proof_argument]. *)
Inductive dep_dug_proof_argument (t : Stack_ty.t) (x : Ty.t) : Set :=
| Dep_dug_proof_argument {s u} :
  With_family.stack_prefix_preservation_witness s (x :: s) t u ->
  With_family.stack_ty u ->
  dep_dug_proof_argument t x.
Arguments Dep_dug_proof_argument {_ _ _ _}.

(** Conversion back to [dug_proof_argument]. *)
Definition to_dug_proof_argument {t x} (x : dep_dug_proof_argument t x) :
  dug_proof_argument :=
  match x with
  | Dep_dug_proof_argument stack_prefix stack_ty =>
    Dug_proof_argument (
      With_family.to_stack_prefix_preservation_witness stack_prefix,
      With_family.to_stack_ty stack_ty
    )
  end.

(** Dependent version of [dipn_proof_argument]. *)
Inductive dep_dipn_proof_argument (s : Stack_ty.t) : Set :=
| Dep_dipn_proof_argument {fs fu u} :
  With_family.stack_prefix_preservation_witness fs fu s u ->
  Alpha_context.context ->
  dep_descr fs fu ->
  With_family.stack_ty u ->
  dep_dipn_proof_argument s.
Arguments Dep_dipn_proof_argument {_ _ _ _}.

(** Dependent version of [dropn_proof_argument]. *)
Inductive dep_dropn_proof_argument (s : Stack_ty.t) : Set :=
| Dep_dropn_proof_argument {fs} :
  With_family.stack_prefix_preservation_witness fs fs s s ->
  With_family.stack_ty fs ->
  dep_dropn_proof_argument s.
Arguments Dep_dropn_proof_argument {_ _}.

(** Dependent version of [comb_proof_argument]. *)
Inductive dep_comb_proof_argument (s : Stack_ty.t) : Set :=
| Dep_comb_proof_argument {u} :
  With_family.comb_gadt_witness s u ->
  With_family.stack_ty u ->
  dep_comb_proof_argument s.
Arguments Dep_comb_proof_argument {_ _}.

(** Dependent version of [uncomb_proof_argument]. *)
Inductive dep_uncomb_proof_argument (s : Stack_ty.t) : Set :=
| Dep_uncomb_proof_argument {u} :
  With_family.uncomb_gadt_witness s u ->
  With_family.stack_ty u ->
  dep_uncomb_proof_argument s.
Arguments Dep_uncomb_proof_argument {_ _}.

(** Dependent version of [comb_get_proof_argument]. *)
Inductive dep_comb_get_proof_argument (before : Ty.t) : Set :=
| Dep_comb_get_proof_argument {after} :
  With_family.comb_get_gadt_witness before after ->
  With_family.ty after ->
  dep_comb_get_proof_argument before.
Arguments Dep_comb_get_proof_argument {_ _}.

(** Conversion back to [comb_get_proof_argument]. *)
Definition to_comb_get_proof_argument {before}
  (x : dep_comb_get_proof_argument before) : comb_get_proof_argument :=
  match x with
  | Dep_comb_get_proof_argument witness ty =>
    Comb_get_proof_argument
      (With_family.to_comb_get_gadt_witness witness)
      (With_family.to_ty ty)
  end.

(** Dependent version of [comb_set_proof_argument]. *)
Inductive dep_comb_set_proof_argument (rest before : Ty.t) : Set :=
| Dep_comb_set_proof_argument {after} :
  With_family.comb_set_gadt_witness rest before after ->
  With_family.ty after ->
  dep_comb_set_proof_argument rest before.
Arguments Dep_comb_set_proof_argument {_ _ _}.

(** Conversion back to [comb_set_proof_argument]. *)
Definition to_comb_set_proof_argument {rest before}
  (x : dep_comb_set_proof_argument rest before) : comb_set_proof_argument :=
  match x with
  | Dep_comb_set_proof_argument witness ty =>
    Comb_set_proof_argument
      (With_family.to_comb_set_gadt_witness witness)
      (With_family.to_ty ty)
  end.

(** Dependent version of [dup_n_proof_argument]. *)
Inductive dep_dup_n_proof_argument (before : Stack_ty.t) : Set :=
| Dep_dup_n_proof_argument {a} :
  With_family.dup_n_gadt_witness before a ->
  With_family.ty a ->
  dep_dup_n_proof_argument before.
Arguments Dep_dup_n_proof_argument {_ _}.

(** Simulation of [make_dug_proof_argument]. *)
Fixpoint dep_make_dug_proof_argument {x s}
  (loc_value : Alpha_context.Script.location) (n_value : int)
  (x_value : With_family.ty x) (stk : With_family.stack_ty s)
  : option (dep_dug_proof_argument s x) :=
  if n_value =? 0 then
    Some
      (Dep_dug_proof_argument
        With_family.KRest (With_family.Item_t x_value stk))
  else
    match stk with
    | With_family.Item_t v_value rest =>
      Option.map
        (fun function_parameter =>
          let 'Dep_dug_proof_argument n' aft' := function_parameter in
          Dep_dug_proof_argument
            (With_family.KPrefix loc_value v_value n')
            (With_family.Item_t v_value aft'))
        (dep_make_dug_proof_argument loc_value (n_value -i 1) x_value rest)
    | With_family.Bot_t => None
    end.

(** Simulation of [make_comb_get_proof_argument]. *)
Fixpoint dep_make_comb_get_proof_argument {b} (n_value : int)
  (ty : With_family.ty b)
  : option (dep_comb_get_proof_argument b) :=
  match n_value, ty with
  | 0, value_ty =>
    Some (Dep_comb_get_proof_argument With_family.Comb_get_zero value_ty)
  | 1, With_family.Pair_t hd_ty _ _annot _ =>
    Some (Dep_comb_get_proof_argument With_family.Comb_get_one hd_ty)
  | n_value, With_family.Pair_t _ tl_ty _annot _ =>
    Option.map
      (fun function_parameter =>
        let 'Dep_comb_get_proof_argument comb_get_left_witness ty' :=
          function_parameter in
        Dep_comb_get_proof_argument
          (With_family.Comb_get_plus_two comb_get_left_witness) ty')
      (dep_make_comb_get_proof_argument (n_value -i 2) tl_ty)
  | _, _ => None
  end.

(** Simulation of [make_comb_set_proof_argument]. *)
Fixpoint dep_make_comb_set_proof_argument {s value before}
  (ctxt : Alpha_context.context) (stack_ty : With_family.stack_ty s)
  (loc_value : Alpha_context.Script.location) (n_value : int)
  (value_ty : With_family.ty value) (ty : With_family.ty before)
  : M? (dep_comb_set_proof_argument value before) :=
  match n_value, ty with
  | 0, _ =>
    return? (Dep_comb_set_proof_argument With_family.Comb_set_zero value_ty)
  | 1, With_family.Pair_t _hd_ty tl_ty _ _ =>
    let? 'Dep_ty_ex_c after_ty :=
      dep_pair_t loc_value value_ty tl_ty in
    return? (Dep_comb_set_proof_argument With_family.Comb_set_one after_ty)
  | n_value, With_family.Pair_t hd_ty tl_ty _ _ =>
    let? 'Dep_comb_set_proof_argument comb_set_left_witness tl_ty' :=
      dep_make_comb_set_proof_argument ctxt stack_ty loc_value (n_value -i 2)
        value_ty tl_ty in
    let? 'Dep_ty_ex_c after_ty :=
      dep_pair_t loc_value hd_ty tl_ty' in
    return?
      (Dep_comb_set_proof_argument
        (With_family.Comb_set_plus_two comb_set_left_witness) after_ty)
  | _, _ =>
    let whole_stack := dep_serialize_stack_for_error ctxt stack_ty in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty)
        (loc_value, Michelson_v1_primitives.I_UPDATE, 2, whole_stack))
  end.

(** Dependent version of [ex_ty_cstr]. *)
Inductive dep_ex_ty_cstr (a : Ty.t) : Set :=
| Dep_ex_ty_cstr {b} :
  With_family.ty b ->
  (With_family.ty_to_dep_Set b -> With_family.ty_to_dep_Set a) ->
  Alpha_context.Script.node ->
  dep_ex_ty_cstr a.
Arguments Dep_ex_ty_cstr {_ _}.

(** Simulation of [find_entrypoint]. *)
Definition dep_find_entrypoint {error_context error_trace} {full : Ty.t}
  (error_details : Script_tc_errors.dep_error_details error_context error_trace)
  (full_value : With_family.ty full)
  (entrypoints : Script_typed_ir.entrypoints)
  (entrypoint : Alpha_context.Entrypoint.t)
  : Gas_monad.t (dep_ex_ty_cstr full)
    (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
  (let fix find_entrypoint {t}
      (ty : With_family.ty t) (entrypoints : Script_typed_ir.entrypoints_node)
      (entrypoint : Alpha_context.Entrypoint.t) {struct ty} :=
      Gas_monad.Syntax.op_letstar
      (Gas_monad.consume_gas Typecheck_costs.find_entrypoint_cycle)
      (fun function_parameter =>
        let '_ := function_parameter in
        match
          ty, entrypoints,
            match ty, entrypoints with
            |
              _, {|
                Script_typed_ir.entrypoints_node.at_node :=
                  Some {|
                    Script_typed_ir.entrypoint_info.name := name;
                      Script_typed_ir.entrypoint_info.original_type_expr
                        := original_type_expr
                      |}
                  |} => Alpha_context.Entrypoint.op_eq name entrypoint
            | _, _ => false
            end
        with
        | _, {|
            Script_typed_ir.entrypoints_node.at_node :=
              Some {|
                Script_typed_ir.entrypoint_info.name := name;
                  Script_typed_ir.entrypoint_info.original_type_expr
                    := original_type_expr
                  |}
              |}, true =>
          Gas_monad.Syntax._return
            (Dep_ex_ty_cstr
              full_value
              (fun e_value => e_value)
              original_type_expr
            )
        | With_family.Union_t tl tr _ _, {|
            Script_typed_ir.entrypoints_node.nested :=
              Script_typed_ir.Entrypoints_Union {|
                Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
                  Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                    := _right
                  |}
              |}, _ =>
            (Gas_monad.bind_recover (find_entrypoint tl _left entrypoint)
              (fun function_parameter =>
                match function_parameter with
                | Pervasives.Ok
                    (Dep_ex_ty_cstr ty construct original_type_expr)
                  =>
                    Gas_monad.Syntax._return
                      (Dep_ex_ty_cstr
                        (* (a := Ty.Union _ _) *)
                        ty
                        (* (fun e: _ => Script_typed_ir.L (construct e)) *)
                        (axiom
                          "With_family.ty_to_dep_Set _ -> With_family.ty_to_dep_Set _")
                        original_type_expr)
                | Pervasives.Error _ =>
                  Gas_monad.Syntax.op_letplus
                    (find_entrypoint tr _right entrypoint)
                    (fun x_value =>
                      let 'Dep_ex_ty_cstr ty construct original_type_expr
                      := x_value in
                      Dep_ex_ty_cstr
                        (* (a := Ty.Union _ _) *)
                        ty
                        (* (fun e: _ => Script_typed_ir.R (construct e)) *)
                        (axiom
                          "With_family.ty_to_dep_Set _ -> With_family.ty_to_dep_Set _")
                        original_type_expr)
                end))
        | _, {|
            Script_typed_ir.entrypoints_node.nested
              := Script_typed_ir.Entrypoints_None
              |}, _ => Gas_monad.of_result (Pervasives.Error tt)
        | _, _, _ => Gas_monad.of_result (Pervasives.Error tt)
        end) in
    let '{|
        Script_typed_ir.entrypoints.root := root_value;
        Script_typed_ir.entrypoints.original_type_expr := original_type_expr
      |} := entrypoints in
    Gas_monad.bind_recover (find_entrypoint full_value root_value entrypoint)
      (fun (function_parameter : Pervasives.result (dep_ex_ty_cstr full) unit) =>
        match function_parameter with
        | Pervasives.Ok f_t => Gas_monad.Syntax._return f_t
        | Pervasives.Error _ =>
          if Alpha_context.Entrypoint.is_default entrypoint then
            Gas_monad.Syntax._return
              (Dep_ex_ty_cstr
                full_value
                (fun e_value => e_value)
                original_type_expr)
          else
            Gas_monad.of_result
              (Pervasives.Error
                match error_details
                in Script_tc_errors.dep_error_details _ error_trace
                return Script_tc_errors.Error_trace_family.to_Set error_trace
                with
                | Script_tc_errors.Dep_fast =>
                  Script_tc_errors.Inconsistent_types_fast
                | Script_tc_errors.Dep_informative _ =>
                  (Error_monad.trace_of_error
                    (Build_extensible "No_such_entrypoint"
                      Alpha_context.Entrypoint.t entrypoint))
                end)
        end)
  ).

(** Simulation of [find_entrypoint_for_type]. *)
Definition dep_find_entrypoint_for_type {error_trace} {full exp : Ty.t}
  (error_details :
    Script_tc_errors.dep_error_details Alpha_context.Script.location
      error_trace)
  (full_value : With_family.ty full) (expected : With_family.ty exp)
  (entrypoints : Script_typed_ir.entrypoints)
  (entrypoint : Alpha_context.Entrypoint.t)
  (loc_value : Alpha_context.Script.location)
  : Gas_monad.t
      (Alpha_context.Entrypoint.t * With_family.ty exp)
      (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
   Gas_monad.Syntax.op_letstar
    (dep_find_entrypoint error_details full_value entrypoints entrypoint)
    (fun res =>
      let 'Dep_ex_ty_cstr ty _ _ := res in
      match
        entrypoints.(Script_typed_ir.entrypoints.root).(Script_typed_ir.entrypoints_node.at_node),
          match
            entrypoints.(Script_typed_ir.entrypoints.root).(Script_typed_ir.entrypoints_node.at_node)
            with
          | Some {|
              Script_typed_ir.entrypoint_info.name := name;
                Script_typed_ir.entrypoint_info.original_type_expr := _
                |} =>
            (Alpha_context.Entrypoint.is_root name) &&
            (Alpha_context.Entrypoint.is_default entrypoint)
          | _ => false
          end
      with
      | Some {|
          Script_typed_ir.entrypoint_info.name := name;
            Script_typed_ir.entrypoint_info.original_type_expr := _
            |}, true =>
        Gas_monad.bind_recover
          (dep_ty_eq Script_tc_errors.Dep_fast ty expected)
          (fun (function_parameter :
               Pervasives.result (_ = _)
                 Script_tc_errors.inconsistent_types_fast_error)
            =>
            match function_parameter with
            | Pervasives.Ok eq =>
              Gas_monad.Syntax._return (
                Alpha_context.Entrypoint.default,
                let 'eq_refl := eq in ty
              )
            | Pervasives.Error Script_tc_errors.Inconsistent_types_fast =>
              Gas_monad.Syntax.op_letplus
                (dep_ty_eq error_details full_value expected)
                (fun eq =>
                  let 'eq_refl := eq in
                  (Alpha_context.Entrypoint.root_value, full_value))
            end)
      | _, _ =>
        Gas_monad.Syntax.op_letplus
          (dep_ty_eq error_details ty expected)
          (fun eq => (entrypoint, let 'eq_refl := eq in ty))
      end).

(** Extracted from [dep_well_formed_entrypoints]. Simulation of
    [merge], which in its turn, is inside of [well_formed_entrypoints] *)
Definition dep_merge {A : Set} {a : Ty.t}
      (path : list A) (dep_ty : With_family.ty a)
      (entrypoints : Script_typed_ir.entrypoints_node) (reachable : bool)
      (function_parameter : option (list A) * Entrypoint_repr._Set.(_Set.S.t))
      : M? ((option (list A) * Entrypoint_repr._Set.(_Set.S.t)) * bool) :=
      let '(first_unreachable, all) as acc_value := function_parameter in
      match entrypoints.(Script_typed_ir.entrypoints_node.at_node) with
      | None => 
        return?
          ((if reachable then
            acc_value
          else
            match dep_ty with
            | With_family.Union_t _ _ _ _ => acc_value
            | _ =>
              match first_unreachable with
              | None => ((Some (List.rev path)), all)
              | Some _ => acc_value
              end
            end), reachable)
      |
        Some {|
          Script_typed_ir.entrypoint_info.name := name;
            Script_typed_ir.entrypoint_info.original_type_expr := _
            |} =>
        if Alpha_context.Entrypoint._Set.(_Set.S.mem) name all then
          Error_monad.error_value
            (Build_extensible "Duplicate_entrypoint" Alpha_context.Entrypoint.t
              name)
        else
          return?
            ((first_unreachable,
              (Alpha_context.Entrypoint._Set.(_Set.S.add) name all)), true)
      end.

(** Extracted from [dep_well_formed_entrypoints]. Simulation of
    [check], which in its turn, is inside of [well_formed_entrypoints] *)
Fixpoint dep_check {a : Ty.t}
      (t_value : With_family.ty a)
      (entrypoints : Script_typed_ir.entrypoints_node)
      (path : list Alpha_context.Script.prim) (reachable : bool)
      (acc_value :
        option (list Alpha_context.Script.prim) *
          Entrypoint_repr._Set.(_Set.S.t)) :
            M? (option (list Alpha_context.Script.prim) *
              Entrypoint_repr._Set.(_Set.S.t)) :=
      match (t_value, entrypoints) with
      |
        (With_family.Union_t tl tr _ _, {|
          Script_typed_ir.entrypoints_node.nested :=
            Script_typed_ir.Entrypoints_Union {|
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._left
                := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right |} |}) =>
        let? '(acc_value, l_reachable) :=
          dep_merge (cons Michelson_v1_primitives.D_Left path)
            tl _left reachable acc_value in
        let? '(acc_value, r_reachable) :=
          dep_merge (cons Michelson_v1_primitives.D_Right path)
            tr _right reachable acc_value in
        let? acc_value :=
          dep_check tl _left (cons Michelson_v1_primitives.D_Left path)
            l_reachable acc_value in
        dep_check tr _right (cons Michelson_v1_primitives.D_Right path)
          r_reachable acc_value
      | _ => return? acc_value
      end.

(** Extracted from [well_formed_entrypoints]. We need this extraction to
    reason about equality with [dep_merge]. *)
Definition merge_from_well_formed_entrypoints {A : Set}
    (path : list A) (ty : Script_typed_ir.ty)
    (entrypoints : Script_typed_ir.entrypoints_node) (reachable : bool)
    (function_parameter : option (list A) * Entrypoint_repr._Set.(_Set.S.t))
    : M? ((option (list A) * Entrypoint_repr._Set.(_Set.S.t)) * bool) :=
    let '(first_unreachable, all) as acc_value := function_parameter in
    match entrypoints.(Script_typed_ir.entrypoints_node.at_node) with
    | None =>
      return?
        ((if reachable then
          acc_value
        else
          match ty with
          | Script_typed_ir.Union_t _ _ _ _ => acc_value
          | _ =>
            match first_unreachable with
            | None => ((Some (List.rev path)), all)
            | Some _ => acc_value
            end
          end), reachable)
    |
      Some {|
        Script_typed_ir.entrypoint_info.name := name;
          Script_typed_ir.entrypoint_info.original_type_expr := _
          |} =>
      if Alpha_context.Entrypoint._Set.(_Set.S.mem) name all then
        Error_monad.error_value
          (Build_extensible "Duplicate_entrypoint" Alpha_context.Entrypoint.t
            name)
      else
        return?
          ((first_unreachable,
            (Alpha_context.Entrypoint._Set.(_Set.S.add) name all)), true)
    end.

(** Extracted from [well_formed_entrypoints]. We need this extraction to
    reason about equality with [dep_check]. *)
Definition check_from_well_formed_entrypoints :=
    (fix check
    (t_value : Script_typed_ir.ty)
    (entrypoints : Script_typed_ir.entrypoints_node)
    (path : list Alpha_context.Script.prim) (reachable : bool)
    (acc_value :
      option (list Alpha_context.Script.prim) * Entrypoint_repr._Set.(_Set.S.t))
    : M?
      (option (list Alpha_context.Script.prim) * Entrypoint_repr._Set.(_Set.S.t)) :=
    match (t_value, entrypoints) with
    |
      (Script_typed_ir.Union_t tl tr _ _, {|
        Script_typed_ir.entrypoints_node.nested :=
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right
              |}
          |}) =>
      let? '(acc_value, l_reachable) :=
        merge_from_well_formed_entrypoints
          (cons Michelson_v1_primitives.D_Left path) tl _left reachable
          acc_value in
      let? '(acc_value, r_reachable) :=
        merge_from_well_formed_entrypoints
          (cons Michelson_v1_primitives.D_Right path) tr _right reachable
          acc_value in
      let? acc_value :=
        check tl _left (cons Michelson_v1_primitives.D_Left path) l_reachable
          acc_value in
      check tr _right (cons Michelson_v1_primitives.D_Right path) r_reachable
        acc_value
    | _ => return? acc_value
    end).

(** Simulation of [well_formed_entrypoints]. *)
Definition dep_well_formed_entrypoints {full : Ty.t}
  (full_value : With_family.ty full)
  (entrypoints : Script_typed_ir.entrypoints_node) : M? unit :=
    let '(init_value, reachable) :=
      match entrypoints.(Script_typed_ir.entrypoints_node.at_node) with
      | None => (Alpha_context.Entrypoint._Set.(_Set.S.empty), false)
      |
        Some {|
          Script_typed_ir.entrypoint_info.name := name;
            Script_typed_ir.entrypoint_info.original_type_expr := _
            |} => ((Alpha_context.Entrypoint._Set.(_Set.S.singleton) name), true)
      end in
    let? '(first_unreachable, all) :=
      dep_check full_value entrypoints nil reachable (None, init_value) in
    if
      Pervasives.not
        (Alpha_context.Entrypoint._Set.(_Set.S.mem)
          Alpha_context.Entrypoint.default all)
    then
      Result.return_unit
    else
      match first_unreachable with
      | None => Result.return_unit
      | Some path =>
        Error_monad.error_value
          (Build_extensible "Unreachable_entrypoint"
            (list Alpha_context.Script.prim) path)
      end.

(** Dependent version of [ex_parameter_ty_and_entrypoints]. *)
Inductive dep_ex_parameter_ty_and_entrypoints : Set :=
| Dep_ex_parameter_ty_and_entrypoints {a} :
  With_family.ty a -> Script_typed_ir.entrypoints ->
  dep_ex_parameter_ty_and_entrypoints.

(** Conversion back to [ex_parameter_ty_and_entrypoints]. *)
Definition to_ex_parameter_ty_and_entrypoints
  (v : dep_ex_parameter_ty_and_entrypoints) : ex_parameter_ty_and_entrypoints :=
  match v with
  | Dep_ex_parameter_ty_and_entrypoints ty entrypoints =>
    Ex_parameter_ty_and_entrypoints {|
      ex_parameter_ty_and_entrypoints.Ex_parameter_ty_and_entrypoints
        .arg_type := With_family.to_ty ty;
      ex_parameter_ty_and_entrypoints.Ex_parameter_ty_and_entrypoints
        .entrypoints := entrypoints;
    |}
  end.

(** Simulation of [parse_parameter_ty_and_entrypoints_aux]. *)
Definition dep_parse_parameter_ty_and_entrypoints_aux
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (dep_ex_parameter_ty_and_entrypoints * Alpha_context.context) :=
  let?
    '(Dep_ex_parameter_ty_and_entrypoints_node arg_type entrypoints, ctxt) :=
    dep_parse_passable_ty_aux_with_ret ctxt (stack_depth +i 1) legacy
      Parse_ty_ret_family.Parse_entrypoints node_value in
  let? '_ :=
    if legacy then
      Result.return_unit
    else
      dep_well_formed_entrypoints arg_type entrypoints in
  let entrypoints :=
    {| Script_typed_ir.entrypoints.root := entrypoints;
      Script_typed_ir.entrypoints.original_type_expr := node_value |} in
  return?
    (Dep_ex_parameter_ty_and_entrypoints arg_type entrypoints, ctxt).

(** Simulation of [parse_passable_ty_aux]. *)
Definition dep_parse_passable_ty_aux
  : Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 x_2 x_3 =>
    dep_parse_passable_ty_aux_with_ret x_1 x_2 x_3
      Parse_ty_ret_family.Don't_parse_entrypoints.

(** Simulation of [opened_ticket_type]. *)
Definition dep_opened_ticket_type {a}
  (loc_value : Alpha_context.Script.location)
  (ty : With_family.ty a) :
  M? (With_family.ty
        (Ty.Pair
           Ty.Address
           (Ty.Pair a (Ty.Num Ty.Num.Nat)))) :=
  dep_comparable_pair_3_t loc_value With_family.Address_t ty
    With_family.Nat_t.

(** Simulation of [comparable_comb_witness1]. *)
Definition dep_comparable_comb_witness1 {t}
  (ty : With_family.ty t) : comb_witness :=
  match ty with
  | With_family.Pair_t _ _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

(** Simulation of [parse_comparable_data]. *)
Fixpoint dep_parse_comparable_data {a}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (ty : With_family.ty a) (expr : Alpha_context.Script.node)
  {struct ty} : M? (With_family.ty_to_dep_Set a * Alpha_context.context) :=
  let parse_data_error (function_parameter : unit) : Error_monad._error :=
    let '_ := function_parameter in
    let ty := dep_serialize_ty_for_error (dep_ty_of_comparable_ty ty) in
    Build_extensible "Invalid_constant"
      (Alpha_context.Script.location *
        Micheline.canonical Alpha_context.Script.prim *
        Micheline.canonical Alpha_context.Script.prim)
      (location expr, Micheline.strip_locations expr, ty) in
  let traced_no_lwt {B : Set} (body : M? B) : M? B :=
    Error_monad.record_trace_eval parse_data_error body in
  let traced {B : Set} (body : M? B) : M? B :=
    Error_monad.trace_eval parse_data_error body in
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_data_cycle
    in
  let legacy := false in
  match
    ty in (With_family.ty a)
    return M? (With_family.ty_to_dep_Set a * Alpha_context.context)
  with
  | With_family.Unit_t =>
    traced_no_lwt (parse_unit ctxt legacy expr)
  | With_family.Bool_t =>
      traced_no_lwt (parse_bool ctxt legacy expr)
  | With_family.String_t =>
      traced_no_lwt (parse_string ctxt expr)
  | With_family.Bytes_t =>
      traced_no_lwt (parse_bytes ctxt expr)
  | With_family.Int_t =>
      traced_no_lwt (parse_int ctxt expr)
  | With_family.Nat_t =>
      traced_no_lwt (parse_nat ctxt expr)
  | With_family.Mutez_t =>
      traced_no_lwt (parse_mutez ctxt expr)
  | With_family.Timestamp_t =>
      traced_no_lwt (parse_timestamp ctxt expr)
  | With_family.Key_t =>
      traced_no_lwt (parse_key ctxt expr)
  | With_family.Key_hash_t =>
      traced_no_lwt (parse_key_hash ctxt expr)
  | With_family.Signature_t =>
      traced_no_lwt (parse_signature ctxt expr)
  | With_family.Chain_id_t =>
      traced_no_lwt (parse_chain_id ctxt expr)
  | With_family.Address_t =>
      traced_no_lwt (parse_address ctxt expr)
  | With_family.Tx_rollup_l2_address_t =>
      traced_no_lwt (parse_tx_rollup_l2_address ctxt expr)
  | With_family.Pair_t tl tr _ Dependent_bool.YesYes =>
    let r_witness := dep_comparable_comb_witness1 tr in
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      dep_parse_comparable_data type_logger_value ctxt tl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      dep_parse_comparable_data type_logger_value ctxt tr v_value in
    traced (parse_pair parse_l parse_r ctxt legacy r_witness expr)
  | With_family.Union_t tl tr _ Dependent_bool.YesYes =>
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      dep_parse_comparable_data type_logger_value ctxt tl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      dep_parse_comparable_data type_logger_value ctxt tr v_value in
                    traced (parse_union parse_l parse_r ctxt legacy expr)
  | With_family.Option_t t_value _ Dependent_bool.Yes =>
    let parse_v
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      dep_parse_comparable_data type_logger_value ctxt t_value v_value in
    traced (parse_option parse_v ctxt legacy expr)
  | With_family.Never_t =>
      traced_no_lwt (parse_never expr)
  | _ => Error_monad.error_value (Build_extensible "Unexpected" unit tt)
  end.

(** Simulation of [comb_witness1]. *)
Definition dep_comb_witness1 {a} (ty : With_family.ty a) : comb_witness :=
  match ty with
  | With_family.Pair_t _ _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

(** Simulation of [parse_view_name]. *)
Definition dep_parse_view_name
  (ctxt : Alpha_context.context)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
   : M? (Script_string.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.String loc_value v_value) as expr =>
    if (String.length v_value) >? 31 then
      Error_monad.error_value
        (Build_extensible "View_name_too_long" string v_value)
    else
      let fix check_char (i_value : nat) : M? string :=
        match i_value with
        | Datatypes.O => return? v_value
        | Datatypes.S i_value =>
          if Script_ir_annot.is_allowed_char (String.get v_value (Z.of_nat i_value)) then
            check_char i_value
          else
            Error_monad.error_value
              (Build_extensible "Bad_view_name" Alpha_context.Script.location
                 loc_value)
        end
      in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt (Typecheck_costs.check_printable v_value)
        in
      Error_monad.record_trace
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc_value, (Micheline.strip_locations expr),
            "string [a-zA-Z0-9_.%@] and the maximum string length of 31 characters"))
        (let? v_value :=
           check_char (Z.to_nat ((String.length v_value) - 1)%Z) in
        let? s_value := Script_string.of_string v_value in
        return? (s_value, ctxt))
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.String_kind ], (kind_value expr)))
  end.

(** Simulation of inline [find_fields]. *)
Fixpoint dep_find_fields
  (ctxt0 : Alpha_context.context)
  (p_value s_value
  c_value : M* (node Alpha_context.Script.location
                  Alpha_context.Script.prim *
                Alpha_context.Script.location * annot))
  (views : With_family.view_map)
  (fields : list
              (node Alpha_context.Script.location
                Alpha_context.Script.prim)) {struct fields} :
    M? (Alpha_context.context *
        (M* (node Alpha_context.Script.location
              Alpha_context.Script.prim *
            Alpha_context.Script.location * annot) *
        M* (node Alpha_context.Script.location
              Alpha_context.Script.prim *
            Alpha_context.Script.location * annot) *
        M* (node Alpha_context.Script.location
              Alpha_context.Script.prim *
            Alpha_context.Script.location * annot) *
        With_family.view_map)) :=
  match fields with
  | [] => return? (ctxt0, (p_value, s_value, c_value, views))
  | n :: rest =>
      match n with
      | Int loc_value _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location *
                list Script_tc_errors.kind * Script_tc_errors.kind)
              (loc_value, [Script_tc_errors.Prim_kind],
              Script_tc_errors.Int_kind))
      | String loc_value _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location *
                list Script_tc_errors.kind * Script_tc_errors.kind)
              (loc_value, [Script_tc_errors.Prim_kind],
              Script_tc_errors.String_kind))
      | Bytes loc_value _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location *
                list Script_tc_errors.kind * Script_tc_errors.kind)
              (loc_value, [Script_tc_errors.Prim_kind],
              Script_tc_errors.Bytes_kind))
      | Prim loc_value name args annot =>
          match name with
          | Alpha_context.Script.K_parameter =>
              match args with
              | [] =>
                  Error_monad.error_value
                    (Build_extensible "Invalid_arity"
                      (Alpha_context.Script.location *
                        Alpha_context.Script.prim * int * int)
                      (loc_value, name, 1, List.length args))
              | arg :: l0 =>
                  match l0 with
                  | [] =>
                      match p_value with
                      | Some _ =>
                          Error_monad.error_value
                            (Build_extensible "Duplicate_field"
                              (Alpha_context.Script.location *
                                Alpha_context.Script.prim)
                              (loc_value,
                              Michelson_v1_primitives.K_parameter))
                      | None =>
                          dep_find_fields ctxt0
                            (Some (arg, loc_value, annot)) s_value
                            c_value views rest
                      end
                  | _ :: _ =>
                      Error_monad.error_value
                        (Build_extensible "Invalid_arity"
                          (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                          (loc_value, name, 1, List.length args))
                  end
              end
          | Alpha_context.Script.K_storage =>
              match args with
              | [] =>
                  Error_monad.error_value
                    (Build_extensible "Invalid_arity"
                      (Alpha_context.Script.location *
                        Alpha_context.Script.prim * int * int)
                      (loc_value, name, 1, List.length args))
              | arg :: l0 =>
                  match l0 with
                  | [] =>
                      match s_value with
                      | Some _ =>
                          Error_monad.error_value
                            (Build_extensible "Duplicate_field"
                              (Alpha_context.Script.location *
                                Alpha_context.Script.prim)
                              (loc_value,
                              Michelson_v1_primitives.K_storage))
                      | None =>
                          dep_find_fields ctxt0 p_value
                            (Some (arg, loc_value, annot)) c_value
                            views rest
                      end
                  | _ :: _ =>
                      Error_monad.error_value
                        (Build_extensible "Invalid_arity"
                          (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                          (loc_value, name, 1, List.length args))
                  end
              end
          | Alpha_context.Script.K_code =>
              match args with
              | [] =>
                  Error_monad.error_value
                    (Build_extensible "Invalid_arity"
                      (Alpha_context.Script.location *
                        Alpha_context.Script.prim * int * int)
                      (loc_value, name, 1, List.length args))
              | arg :: l0 =>
                  match l0 with
                  | [] =>
                      match c_value with
                      | Some _ =>
                          Error_monad.error_value
                            (Build_extensible "Duplicate_field"
                              (Alpha_context.Script.location *
                                Alpha_context.Script.prim)
                              (loc_value,
                              Michelson_v1_primitives.K_code))
                      | None =>
                          dep_find_fields ctxt0 p_value s_value
                            (Some (arg, loc_value, annot)) views
                            rest
                      end
                  | _ :: _ =>
                      Error_monad.error_value
                        (Build_extensible "Invalid_arity"
                          (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                          (loc_value, name, 1, List.length args))
                  end
              end
          | Alpha_context.Script.K_view =>
              match args with
              | [] =>
                  Error_monad.error_value
                    (Build_extensible "Invalid_arity"
                      (Alpha_context.Script.location *
                        Alpha_context.Script.prim * int * int)
                      (loc_value, Michelson_v1_primitives.K_view,
                      4, List.length args))
              | name0 :: l0 =>
                  match l0 with
                  | [] =>
                      Error_monad.error_value
                        (Build_extensible "Invalid_arity"
                          (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                          (loc_value,
                          Michelson_v1_primitives.K_view, 4,
                          List.length args))
                  | input_ty :: l1 =>
                      match l1 with
                      | [] =>
                          Error_monad.error_value
                            (Build_extensible "Invalid_arity"
                              (Alpha_context.Script.location *
                                Alpha_context.Script.prim * int *
                                int)
                              (loc_value,
                              Michelson_v1_primitives.K_view, 4,
                              List.length args))
                      | output_ty :: l2 =>
                          match l2 with
                          | [] =>
                              Error_monad.error_value
                                (Build_extensible "Invalid_arity"
                                  (Alpha_context.Script.location *
                                    Alpha_context.Script.prim *
                                    int * int)
                                  (loc_value,
                                  Michelson_v1_primitives.K_view,
                                  4, List.length args))
                          | view_code :: l3 =>
                              match l3 with
                              | [] =>
                                  let? '
                                  (str, ctxt1)
                                  := dep_parse_view_name ctxt0 name0
                                  in let? ' ctxt2
                                    :=
                                    Alpha_context.Gas.consume
                                      ctxt1
                                      (Michelson_v1_gas.Cost_of.Interpreter.view_update
                                      str
                                      (With_family.to_view_map
                                      views))
                                    in
                                    let str : With_family.ty_to_dep_Set Ty.String := str in
                                    if
                                      Script_map.dep_mem str views
                                    then
                                      Error_monad.error_value
                                      (Build_extensible
                                      "Duplicated_view_name"
                                      Alpha_context.Script.location
                                      loc_value)
                                    else
                                      dep_find_fields ctxt2
                                      p_value s_value c_value
                                      (Script_map.dep_update str
                                      (Some {|
                                      Script_typed_ir.view.input_ty :=
                                      input_ty;
                                      Script_typed_ir.view.output_ty :=
                                      output_ty;
                                      Script_typed_ir.view.view_code :=
                                      view_code
                                      |}) views) rest
                              | _ :: _ =>
                                  Error_monad.error_value
                                    (Build_extensible
                                      "Invalid_arity"
                                      (Alpha_context.Script.location *
                                      Alpha_context.Script.prim *
                                      int * int)
                                      (loc_value,
                                      Michelson_v1_primitives.K_view,
                                      4,
                                      List.length args))
                              end
                          end
                      end
                  end
              end
          | _ =>
              Error_monad.error_value
                (Build_extensible "Invalid_primitive"
                  (Alpha_context.Script.location *
                    list Alpha_context.Script.prim *
                    Alpha_context.Script.prim)
                  (loc_value,
                  [Michelson_v1_primitives.K_parameter;
                  Michelson_v1_primitives.K_storage;
                  Michelson_v1_primitives.K_code;
                  Michelson_v1_primitives.K_view], name))
          end
      | Seq loc_value _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location *
                list Script_tc_errors.kind * Script_tc_errors.kind)
              (loc_value, [Script_tc_errors.Prim_kind],
              Script_tc_errors.Seq_kind))
      end
  end.

(** Inline definition of [fix find_fields] *)
Definition find_fields :=
  (fix find_fields
    (ctxt0 : Alpha_context.context)
    (p_value s_value
    c_value : M* (node Alpha_context.Script.location
                    Alpha_context.Script.prim *
                  Alpha_context.Script.location * annot))
    (views : Script_typed_ir.view_map)
    (fields : list
                (node Alpha_context.Script.location
                  Alpha_context.Script.prim)) {struct fields} :
      M? (Alpha_context.context *
          (M* (node Alpha_context.Script.location
                Alpha_context.Script.prim *
              Alpha_context.Script.location * annot) *
          M* (node Alpha_context.Script.location
                Alpha_context.Script.prim *
              Alpha_context.Script.location * annot) *
          M* (node Alpha_context.Script.location
                Alpha_context.Script.prim *
              Alpha_context.Script.location * annot) *
          Script_typed_ir.view_map)) :=
    match fields with
    | [] => return? (ctxt0, (p_value, s_value, c_value, views))
    | n :: rest =>
        match n with
        | Int loc_value _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_kind"
                (Alpha_context.Script.location *
                  list Script_tc_errors.kind * Script_tc_errors.kind)
                (loc_value, [Script_tc_errors.Prim_kind],
                Script_tc_errors.Int_kind))
        | String loc_value _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_kind"
                (Alpha_context.Script.location *
                  list Script_tc_errors.kind * Script_tc_errors.kind)
                (loc_value, [Script_tc_errors.Prim_kind],
                Script_tc_errors.String_kind))
        | Bytes loc_value _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_kind"
                (Alpha_context.Script.location *
                  list Script_tc_errors.kind * Script_tc_errors.kind)
                (loc_value, [Script_tc_errors.Prim_kind],
                Script_tc_errors.Bytes_kind))
        | Prim loc_value name args annot =>
            match name with
            | Alpha_context.Script.K_parameter =>
                match args with
                | [] =>
                    Error_monad.error_value
                      (Build_extensible "Invalid_arity"
                        (Alpha_context.Script.location *
                          Alpha_context.Script.prim * int * int)
                        (loc_value, name, 1, List.length args))
                | arg :: l0 =>
                    match l0 with
                    | [] =>
                        match p_value with
                        | Some _ =>
                            Error_monad.error_value
                              (Build_extensible "Duplicate_field"
                                (Alpha_context.Script.location *
                                  Alpha_context.Script.prim)
                                (loc_value,
                                Michelson_v1_primitives.K_parameter))
                        | None =>
                            find_fields ctxt0
                              (Some (arg, loc_value, annot)) s_value
                              c_value views rest
                        end
                    | _ :: _ =>
                        Error_monad.error_value
                          (Build_extensible "Invalid_arity"
                            (Alpha_context.Script.location *
                              Alpha_context.Script.prim * int * int)
                            (loc_value, name, 1, List.length args))
                    end
                end
            | Alpha_context.Script.K_storage =>
                match args with
                | [] =>
                    Error_monad.error_value
                      (Build_extensible "Invalid_arity"
                        (Alpha_context.Script.location *
                          Alpha_context.Script.prim * int * int)
                        (loc_value, name, 1, List.length args))
                | arg :: l0 =>
                    match l0 with
                    | [] =>
                        match s_value with
                        | Some _ =>
                            Error_monad.error_value
                              (Build_extensible "Duplicate_field"
                                (Alpha_context.Script.location *
                                  Alpha_context.Script.prim)
                                (loc_value,
                                Michelson_v1_primitives.K_storage))
                        | None =>
                            find_fields ctxt0 p_value
                              (Some (arg, loc_value, annot)) c_value
                              views rest
                        end
                    | _ :: _ =>
                        Error_monad.error_value
                          (Build_extensible "Invalid_arity"
                            (Alpha_context.Script.location *
                              Alpha_context.Script.prim * int * int)
                            (loc_value, name, 1, List.length args))
                    end
                end
            | Alpha_context.Script.K_code =>
                match args with
                | [] =>
                    Error_monad.error_value
                      (Build_extensible "Invalid_arity"
                        (Alpha_context.Script.location *
                          Alpha_context.Script.prim * int * int)
                        (loc_value, name, 1, List.length args))
                | arg :: l0 =>
                    match l0 with
                    | [] =>
                        match c_value with
                        | Some _ =>
                            Error_monad.error_value
                              (Build_extensible "Duplicate_field"
                                (Alpha_context.Script.location *
                                  Alpha_context.Script.prim)
                                (loc_value,
                                Michelson_v1_primitives.K_code))
                        | None =>
                            find_fields ctxt0 p_value s_value
                              (Some (arg, loc_value, annot)) views rest
                        end
                    | _ :: _ =>
                        Error_monad.error_value
                          (Build_extensible "Invalid_arity"
                            (Alpha_context.Script.location *
                              Alpha_context.Script.prim * int * int)
                            (loc_value, name, 1, List.length args))
                    end
                end
            | Alpha_context.Script.K_view =>
                match args with
                | [] =>
                    Error_monad.error_value
                      (Build_extensible "Invalid_arity"
                        (Alpha_context.Script.location *
                          Alpha_context.Script.prim * int * int)
                        (loc_value, Michelson_v1_primitives.K_view, 4,
                        List.length args))
                | name0 :: l0 =>
                    match l0 with
                    | [] =>
                        Error_monad.error_value
                          (Build_extensible "Invalid_arity"
                            (Alpha_context.Script.location *
                              Alpha_context.Script.prim * int * int)
                            (loc_value, Michelson_v1_primitives.K_view,
                            4, List.length args))
                    | input_ty :: l1 =>
                        match l1 with
                        | [] =>
                            Error_monad.error_value
                              (Build_extensible "Invalid_arity"
                                (Alpha_context.Script.location *
                                  Alpha_context.Script.prim * int * int)
                                (loc_value,
                                Michelson_v1_primitives.K_view, 4,
                                List.length args))
                        | output_ty :: l2 =>
                            match l2 with
                            | [] =>
                                Error_monad.error_value
                                  (Build_extensible "Invalid_arity"
                                    (Alpha_context.Script.location *
                                      Alpha_context.Script.prim * int *
                                      int)
                                    (loc_value,
                                    Michelson_v1_primitives.K_view, 4,
                                    List.length args))
                            | view_code :: l3 =>
                                match l3 with
                                | [] =>
                                    let? ' (str, ctxt1)
                                    := parse_view_name ctxt0 name0
                                    in let? ' ctxt2
                                      := Alpha_context.Gas.consume
                                            ctxt1
                                            (Michelson_v1_gas.Cost_of.Interpreter.view_update
                                            str views)
                                      in if Script_map.mem str views
                                          then
                                          Error_monad.error_value
                                            (Build_extensible
                                            "Duplicated_view_name"
                                            Alpha_context.Script.location
                                            loc_value)
                                          else
                                          find_fields ctxt2 p_value
                                            s_value c_value
                                            (Script_map.update str
                                            (Some
                                            {|
                                            Script_typed_ir.view.input_ty :=
                                            input_ty;
                                            Script_typed_ir.view.output_ty :=
                                            output_ty;
                                            Script_typed_ir.view.view_code :=
                                            view_code
                                            |}) views) rest
                                | _ :: _ =>
                                    Error_monad.error_value
                                      (Build_extensible "Invalid_arity"
                                        (Alpha_context.Script.location *
                                          Alpha_context.Script.prim *
                                          int * int)
                                        (loc_value,
                                        Michelson_v1_primitives.K_view,
                                        4, List.length args))
                                end
                            end
                        end
                    end
                end
            | _ =>
                Error_monad.error_value
                  (Build_extensible "Invalid_primitive"
                    (Alpha_context.Script.location *
                      list Alpha_context.Script.prim *
                      Alpha_context.Script.prim)
                    (loc_value,
                    [Michelson_v1_primitives.K_parameter;
                    Michelson_v1_primitives.K_storage;
                    Michelson_v1_primitives.K_code;
                    Michelson_v1_primitives.K_view], name))
            end
        | Seq loc_value _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_kind"
                (Alpha_context.Script.location *
                  list Script_tc_errors.kind * Script_tc_errors.kind)
                (loc_value, [Script_tc_errors.Prim_kind],
                Script_tc_errors.Seq_kind))
        end
    end).

(** Simulation of [parse_toplevel_aux]. *)
Definition dep_parse_toplevel_aux
  (ctxt : Alpha_context.context) (legacy : bool)
  (toplevel_value : Alpha_context.Script.expr)
  : M? (dep_toplevel * Alpha_context.context) :=
   (Error_monad.record_trace
    (Build_extensible "Ill_typed_contract"
      (Alpha_context.Script.expr * Script_tc_errors.type_map)
      (toplevel_value, nil))
    match Micheline.root_value toplevel_value with
    | Micheline.Int loc_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc_value, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Int_kind))
    | Micheline.String loc_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc_value, [ Script_tc_errors.Seq_kind ],
            Script_tc_errors.String_kind))
    | Micheline.Bytes loc_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc_value, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Bytes_kind))
    | Micheline.Prim loc_value _ _ _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc_value, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Prim_kind))
    | Micheline.Seq _ fields =>
      let? '(ctxt, toplevel_value) :=
        dep_find_fields ctxt None None None
          (Script_map.dep_empty dep_string_key) fields in
      match toplevel_value with
      | (None, _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_parameter)
      | (Some _, None, _, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_storage)
      | (Some _, Some _, None, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_code)
      | (Some (p_value, ploc, pannot), Some (s_value, sloc, sannot),
          Some (c_value, cloc, cannot), views) =>
        let p_pannot :=
          let? function_parameter := Script_ir_annot.has_field_annot p_value in
          match function_parameter with
          | true => return? (p_value, pannot)
          | false =>
            match
              (pannot,
                match pannot with
                | cons single [] => legacy
                | _ => false
                end) with
            | (cons single [], true) =>
              let? is_field_annot := Script_ir_annot.is_field_annot ploc single
                in
              match (is_field_annot, p_value) with
              | (true, Micheline.Prim loc_value prim args annots) =>
                return?
                  ((Micheline.Prim loc_value prim args (cons single annots)),
                    nil)
              | _ => return? (p_value, nil)
              end
            | (_, _) => return? (p_value, pannot)
            end
          end in
        let? '(arg_type, pannot) := p_pannot in
        let? '_ := Script_ir_annot.error_unexpected_annot ploc pannot in
        let? '_ := Script_ir_annot.error_unexpected_annot cloc cannot in
        let? '_ := Script_ir_annot.error_unexpected_annot sloc sannot in
        return?
          ({|
              dep_toplevel.code_field := c_value;
              dep_toplevel.arg_type := arg_type;
              dep_toplevel.storage_type := s_value;
              dep_toplevel.views := views
            |}, ctxt)
      end
    end).

Fixpoint dep_parse_data_aux_fuel {a}
  (type_logger_value : option type_logger) (fuel : nat)
  (ctxt : Alpha_context.context) (legacy : bool) (allow_forged : bool)
  (ty : With_family.ty a) (expr : Alpha_context.Script.node)
  {struct fuel} : M? (With_family.ty_to_dep_Set a * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_data_cycle
  in
  let non_terminal_recursion {a} fuel
    (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
    (legacy : bool) (ty : With_family.ty a)
    (script_data : Alpha_context.Script.node)
    : M? (With_family.ty_to_dep_Set a * Alpha_context.context) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
      dep_parse_data_aux_fuel type_logger_value fuel ctxt legacy
        allow_forged ty script_data
    end
  in
  let parse_data_error (function_parameter : unit) : Error_monad._error :=
    let '_ := function_parameter in
    let ty := dep_serialize_ty_for_error ty in
    Build_extensible "Invalid_constant"
      (Alpha_context.Script.location *
        Micheline.canonical Alpha_context.Script.prim *
        Micheline.canonical Alpha_context.Script.prim)
      ((location expr), (Micheline.strip_locations expr), ty) in
  let fail_parse_data {B : Set} (function_parameter : unit) : M? B :=
    let '_ := function_parameter in
    Error_monad.fail (parse_data_error tt) in
  let traced_no_lwt {B : Set} (body : M? B) : M? B :=
    Error_monad.record_trace_eval parse_data_error body in
  let traced {B : Set} (body : M? B) : M? B :=
    Error_monad.trace_eval parse_data_error body in
  let traced_fail {B : Set} (err : Error_monad._error) : M? B :=
    traced_no_lwt (Error_monad.error_value err) in
  let parse_items {tk tv: Ty.t}
    (type_logger_value : option type_logger)
    (ctxt : Alpha_context.context)
    (expr : Micheline.node
              Alpha_context.Script.location
              Alpha_context.Script.prim)
    (key_type : With_family.ty tk)
    (value_type : With_family.ty tv)
    (items :
      list
        (Micheline.node Alpha_context.Script.location
           Alpha_context.Script.prim))
    : M? (With_family.map tk (With_family.ty_to_dep_Set tv) *
            Alpha_context.context) :=
    let? '(_, items, ctxt) :=
      traced
        (List.fold_left_es
           (fun (function_parameter :
                option (With_family.ty_to_dep_Set tk) *
                  With_family.map tk (With_family.ty_to_dep_Set tv) *
                  Alpha_context.context) =>
            let '(last_value, map, ctxt) := function_parameter in
            fun (item :
              Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim) =>
              match item with
              |
                Micheline.Prim loc_value Michelson_v1_primitives.D_Elt
                  (cons k_value (cons v_value [])) annot =>
                let? '_ :=
                  if legacy then
                    Result.return_unit
                  else
                    Script_ir_annot.error_unexpected_annot loc_value annot in
                let? '(k_value, ctxt) :=
                  dep_parse_comparable_data
                    type_logger_value ctxt key_type k_value
                  in
                let? '(v_value, ctxt) :=
                  non_terminal_recursion fuel type_logger_value ctxt legacy
                    value_type v_value in
                let? ctxt :=
                  match last_value with
                  | Some value_value =>
                    let? ctxt :=
                      Alpha_context.Gas.consume ctxt
                        (Michelson_v1_gas.Cost_of.Interpreter.compare
                           (With_family.to_ty key_type)
                           (With_family.to_value value_value)
                           (With_family.to_value k_value)) in
                    let c_value :=
                      Script_comparable.dep_compare_comparable
                        tk (With_family.to_value value_value)
                        (With_family.to_value k_value) in
                    if 0 <=i c_value then
                      if 0 =i c_value then
                        Error_monad.error_value
                          (Build_extensible "Duplicate_map_keys"
                            (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                            (loc_value, (Micheline.strip_locations expr)))
                      else
                        Error_monad.error_value
                          (Build_extensible "Unordered_map_keys"
                            (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                            (loc_value, (Micheline.strip_locations expr)))
                    else
                      return? ctxt
                  | None => return? ctxt
                  end in
                let? ctxt :=
                  Alpha_context.Gas.consume ctxt
                    (Michelson_v1_gas.Cost_of.Interpreter.map_update
                       (With_family.to_value k_value) (With_family.to_map map))
                  in
                return?
                  ((Some k_value),
                    (Script_map.dep_update k_value (Some v_value) map),
                    ctxt)
              | Micheline.Prim loc_value Michelson_v1_primitives.D_Elt l_value _
                =>
                Error_monad.fail
                  (Build_extensible "Invalid_arity"
                    (Alpha_context.Script.location * Alpha_context.Script.prim *
                      int * int)
                    (loc_value, Michelson_v1_primitives.D_Elt, 2,
                      (List.length l_value)))
              | Micheline.Prim loc_value name _ _ =>
                Error_monad.fail
                  (Build_extensible "Invalid_primitive"
                    (Alpha_context.Script.location *
                       list Alpha_context.Script.prim *
                       Alpha_context.Script.prim)
                    (loc_value, [ Michelson_v1_primitives.D_Elt ], name))
              |
                (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _
                | Micheline.Seq _ _) => fail_parse_data tt
              end) (None, (Script_map.dep_empty key_type), ctxt) items) in
    return? (items, ctxt) in
  let parse_big_map_items {tk tv : Ty.t}
    (type_logger_value : option type_logger)
    (ctxt : Alpha_context.context)
    (expr : Micheline.node
              Alpha_context.Script.location
              Alpha_context.Script.prim)
    (key_type : With_family.ty tk)
    (value_type : With_family.ty tv)
    (items : list (Micheline.node
                     Alpha_context.Script.location
                     Alpha_context.Script.prim))
    : M? (Script_typed_ir.big_map_overlay
           (With_family.ty_to_dep_Set tk)
           (With_family.ty_to_dep_Set tv) * Alpha_context.context) :=
    let? '(_, map, ctxt) :=
      traced
        (List.fold_left_es
           (fun (function_parameter :
                option (With_family.ty_to_dep_Set tk) *
                  Script_typed_ir.big_map_overlay
                    (With_family.ty_to_dep_Set tk)
                    (With_family.ty_to_dep_Set tv) *
                  Alpha_context.context) =>
              let
                '(last_key, {|
                              Script_typed_ir.big_map_overlay.map := map;
                              Script_typed_ir.big_map_overlay.size := size_value
                            |}, ctxt) := function_parameter in
              fun (item :
                  Micheline.node Alpha_context.Script.location
                    Alpha_context.Script.prim) =>
                match item with
                |
                  Micheline.Prim loc_value Michelson_v1_primitives.D_Elt
                    (cons k_value (cons v_value [])) annot =>
                    let? '_ :=
                      if legacy then
                        Result.return_unit
                      else
                        Script_ir_annot.error_unexpected_annot loc_value annot in
                    let? '(k_value, ctxt) :=
                      dep_parse_comparable_data
                        type_logger_value ctxt key_type k_value
                    in
                    let? '(key_hash, ctxt) :=
                      dep_hash_comparable_data ctxt key_type k_value in
                    let? '(v_value, ctxt) :=
                      non_terminal_recursion
                        fuel type_logger_value ctxt legacy value_type v_value in
                    let? ctxt :=
                      match last_key with
                      | Some last_key =>
                          let? ctxt :=
                            Alpha_context.Gas.consume ctxt
                              (Michelson_v1_gas.Cost_of.Interpreter.compare
                                 (With_family.to_ty key_type)
                                 (With_family.to_value last_key)
                                 (With_family.to_value k_value)) in
                          let c_value :=
                            Script_comparable.dep_compare_comparable tk
                              (With_family.to_value last_key)
                              (With_family.to_value k_value) in
                          if 0 <=i c_value then
                            if 0 =i c_value then
                              Error_monad.error_value
                                (Build_extensible "Duplicate_map_keys"
                                   (Alpha_context.Script.location *
                                      Micheline.canonical Alpha_context.Script.prim)
                                   (loc_value, (Micheline.strip_locations expr)))
                            else
                              Error_monad.error_value
                                (Build_extensible "Unordered_map_keys"
                                   (Alpha_context.Script.location *
                                      Micheline.canonical Alpha_context.Script.prim)
                                   (loc_value, (Micheline.strip_locations expr)))
                          else
                            return? ctxt
                      | None => return? ctxt
                      end in
                    let? ctxt :=
                      Alpha_context.Gas.consume ctxt
                        (Michelson_v1_gas.Cost_of.Interpreter.big_map_update
                           {| Script_typed_ir.big_map_overlay.map := map;
                             Script_typed_ir.big_map_overlay.size := size_value |})
                    in
                    if
                      Script_typed_ir.Big_map_overlay.(Map.S.mem) key_hash map
                    then
                      Error_monad.error_value
                        (Build_extensible "Duplicate_map_keys"
                           (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                           (loc_value, (Micheline.strip_locations expr)))
                    else
                      return?
                        ((Some k_value),
                          {|
                            Script_typed_ir.big_map_overlay.map :=
                              Script_typed_ir.Big_map_overlay.(Map.S.add)
                                key_hash (k_value, Some v_value) map;
                            Script_typed_ir.big_map_overlay.size := size_value +i 1
                          |}, ctxt)
                | Micheline.Prim loc_value Michelson_v1_primitives.D_Elt l_value _
                  =>
                    Error_monad.fail
                      (Build_extensible "Invalid_arity"
                         (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                         (loc_value, Michelson_v1_primitives.D_Elt, 2,
                           (List.length l_value)))
                | Micheline.Prim loc_value name _ _ =>
                    Error_monad.fail
                      (Build_extensible "Invalid_primitive"
                         (Alpha_context.Script.location *
                            list Alpha_context.Script.prim *
                            Alpha_context.Script.prim)
                         (loc_value, [ Michelson_v1_primitives.D_Elt ], name))
                | (Micheline.Int _ _ | Micheline.String _ _ |
                    Micheline.Bytes _ _ | Micheline.Seq _ _)
                  => fail_parse_data tt
                end)
           (None,
             {| Script_typed_ir.big_map_overlay.map :=
                 Script_typed_ir.Big_map_overlay.(Map.S.empty);
               Script_typed_ir.big_map_overlay.size := 0 |}, ctxt) items) in
    return? (map, ctxt) in
  (* @TODO: remove parse_big_map_items2, use parse_big_map_items instead *)
  let parse_big_map_items2 {tk tv : Ty.t}
    (type_logger_value : option type_logger)
    (ctxt : Alpha_context.context)
    (expr : Micheline.node
              Alpha_context.Script.location
              Alpha_context.Script.prim)
    (key_type : With_family.ty tk)
    (value_type : With_family.ty (Ty.Option tv))
    (items : list (Micheline.node
                     Alpha_context.Script.location
                     Alpha_context.Script.prim))
    : M? (Script_typed_ir.big_map_overlay
            (With_family.ty_to_dep_Set tk)
            (With_family.ty_to_dep_Set tv) * Alpha_context.context) :=
    let? '(_, map, ctxt) :=
      traced
        (List.fold_left_es
           (fun (function_parameter :
                option (With_family.ty_to_dep_Set tk) *
                  Script_typed_ir.big_map_overlay
                    (With_family.ty_to_dep_Set tk)
                    (With_family.ty_to_dep_Set tv) *
                  Alpha_context.context) =>
              let
                '(last_key, {|
                              Script_typed_ir.big_map_overlay.map := map;
                              Script_typed_ir.big_map_overlay.size := size_value
                            |}, ctxt) := function_parameter in
              fun (item :
                  Micheline.node Alpha_context.Script.location
                    Alpha_context.Script.prim) =>
                match item with
                |
                  Micheline.Prim loc_value Michelson_v1_primitives.D_Elt
                    (cons k_value (cons v_value [])) annot =>
                    let? '_ :=
                      if legacy then
                        Result.return_unit
                      else
                        Script_ir_annot.error_unexpected_annot loc_value annot in
                    let? '(k_value, ctxt) :=
                      dep_parse_comparable_data
                        type_logger_value ctxt key_type k_value
                    in
                    let? '(key_hash, ctxt) :=
                      dep_hash_comparable_data ctxt key_type k_value in
                    let? '(v_value, ctxt) :=
                      non_terminal_recursion
                        fuel type_logger_value ctxt legacy value_type v_value in
                    let? ctxt :=
                      match last_key with
                      | Some last_key =>
                          let? ctxt :=
                            Alpha_context.Gas.consume ctxt
                              (Michelson_v1_gas.Cost_of.Interpreter.compare
                                 (With_family.to_ty key_type)
                                 (With_family.to_value last_key)
                                 (With_family.to_value k_value)) in
                          let c_value :=
                            Script_comparable.dep_compare_comparable tk
                              (With_family.to_value last_key)
                              (With_family.to_value k_value) in
                          if 0 <=i c_value then
                            if 0 =i c_value then
                              Error_monad.error_value
                                (Build_extensible "Duplicate_map_keys"
                                   (Alpha_context.Script.location *
                                      Micheline.canonical Alpha_context.Script.prim)
                                   (loc_value, (Micheline.strip_locations expr)))
                            else
                              Error_monad.error_value
                                (Build_extensible "Unordered_map_keys"
                                   (Alpha_context.Script.location *
                                      Micheline.canonical Alpha_context.Script.prim)
                                   (loc_value, (Micheline.strip_locations expr)))
                          else
                            return? ctxt
                      | None => return? ctxt
                      end in
                    let? ctxt :=
                      Alpha_context.Gas.consume ctxt
                        (Michelson_v1_gas.Cost_of.Interpreter.big_map_update
                           {| Script_typed_ir.big_map_overlay.map := map;
                             Script_typed_ir.big_map_overlay.size := size_value |})
                    in
                    if
                      Script_typed_ir.Big_map_overlay.(Map.S.mem) key_hash map
                    then
                      Error_monad.error_value
                        (Build_extensible "Duplicate_map_keys"
                           (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                           (loc_value, (Micheline.strip_locations expr)))
                    else
                      return?
                        ((Some k_value),
                          {|
                            Script_typed_ir.big_map_overlay.map :=
                              Script_typed_ir.Big_map_overlay.(Map.S.add)
                                key_hash (k_value, v_value) map;
                            Script_typed_ir.big_map_overlay.size := size_value +i 1
                          |}, ctxt)
                | Micheline.Prim loc_value Michelson_v1_primitives.D_Elt l_value _
                  =>
                    Error_monad.fail
                      (Build_extensible "Invalid_arity"
                         (Alpha_context.Script.location *
                            Alpha_context.Script.prim * int * int)
                         (loc_value, Michelson_v1_primitives.D_Elt, 2,
                           (List.length l_value)))
                | Micheline.Prim loc_value name _ _ =>
                    Error_monad.fail
                      (Build_extensible "Invalid_primitive"
                         (Alpha_context.Script.location *
                            list Alpha_context.Script.prim *
                            Alpha_context.Script.prim)
                         (loc_value, [ Michelson_v1_primitives.D_Elt ], name))
                | (Micheline.Int _ _ | Micheline.String _ _ |
                    Micheline.Bytes _ _ | Micheline.Seq _ _)
                  => fail_parse_data tt
                end)
           (None,
             {| Script_typed_ir.big_map_overlay.map :=
                 Script_typed_ir.Big_map_overlay.(Map.S.empty);
               Script_typed_ir.big_map_overlay.size := 0 |}, ctxt) items) in
    return? (map, ctxt) in
  match ty
        in (With_family.ty t), expr
        return M? (With_family.ty_to_dep_Set t * Alpha_context.context) with
  | With_family.Unit_t, _ =>
      traced_no_lwt (parse_unit ctxt legacy expr)
  | With_family.Bool_t, _ =>
      traced_no_lwt (parse_bool ctxt legacy expr)
  | With_family.String_t, _ =>
      traced_no_lwt (parse_string ctxt expr)
  | With_family.Bytes_t, _ =>
      traced_no_lwt (parse_bytes ctxt expr)
  | With_family.Int_t, _ =>
      traced_no_lwt (parse_int ctxt expr)
  | With_family.Nat_t, _ =>
      traced_no_lwt (parse_nat ctxt expr)
  | With_family.Mutez_t, _ =>
      traced_no_lwt (parse_mutez ctxt expr)
  | With_family.Timestamp_t, _ =>
      traced_no_lwt (parse_timestamp ctxt expr)
  | With_family.Key_t, _ =>
      traced_no_lwt (parse_key ctxt expr)
  | With_family.Key_hash_t, _ =>
      traced_no_lwt (parse_key_hash ctxt expr)
  | With_family.Signature_t, _ =>
      traced_no_lwt (parse_signature ctxt expr)
  | With_family.Operation_t, _ =>
      (* ❌ Assert instruction is not handled. *)
      (* @TODO *)
      assert
        (M? (With_family.ty_to_dep_Set Ty.Operation * Alpha_context.context))
        false
  | With_family.Chain_id_t, _ =>
      traced_no_lwt (parse_chain_id ctxt expr)
  | With_family.Address_t, _ =>
      traced_no_lwt (parse_address ctxt expr)
  | With_family.Tx_rollup_l2_address_t, _ =>
      traced_no_lwt (parse_tx_rollup_l2_address ctxt expr)
  | With_family.Contract_t arg_ty _, expr =>
      traced
        (let? '(address, ctxt) := parse_address ctxt expr in
         let loc_value := location expr in
         let? '(ctxt, _) :=
           match fuel with
           | Datatypes.O =>
               Error_monad.fail
                 (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
           | Datatypes.S fuel =>
               dep_parse_contract_data_aux_fuel fuel ctxt loc_value arg_ty
                 address.(Script_typed_ir.address.destination)
                           address.(Script_typed_ir.address.entrypoint)
           end in
         return?
           (With_family.Typed_contract arg_ty address, ctxt))
  | With_family.Pair_t tl tr _ _, expr =>
    let r_witness := dep_comb_witness1 tr in
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      non_terminal_recursion fuel type_logger_value ctxt legacy tl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      non_terminal_recursion fuel type_logger_value ctxt legacy tr v_value in
    traced (parse_pair parse_l parse_r ctxt legacy r_witness expr)
  | With_family.Union_t tl tr _ _, expr =>
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      non_terminal_recursion fuel type_logger_value ctxt legacy tl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      non_terminal_recursion fuel type_logger_value ctxt legacy tr v_value in
    traced (parse_union parse_l parse_r ctxt legacy expr)
  | With_family.Lambda_t ta tr _ty_name,
    (Micheline.Seq _loc _) as script_instr =>
      traced
         match fuel with
         | Datatypes.O =>
             Error_monad.fail
               (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
         | Datatypes.S fuel =>
             dep_parse_returning_fuel type_logger_value fuel Script_tc_context.dep_data ctxt
                legacy ta tr script_instr
         end
  | With_family.Lambda_t _ _ _, expr =>
    ((traced_fail (B := _ * Raw_context.t))
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Seq_kind ], kind_value expr)))
  | With_family.Option_t t_value _ _, expr =>
    let parse_v
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node)
      : M? (_ * Alpha_context.context) :=
      non_terminal_recursion fuel type_logger_value ctxt legacy t_value v_value in
     traced (parse_option parse_v ctxt legacy expr)
  | With_family.List_t t_value _ty_name, Micheline.Seq _loc items =>
    traced
      (List.fold_right_es
        (fun (v_value : Alpha_context.Script.node) =>
          fun (function_parameter :
            Script_typed_ir.boxed_list _ * Alpha_context.context) =>
            let '(rest, ctxt) := function_parameter in
            let? '(v_value, ctxt) :=
              non_terminal_recursion fuel type_logger_value ctxt legacy t_value
                v_value in
            return? ((Script_list.cons_value v_value rest), ctxt)) items
        (Script_list.empty, ctxt))
  | With_family.List_t _ _, _ =>
    traced_fail (B := _ * Raw_context.t)
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Seq_kind ], kind_value expr))
  | With_family.Ticket_t t_value _ty_name, _ =>
      if allow_forged then
      let? ty := dep_opened_ticket_type (location expr) t_value in
      let?
        '(({|
          Script_typed_ir.address.destination := destination;
            Script_typed_ir.address.entrypoint := _
            |}, (contents, amount)), ctxt) :=
        dep_parse_comparable_data type_logger_value ctxt ty expr in
      match destination with
      | Alpha_context.Destination.Contract ticketer =>
        return?
          ({| Script_typed_ir.ticket.ticketer := ticketer;
            Script_typed_ir.ticket.contents := contents;
            Script_typed_ir.ticket.amount := amount |}, ctxt)
      | Alpha_context.Destination.Tx_rollup _
      | Alpha_context.Destination.Sc_rollup _ =>
        Error_monad.fail
          (Build_extensible "Unexpected_ticket_owner"
            Alpha_context.Destination.t destination)
      end
    else
      traced_fail
        (Build_extensible "Unexpected_forged_value"
          Alpha_context.Script.location (location expr))
  | @With_family.Set_t t_type t_value _ty_name, (Micheline.Seq loc_value vs) as expr =>
    let? '(_, set, ctxt) :=
      traced
        (List.fold_left_es
          (fun (function_parameter :
            option _ * With_family.set _ * Alpha_context.context) =>
            let '(last_value, set, ctxt) := function_parameter in
            fun (v_value : Alpha_context.Script.node) =>
              let? '(v_value, ctxt) :=
                dep_parse_comparable_data type_logger_value ctxt t_value v_value in
              let? ctxt :=
                match last_value with
                | Some value_value =>
                  let? ctxt :=
                    Alpha_context.Gas.consume ctxt
                      (Michelson_v1_gas.Cost_of.Interpreter.compare
                         (With_family.to_ty t_value)
                         (With_family.to_value value_value)
                         (With_family.to_value v_value)) in
                  let c_value :=
                    Script_comparable.dep_compare_comparable
                      t_type
                      (With_family.to_value value_value)
                      (With_family.to_value v_value) in
                  if 0 <=i c_value then
                    if 0 =i c_value then
                      Error_monad.error_value
                        (Build_extensible "Duplicate_set_values"
                          (Alpha_context.Script.location *
                            Micheline.canonical Alpha_context.Script.prim)
                          (loc_value, Micheline.strip_locations expr))
                    else
                      Error_monad.error_value
                        (Build_extensible "Unordered_set_values"
                          (Alpha_context.Script.location *
                            Micheline.canonical Alpha_context.Script.prim)
                          (loc_value, Micheline.strip_locations expr))
                  else
                    return? ctxt
                | None => return? ctxt
                end in
              let? ctxt :=
                Alpha_context.Gas.consume ctxt
                  (Michelson_v1_gas.Cost_of.Interpreter.set_update
                     (With_family.to_value v_value) (With_family.to_set set))
                in
              return?
                (Some v_value, Script_set.dep_update v_value true set, ctxt))
          (None, (Script_set.dep_empty t_value), ctxt) vs) in
    return? (set, ctxt)
  | With_family.Set_t _ _, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Seq_kind ], kind_value expr))
  | With_family.Map_t tyk tyv meta, (Micheline.Seq _ vs) as expr =>
      parse_items type_logger_value ctxt expr tyk tyv vs
  | With_family.Map_t _ _ _, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Seq_kind ], kind_value expr))
  | @With_family.Big_map_t tk tv tyk tyv _ty_name, expr =>
    (let? '(id_opt, diff_value, ctxt) :=
      match expr with
      | Micheline.Int loc_value id =>
        ((return?
          ((Some (id, loc_value)),
            {|
              Script_typed_ir.big_map_overlay.map :=
                Script_typed_ir.Big_map_overlay.(Map.S.empty);
              Script_typed_ir.big_map_overlay.size := 0 |}, ctxt)) :
          M?
            (option (Z.t * Alpha_context.Script.location) *
              Script_typed_ir.big_map_overlay (With_family.ty_to_dep_Set tk) (With_family.ty_to_dep_Set tv) * Alpha_context.context))
      | Micheline.Seq _ vs =>
          let? '(diff_value, ctxt) :=
            parse_big_map_items type_logger_value ctxt expr tyk tyv vs in
        return? (None, diff_value, ctxt)
      |
        Micheline.Prim loc_value Michelson_v1_primitives.D_Pair
          (cons (Micheline.Int loc_id id) (cons (Micheline.Seq _ vs) [])) annot
        =>
        let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
        let? tyv_opt := Script_typed_ir.dep_option_t loc_value tyv in
        let? '(diff_value, ctxt) :=
          parse_big_map_items2 type_logger_value ctxt expr tyk tyv_opt vs in
        return? ((Some (id, loc_id)), diff_value, ctxt)
      |
        Micheline.Prim _ Michelson_v1_primitives.D_Pair
          (cons (Micheline.Int _ _) (cons expr [])) _ =>
        traced_fail
          (Build_extensible "Invalid_kind"
            (Alpha_context.Script.location * list Script_tc_errors.kind *
              Script_tc_errors.kind)
            ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
      |
        Micheline.Prim _ Michelson_v1_primitives.D_Pair (cons expr (cons _ []))
          _ =>
        traced_fail
          (Build_extensible "Invalid_kind"
            (Alpha_context.Script.location * list Script_tc_errors.kind *
              Script_tc_errors.kind)
            ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
      | Micheline.Prim loc_value Michelson_v1_primitives.D_Pair l_value _ =>
        traced_fail
          (Build_extensible "Invalid_arity"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              int)
            (loc_value, Michelson_v1_primitives.D_Pair, 2, (List.length l_value)))
      | _ =>
        traced_fail
          (unexpected expr
            [ Script_tc_errors.Seq_kind; Script_tc_errors.Int_kind ]
            Michelson_v1_primitives.Constant_namespace
            [ Michelson_v1_primitives.D_Pair ])
      end in
    let? '(id, ctxt) :=
      match id_opt with
      | None => return? (None, ctxt)
      | Some (id, loc_value) =>
        if allow_forged then
          let id := Alpha_context.Big_map.Id.parse_z id in
          let? function_parameter := Alpha_context.Big_map._exists ctxt id in
          match function_parameter with
          | (_, None) =>
            traced_fail
              (Build_extensible "Invalid_big_map"
                (Alpha_context.Script.location * Alpha_context.Big_map.Id.t)
                (loc_value, id))
          | (ctxt, Some (btk, btv)) =>
            let? '(Dep_ex_comparable_ty btk, ctxt) :=
              dep_parse_comparable_ty_aux_fuel fuel ctxt
                (Micheline.root_value btk) in
            let? '(Dep_ex_ty btv, ctxt) :=
                  match fuel with
                  | Datatypes.O =>
                      Error_monad.fail
                        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
                  | Datatypes.S fuel =>
                      'dep_parse_big_map_value_ty_aux_fuel ctxt fuel legacy
                        (Micheline.root_value btv)
                  end in
            let? '(eq_value, ctxt) :=
              Gas_monad.run ctxt
                (let error_details :=
                  Script_tc_errors.Dep_informative loc_value in
                Gas_monad.Syntax.op_letstar
                  (dep_ty_eq error_details tyk btk)
                  (fun function_parameter =>
                    let 'eq_refl := function_parameter in
                    dep_ty_eq error_details tyv btv)) in
            let? 'eq_refl := eq_value in
            return? ((Some id), ctxt)
          end
        else
          traced_fail
            (Build_extensible "Unexpected_forged_value"
              Alpha_context.Script.location loc_value)
      end in
    return?
      ({| With_family.big_map.id := id;
          With_family.big_map.diff := diff_value;
          With_family.big_map.key_type := tyk;
          With_family.big_map.value_type := tyv |}, ctxt))
  | With_family.Never_t, _ =>
      traced_no_lwt (parse_never expr)
  | With_family.Bls12_381_g1_t, Micheline.Bytes _ bs =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_g1 in
    match
      Script_typed_ir.Script_bls.G1
        .(Script_typed_ir.Script_bls.S.of_bytes_opt) bs
    with
    | Some pt => return? (pt, ctxt)
    | None => fail_parse_data tt
    end
  | With_family.Bls12_381_g1_t, _ =>
    traced_fail (B := _ * Raw_context.t)
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Bls12_381_g2_t, Micheline.Bytes _ bs =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_g2 in
    match
      Script_typed_ir.Script_bls.G2
        .(Script_typed_ir.Script_bls.S.of_bytes_opt) bs
    with
    | Some pt => return? (pt, ctxt)
    | None => fail_parse_data tt
    end
  | With_family.Bls12_381_g2_t, _ =>
    traced_fail (B := _ * Raw_context.t)
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Bls12_381_fr_t, Micheline.Bytes _ bs =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_fr in
    match Script_typed_ir.Script_bls.Fr.of_bytes_opt bs with
    | Some pt => return? (pt, ctxt)
    | None => fail_parse_data tt
    end
  | With_family.Bls12_381_fr_t, Micheline.Int _ v_value =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_fr in
    return? ((Script_typed_ir.Script_bls.Fr.of_z v_value), ctxt)
  | With_family.Bls12_381_fr_t, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Sapling_transaction_t memo_size,
      Micheline.Bytes _ bytes_value =>
    match
      Data_encoding.Binary.of_bytes_opt
        Alpha_context.Sapling.transaction_encoding bytes_value with
    | Some transaction =>
      match Alpha_context.Sapling.transaction_get_memo_size transaction with
      | None => return? (transaction, ctxt)
      | Some transac_memo_size =>
        let? '_ :=
          memo_size_eq (Script_tc_errors.Informative tt) memo_size
            transac_memo_size
          in
        return? (transaction, ctxt)
      end
    | None => fail_parse_data tt
    end
  | With_family.Sapling_transaction_t _, expr =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Sapling_transaction_deprecated_t memo_size,
      Micheline.Bytes _ bytes_value =>
    match
      Data_encoding.Binary.of_bytes_opt
        Alpha_context.Sapling.Legacy.transaction_encoding bytes_value with
    | Some transaction =>
      match Alpha_context.Sapling.Legacy.transaction_get_memo_size transaction
        with
      | None => return? (transaction, ctxt)
      | Some transac_memo_size =>
        let? '_ :=
          memo_size_eq (Script_tc_errors.Informative tt) memo_size
            transac_memo_size
          in
        return? (transaction, ctxt)
      end
    | None => fail_parse_data tt
    end
  | With_family.Sapling_transaction_deprecated_t _, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
                (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Sapling_state_t memo_size, Micheline.Int loc_value id =>
    if allow_forged then
      let id := Alpha_context.Sapling.Id.parse_z id in
      let? '(state_value, ctxt) := Alpha_context.Sapling.state_from_id ctxt id
        in
      let? '_ :=
        traced_no_lwt
          (memo_size_eq (Script_tc_errors.Informative tt) memo_size
            state_value.(Alpha_context.Sapling.state.memo_size)) in
      return? (state_value, ctxt)
    else
      traced_fail
        (Build_extensible "Unexpected_forged_value"
          Alpha_context.Script.location loc_value)
  | With_family.Sapling_state_t memo_size, Micheline.Seq _ [] =>
    (Error_monad._return :
      Alpha_context.Sapling.state * Alpha_context.context ->
      M? (Alpha_context.Sapling.state * Alpha_context.context))
      (Alpha_context.Sapling.empty_state None memo_size tt, ctxt)
  | With_family.Sapling_state_t _, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr,
          [ Script_tc_errors.Int_kind; Script_tc_errors.Seq_kind ],
          kind_value expr))
  | With_family.Chest_key_t, Micheline.Bytes _ bytes_value =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.chest_key_value
    in
    match
      Data_encoding.Binary.of_bytes_opt
        Script_typed_ir.Script_timelock.chest_key_encoding bytes_value with
    | Some chest_key_value => return? (chest_key_value, ctxt)
    | None => fail_parse_data tt
    end
  | With_family.Chest_key_t, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  | With_family.Chest_t, Micheline.Bytes _ bytes_value =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Typecheck_costs.chest_value (Bytes.length bytes_value)) in
    match
      Data_encoding.Binary.of_bytes_opt
        Script_typed_ir.Script_timelock.chest_encoding bytes_value with
    | Some chest_value => return? (chest_value, ctxt)
    | None => fail_parse_data tt
    end
  | With_family.Chest_t, _ =>
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
            (location expr, [ Script_tc_errors.Bytes_kind ], kind_value expr))
  (* | _, _ => axiom *)
  end
  (* to see the an order *)
  (* with parse_view_returning *)
  (* with typecheck_views *)

(** Simulation of [dep_parse_instr_aux] (the body using fuel). *)
with dep_parse_instr_aux_fuel {s}
  (type_logger_value : option type_logger) (fuel : nat)
  (tc_context_value : dep_tc_context) (ctxt : Alpha_context.context)
  (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : With_family.stack_ty s) :
  M? (dep_judgement s * Alpha_context.context) := axiom
  (* dep_parse_view_name is moved up *)

(** Simulation of [parse_returning]. *)
with dep_parse_returning_fuel
  {targ tret} (type_logger_value : option type_logger) (fuel : nat)
  (tc_context_value : dep_tc_context) (ctxt : Alpha_context.context)
  (legacy : bool) (arg : With_family.ty targ) (ret_value : With_family.ty tret)
  (script_instr : Alpha_context.Script.node) :=
  let? '(judg, ctxt) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
        dep_parse_instr_aux_fuel type_logger_value fuel tc_context_value
          ctxt legacy script_instr (With_family.Item_t arg With_family.Bot_t)
    end
  in
  match
    judg
  return M? (With_family.lambda targ tret * Alpha_context.context)
  with
  | Dep_typed
      ({|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}) =>
    match aft, instr with
    | (With_family.Item_t ty With_family.Bot_t) as stack_ty, _ =>
      let error_details := Script_tc_errors.Dep_informative loc_value in
      let? '(eq_value, ctxt) :=
      Gas_monad.run ctxt
        (Gas_monad.record_trace_eval
          (Script_tc_errors.to_error_details error_details)
          (fun (loc_value : Alpha_context.Script.location) =>
            let ret_value := dep_serialize_ty_for_error ret_value in
            let stack_ty := dep_serialize_stack_for_error ctxt stack_ty in
            Build_extensible "Bad_return"
              (Alpha_context.Script.location *
                Script_tc_errors.unparsed_stack_ty *
                Micheline.canonical Alpha_context.Script.prim)
              (loc_value, stack_ty, ret_value))
          (dep_ty_eq error_details ty ret_value)) in
    let? e := eq_value in
    let descr_value : dep_descr [targ] [tret] :=
      let 'eq_refl := e in
      dep_descr.Build _ _ loc_value bef stack_ty instr in
    return?
      ((dep_close_descr descr_value, script_instr), ctxt)
    | stack_ty, _ =>
      let ret_value := dep_serialize_ty_for_error ret_value in
      let stack_ty := dep_serialize_stack_for_error ctxt stack_ty in
      Error_monad.fail
        (Build_extensible "Bad_return"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Micheline.canonical Alpha_context.Script.prim)
          (loc_value, stack_ty, ret_value))
    end
  | Dep_failed descr_value =>
    return?
      ((dep_close_descr
          (descr_value _
            (With_family.Item_t ret_value With_family.Bot_t)),
        script_instr), ctxt)
  end

with dep_parse_contract_fuel {err a} (fuel : nat)
  (ctxt : Alpha_context.context)
  (error_details :
    Script_tc_errors.error_details Alpha_context.Script.location)
  (loc_value : Alpha_context.Script.location) (arg : With_family.ty a)
  (destination : Alpha_context.Destination.t)
  (entrypoint : Alpha_context.Entrypoint.t) :
  M? (
    Alpha_context.context *
    Pervasives.result (With_family.typed_contract a) err
  ):=
  axiom (* TODO *)

with dep_parse_contract_data_aux_fuel {a}
  (fuel : nat) (ctxt : Alpha_context.context)
  (loc_value : Alpha_context.Script.location) (arg : With_family.ty a)
  (destination : Alpha_context.Destination.t)
  (entrypoint : Alpha_context.Entrypoint.t) :=
  let parse_contract {err} := 'parse_contract err in
  let error_details := Script_tc_errors.Informative loc_value in
  let? '(ctxt, res) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
      dep_parse_contract_fuel fuel ctxt error_details loc_value arg
        destination entrypoint
    end in
  let? res := res in
  return? (ctxt, res).

Definition dep_parse_data_aux {a} type_logger_value (stack_depth : int) :=
  @dep_parse_data_aux_fuel a type_logger_value (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_instr_aux]. *)
Definition dep_parse_instr_aux {s}
  (type_logger_value : option type_logger) (stack_depth : int)
  (tc_context_value : dep_tc_context) (ctxt : Alpha_context.context)
  (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : With_family.stack_ty s) :
  M? (dep_judgement s * Alpha_context.context).
Admitted.

(** Simuation of [parse_view]. *)
Definition dep_parse_view {storage}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (storage_type : With_family.ty storage)
  (function_parameter : Script_typed_ir.view) :
  M? (dep_typed_view storage * Alpha_context.context).
Admitted.

(** Simuation of [parse_views]. *)
Definition dep_parse_views {storage}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (storage_type : With_family.ty storage)
  (views : With_family.view_map) :
  M? (dep_typed_view_map storage * Alpha_context.context).
Admitted.

(** Simulation of [parse_returning]. *)
Definition dep_parse_returning {targ tret}
  (type_logger_value : option type_logger) (stack_depth : int) :=
  @dep_parse_returning_fuel targ tret
    type_logger_value (stack_depth_to_fuel stack_depth).

Fixpoint dep_parse_instr_aux_fuel1 {s : Stack_ty.t}
  (type_logger_value : option type_logger) (fuel : nat)
  (tc_context_value : dep_tc_context) (ctxt : Alpha_context.context) (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : With_family.stack_ty s)
  : M? (dep_judgement s * Alpha_context.context) :=
  let check_item_ty {a b : Ty.t}
    (ctxt : Alpha_context.context) (exp : With_family.ty a)
    (got : With_family.ty b) (loc_value : Alpha_context.Script.location)
    (name : Alpha_context.Script.prim) (n_value : int) (m_value : int)
    : M? ((a = b) * Alpha_context.context) :=
    Error_monad.record_trace_eval
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        let stack_ty := dep_serialize_stack_for_error ctxt stack_ty in
        Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty)
          (loc_value, name, m_value, stack_ty))
      (Error_monad.record_trace (Build_extensible "Bad_stack_item" int n_value)
        (let? '(eq_value, ctxt) :=
          Gas_monad.run ctxt
            (dep_ty_eq (Script_tc_errors.Dep_informative loc_value) exp got) in
        let? 'eq_refl := eq_value in
        return? (eq_refl, ctxt))) in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
  let non_terminal_recursion {s : Stack_ty.t}
    (type_logger_value : option type_logger) (tc_context_value : dep_tc_context)
    (ctxt : Alpha_context.context) (legacy : bool)
    (script_instr : Alpha_context.Script.node)
    (stack_ty : With_family.stack_ty s)
    : M? (dep_judgement s * Alpha_context.context) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
      dep_parse_instr_aux_fuel1 type_logger_value fuel tc_context_value ctxt
        legacy script_instr stack_ty
    end
  in
  match
    script_instr,
    stack_ty in With_family.stack_ty s
  return M? (dep_judgement s * Alpha_context.context)
  with
  | Micheline.Prim loc_value Michelson_v1_primitives.I_DROP [] annot,
      With_family.Item_t _ rest as stack_ty =>
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := rest;
      dep_descr.instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IDrop loc_value k_value;
      |};
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DROP (cons n_value [])
      result_annot, whole_stack =>
    let? whole_n := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument whole_n) in
    let fix make_proof_argument {ts} (n_value : int) (stk : With_family.stack_ty ts)
      : M? dep_dropn_proof_argument ts :=
      match (n_value =? 0), stk with
      | true, rest =>
        return? (Dep_dropn_proof_argument With_family.KRest rest)
      | false, With_family.Item_t a_value rest =>
        let? 'Dep_dropn_proof_argument n' stack_after_drops :=
          make_proof_argument (n_value - 1) rest in
        return?
          (Dep_dropn_proof_argument (With_family.KPrefix loc_value a_value n')
            stack_after_drops)
      | _, _ =>
        let whole_stack := dep_serialize_stack_for_error ctxt whole_stack in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc_value, Michelson_v1_primitives.I_DROP, whole_n, whole_stack))
      end in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value result_annot in
    let? 'Dep_dropn_proof_argument n' stack_after_drops :=
      make_proof_argument whole_n whole_stack in
    return?
      (Dep_typed {|
         dep_descr.loc := loc_value;
         dep_descr.bef := stack_ty;
         dep_descr.aft := stack_after_drops;
         dep_descr.instr := {|
           dep_cinstr.apply _ k_value :=
             With_family.IDropn loc_value whole_n n' k_value;
         |}
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DROP
      ((cons _ (cons _ _)) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, Michelson_v1_primitives.I_DROP, 1, (List.length l_value)))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DUP [] annot,
    @With_family.Item_t ty_ rest_ v_value rest as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let? ctxt :=
      Error_monad.record_trace_eval
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          let t_value := dep_serialize_ty_for_error v_value in
          Build_extensible "Non_dupable_type"
            (Alpha_context.Script.location *
              Micheline.canonical Alpha_context.Script.prim)
            (loc_value, t_value))
        (dep_check_dupable_ty ctxt loc_value v_value) in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := With_family.Item_t v_value stack_value;
      dep_descr.instr :=
      {| dep_cinstr.apply _ k_value :=
         With_family.IDup loc_value k_value;
     |};
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DUP (cons n_value [])
      v_annot, stack_ty =>
    let? '_ := Script_ir_annot.check_var_annot loc_value v_annot in
    let fix make_proof_argument {t_bef}
      (n_value : int) (stack_ty : With_family.stack_ty t_bef)
      : M? dep_dup_n_proof_argument t_bef :=
      match n_value, stack_ty with
      | 1, With_family.Item_t hd_ty _ =>
          return? (Dep_dup_n_proof_argument With_family.Dup_n_zero hd_ty)
      | n_value, With_family.Item_t el_a tl_ty =>
          let? '(Dep_dup_n_proof_argument dup_n_witness b_ty) :=
          make_proof_argument (n_value - 1) tl_ty in
          return?
            (Dep_dup_n_proof_argument
               (With_family.Dup_n_succ dup_n_witness) b_ty)
      | _, _ =>
          let whole_stack := dep_serialize_stack_for_error ctxt stack_ty in
          Error_monad.error_value
            (Build_extensible "Bad_stack"
               (Alpha_context.Script.location *
                  Alpha_context.Script.prim * int *
                  Script_tc_errors.unparsed_stack_ty)
               (loc_value, Michelson_v1_primitives.I_DUP, 1, whole_stack))
      end in
    let? n_value := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    let? '_ :=
      Error_monad.error_unless (n_value >i 0)
        (Build_extensible "Dup_n_bad_argument" Alpha_context.Script.location
          loc_value) in
    let? 'Dep_dup_n_proof_argument witness after_ty :=
      Error_monad.record_trace
        (Build_extensible "Dup_n_bad_stack" Alpha_context.Script.location
          loc_value) (make_proof_argument n_value stack_ty) in
    let? ctxt :=
      Error_monad.record_trace_eval
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          let t_value := dep_serialize_ty_for_error after_ty in
          Build_extensible "Non_dupable_type"
            (Alpha_context.Script.location *
              Micheline.canonical Alpha_context.Script.prim)
            (loc_value, t_value))
        (dep_check_dupable_ty ctxt loc_value after_ty) in
    let dupn :=
    {| dep_cinstr.apply _ k_value :=
          With_family.IDup_n loc_value n_value witness k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := With_family.Item_t after_ty stack_ty;
      dep_descr.instr := dupn;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DIG (cons n_value [])
      result_annot, stack_value =>
      let fix make_proof_argument {t} (n_value : int)
            (stk : With_family.stack_ty t)
      : M? (dep_dig_proof_argument t) :=
      match (n_value =? 0), stk with
      | true, With_family.Item_t v_value rest =>
        return? (Dep_dig_proof_argument With_family.KRest v_value rest)
      | false, With_family.Item_t v_value rest =>
        let? 'Dep_dig_proof_argument n' x_value aft' :=
          make_proof_argument (n_value - 1) rest in
        return?
          (Dep_dig_proof_argument (With_family.KPrefix loc_value v_value n')
             x_value (With_family.Item_t v_value aft'))
      | _, _ =>
        let whole_stack := dep_serialize_stack_for_error ctxt stack_value in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc_value, Michelson_v1_primitives.I_DIG, 3, whole_stack))
      end in
    let? n_value := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value result_annot in
    let? 'Dep_dig_proof_argument n' x_value aft :=
      make_proof_argument n_value stack_value in
    let dig := {| dep_cinstr.apply _ k_value :=
                   With_family.IDig loc_value n_value n' k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := With_family.Item_t x_value aft;
      dep_descr.instr := dig; |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DIG
      (([] | cons _ (cons _ _)) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, Michelson_v1_primitives.I_DIG, 1, (List.length l_value)))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DUG (cons n_value [])
      result_annot, (With_family.Item_t x_value whole_stack) as before =>
    let? whole_n := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument whole_n) in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value result_annot in
    match dep_make_dug_proof_argument loc_value whole_n x_value whole_stack with
    | None =>
      let whole_stack := dep_serialize_stack_for_error ctxt whole_stack in
      Error_monad.fail
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty)
          (loc_value, Michelson_v1_primitives.I_DUG, whole_n, whole_stack))
    | Some (Dep_dug_proof_argument n' aft) =>
      let dug :=  {| dep_cinstr.apply _ k_value :=
                  With_family.IDug loc_value whole_n n' k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := before;
        dep_descr.aft := aft;
        dep_descr.instr := dug; |}, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DUG (cons _ [])
      result_annot, stack_value =>
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value result_annot in
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty)
        (loc_value, Michelson_v1_primitives.I_DUG, 1, stack_value))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DUG
      (([] | cons _ (cons _ _)) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, Michelson_v1_primitives.I_DUG, 1, (List.length l_value)))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SWAP [] annot,
    @With_family.Item_t
      t_v_value (t_w_value :: t_rest) v_value w_rest as before =>
      match
        before
        return
          With_family.ty t_v_value ->
          With_family.stack_ty (t_w_value :: t_rest) ->
          M? (dep_judgement (t_v_value :: t_w_value :: t_rest) *
                Alpha_context.context)
      with
      | _ =>
          fun v_value '(With_family.Item_t w_value rest) =>
            let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
            let swap :=
              {| dep_cinstr.apply _ k_value :=
                  With_family.ISwap loc_value k_value |} in
            let stack_ty :=
              With_family.Item_t w_value (With_family.Item_t v_value rest) in
            return? (Dep_typed {|
              dep_descr.loc := loc_value;
              dep_descr.bef :=
                With_family.Item_t v_value (With_family.Item_t w_value rest);
              dep_descr.aft := stack_ty;
              dep_descr.instr := swap;
            |}, ctxt)
      end v_value w_rest
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_PUSH
      (cons t_value (cons d_value [])) annot, stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let? '(Dep_ex_ty t_value, ctxt) :=
      dep_parse_packable_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z
        legacy t_value in
    let? '(v_value, ctxt) :=
      match fuel with
      | Datatypes.O =>
          Error_monad.fail
            (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
      | Datatypes.S fuel =>
          dep_parse_data_aux_fuel type_logger_value fuel ctxt legacy false
            t_value d_value
      end
    in
    let const :=
       {| dep_cinstr.apply _ k_value :=
            With_family.IConst loc_value t_value v_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := With_family.Item_t t_value stack_value;
      dep_descr.instr := const;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UNIT [] annot,
      stack_value =>
      let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
      let const : dep_cinstr s (Ty.Unit :: s) :=
       {| dep_cinstr.apply _ k_value :=
          With_family.IConst
            loc_value Script_typed_ir.dep_unit_t tt k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft :=
        With_family.Item_t Script_typed_ir.dep_unit_t stack_value;
      dep_descr.instr := const;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SOME [] annot,
      (With_family.Item_t t_value rest) as bef =>
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let cons_some :=
     {| dep_cinstr.apply _ k_value :=
          With_family.ICons_some loc_value k_value |}
    in
    let? ty := Script_typed_ir.dep_option_t loc_value t_value in
    (return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef;
      dep_descr.aft := With_family.Item_t ty rest;
      dep_descr.instr := cons_some; |}, ctxt))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NONE (cons t_value [])
      annot, stack_value =>
    let? '(Dep_ex_ty t_value, ctxt) :=
      dep_parse_any_ty_aux ctxt  (fuel_to_stack_depth fuel + 1)%Z
        legacy t_value in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let cons_none :=
      {| dep_cinstr.apply _ k_value :=
                  With_family.ICons_none loc_value t_value k_value |}
    in
    let? ty := Script_typed_ir.dep_option_t loc_value t_value in
    let stack_ty := With_family.Item_t ty stack_value in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := stack_ty;
      dep_descr.instr := cons_none;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MAP (cons body []) annot,
    @With_family.Item_t
      (Ty.Option t) _
      (With_family.Option_t t_value _ _ as hd) rest as stack_ty =>
      match
        stack_ty in With_family.stack_ty (Ty.Option t1 :: t_rest)
        return
          With_family.ty (Ty.Option t1) ->
          M? (dep_judgement (Ty.Option t1 :: t_rest) * Alpha_context.context)
      with
      | _ =>
          fun '(With_family.Option_t t_value md cmp) =>
            let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
            let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
            let? '(judgement_value, ctxt) :=
              non_terminal_recursion
                type_logger_value tc_context_value
                ctxt legacy body (With_family.Item_t t_value rest) in
            match judgement_value with
            |
              Dep_typed
                ({|
                    dep_descr.loc := loc_value;
                    dep_descr.aft :=
                      @With_family.Item_t t_rv t_aft_rest rv aft_rest as aft;
                    dep_descr.bef := bef;
                    dep_descr.instr := instr;
                  |}) =>
                let invalid_map_body (function_parameter : unit)
                  : Error_monad._error :=
                  let '_ := function_parameter in
                  let aft := dep_serialize_stack_for_error ctxt aft in
                  Build_extensible "Invalid_map_body"
                    (Alpha_context.Script.location *
                       Script_tc_errors.unparsed_stack_ty)
                    (loc_value, aft) in
                Error_monad.record_trace_eval invalid_map_body
                  (let? '(e, ctxt)
                     := dep_stack_eq loc_value ctxt 1 aft_rest rest in
                   let? opt_ty := Script_typed_ir.dep_option_t loc_value rv in
                   let kibody := dep_descr.Build _ _ loc_value bef aft instr in
                   let final_stack := With_family.Item_t opt_ty rest in
                   let body :=
                     let 'eq_refl := e in
                     kibody.(dep_descr.instr).(dep_cinstr.apply)
                        (With_family.IHalt loc_value) in
                   let instr :=
                     {| dep_cinstr.apply _ k_value :=
                         With_family.IOpt_map loc_value body k_value |} in
                   return?
                     (Dep_typed {|
                        dep_descr.loc := loc_value;
                        dep_descr.bef :=
                          With_family.Item_t
                            (With_family.Option_t t_value md cmp) rest;
                        dep_descr.aft := final_stack;
                        dep_descr.instr := instr; |}, ctxt))
            | Dep_typed {| dep_descr.aft := With_family.Bot_t; |} =>
                let aft :=
                  dep_serialize_stack_for_error ctxt With_family.Bot_t in
                Error_monad.error_value
                  (Build_extensible "Invalid_map_body"
                     (Alpha_context.Script.location *
                        Script_tc_errors.unparsed_stack_ty)
                     (loc_value, aft))
            | Dep_failed _ =>
                Error_monad.error_value
                  (Build_extensible "Invalid_map_block_fail"
                     Alpha_context.Script.location loc_value)
            end
      end hd
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_IF_NONE
      (cons bt (cons bf [])) annot,
    @With_family.Item_t (Ty.Option t_t_value) t_rest hd rest as bef =>
      match
        bef
        return
          With_family.ty (Ty.Option t_t_value) ->
          M? (dep_judgement (Ty.Option t_t_value :: t_rest) *
                Alpha_context.context)
      with
      | _ =>
          fun '(With_family.Option_t t_value meta comp) =>
            let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bt in
            let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bf in
            let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
            let? '(btr, ctxt) :=
              non_terminal_recursion
                type_logger_value tc_context_value ctxt legacy bt rest in
            let stack_ty := With_family.Item_t t_value rest in
            let? '(bfr, ctxt) :=
              non_terminal_recursion
                type_logger_value tc_context_value ctxt legacy bf stack_ty in
            let branch f ibt ibf :=
              let ifnone :=
                {|
                  dep_cinstr.apply _ k_value :=
                    let hloc := dep_kinstr_location k_value in
                    let branch_if_none :=
                      ibt.(dep_descr.instr).(dep_cinstr.apply)
                        (With_family.IHalt hloc) in
                    let branch_if_some :=
                      ibf.(dep_descr.instr).(dep_cinstr.apply)
                        (With_family.IHalt hloc) in
                    With_family.IIf_none
                      loc_value branch_if_none branch_if_some k_value |} in
              {|
                dep_descr.loc := loc_value;
                dep_descr.bef :=
                  (With_family.Item_t
                     (With_family.Option_t t_value meta comp) rest);
                dep_descr.aft := ibt.(dep_descr.aft);
                dep_descr.instr := ifnone |} in
            dep_merge_branches
              ctxt loc_value btr bfr {| dep_branch.branch := branch |}
      end hd
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_PAIR [] annot,
    @With_family.Item_t t_a (t_b :: t_rest) a_value brest =>
      let '(b_value, rest) :=
        let '(With_family.Item_t b_value rest) := brest in
        (b_value, rest) in
      let? '_ := Script_ir_annot.check_constr_annot loc_value annot in
      let? 'Script_typed_ir.Dep_ty_ex_c ty :=
        Script_typed_ir.dep_pair_t loc_value a_value b_value in
      let stack_ty := With_family.Item_t ty rest in
      let cons_pair :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.ICons_pair loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t a_value (With_family.Item_t b_value rest);
        dep_descr.aft := stack_ty;
        dep_descr.instr := cons_pair;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_PAIR (cons n_value [])
      annot, stack_ty =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let fix make_proof_argument {s}
      (n_value : int) (stack_ty : With_family.stack_ty s)
      : M? dep_comb_proof_argument s :=
      match
        n_value, stack_ty in With_family.stack_ty s'
        return M? dep_comb_proof_argument s'
      with
      | 1, With_family.Item_t a tl =>
        return?
          Dep_comb_proof_argument With_family.Comb_one (With_family.Item_t a tl)
      | n_value, @With_family.Item_t t_a t_tl a tl =>
          let? function_parameter := make_proof_argument (n_value - 1) tl in
        match function_parameter with
        | @Dep_comb_proof_argument _ (x::xs) cw1 pa as dcpa =>
            match
              dcpa
              return
                With_family.stack_ty (x :: xs) ->
                With_family.comb_gadt_witness t_tl (x :: xs) ->
                M? dep_comb_proof_argument (t_a :: t_tl)
            with
            | _ =>
                fun '(With_family.Item_t b tl') cw =>
                  let? 'Script_typed_ir.Dep_ty_ex_c pair_t :=
                    Script_typed_ir.dep_pair_t loc_value a b in
                  return?
                    Dep_comb_proof_argument
                      (With_family.Comb_succ cw)
                      (With_family.Item_t pair_t tl')
            end pa cw1
        | _ => Error_monad.fail (Build_extensible "Unreachable" unit tt)
        end
      | _ , _ =>
        let whole_stack := dep_serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc_value, Michelson_v1_primitives.I_PAIR, 1, whole_stack))
      end in
    let? n_value := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    let? '_ :=
      Error_monad.error_unless (n_value >? 1)
        (Build_extensible
           "Pair_bad_argument" Alpha_context.Script.location loc_value) in
    let? 'Dep_comb_proof_argument witness after_ty :=
      make_proof_argument n_value stack_ty in
    let comb :=
      {| dep_cinstr.apply _ k_value :=
           With_family.IComb loc_value n_value witness k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := after_ty;
      dep_descr.instr := comb;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UNPAIR (cons n_value [])
      annot, stack_ty =>
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    let fix make_proof_argument {s}
      (n_value : nat) (stack_ty : With_family.stack_ty s) {struct n_value}
      : M? dep_uncomb_proof_argument s :=
      match n_value, stack_ty with
      | Datatypes.S Datatypes.O, stack_value =>
        return? (Dep_uncomb_proof_argument With_family.Uncomb_one stack_value)
      |
        Datatypes.S n_value,
        @With_family.Item_t (Ty.Pair t_a t_b) t_tl pr tl_ty as st =>
          match
            st
            return
               With_family.ty (Ty.Pair t_a t_b) ->
               M? dep_uncomb_proof_argument (Ty.Pair t_a t_b :: t_tl)
          with
          | _ =>
              fun '(With_family.Pair_t a_ty b_ty _ _) =>
                let? 'Dep_uncomb_proof_argument uncomb_witness after_ty :=
                  make_proof_argument n_value (With_family.Item_t b_ty tl_ty) in
                return?
                  Dep_uncomb_proof_argument
                  (With_family.Uncomb_succ uncomb_witness)
                  (With_family.Item_t a_ty after_ty)
          end pr
      | _, _ =>
        let whole_stack := dep_serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc_value, Michelson_v1_primitives.I_UNPAIR, 1, whole_stack))
      end in
    let? n_value := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    let? '_ :=
      Error_monad.error_unless (n_value >? 1)
        (Build_extensible
           "Unpair_bad_argument" Alpha_context.Script.location loc_value) in
    let? 'Dep_uncomb_proof_argument witness after_ty :=
      make_proof_argument (Z.to_nat n_value) stack_ty in
    let uncomb :=
      {|
        dep_cinstr.apply _ k_value :=
          With_family.IUncomb loc_value n_value witness k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := after_ty;
      dep_descr.instr := uncomb;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GET (cons n_value [])
      annot, (With_family.Item_t comb_ty rest_ty) as bef =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let? n_value := parse_uint11 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    match dep_make_comb_get_proof_argument n_value comb_ty with
    | None =>
      let whole_stack := dep_serialize_stack_for_error ctxt stack_ty in
      Error_monad.fail
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty)
          (loc_value, Michelson_v1_primitives.I_GET, 1, whole_stack))
    | Some (Dep_comb_get_proof_argument witness ty') =>
      let after_stack_ty := With_family.Item_t ty' rest_ty in
      let comb_get :=
         {| dep_cinstr.apply _ k_value :=
              With_family.IComb_get loc_value n_value witness k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := after_stack_ty;
        dep_descr.instr := comb_get;
      |}, ctxt)
    end
 |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UPDATE (cons n_value [])
      annot, @With_family.Item_t t_value_ty (t_comb :: t_rest) value_ty rst =>
      let '(With_family.Item_t comb_ty rest_ty) := rst in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? n_value := parse_uint11 n_value in
      let? ctxt := Alpha_context.Gas.consume ctxt
                     (Typecheck_costs.proof_argument n_value) in
      let? 'Dep_comb_set_proof_argument witness after_ty :=
              dep_make_comb_set_proof_argument ctxt stack_ty
                loc_value n_value value_ty comb_ty in
      let after_stack_ty := With_family.Item_t after_ty rest_ty in
      let comb_set :=
            {| dep_cinstr.apply _ k_value :=
                With_family.IComb_set loc_value n_value witness k_value
            |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t value_ty (With_family.Item_t comb_ty rest_ty);
        dep_descr.aft := after_stack_ty;
        dep_descr.instr := comb_set;
      |}, ctxt)
 |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UNPAIR [] annot,
    @With_family.Item_t (Ty.Pair ty_a1 ty_b1) ty_rest pr rest =>
      match tt return
        With_family.ty (Ty.Pair ty_a1 ty_b1) ->
        M? (dep_judgement (Ty.Pair ty_a1 ty_b1 :: ty_rest) *
              Alpha_context.context)
      with
      | _ =>
          fun '(@With_family.Pair_t ty_a ty_b a_value b_value meta cmp) =>
            let? '_ := Script_ir_annot.check_unpair_annot loc_value annot in
            let unpair : dep_cinstr (Ty.Pair ty_a ty_b :: ty_rest)
                           (ty_a :: ty_b :: ty_rest) :=
         {| dep_cinstr.apply _ k_value :=
           With_family.IUnpair loc_value k_value |} in
            return? (Dep_typed {|
              dep_descr.loc := loc_value;
              dep_descr.bef :=
                With_family.Item_t
                  (With_family.Pair_t a_value b_value meta cmp) rest;
              dep_descr.aft :=
                With_family.Item_t a_value (With_family.Item_t b_value rest);
              dep_descr.instr := unpair;
            |}, ctxt)
       end pr
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CAR [] annot,
    @With_family.Item_t (Ty.Pair ty_a1 ty_b1) ty_rest pr rest =>
      match tt return
        With_family.ty (Ty.Pair ty_a1 ty_b1) ->
        M? (dep_judgement (Ty.Pair ty_a1 ty_b1 :: ty_rest) *
              Alpha_context.context)
      with
      | _ =>
         fun '(@With_family.Pair_t ty_a ty_b a_value b_value meta cmp) =>
           let? '_ := Script_ir_annot.check_destr_annot loc_value annot in
           let car :=
             {| dep_cinstr.apply _ k_value :=
                 With_family.ICar loc_value k_value |} in
           return? (Dep_typed {|
             dep_descr.loc := loc_value;
             dep_descr.bef :=
               With_family.Item_t
                 (With_family.Pair_t a_value b_value meta cmp) rest;
             dep_descr.aft := With_family.Item_t a_value rest;
             dep_descr.instr := car;
           |}, ctxt)
      end pr
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CDR [] annot,
    @With_family.Item_t (Ty.Pair ty_a1 ty_b1) ty_rest pr rest =>
      match tt return
       With_family.ty (Ty.Pair ty_a1 ty_b1) ->
       M? (dep_judgement (Ty.Pair ty_a1 ty_b1 :: ty_rest) *
             Alpha_context.context)
      with
      | _ =>
         fun '(@With_family.Pair_t ty_a ty_b a_value b_value meta cmp) =>
           let? '_ := Script_ir_annot.check_destr_annot loc_value annot in
           let cdr :=
             {| dep_cinstr.apply _ k_value :=
                 With_family.ICdr loc_value k_value |} in
           return? (Dep_typed {|
             dep_descr.loc := loc_value;
             dep_descr.bef := With_family.Item_t
                                (With_family.Pair_t a_value b_value meta cmp) rest;
             dep_descr.aft := With_family.Item_t b_value rest;
             dep_descr.instr := cdr;
           |}, ctxt)
      end pr
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LEFT (cons tr []) annot,
      (With_family.Item_t tl rest) as bef =>
      let? '(Dep_ex_ty tr, ctxt) :=
        dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z legacy tr in
    let? '_ := Script_ir_annot.check_constr_annot loc_value annot in
    let cons_left :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ICons_left loc_value tr k_value |}
    in
    let? 'Script_typed_ir.Dep_ty_ex_c ty :=
      Script_typed_ir.dep_union_t loc_value tl tr in
    let stack_ty := With_family.Item_t ty rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef;
      dep_descr.aft := stack_ty;
      dep_descr.instr := cons_left;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_RIGHT (cons tl []) annot,
      (With_family.Item_t tr rest) as bef =>
      let? '(Dep_ex_ty tl, ctxt) :=
        dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z legacy tl in
    let? '_ := Script_ir_annot.check_constr_annot loc_value annot in
    let cons_right :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ICons_right loc_value tl k_value |}
    in
    let? 'Script_typed_ir.Dep_ty_ex_c ty :=
      Script_typed_ir.dep_union_t loc_value tl tr in
    let stack_ty := With_family.Item_t ty rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef;
      dep_descr.aft := stack_ty;
      dep_descr.instr := cons_right;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_IF_LEFT
      (cons bt (cons bf [])) annot,
    (@With_family.Item_t (Ty.Union t_tl t_tr) t_rest un rest) =>
      let '(With_family.Union_t tl tr meta cmp) := un in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bt in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bf in
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      let? '(btr, ctxt) :=
        non_terminal_recursion
          type_logger_value tc_context_value ctxt
          legacy bt (With_family.Item_t tl rest) in
      let? '(bfr, ctxt) :=
        non_terminal_recursion
          type_logger_value tc_context_value ctxt
          legacy bf (With_family.Item_t tr rest) in
      let branch _ ibt ibf :=
        let instr :=
          {|
            dep_cinstr.apply _ k_value :=
              let hloc := Script_typed_ir.dep_kinstr_location k_value in
              let branch_if_left :=
                ibt.(dep_descr.instr).(dep_cinstr.apply)
                  (With_family.IHalt hloc) in
              let branch_if_right :=
                ibf.(dep_descr.instr).(dep_cinstr.apply)
                  (With_family.IHalt hloc) in
              With_family.IIf_left
                loc_value branch_if_left branch_if_right k_value
              |} in
        {| dep_descr.loc := loc_value;
           dep_descr.bef :=
             With_family.Item_t (With_family.Union_t tl tr meta cmp) rest;
           dep_descr.aft := ibt.(dep_descr.aft);
           dep_descr.instr := instr |} in
      dep_merge_branches
        ctxt loc_value btr bfr {| dep_branch.branch := branch |}
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NIL (cons t_value [])
      annot, stack_value =>
    let? '(Dep_ex_ty t_value, ctxt) :=
      dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z
        legacy t_value in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let nil := {| dep_cinstr.apply _ k_value :=
                  With_family.INil loc_value t_value k_value |} in
    let? ty := Script_typed_ir.dep_list_t loc_value t_value in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := (With_family.Item_t ty stack_value);
      dep_descr.instr := nil;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONS [] annot,
    @With_family.Item_t t_tv (Ty.List t_t_value :: t_rest) tv lrest =>
      let '@With_family.Item_t (Ty.List _) t_rest lv rest := lrest in
      let '@With_family.List_t t_t_value t_value meta := lv in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt tv t_value loc_value Michelson_v1_primitives.I_CONS 1 2 in
      let e : t_t_value = t_tv := let 'eq_refl := e in eq_refl in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let cons_list :=
        {| dep_cinstr.apply _ k_value :=
             With_family.ICons_list loc_value k_value |} in
      let aft := With_family.Item_t (With_family.List_t t_value meta) rest in
      let bef := With_family.Item_t tv aft in
      let cons_list := let 'eq_refl := e in cons_list in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := cons_list;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_IF_CONS
      (cons bt (cons bf [])) annot,
    @With_family.Item_t (Ty.List t_v) t_rest ls rest =>
      let t_value := let '(With_family.List_t t_value meta) := ls in t_value in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bt in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bf in
      let bef := (With_family.Item_t ls rest) in
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      let? '(btr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
        (With_family.Item_t t_value bef) in
    let? '(bfr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
        rest in
    let branch _ ibt ibf :=
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            let hloc := Script_typed_ir.dep_kinstr_location k_value in
            let branch_if_cons : With_family.kinstr _ _ :=
              ibt.(dep_descr.instr).(dep_cinstr.apply)
                (With_family.IHalt hloc)
            in let branch_if_nil : With_family.kinstr _ _ :=
              ibf.(dep_descr.instr).(dep_cinstr.apply)
                (With_family.IHalt hloc) in
               With_family.IIf_cons
                 hloc branch_if_cons branch_if_nil k_value
        |} in
      {| dep_descr.loc := loc_value;
         dep_descr.bef := bef;
         dep_descr.aft := ibt.(dep_descr.aft);
         dep_descr.instr := instr |} in
    dep_merge_branches ctxt loc_value btr bfr {| dep_branch.branch := branch |}
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SIZE [] annot,
    @With_family.Item_t (Ty.List t_v) ty_rest ls rest =>
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let list_size :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IList_size loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t ls rest;
      dep_descr.aft := With_family.Item_t Script_typed_ir.dep_nat_t rest;
      dep_descr.instr := list_size;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MAP (cons body []) annot,
    @With_family.Item_t
      (Ty.List t_v) ty_rest ls starting_rest =>
      let elt_value :=
        let '(With_family.List_t elt_value meta) := ls in
        elt_value in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (With_family.Item_t elt_value starting_rest) in
    match judgement_value with
    |
      Dep_typed
        ({|
            dep_descr.loc := loc_value;
            dep_descr.aft :=
            @With_family.Item_t t_rv t_aft_rest ret_value aft_rest as aft;
            dep_descr.bef := bef;
            dep_descr.instr := instr;
        |} as kibody) =>
        let invalid_map_body (function_parameter : unit) : Error_monad._error :=
          let '_ := function_parameter in
          let aft := dep_serialize_stack_for_error ctxt aft in
          Build_extensible "Invalid_map_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
            (loc_value, aft) in
        Error_monad.record_trace_eval
          invalid_map_body
          (let? '(e, ctxt) :=
             dep_stack_eq loc_value ctxt 1 aft_rest starting_rest in
           let aft_rest := let 'eq_refl := e in aft_rest in
           let instr := let 'eq_refl := e in instr in
           let kibody := {|
             dep_descr.loc := loc_value;
             dep_descr.aft := With_family.Item_t ret_value aft_rest;
             dep_descr.bef := bef;
             dep_descr.instr := instr; |} in
           let hloc := loc_value in
           let ibody :=
             kibody.(dep_descr.instr).(dep_cinstr.apply)
               (With_family.IHalt hloc) in
           let? ty_value := Script_typed_ir.dep_list_t loc_value ret_value in
           let list_map :=
             {| dep_cinstr.apply _ k_value :=
                 With_family.IList_map loc_value ibody ty_value k_value |} in
           let? ty := Script_typed_ir.dep_list_t loc_value ret_value in
           let stack_value := With_family.Item_t ty aft_rest in
           return? (Dep_typed {|
             dep_descr.loc := loc_value;
             dep_descr.bef := With_family.Item_t ls starting_rest;
             dep_descr.aft := stack_value;
             dep_descr.instr := list_map; |}, ctxt))
    |
      Dep_typed {| dep_descr.aft := aft |} =>
        let aft := dep_serialize_stack_for_error ctxt aft in
        Error_monad.error_value
          (Build_extensible "Invalid_map_body"
             (Alpha_context.Script.location *
                Script_tc_errors.unparsed_stack_ty) (loc_value, aft))
    |
      Dep_failed _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_map_block_fail" Alpha_context.Script.location
           loc_value)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ITER (cons body [])
      annot, @With_family.Item_t (Ty.List t_v) ty_rest ls rest =>
    let '(With_family.List_t elt_value md) := ls in
    let elt_value :=
      let '(With_family.List_t elt_value md) := ls in elt_value in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (With_family.Item_t elt_value rest) in
    let mk_list_iter (ibody : dep_descr _ _) : dep_cinstr _ _ :=
      {|
        dep_cinstr.apply _ k_value :=
          let hinfo := loc_value in
          let ibody :=
            ibody.(dep_descr.instr).(dep_cinstr.apply)
              (With_family.IHalt hinfo) in
          With_family.IList_iter loc_value elt_value ibody k_value |} in
    match judgement_value with
    | Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |} =>
      let invalid_iter_body (function_parameter : unit) : Error_monad._error :=
        let '_ := function_parameter in
        let aft := dep_serialize_stack_for_error ctxt aft in
        let rest := dep_serialize_stack_for_error ctxt rest in
        Build_extensible "Invalid_iter_body"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc_value, rest, aft) in
      Error_monad.record_trace_eval invalid_iter_body
        (let? '(e, ctxt) := dep_stack_eq loc_value ctxt 1 aft rest in
         let aft := let 'eq_refl := e in aft in
         let instr := let 'eq_refl := e in instr in
         let ibody : dep_descr _ _ := {|
           dep_descr.loc := loc_value;
           dep_descr.bef := bef;
           dep_descr.aft := aft;
           dep_descr.instr := instr;
         |} in
         let instr := mk_list_iter ibody in
         return? (Dep_typed {|
           dep_descr.loc := loc_value;
           dep_descr.bef := With_family.Item_t
                              (With_family.List_t elt_value md) rest;
           dep_descr.aft := rest;
           dep_descr.instr := instr;
         |}, ctxt))
    | Dep_failed descr_value =>
        return? (Dep_typed {|
          dep_descr.loc := loc_value;
          dep_descr.bef := With_family.Item_t
                             (With_family.List_t elt_value md) rest;
          dep_descr.aft := rest;
          dep_descr.instr := (mk_list_iter (descr_value _ rest));
        |}, ctxt)
    end
 |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EMPTY_SET
      (cons t_value []) annot, rest =>
    let? '(Dep_ex_comparable_ty t_value, ctxt) :=
      dep_parse_comparable_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z
        t_value in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
           With_family.IEmpty_set loc_value t_value k_value |} in
    let? ty := Script_typed_ir.dep_set_t loc_value t_value in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := rest;
      dep_descr.aft := (With_family.Item_t ty rest);
      dep_descr.instr := instr;
    |}, ctxt)
 |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ITER (cons body [])
      annot, @With_family.Item_t (Ty.Set_ t_v) ty_rest ls rest =>
      let '(@With_family.Set_t t_comp comp_elt md) := ls in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    let elt_value := dep_ty_of_comparable_ty comp_elt in
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (@With_family.Item_t t_comp (* t_elt_value *) ty_rest elt_value rest) in
     let mk_iset_iter (ibody : dep_descr _ _) : dep_cinstr _ _ :=
      {|
        dep_cinstr.apply _ k_value :=
          let hinfo := loc_value in
          let ibody :=
            ibody.(dep_descr.instr).(dep_cinstr.apply)
              (With_family.IHalt hinfo) in
          With_family.ISet_iter loc_value elt_value ibody k_value |} in
    match judgement_value with
    | @Dep_typed _ t_aft
        ({|
            dep_descr.loc := loc_value;
            dep_descr.bef := bef;
            dep_descr.aft := aft;
            dep_descr.instr := instr;
          |}) =>
      let invalid_iter_body (function_parameter : unit) : Error_monad._error :=
        let '_ := function_parameter in
        let aft := dep_serialize_stack_for_error ctxt aft in
        let rest := dep_serialize_stack_for_error ctxt rest in
        Build_extensible "Invalid_iter_body"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc_value, rest, aft) in
      Error_monad.record_trace_eval invalid_iter_body
        (let? '(e, ctxt) := dep_stack_eq loc_value ctxt 1 aft rest in
         let e : t_aft = ty_rest := e in
         let aft : With_family.stack_ty ty_rest := let 'eq_refl := e in aft in
         let instr : dep_cinstr (t_comp :: ty_rest) ty_rest :=
           let 'eq_refl := e in instr in
         let ibody : dep_descr _ _ :=
            ({|
             dep_descr.loc := loc_value;
             dep_descr.bef := bef;
             dep_descr.aft := aft;
             dep_descr.instr := instr;
              |}) in
           let instr := mk_iset_iter ibody in
         return? (Dep_typed {|
             dep_descr.loc := loc_value;
             dep_descr.bef := With_family.Item_t
                                (With_family.Set_t comp_elt md) rest;
             dep_descr.aft := rest;
             dep_descr.instr := instr; |}, ctxt))
    | Dep_failed descr_value =>
        return? (Dep_typed {|
             dep_descr.loc := loc_value;
             dep_descr.bef := With_family.Item_t
                                (With_family.Set_t comp_elt md) rest;
             dep_descr.aft := rest;
             dep_descr.instr := (mk_iset_iter (descr_value _ rest)); |}, ctxt)
    end
 |
   Micheline.Prim loc_value Michelson_v1_primitives.I_MEM [] annot,
   @With_family.Item_t t_v (Ty.Set_ t_elt_value :: t_rest) v_value ls =>
     let '(@With_family.Item_t (Ty.Set_ t_elt_value) t_rest ls2 rest) := ls in
     let '(@With_family.Set_t t_elt_value elt_value md) := ls2 in
     let elt_value := dep_ty_of_comparable_ty elt_value in
     let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
     let? '(e, ctxt) :=
       check_item_ty
         ctxt elt_value v_value loc_value Michelson_v1_primitives.I_MEM 1 2 in
     let instr :=
       {| dep_cinstr.apply _ k_value :=
           With_family.ISet_mem loc_value k_value |} in
     let instr := let 'eq_refl := e in instr in
     return? (Dep_typed {|
       dep_descr.loc := loc_value;
       dep_descr.bef :=
         With_family.Item_t
          v_value
          (With_family.Item_t (With_family.Set_t elt_value md) rest);
       dep_descr.aft := With_family.Item_t Script_typed_ir.dep_bool_t rest;
       dep_descr.instr := instr;
     |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UPDATE [] annot,
    @With_family.Item_t
      t_v_value (Ty.Bool :: Ty.Set_ t_elt_value :: t_rest) v_value bsrest =>
      let '@With_family.Item_t
            Ty.Bool (Ty.Set_ t_elt_value :: t_rest) _ srest := bsrest in
      let '@With_family.Item_t (Ty.Set_ t_elt_value) t_rest s rest := srest in
      let '@With_family.Set_t t_elt_value elt_value md := s in
      let? '(e, ctxt) :=
        check_item_ty ctxt (dep_ty_of_comparable_ty elt_value) v_value loc_value
          Michelson_v1_primitives.I_UPDATE 1 3 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let aft := With_family.Item_t (With_family.Set_t elt_value md) rest in
      let bef := let 'eq_refl := e in
        With_family.Item_t
          elt_value (With_family.Item_t With_family.Bool_t aft) in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISet_update loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SIZE [] annot,
      @With_family.Item_t (Ty.Set_ _) ty_rest set_t rest =>
    let '(With_family.Set_t a b) := set_t in 
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
          {| dep_cinstr.apply _ k_value :=
              With_family.ISet_size loc_value k_value |} in
          return? (Dep_typed {|
            dep_descr.loc := loc_value;
            dep_descr.bef := With_family.Item_t (With_family.Set_t a b) rest;
            dep_descr.aft := (With_family.Item_t With_family.Nat_t rest);
            dep_descr.instr := instr;
          |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EMPTY_MAP
      (cons tk (cons tv [])) annot, stack_value =>
    let? '(Dep_ex_comparable_ty tk, ctxt) :=
      dep_parse_comparable_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z tk in
    let? '(Dep_ex_ty tv, ctxt) :=
      dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z legacy tv in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
          With_family.IEmpty_map loc_value tk tv k_value |} in
    let? ty := Script_typed_ir.dep_map_t loc_value tk tv in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := (With_family.Item_t ty stack_value);
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MAP (cons body []) annot,
    @With_family.Item_t (Ty.Map key value) ty_rest ls starting_rest =>
      let '(@With_family.Map_t t_ck t_elt_value kt elt_value md) := ls in
      let k_value := dep_ty_of_comparable_ty kt in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
      let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
      let? 'Script_typed_ir.Dep_ty_ex_c ty :=
        Script_typed_ir.dep_pair_t loc_value k_value elt_value in
      let? '(judgement_value, ctxt) :=
        non_terminal_recursion type_logger_value tc_context_value
          ctxt legacy body (With_family.Item_t ty starting_rest) in
      match judgement_value with
      |
        Dep_typed
          {|
            dep_descr.loc := loc_value;
            dep_descr.bef := bef;
            dep_descr.aft := @With_family.Item_t t_ret_value
                               t_rest ret_value rest;
            dep_descr.instr := instr;
          |}  =>
          let aft : With_family.stack_ty (t_ret_value :: t_rest) :=
            With_family.Item_t ret_value rest in
          let invalid_map_body (function_parameter : unit) :
            Error_monad._error :=
            let '_ := function_parameter in
            let aft := dep_serialize_stack_for_error ctxt aft in
            Build_extensible "Invalid_map_body"
              (Alpha_context.Script.location *
                 Script_tc_errors.unparsed_stack_ty)
              (loc_value, aft) in
          Error_monad.record_trace_eval invalid_map_body
            (let? '(e, ctxt) := dep_stack_eq loc_value ctxt 1
                                  rest starting_rest in
             let e : t_rest = ty_rest := e in
             let rest := let 'eq_refl := e in rest in
             let aft := let 'eq_refl := e in aft in
             let instr := let 'eq_refl := e in instr in
             let ibody : dep_descr _ _ :=
               {|
                 dep_descr.loc := loc_value;
                 dep_descr.bef := bef;
                 dep_descr.aft := With_family.Item_t ret_value rest;
                 dep_descr.instr := instr;
               |} in
             let? ty_value :=
               Script_typed_ir.dep_map_t loc_value kt ret_value in
             let instr :=
               {|
                 dep_cinstr.apply _ k_value :=
                   let hinfo := loc_value in
                   let ibody :=
                      ibody.(dep_descr.instr).(dep_cinstr.apply)
                        (With_family.IHalt hinfo) in
                   With_family.IMap_map loc_value ty_value ibody k_value |} in
             let? ty := Script_typed_ir.dep_map_t loc_value kt ret_value in
             let stack_value := With_family.Item_t ty rest in
             return? (Dep_typed
               {|
                 dep_descr.loc := loc_value;
                 dep_descr.bef := With_family.Item_t
                                    (With_family.Map_t kt elt_value md)
                   starting_rest;
                 dep_descr.aft := stack_value;
                 dep_descr.instr := instr; |}, ctxt))
      | Dep_typed
          {|
            dep_descr.loc := loc_value;
            dep_descr.bef := bef;
            dep_descr.aft := aft;
            dep_descr.instr := instr;
          |}
        =>
          let aft := dep_serialize_stack_for_error ctxt aft in
          Error_monad.error_value
            (Build_extensible "Invalid_map_body"
               (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty)
               (loc_value, aft))
      | Dep_failed _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_map_block_fail"
               Alpha_context.Script.location
               loc_value)
      end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ITER (cons body [])
      annot,
    @With_family.Item_t (Ty.Map t_comp_elt t_element_ty) t_rest map rest =>
      let '@With_family.Map_t
            t_comp_elt t_element_ty comp_elt element_ty meta := map in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    let gbef :=
      With_family.Item_t (With_family.Map_t comp_elt element_ty meta) rest in
    let key_value := dep_ty_of_comparable_ty comp_elt in
    let? 'Dep_ty_ex_c ty_value :=
      dep_pair_t loc_value key_value element_ty in
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (With_family.Item_t ty_value rest) in
    let make_instr ibody :=
      {|
        dep_cinstr.apply _ k_value :=
          let hinfo := loc_value in
          let ibody :=
            ibody.(dep_descr.instr).(dep_cinstr.apply)
              (With_family.IHalt hinfo) in
          With_family.IMap_iter loc_value ty_value ibody k_value |} in
    match judgement_value with
    | @Dep_typed _ t_aft {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |} =>
      let invalid_iter_body (function_parameter : unit) : Error_monad._error :=
        let '_ := function_parameter in
        let aft := dep_serialize_stack_for_error ctxt aft in
        let rest := dep_serialize_stack_for_error ctxt rest in
        Build_extensible "Invalid_iter_body"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc_value, rest, aft) in
      Error_monad.record_trace_eval invalid_iter_body
        (let? '(e, ctxt) := dep_stack_eq loc_value ctxt 1 aft rest in
         let aft := let 'eq_refl := e in aft in
         let instr := let 'eq_refl := e in instr in
         let instr :=
           make_instr (dep_descr.Build _ _ loc_value bef aft instr) in
         return? (Dep_typed {|
           dep_descr.loc := loc_value;
           dep_descr.bef := gbef;
           dep_descr.aft := aft;
           dep_descr.instr := instr;
         |}, ctxt))
    | Dep_failed descr_value =>
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := gbef;
        dep_descr.aft := rest;
        dep_descr.instr := (make_instr (descr_value _ rest));
      |}, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MEM [] annot,
    @With_family.Item_t t_vk (Ty.Map t_ck t_elt :: t_rest) vk mrest =>
      let '@With_family.Item_t (Ty.Map t_ck t_elt) t_rest m rest := mrest in
      let 'With_family.Map_t ck elt meta := m in
      let k_value := dep_ty_of_comparable_ty ck in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt vk k_value loc_value Michelson_v1_primitives.I_MEM 1 2 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IMap_mem loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            vk (With_family.Item_t (With_family.Map_t ck elt meta) rest);
        dep_descr.aft := With_family.Item_t dep_bool_t rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GET [] annot,
    @With_family.Item_t t_vk (Ty.Map _ _ :: _) vk mrest =>
      let '@With_family.Item_t (Ty.Map _ _) _ m rest := mrest in
      let 'With_family.Map_t ck elt_value meta := m in
      let k_value := dep_ty_of_comparable_ty ck in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt vk k_value loc_value Michelson_v1_primitives.I_GET 1 2 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IMap_get loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      let? ty := dep_option_t loc_value elt_value in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            vk (With_family.Item_t (With_family.Map_t ck elt_value meta) rest);
        dep_descr.aft := With_family.Item_t ty rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UPDATE [] annot,
    @With_family.Item_t _ (Ty.Option _ :: Ty.Map _ _ :: t_rest) vk omrest =>
      let '@With_family.Item_t
            (Ty.Option _) (Ty.Map _ _ :: _) o mrest := omrest in
      let 'With_family.Option_t vv ometa ocmp := o in
      let '@With_family.Item_t (Ty.Map _ _) _ m rest := mrest in
      let 'With_family.Map_t ck v_value mmeta := m in
      let aft := With_family.Item_t (With_family.Map_t ck v_value mmeta) rest in
      let bef :=
        With_family.Item_t
          vk (With_family.Item_t (With_family.Option_t vv ometa ocmp) aft) in
      let k_value := dep_ty_of_comparable_ty ck in
      let? '(e1, ctxt) :=
        check_item_ty
          ctxt vk k_value loc_value Michelson_v1_primitives.I_UPDATE 1 3 in
      let? '(e2, ctxt) :=
        check_item_ty
          ctxt vv v_value loc_value Michelson_v1_primitives.I_UPDATE 2 3 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IMap_update loc_value k_value |} in
      let instr := let 'eq_refl := e1 in let 'eq_refl := e2 in instr in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GET_AND_UPDATE [] annot,
    @With_family.Item_t _ (Ty.Option _ :: Ty.Map _ _ :: _) vk omrest =>
      let '@With_family.Item_t
            (Ty.Option _) (Ty.Map _ _ :: _) o mrest := omrest in
      let '@With_family.Item_t (Ty.Map _ _) _ m rest := mrest in
      let 'With_family.Option_t vv ometa ocmp := o in
      let 'With_family.Map_t ck v_value mmeta := m in
      let aft :=
        With_family.Item_t
          (With_family.Option_t vv ometa ocmp)
          (With_family.Item_t (With_family.Map_t ck v_value mmeta) rest) in
      let bef := With_family.Item_t vk aft in
      let k_value := dep_ty_of_comparable_ty ck in
      let? '(e1, ctxt) :=
        check_item_ty ctxt vk k_value loc_value
          Michelson_v1_primitives.I_GET_AND_UPDATE 1 3 in
      let? '(e2, ctxt) :=
        check_item_ty ctxt vv v_value loc_value
          Michelson_v1_primitives.I_GET_AND_UPDATE 2 3 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IMap_get_and_update loc_value k_value |} in
      let instr := let 'eq_refl := e1 in let 'eq_refl := e2 in instr in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SIZE [] annot,
      @With_family.Item_t (Ty.Map key value) t_rest 
        map rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
            With_family.IMap_size loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t map rest;
        dep_descr.aft := With_family.Item_t With_family.Nat_t rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EMPTY_BIG_MAP
      (cons tk (cons tv [])) annot, stack_value =>
      let? '(Dep_ex_comparable_ty tk, ctxt) :=
        dep_parse_comparable_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z tk in
      let? '(Dep_ex_ty tv, ctxt) :=
        dep_parse_big_map_value_ty_aux
          ctxt (fuel_to_stack_depth fuel + 1)%Z legacy tv in
      let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IEmpty_big_map loc_value tk tv k_value;
      |} in
      let? ty := dep_big_map_t loc_value tk tv in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := stack_value;
        dep_descr.aft := With_family.Item_t ty stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MEM [] annot,
    @With_family.Item_t _ (Ty.Big_map _ _ :: _) set_key brest =>
      let '@With_family.Item_t (Ty.Big_map _ _) _ bm rest := brest in
      let 'With_family.Big_map_t map_key mval meta := bm in
      let k_value := dep_ty_of_comparable_ty map_key in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt set_key k_value loc_value Michelson_v1_primitives.I_MEM 1 2 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IBig_map_mem loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      let aft := With_family.Item_t dep_bool_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t set_key
            (With_family.Item_t (With_family.Big_map_t map_key mval meta) rest);
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GET [] annot,
    @With_family.Item_t _ (Ty.Big_map _ _ :: _) vk brest =>
      let '@With_family.Item_t (Ty.Big_map _ _) _ bm rest := brest in
      let 'With_family.Big_map_t ck elt_value meta := bm in
      let k_value := dep_ty_of_comparable_ty ck in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt vk k_value loc_value Michelson_v1_primitives.I_GET 1 2 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IBig_map_get loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      let? ty := dep_option_t loc_value elt_value in
      let stack_value := With_family.Item_t ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t vk
            (With_family.Item_t (With_family.Big_map_t ck elt_value meta) rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_UPDATE [] annot,
    @With_family.Item_t _ (Ty.Option _ :: Ty.Big_map _ _ :: _) set_key omrest =>
      let '@With_family.Item_t
            (Ty.Option _) (Ty.Big_map _ _ :: _) o mrest := omrest in
      let '@With_family.Item_t (Ty.Big_map _ _) _ m rest := mrest in
      let 'With_family.Option_t set_value ometa ocmp := o in
      let 'With_family.Big_map_t map_key map_value mmeta := m in
      let aft :=
        With_family.Item_t
          (With_family.Big_map_t map_key map_value mmeta) rest in
      let bef :=
        With_family.Item_t
          set_key
          (With_family.Item_t
             (With_family.Option_t set_value ometa ocmp) aft) in
      let k_value := dep_ty_of_comparable_ty map_key in
      let? '(e1, ctxt) :=
        check_item_ty ctxt set_key k_value loc_value
          Michelson_v1_primitives.I_UPDATE 1 3 in
      let? '(e2, ctxt) :=
        check_item_ty ctxt set_value map_value loc_value
          Michelson_v1_primitives.I_UPDATE 2 3 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IBig_map_update loc_value k_value |} in
      let instr := let 'eq_refl := e1 in let 'eq_refl := e2 in instr in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := aft;
        dep_descr.instr := instr;
      |}, ctxt)
 |
   Micheline.Prim loc_value Michelson_v1_primitives.I_GET_AND_UPDATE [] annot,
   @With_family.Item_t _ (Ty.Option _ :: Ty.Big_map _ _ :: _) vk obrest =>
     let '@With_family.Item_t
           (Ty.Option _) (Ty.Big_map _ _ :: _) o brest := obrest in
     let '@With_family.Item_t (Ty.Big_map _ _) _ b rest := brest in
     let 'With_family.Option_t vv ometa ocmp := o in
     let 'With_family.Big_map_t ck v_value mcmp := b in
     let stack_value :=
       With_family.Item_t
         (With_family.Option_t vv ometa ocmp)
         (With_family.Item_t (With_family.Big_map_t ck v_value mcmp) rest) in
     let k_value := dep_ty_of_comparable_ty ck in
     let? '(e1, ctxt) :=
       check_item_ty ctxt vk k_value loc_value
         Michelson_v1_primitives.I_GET_AND_UPDATE 1 3 in
     let? '(e2, ctxt) :=
       check_item_ty ctxt vv v_value loc_value
         Michelson_v1_primitives.I_GET_AND_UPDATE 2 3 in
     let? '_ := Script_ir_annot.check_var_annot loc_value annot in
     let instr := {|
       dep_cinstr.apply _ k_value :=
         With_family.IBig_map_get_and_update loc_value k_value |} in
     let instr := let 'eq_refl := e1 in let 'eq_refl := e2 in instr in
     return? (Dep_typed {|
       dep_descr.loc := loc_value;
       dep_descr.bef := With_family.Item_t vk stack_value;
       dep_descr.aft := stack_value;
       dep_descr.instr := instr;
     |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SAPLING_EMPTY_STATE
      (cons memo_size []) annot, rest =>
    let? memo_size := parse_memo_size memo_size in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr := {| dep_cinstr.apply _ k_value :=
          With_family.ISapling_empty_state loc_value memo_size k_value |} in
    let stack_value :=
      With_family.Item_t (Script_typed_ir.dep_sapling_state_t memo_size) rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  (* @TODO see the issue https://gitlab.com/formal-land/coq-tezos-of-ocaml/-/issues/358 *)
  (* | *)
  (*   Micheline.Prim loc_value Michelson_v1_primitives.I_SAPLING_VERIFY_UPDATE [] *)
  (*     _, *)
  (*   @With_family.Item_t *)
  (*     Ty.Sapling_transaction_deprecated (Ty.Sapling_state :: _) st srest => *)
  (*     let '@With_family.Item_t Ty.Sapling_state t_rest s rest := srest in *)
  (*     let 'With_family.Sapling_transaction_deprecated_t *)
  (*           transaction_memo_size := st in *)
  (*     let 'With_family.Sapling_state_t state_memo_size := s in *)
  (*     let state_ty := With_family.Sapling_state_t state_memo_size in *)
  (*     let bef := *)
  (*       With_family.Item_t *)
  (*         (With_family.Sapling_transaction_deprecated_t transaction_memo_size) *)
  (*         (With_family.Item_t state_ty rest) in *)
  (*     if legacy then *)
  (*       let? '_ := *)
  (*         memo_size_eq Script_tc_errors.Informative state_memo_size *)
  (*           transaction_memo_size in *)
  (*       let instr := {| *)
  (*         dep_cinstr.apply _ kinfo_value k_value := *)
  (*           With_family.ISapling_verify_update_deprecated kinfo_value k_value; *)
  (*       |} in *)
  (*       let? 'Dep_ty_ex_c pair_ty := dep_pair_t loc_value dep_int_t state_ty in *)
  (*       let? ty := dep_option_t loc_value pair_ty in *)
  (*       let stack_value := With_family.Item_t ty rest in *)
  (*       return? (Dep_typed {| *)
  (*         dep_descr.loc := loc_value; *)
  (*         dep_descr.bef := bef; *)
  (*         dep_descr.aft := stack_value; *)
  (*         dep_descr.instr := instr; *)
  (*       |}, ctxt) *)
  (*     else *)
  (*       Error_monad.fail *)
  (*         (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim *)
  (*            Michelson_v1_primitives.T_sapling_transaction_deprecated) *)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SAPLING_VERIFY_UPDATE []
      _,
    @With_family.Item_t
      Ty.Sapling_transaction (Ty.Sapling_state :: _) tr srest =>
      let '@With_family.Item_t Ty.Sapling_state _ s rest := srest in
      let 'With_family.Sapling_transaction_t transaction_memo_size := tr in
      let 'With_family.Sapling_state_t state_memo_size := s in
      let state_ty := With_family.Sapling_state_t state_memo_size in
      let bef :=
        With_family.Item_t
          (With_family.Sapling_transaction_t transaction_memo_size)
          (With_family.Item_t state_ty rest) in
      let? '_ :=
        memo_size_eq (Script_tc_errors.Informative tt) state_memo_size
          transaction_memo_size in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISapling_verify_update loc_value k_value |} in
      let? 'Dep_ty_ex_c pair_ty :=
        dep_pair_t loc_value dep_int_t state_ty in
      let? 'Dep_ty_ex_c pair_ty :=
        dep_pair_t loc_value dep_bytes_t pair_ty in
      let? ty := dep_option_t loc_value pair_ty in
      let stack_value := With_family.Item_t ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  | Micheline.Seq loc_value [], stack_value =>
      let instr := {|
        dep_cinstr.apply _ k_value := k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  | Micheline.Seq _ (cons single []), stack_value =>
    non_terminal_recursion type_logger_value tc_context_value ctxt legacy single
      stack_value

  | Micheline.Seq loc_value (cons hd tl), stack_value =>
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy hd
        stack_value in
    match judgement_value with
    | Dep_failed _ =>
      Error_monad.fail
        (Build_extensible "Fail_not_in_tail_position"
          Alpha_context.Script.location (Micheline.location hd))
    | Dep_typed ({| dep_descr.aft := middle |} as ihd) =>
      let? '(judgement_value, ctxt) :=
        non_terminal_recursion type_logger_value tc_context_value ctxt legacy
          (Micheline.Seq Micheline.dummy_location tl) middle in
      let judgement_value :=
        match judgement_value with
        | Dep_failed descr_value =>
          let descr_value _ := fun ret_value =>
            dep_compose_descr loc_value ihd (descr_value _ ret_value) in
          Dep_failed descr_value
        | Dep_typed itl => Dep_typed (dep_compose_descr loc_value ihd itl)
        end in
      return? (judgement_value, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_IF
      (cons bt (cons bf [])) annot,
    @With_family.Item_t Ty.Bool _ _ rest =>
      let bef := With_family.Item_t With_family.Bool_t rest in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bt in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] bf in
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      let? '(btr, ctxt) :=
        non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
          rest in
      let? '(bfr, ctxt) :=
        non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
          rest in
      let branch _ ibt ibf :=
        let instr := {|
          dep_cinstr.apply _ k_value :=
            let hloc := Script_typed_ir.dep_kinstr_location k_value in
            let branch_if_true :=
              ibt.(dep_descr.instr).(dep_cinstr.apply)
                (With_family.IHalt hloc) in
            let branch_if_false :=
              ibf.(dep_descr.instr).(dep_cinstr.apply)
                (With_family.IHalt hloc) in
            With_family.IIf
              loc_value branch_if_true branch_if_false k_value |} in
        {| dep_descr.loc := loc_value;
           dep_descr.bef := bef;
           dep_descr.aft := ibt.(dep_descr.aft);
           dep_descr.instr := instr |} in
      dep_merge_branches ctxt loc_value btr bfr {| dep_branch.branch := branch |}
  | Micheline.Prim loc_value Michelson_v1_primitives.I_LOOP (cons body [])
    annot, @With_family.Item_t Ty.Bool t_rest _ rest =>
    let stack_value := @With_family.Item_t Ty.Bool t_rest
                         With_family.Bool_t rest in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
    let? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value
        tc_context_value ctxt legacy body rest in
    let judgement_value : dep_judgement t_rest := judgement_value in
    match judgement_value with
    | @Dep_typed _ t_aft ibody =>
        let unmatched_branches (function_parameter : unit) :
          Error_monad._error :=
          let '_ := function_parameter in
          let aft := dep_serialize_stack_for_error ctxt ibody.(dep_descr.aft) in
          let stack_value := dep_serialize_stack_for_error ctxt stack_value in
          Build_extensible "Unmatched_branches" _
            (loc_value, aft, stack_value) in
        Error_monad.record_trace_eval unmatched_branches (
          let? '(e, ctxt) :=
            dep_stack_eq loc_value ctxt 1 ibody.(dep_descr.aft) stack_value in
          let t_brest := Ty.Bool :: t_rest in
          let e : t_aft = t_brest := e in
          let ibody := let 'eq_refl := e in ibody in
          let instr := {|
            dep_cinstr.apply _ k_value :=
              let loc_value := Script_typed_ir.dep_kinstr_location k_value in
              let ibody :=
                ibody.(dep_descr.instr).(dep_cinstr.apply)
                  (With_family.IHalt loc_value) in
              With_family.ILoop loc_value ibody k_value;
          |} in
          return? (Dep_typed {|
            dep_descr.loc := loc_value;
            dep_descr.bef := With_family.Item_t With_family.Bool_t rest;
            dep_descr.aft := rest;
            dep_descr.instr := instr; |}, ctxt)
        )
    | Dep_failed descr_value =>
      let instr := {|
        dep_cinstr.apply _ k_value :=
          let loc_value := Script_typed_ir.dep_kinstr_location k_value in
          let ibody := descr_value _ stack_value in
          let ibody := ibody.(dep_descr.instr).(dep_cinstr.apply)
            (With_family.IHalt loc_value) in
          With_family.ILoop loc_value ibody k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Bool_t rest;
        dep_descr.aft := rest;
        dep_descr.instr := instr; |}, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LOOP_LEFT (cons body [])
      annot, @With_family.Item_t (Ty.Union t1 t2) t_rest ls rest =>
      let '(@With_family.Union_t t1 t2 tl tr md bd) := ls in
      let stack_value := @With_family.Item_t (Ty.Union t1 t2)
        t_rest (@With_family.Union_t t1 t2 tl tr md bd) rest in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] body in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? '(judgement_value, ctxt) := non_terminal_recursion
        type_logger_value tc_context_value ctxt legacy body
        (With_family.Item_t tl rest) in
    match judgement_value with
    | @Dep_typed _ t_aft {|
         dep_descr.loc := loc_value;
         dep_descr.bef := bef;
         dep_descr.aft := aft;
         dep_descr.instr := instr;
      |} =>
      let unmatched_branches (function_parameter : unit) : Error_monad._error :=
      let '_ := function_parameter in
      let aft := dep_serialize_stack_for_error ctxt aft in
      let stack_value := dep_serialize_stack_for_error ctxt stack_value in
        Build_extensible "Unmatched_branches"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc_value, aft, stack_value) in
      Error_monad.record_trace_eval unmatched_branches
        (let? '(e, ctxt) := dep_stack_eq loc_value ctxt 1 aft stack_value in
         let t_urest := Ty.Union t1 t2 :: t_rest in
         let e : t_aft = t_urest := e in
         let aft := let 'eq_refl := e in aft in
         let instr := let 'eq_refl := e in instr in
         let kibody := {|
           dep_descr.loc := loc_value;
           dep_descr.bef := bef;
           dep_descr.aft := aft;
           dep_descr.instr := instr;
         |} in
         let instr := {|
           dep_cinstr.apply _ k_value :=
            let loc_value := Script_typed_ir.dep_kinstr_location k_value in
            let ibody := kibody.(dep_descr.instr).(dep_cinstr.apply)
              (With_family.IHalt loc_value) in
            With_family.ILoop_left loc_value ibody k_value; |} in
         let stack_value := With_family.Item_t tr rest in
         return? (Dep_typed {|
           dep_descr.loc := loc_value;
           dep_descr.bef :=
            With_family.Item_t (With_family.Union_t tl tr md bd) rest;
           dep_descr.aft := stack_value;
           dep_descr.instr := instr;
         |}, ctxt))
    | Dep_failed descr_value =>
      let instr := {|
        dep_cinstr.apply _ k_value :=
          let loc_value := Script_typed_ir.dep_kinstr_location k_value in
          let ibody := descr_value _ stack_value in
          let ibody :=
            ibody.(dep_descr.instr).(dep_cinstr.apply)
              (With_family.IHalt loc_value) in
          With_family.ILoop_left loc_value ibody k_value; |} in
      let stack_value := With_family.Item_t tr rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t (With_family.Union_t tl tr md bd) rest;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LAMBDA
      (cons arg (cons ret_value (cons code []))) annot, stack_value =>
    let? '(Dep_ex_ty arg, ctxt) :=
      dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z legacy arg in
    let? '(Dep_ex_ty ret_value, ctxt) :=
      dep_parse_any_ty_aux
        ctxt (fuel_to_stack_depth fuel + 1)%Z legacy ret_value in
    let? '_ := check_kind [ Script_tc_errors.Seq_kind ] code in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let? '(lambda, ctxt) :=
      dep_parse_returning type_logger_value
        (fuel_to_stack_depth fuel + 1)%Z
        (Script_tc_context.dep_add_lambda tc_context_value)
        ctxt legacy arg ret_value code in
    let instr := {| dep_cinstr.apply _ k_value :=
          With_family.ILambda loc_value lambda k_value |} in
    let? ty := Script_typed_ir.dep_lambda_t loc_value arg ret_value in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := With_family.Item_t ty stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EXEC [] annot,
    @With_family.Item_t _ (Ty.Lambda _ _ :: _) arg lrest =>
      let '@With_family.Item_t (Ty.Lambda _ _) _ l rest := lrest in
      let 'With_family.Lambda_t param ret_value meta := l in
      let bef :=
        With_family.Item_t arg
          (With_family.Item_t
             (With_family.Lambda_t param ret_value meta) rest) in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt arg param loc_value Michelson_v1_primitives.I_EXEC 1 2 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let stack_value := With_family.Item_t ret_value rest in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IExec loc_value stack_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value := With_family.Item_t ret_value rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_APPLY [] annot,
    @With_family.Item_t
      t_capture (Ty.Lambda (Ty.Pair _ _) _ :: _) capture lrest =>
      let '@With_family.Item_t (Ty.Lambda (Ty.Pair _ _) _) _ l rest := lrest in
      let '@With_family.Lambda_t (Ty.Pair _ _) _ parg ret_value meta := l in
      let '@With_family.Pair_t
            t_capture_ty _ capture_ty arg_ty pmeta pcmp := parg in
      let? '_ := dep_check_packable false loc_value capture_ty in
      let? '(e, ctxt) :=
        check_item_ty ctxt capture capture_ty loc_value
          Michelson_v1_primitives.I_APPLY 1 2 in
      let e : t_capture_ty = t_capture := let 'eq_refl := e in eq_refl in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IApply loc_value capture_ty k_value |} in
      let instr := let 'eq_refl := e in instr in
      let? res_ty := dep_lambda_t loc_value arg_ty ret_value in
      let stack_value := With_family.Item_t res_ty rest in
      let bef :=
        With_family.Item_t capture
          (With_family.Item_t
             (With_family.Lambda_t
                (With_family.Pair_t capture_ty arg_ty pmeta pcmp)
                ret_value meta) rest) in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DIP (cons code []) annot,
    With_family.Item_t v_value rest =>
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      let? '_ := check_kind [ Script_tc_errors.Seq_kind ] code in
      let? '(judgement_value, ctxt) :=
        non_terminal_recursion
          type_logger_value tc_context_value ctxt legacy code rest in
      match judgement_value with
      | Dep_typed descr_value =>
        let instr := {|
          dep_cinstr.apply _ k_value :=
            let b_value :=
              descr_value.(dep_descr.instr).(dep_cinstr.apply)
                (With_family.IHalt descr_value.(dep_descr.loc)) in
            With_family.IDip loc_value b_value v_value k_value
        |} in
        let stack_value :=
          With_family.Item_t v_value descr_value.(dep_descr.aft) in
        return? (Dep_typed {|
          dep_descr.loc := loc_value;
          dep_descr.bef := With_family.Item_t v_value rest;
          dep_descr.aft := stack_value;
          dep_descr.instr := instr;
        |}, ctxt)
      | Dep_failed _ =>
        Error_monad.fail
          (Build_extensible "Fail_not_in_tail_position"
            Alpha_context.Script.location loc_value)
      end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DIP
      (cons n_value (cons code [])) result_annot, stack_value =>
    let? n_value := parse_uint10 n_value in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n_value) in
    let fix make_proof_argument {s} (n_value : nat) (stk : With_family.stack_ty s)
      : M? dep_dipn_proof_argument s :=
      match n_value, stk with
      | Datatypes.O, rest =>
        let? '(judgement_value, ctxt) :=
          non_terminal_recursion
            type_logger_value tc_context_value ctxt legacy code rest in
        match judgement_value with
        | Dep_typed descr_value =>
          return? (
            Dep_dipn_proof_argument
              With_family.KRest ctxt descr_value descr_value.(dep_descr.aft)
          )
        | Dep_failed _ =>
            Error_monad.error_value
              (Build_extensible "Fail_not_in_tail_position"
                 Alpha_context.Script.location loc_value)
        end
      | Datatypes.S n_value, With_family.Item_t v_value rest =>
          let? 'Dep_dipn_proof_argument n' ctxt descr_value aft' :=
            make_proof_argument n_value rest in
          let w_value := With_family.KPrefix loc_value v_value n' in
          return?
            (Dep_dipn_proof_argument w_value ctxt descr_value
               (With_family.Item_t v_value aft'))
      | _, _ =>
          let whole_stack := dep_serialize_stack_for_error ctxt stack_value in
          Error_monad.error_value
            (Build_extensible "Bad_stack"
               (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                  Script_tc_errors.unparsed_stack_ty)
               (loc_value, Michelson_v1_primitives.I_DIP, 1, whole_stack))
      end in
    let? '_ := Script_ir_annot.error_unexpected_annot loc_value result_annot in
    let? 'Dep_dipn_proof_argument n' ctxt descr_value aft := make_proof_argument (Z.to_nat n_value) stack_value in
    let b_value :=
      descr_value.(dep_descr.instr).(dep_cinstr.apply)
        (With_family.IHalt descr_value.(dep_descr.loc)) in
    let res := {|
      dep_cinstr.apply _ k_value :=
        With_family.IDipn loc_value n_value n' b_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := aft;
      dep_descr.instr := res;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_DIP
      (([] | cons _ (cons _ (cons _ _))) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, Michelson_v1_primitives.I_DIP, 2, (List.length l_value)))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_FAILWITH [] annot,
    @With_family.Item_t t t_rest v_value _rest =>
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      let? '_ :=
        if legacy then
          Result.return_unit
        else
          dep_check_packable false loc_value v_value in
      return?
        (let descr_value u (aft : With_family.stack_ty u) := {|
          dep_descr.loc := loc_value;
          dep_descr.bef := With_family.Item_t v_value _rest;
          dep_descr.aft := aft;
          dep_descr.instr := {|
          dep_cinstr.apply _ _k :=
            With_family.IFailwith loc_value v_value |};
        |} in (Dep_failed descr_value, ctxt))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEVER [] annot,
    @With_family.Item_t Ty.Never _ With_family.Never_t _rest =>
      let? '_ := Script_ir_annot.error_unexpected_annot loc_value annot in
      return?
        (let descr_value u (aft : With_family.stack_ty u) :=  {|
          dep_descr.loc := loc_value;
          dep_descr.bef := With_family.Item_t With_family.Never_t _rest;
          dep_descr.aft := aft;
          dep_descr.instr := {|
            dep_cinstr.apply _ _ :=
              With_family.INever loc_value; |}
        |} in (Dep_failed descr_value, ctxt))

  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
    @With_family.Item_t
      Ty.Timestamp (Ty.Num Ty.Num.Int :: t_rest) With_family.Timestamp_t nrest =>
      let '(@With_family.Item_t (Ty.Num Ty.Num.Int) _ _ rest) := nrest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {| dep_cinstr.apply _ k_value :=
            With_family.IAdd_timestamp_to_seconds loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            With_family.Timestamp_t (With_family.Item_t With_family.Int_t rest);
        dep_descr.aft := (With_family.Item_t With_family.Timestamp_t rest);
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
    @With_family.Item_t (Ty.Num Ty.Num.Int) (Ty.Timestamp :: t_rest) iv trest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IAdd_seconds_to_timestamp loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Int_t trest;
        dep_descr.aft := trest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
    @With_family.Item_t Ty.Timestamp (Ty.Num Ty.Num.Int :: t_rest) _ nrest =>
      let '(@With_family.Item_t (Ty.Num Ty.Num.Int) _ _ rest) := nrest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.ISub_timestamp_seconds loc_value k_value |} in
      let stack_value := With_family.Item_t With_family.Timestamp_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            With_family.Timestamp_t (With_family.Item_t With_family.Int_t rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
    @With_family.Item_t Ty.Timestamp (Ty.Timestamp :: t_rest) _ trest =>
      let '(@With_family.Item_t Ty.Timestamp _ _ rest) := trest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IDiff_timestamps loc_value k_value |} in
      let stack_value := With_family.Item_t Script_typed_ir.dep_int_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            With_family.Timestamp_t
            (With_family.Item_t With_family.Timestamp_t rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONCAT [] annot,
    @With_family.Item_t Ty.String (Ty.String :: t_rest) _ srest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IConcat_string_pair loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.String_t srest;
        dep_descr.aft := srest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONCAT [] annot,
    @With_family.Item_t
      (Ty.List Ty.String) _ (With_family.List_t _ meta) rest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IConcat_string loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            (With_family.List_t With_family.String_t meta) rest;
        dep_descr.aft := With_family.Item_t With_family.String_t rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SLICE [] annot,
    @With_family.Item_t
      (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat :: Ty.String :: t_rest) _ nsrest =>
      let '(@With_family.Item_t
              (Ty.Num Ty.Num.Nat) (Ty.String :: t_rest) _ srest) := nsrest in
      let '(@With_family.Item_t Ty.String _ _ rest) := srest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {| dep_cinstr.apply _ k_value :=
            With_family.ISlice_string loc_value k_value |} in
      let stack_value :=
        With_family.Item_t Script_typed_ir.dep_option_string_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            With_family.Nat_t
            (With_family.Item_t
               With_family.Nat_t
               (With_family.Item_t With_family.String_t rest));
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SIZE [] annot,
    (@With_family.Item_t Ty.String t_rest With_family.String_t rest) as bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IString_size loc_value k_value |} in
      let stack_value := With_family.Item_t Script_typed_ir.dep_nat_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONCAT [] annot,
      @With_family.Item_t (Ty.Bytes) (Ty.Bytes :: t_rs) _ b_rs =>
    let '(@With_family.Item_t Ty.Bytes t_rs _ rs) := b_rs in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IConcat_bytes_pair loc_value k_value |} in
    let stack_value := (With_family.Item_t With_family.Bytes_t rs) in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t
                         (With_family.Item_t With_family.Bytes_t rs);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONCAT [] annot,
    @With_family.Item_t (Ty.List Ty.Bytes) ty_rest
      (With_family.List_t With_family.Bytes_t ls)
        rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IConcat_bytes loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bytes_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t
                         (With_family.List_t With_family.Bytes_t ls) rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SLICE [] annot,
    @With_family.Item_t
      (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat :: Ty.Bytes :: t_rest) _ nb_rest
    =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) (Ty.Bytes :: t_rest) _
            b_rest) := nb_rest in
    let '(@With_family.Item_t Ty.Bytes t_rest _ rest) := b_rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISlice_bytes loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_bytes_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t
                         (With_family.Item_t With_family.Nat_t
                           (With_family.Item_t With_family.Bytes_t rest));
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SIZE [] annot,
      (@With_family.Item_t Ty.Bytes t_rest With_family.Bytes_t rest) as bef =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IBytes_size loc_value k_value |} in
    let stack_value := With_family.Item_t Script_typed_ir.dep_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
    @With_family.Item_t Ty.Mutez (Ty.Mutez :: t_rest1) With_family.Mutez_t
      (With_family.Item_t With_family.Mutez_t rest as m_rest) =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
        {|
          dep_cinstr.apply _ k_value :=
            With_family.IAdd_tez loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Mutez_t m_rest;
        dep_descr.aft := m_rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
    @With_family.Item_t Ty.Mutez (Ty.Mutez :: t_rest) With_family.Mutez_t
      (stack_value) =>
        let '(@With_family.Item_t (Ty.Mutez) t_rest _ rest) := stack_value in
      if legacy then
        let? '_ := Script_ir_annot.check_var_annot loc_value annot in
        let instr :=
          {| dep_cinstr.apply _ k_value :=
              With_family.ISub_tez_legacy loc_value k_value |} in
          return? (Dep_typed {|
            dep_descr.loc := loc_value;
            dep_descr.bef := With_family.Item_t With_family.Mutez_t
              (With_family.Item_t With_family.Mutez_t rest);
            dep_descr.aft := (With_family.Item_t With_family.Mutez_t rest);
            dep_descr.instr := instr;
          |}, ctxt)
      else
        Error_monad.fail
          (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
            Michelson_v1_primitives.I_SUB)
  |

    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB_MUTEZ [] annot,
      @With_family.Item_t Ty.Mutez (Ty.Mutez :: t_rest) With_family.Mutez_t
        (stack_info) =>
    let '(@With_family.Item_t Ty.Mutez t_rest _ rest) := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISub_tez loc_value k_value |} in
      let stack_value :=
        With_family.Item_t dep_option_mutez_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Mutez_t 
          (With_family.Item_t With_family.Mutez_t rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t Ty.Mutez (Ty.Num Ty.Num.Nat :: t_rest) 
        _ stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
              With_family.IMul_teznat loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Mutez_t 
      (With_family.Item_t With_family.Nat_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Mutez_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) (Ty.Mutez :: t_rest) 
        With_family.Nat_t stack_value => 
    let '(@With_family.Item_t Ty.Mutez t_rest _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IMul_nattez loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_OR [] annot,
      @With_family.Item_t Ty.Bool (Ty.Bool :: t_rest) With_family.Bool_t
        (stack_value) =>
    let '(@With_family.Item_t Ty.Bool t_rest _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value :=
      With_family.IOr loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bool_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_AND [] annot,
      @With_family.Item_t Ty.Bool (Ty.Bool :: t_rest) 
      With_family.Bool_t stack_value =>
      let '(@With_family.Item_t Ty.Bool t_rest _ rest)
        := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IAnd loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bool_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_XOR [] annot,
      @With_family.Item_t Ty.Bool (Ty.Bool :: t_rest) With_family.Bool_t
        ((With_family.Item_t With_family.Bool_t _) as stack_value) =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IXor loc_value k_value |} in
      return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bool_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NOT [] annot,
      (@With_family.Item_t Ty.Bool _ With_family.Bool_t _) as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.INot loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ABS [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Nat) :: t_rest)
        With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IAbs_int loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Nat_t rest in
    return? (Dep_typed {|
    dep_descr.loc := loc_value;
    dep_descr.bef := With_family.Item_t With_family.Int_t rest;
    dep_descr.aft := stack_value;
    dep_descr.instr := instr;
  |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ISNAT [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) _ With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr := 
      {| dep_cinstr.apply _ k_value  :=
        With_family.IIs_nat loc_value k_value |} in
    let stack_value := With_family.Item_t dep_option_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t 
        rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_INT [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) _ With_family.Nat_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr := 
      {| dep_cinstr.apply _ k_value  :=
        With_family.IInt_nat loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t 
        rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEG [] annot,
      (@With_family.Item_t (Ty.Num Ty.Num.Int) _ With_family.Int_t rest) 
        as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
            With_family.INeg loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEG [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) _ With_family.Nat_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
            With_family.INeg loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t 
      (Ty.Num Ty.Num.Int) 
      ((Ty.Num Ty.Num.Int) :: trest) _ rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value  :=
            With_family.IAdd_int loc_value k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Int_t rest;
        dep_descr.aft := rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Nat) :: t_rest)
       With_family.Int_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest) 
      := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
            With_family.IAdd_int loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t 
        (With_family.Item_t With_family.Nat_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Int) :: t_rest) 
       With_family.Nat_t ((With_family.Item_t With_family.Int_t _) as stack_value) =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
            With_family.IAdd_int loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t 
        stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest) 
      With_family.Nat_t ((With_family.Item_t With_family.Nat_t _) 
        as stack_value) =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
            With_family.IAdd_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t 
        stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
      @With_family.Item_t 
      (Ty.Num Ty.Num.Int) 
      ((Ty.Num Ty.Num.Int) :: trest) _ rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value :=
      With_family.ISub_int loc_value k_value |} in
    return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Int_t rest;
        dep_descr.aft := rest;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Nat) :: t_rest)
      With_family.Int_t stack_info => 
      let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest) 
        := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.ISub_int loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Int_t
          (With_family.Item_t With_family.Nat_t rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Int) :: t_rest)
      With_family.Nat_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) t_rest _ rest) 
      := stack_value in
    let aft := With_family.Item_t With_family.Int_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.ISub_int loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t aft;
      dep_descr.aft := aft;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SUB [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest)
      With_family.Nat_t (stack_info) =>
        let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest) := 
          stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.ISub_int loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t
        (With_family.Item_t With_family.Nat_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) (Ty.Num Ty.Num.Int :: ty_stack) 
      With_family.Int_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_int loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Int_t
      (With_family.Item_t With_family.Int_t rest) in 
    let aft_stack_value := With_family.Item_t With_family.Int_t rest in 
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value; 
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)

  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) (Ty.Num Ty.Num.Nat :: ty_stack) 
        With_family.Int_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_int loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Int_t 
      (With_family.Item_t With_family.Nat_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value; 
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Int :: ty_stack) 
        With_family.Nat_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_nat loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Nat_t 
      (With_family.Item_t With_family.Int_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value; 
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)

  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat :: ty_stack) 
      With_family.Int_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_nat loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Nat_t
      (With_family.Item_t With_family.Nat_t rest) in 
    let aft_stack_value := With_family.Item_t With_family.Nat_t rest in 
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value; 
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t Ty.Mutez ((Ty.Num Ty.Num.Nat) :: t_rest)
      With_family.Mutez_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest)
     := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
          With_family.IEdiv_teznat loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_mutez_mutez_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Mutez_t
        (With_family.Item_t With_family.Nat_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t Ty.Mutez (Ty.Mutez :: t_rest) 
      With_family.Mutez_t stack_info =>
    let '(@With_family.Item_t Ty.Mutez t_rest _ rest) 
     := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
          With_family.IEdiv_tez loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_nat_mutez_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Mutez_t
      (With_family.Item_t With_family.Mutez_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Int) :: t_rest) 
      With_family.Int_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) t_rest _ rest) 
      := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
          With_family.IEdiv_int loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_int_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t
      (With_family.Item_t With_family.Int_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Nat) :: t_rest) 
        With_family.Int_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest)
      := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
          With_family.IEdiv_int loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_int_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t
        (With_family.Item_t With_family.Nat_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Int) :: t_rest)
        With_family.Nat_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) t_rest _ rest)
      := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
          With_family.IEdiv_nat loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_int_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t
        (With_family.Item_t With_family.Int_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EDIV [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest)
        With_family.Nat_t stack_info =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest) 
      := stack_info in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value  :=
        With_family.IEdiv_nat loc_value k_value |} in
    let stack_value :=
      With_family.Item_t dep_option_pair_nat_nat_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t
        (With_family.Item_t With_family.Nat_t rest);
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LSL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest)
        With_family.Nat_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ _) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.ILsl_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LSR [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest) 
        With_family.Nat_t ((With_family.Item_t With_family.Nat_t _) 
        as stack_value) =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value :=
    With_family.ILsr_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_OR [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest) 
      With_family.Nat_t (stack_value) =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest)
      := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IOr_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  | 
    Micheline.Prim loc_value Michelson_v1_primitives.I_AND [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest)
      With_family.Nat_t stack_value =>
        let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) t_rest _ rest) := 
          stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IAnd_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_AND [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) ((Ty.Num Ty.Num.Nat) :: t_rest)
      With_family.Int_t stack_value =>
      let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) ty_rest _ rest) := 
        stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IAnd_int_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_XOR [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) ((Ty.Num Ty.Num.Nat) :: t_rest)
        With_family.Nat_t ((With_family.Item_t With_family.Nat_t _) 
          as stack_value) =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IXor_nat loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NOT [] annot,
      (@With_family.Item_t (Ty.Num Ty.Num.Int) _ With_family.Int_t _) as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.INot_int loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NOT [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) _ With_family.Nat_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.INot_int loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Nat_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_COMPARE [] annot,
    @With_family.Item_t _ (_ :: _) t1 t2rest =>
      let '@With_family.Item_t _ _ t2 rest := t2rest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? '(e, ctxt) :=
        check_item_ty
          ctxt t1 t2 loc_value Michelson_v1_primitives.I_COMPARE 1 2 in
      let? 'Eq := dep_check_comparable loc_value t1 in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ICompare loc_value t1 k_value |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value := With_family.Item_t dep_int_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t t1 (With_family.Item_t t2 rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_EQ [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IEq loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEQ [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.INeq loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LT [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value :=
    With_family.ILt loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GT [] annot,
    @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IGt loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LE [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.ILe loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_GE [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) t_rest With_family.Int_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
      With_family.IGe loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Int_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CAST (cons cast_t [])
      annot, With_family.Item_t t_value rest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? '(Dep_ex_ty cast_t, ctxt) :=
        dep_parse_any_ty_aux ctxt (fuel_to_stack_depth fuel + 1)%Z legacy cast_t in
      let? '(eq_value, ctxt) :=
        Gas_monad.run ctxt
          (dep_ty_eq (Script_tc_errors.Dep_informative loc_value) cast_t
            t_value) in
      let? e := eq_value in
      let instr :=
        {| dep_cinstr.apply _ k_value := k_value |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value := With_family.Item_t t_value rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := stack_value;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_RENAME [] annot,
    With_family.Item_t v rest =>
      let stack_value := With_family.Item_t v rest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {| dep_cinstr.apply _ k_value := k_value |} in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := stack_value;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  | 
    Micheline.Prim loc_value Michelson_v1_primitives.I_PACK [] annot,
      With_family.Item_t t_value rest as stack_ty =>
    let? '_ := dep_check_packable true loc_value t_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
          With_family.IPack loc_value t_value k_value; |} in
    let stack_value := With_family.Item_t Script_typed_ir.dep_bytes_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim
      loc_value Michelson_v1_primitives.I_UNPACK (cons ty []) annot,
    @With_family.Item_t
      Ty.Bytes t_rest With_family.Bytes_t rest as stack_ty =>
    let? '(@Dep_ex_ty t_ty ty_value, ctxt) :=
      match fuel with
      | Datatypes.O =>
          Error_monad.fail
            (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
      | Datatypes.S fuel =>
          dep_parse_packable_ty_aux ctxt (fuel_to_stack_depth fuel) legacy ty
      end in
    let? '_ := Script_ir_annot.check_var_type_annot loc_value annot in
    let? res_ty := Script_typed_ir.dep_option_t loc_value ty_value in
    let stack_value := With_family.Item_t res_ty rest in
    let cinstr :=
      {|
        dep_cinstr.apply f k_value :=
          @With_family.IUnpack _ t_rest f loc_value ty_value k_value; |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_ty;
      dep_descr.aft := stack_value;
      dep_descr.instr := cinstr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADDRESS [] annot,
    @With_family.Item_t
      (Ty.Contract t1 as t11) ts_rest
      (With_family.Contract_t t2 _ as t22) st_rest as st' =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let stack_value : With_family.stack_ty (Ty.Address :: ts_rest) :=
      With_family.Item_t Script_typed_ir.dep_address_t st_rest in
    match t11, t22 in With_family.ty (Ty.Contract t21)
      return With_family.ty t11 -> _ with
    | _, _ =>
        fun x =>
          let instr :=
            {|
              dep_cinstr.apply _ kinstr :=
                With_family.IAddress loc_value kinstr |} in
          return? (Dep_typed {|
            dep_descr.loc := loc_value;
            dep_descr.bef := (With_family.Item_t x st_rest);
            dep_descr.aft := stack_value;
            dep_descr.instr := instr;
          |}, ctxt)
    end t22
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CONTRACT (cons ty [])
      annot, @With_family.Item_t Ty.Address _ _ rest =>
      let? '(Dep_ex_ty t_value, ctxt) :=
        dep_parse_passable_ty_aux
          ctxt (fuel_to_stack_depth fuel + 1)%Z legacy ty in
      let? contract_ty := dep_contract_t loc_value t_value in
      let? res_ty := dep_option_t loc_value contract_ty in
      let? entrypoint :=
        Script_ir_annot.parse_entrypoint_annot_strict loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IContract loc_value t_value entrypoint k_value
      |} in
      let stack_value := With_family.Item_t res_ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t With_family.Address_t rest;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_VIEW
      (cons name (cons output_ty [])) annot,
    @With_family.Item_t _ (Ty.Address :: _) input_ty arest =>
      let '@With_family.Item_t Ty.Address _ _ rest := arest in
      let output_ty_loc := location output_ty in
      let? '(name, ctxt) := dep_parse_view_name ctxt name in
      let? '(Dep_ex_ty output_ty, ctxt) :=
        dep_parse_view_output_ty ctxt 0 legacy output_ty in
      let? res_ty := dep_option_t output_ty_loc output_ty in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let sty := With_family.Item_t output_ty rest in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IView
            loc_value
            (With_family.View_signature name input_ty output_ty)
            sty
            k_value
      |} in
      let stack_value := With_family.Item_t res_ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef :=
          With_family.Item_t
            input_ty (With_family.Item_t With_family.Address_t rest);
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value
      (Michelson_v1_primitives.I_TRANSFER_TOKENS as prim) [] annot,
    @With_family.Item_t _ (Ty.Mutez :: Ty.Contract _ :: _) p_value mcrest =>
      let '@With_family.Item_t Ty.Mutez (Ty.Contract _ :: _) _ crest := mcrest in
      let '@With_family.Item_t (Ty.Contract _) _ c rest := crest in
      let 'With_family.Contract_t cp meta := c in
      let bef :=
        With_family.Item_t p_value
          (With_family.Item_t With_family.Mutez_t
             (With_family.Item_t (With_family.Contract_t cp meta) rest)) in
      let? '_ :=
        Script_tc_context.dep_check_not_in_view
          loc_value legacy tc_context_value prim in
      let? '(e, ctxt) := check_item_ty ctxt p_value cp loc_value prim 1 4 in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ITransfer_tokens loc_value k_value |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value := With_family.Item_t dep_operation_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value (Michelson_v1_primitives.I_SET_DELEGATE as prim)
      [] annot,
    @With_family.Item_t (Ty.Option Ty.Key_hash) _ o rest =>
      let? '_ :=
        Script_tc_context.dep_check_not_in_view
          loc_value legacy tc_context_value prim in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISet_delegate loc_value k_value
      |} in
      let stack_value := With_family.Item_t dep_operation_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t o rest;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  | Micheline.Prim _ Michelson_v1_primitives.I_CREATE_ACCOUNT _ _, _ =>
    Error_monad.fail
      (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
        Michelson_v1_primitives.I_CREATE_ACCOUNT)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_IMPLICIT_ACCOUNT []
      annot, @With_family.Item_t Ty.Key_hash _ kh rest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IImplicit_account loc_value k_value |} in
      let stack_value := With_family.Item_t dep_contract_unit_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t kh rest;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value
      (Michelson_v1_primitives.I_CREATE_CONTRACT as prim)
      (cons ((Micheline.Seq _ _) as code) []) annot,
    @With_family.Item_t (Ty.Option Ty.Key_hash) (Ty.Mutez :: _ :: _) o mgrest =>
      let '@With_family.Item_t Ty.Mutez (_ :: _) m grest := mgrest in
      let '@With_family.Item_t _ _ ginit rest := grest in
      let gbef :=
        With_family.Item_t o
          (With_family.Item_t m (With_family.Item_t ginit rest)) in
      let? '_ :=
        Script_tc_context.dep_check_not_in_view
          loc_value legacy tc_context_value prim in
      let? '_ := Script_ir_annot.check_two_var_annot loc_value annot in
      let canonical_code := Micheline.strip_locations code in
      let? '({|
        dep_toplevel.code_field := code_field;
        dep_toplevel.arg_type := arg_type;
        dep_toplevel.storage_type := storage_type;
        dep_toplevel.views := views
      |}, ctxt) := dep_parse_toplevel_aux ctxt legacy canonical_code in
      let? '(Dep_ex_parameter_ty_and_entrypoints arg_type entrypoints, ctxt) :=
        Error_monad.record_trace
          (Build_extensible "Ill_formed_type"
             (option string * Micheline.canonical Alpha_context.Script.prim *
                Alpha_context.Script.location)
             ((Some "parameter"), canonical_code, (location arg_type)))
          (dep_parse_parameter_ty_and_entrypoints_aux
             ctxt (fuel_to_stack_depth fuel + 1)%Z legacy arg_type) in
      let? '(Dep_ex_ty storage_type, ctxt) :=
        Error_monad.record_trace
          (Build_extensible "Ill_formed_type"
             (option string * Micheline.canonical Alpha_context.Script.prim *
                Alpha_context.Script.location)
             ((Some "storage"), canonical_code, (location storage_type)))
          (dep_parse_storage_ty
             ctxt (fuel_to_stack_depth fuel + 1)%Z legacy storage_type) in
      let? 'Dep_ty_ex_c arg_type_full :=
        dep_pair_t loc_value arg_type storage_type in
      let? 'Dep_ty_ex_c ret_type_full :=
        dep_pair_t loc_value dep_list_operation_t storage_type in
      let? function_parameter :=
        Error_monad.trace_value
          (Build_extensible "Ill_typed_contract"
             (Micheline.canonical Alpha_context.Script.prim *
                Script_tc_errors.type_map) (canonical_code, nil))
          (dep_parse_returning type_logger_value (fuel_to_stack_depth fuel + 1)%Z
             (Script_tc_context.dep_toplevel_value
                storage_type arg_type entrypoints) ctxt
             legacy arg_type_full ret_type_full code_field) in
    match function_parameter with
    |
      (({|
          With_family.kdescr.kbef :=
            With_family.Item_t arg With_family.Bot_t;
          With_family.kdescr.kaft :=
            With_family.Item_t ret_value With_family.Bot_t;
          |}, _), ctxt) =>
        let views_result :=
          dep_parse_views
            type_logger_value ctxt legacy storage_type views in
        let? '(_typed_views, ctxt) :=
          Error_monad.trace_value
            (Build_extensible "Ill_typed_contract"
               (Micheline.canonical Alpha_context.Script.prim *
                  Script_tc_errors.type_map) (canonical_code, nil)) views_result in
        let? '(storage_eq, ctxt) :=
          Gas_monad.run ctxt
            (Gas_monad.Syntax.op_letstar
               (dep_ty_eq (Script_tc_errors.Dep_informative loc_value) arg arg_type_full)
               (fun e1 =>
                  Gas_monad.Syntax.op_letstar
                    (dep_ty_eq
                      (Script_tc_errors.Dep_informative loc_value)
                      ret_value ret_type_full)
                (fun e2 =>
                  dep_ty_eq
                    (Script_tc_errors.Dep_informative loc_value)
                    storage_type ginit))) in
      let? e := storage_eq in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ICreate_contract
            loc_value storage_type canonical_code k_value
      |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value :=
        With_family.Item_t dep_operation_t
          (With_family.Item_t dep_address_t rest) in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := gbef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
    | _ => Error_monad.fail (Build_extensible "unreachable" unit tt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NOW [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.INow loc_value k_value
      |} in
      let stack_value :=
        With_family.Item_t dep_timestamp_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  | Micheline.Prim loc_value Michelson_v1_primitives.I_MIN_BLOCK_TIME [] _,
    stack_value =>
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := stack_value;
        dep_descr.aft := With_family.Item_t dep_nat_t stack_value;
        dep_descr.instr := {|
          dep_cinstr.apply _ k_value :=
            With_family.IMin_block_time loc_value k_value
        |};
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_AMOUNT [] annot, bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IAmount loc_value k_value |} in
      let stack_value :=
        With_family.Item_t dep_mutez_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CHAIN_ID [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IChainId loc_value k_value |} in
      let stack_value :=
        With_family.Item_t dep_chain_id_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_BALANCE [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IBalance loc_value k_value
      |} in
      let stack_value :=
        With_family.Item_t dep_mutez_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LEVEL [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ILevel loc_value k_value |} in
      let stack_value := With_family.Item_t dep_nat_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_VOTING_POWER [] annot,
    @With_family.Item_t Ty.Key_hash _ kh rest =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IVoting_power loc_value k_value
      |} in
      let stack_value := With_family.Item_t dep_nat_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := With_family.Item_t kh rest;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_TOTAL_VOTING_POWER []
      annot, bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ITotal_voting_power loc_value k_value
      |} in
      let stack_value := With_family.Item_t dep_nat_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  | Micheline.Prim _ Michelson_v1_primitives.I_STEPS_TO_QUOTA _ _, _ =>
    Error_monad.fail
      (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
         Michelson_v1_primitives.I_STEPS_TO_QUOTA)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SOURCE [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISource loc_value k_value |} in
      let stack_value := With_family.Item_t dep_address_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SENDER [] annot,
    bef =>
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISender loc_value k_value |} in
      let stack_value := With_family.Item_t dep_address_t bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value (Michelson_v1_primitives.I_SELF as prim) [] annot,
    bef =>
    let? entrypoint :=
      Script_ir_annot.parse_entrypoint_annot_lax loc_value annot in
    match
      tc_context_value.(Script_tc_context.dep_t.callsite),
        (let '_ := tc_context_value.(Script_tc_context.dep_t.callsite) in
        Script_tc_context.dep_is_in_lambda tc_context_value) with
    | _, true =>
        Error_monad.error_value
          (Build_extensible "Forbidden_instr_in_context"
             (Alpha_context.Script.location * Script_tc_errors.context_desc *
                Alpha_context.Script.prim)
             (loc_value, Script_tc_errors.Lambda, prim))
    | Script_tc_context.Dep_data, _ =>
        Error_monad.error_value
          (Build_extensible "Forbidden_instr_in_context"
             (Alpha_context.Script.location * Script_tc_errors.context_desc *
                Alpha_context.Script.prim)
             (loc_value, Script_tc_errors.Lambda, prim))
    | Script_tc_context.Dep_view, _ =>
        Error_monad.error_value
          (Build_extensible "Forbidden_instr_in_context"
             (Alpha_context.Script.location * Script_tc_errors.context_desc *
                Alpha_context.Script.prim) (loc_value, Script_tc_errors.View, prim))
    |
      Script_tc_context.Dep_toplevel _  param_type entrypoints, _ =>
      let? '(r_value, ctxt) :=
        Gas_monad.run ctxt
          ((dep_find_entrypoint
              (Script_tc_errors.Dep_informative tt)
              param_type entrypoints entrypoint) :
            Gas_monad.t (dep_ex_ty_cstr _)
              (Error_monad.trace Error_monad._error)) in
      let? 'Dep_ex_ty_cstr param_type _ _ := r_value in
      let? res_ty := dep_contract_t loc_value param_type in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISelf loc_value param_type entrypoint k_value |} in
      let stack_value := With_family.Item_t res_ty bef in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
    end
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SELF_ADDRESS [] annot,
      stack_value_b =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISelf_address loc_value k_value |} in
    let stack_value :=
      With_family.Item_t Script_typed_ir.dep_address_t stack_value_b in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := stack_value_b;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_HASH_KEY [] annot,
      @With_family.Item_t Ty.Key ty_rest With_family.Key_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IHash_key loc_value k_value |} in
    let stack_value := With_family.Item_t Script_typed_ir.dep_key_hash_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Key_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CHECK_SIGNATURE [] annot,
    @With_family.Item_t
      Ty.Key (Ty.Signature :: Ty.Bytes :: ty_rest) _ sbrest =>
      let '@With_family.Item_t Ty.Signature _ _ brest := sbrest in
      let '@With_family.Item_t Ty.Bytes _ _ rest := brest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr :=
      {| dep_cinstr.apply _ k_value :=
           With_family.ICheck_signature loc_value k_value |} in
    let stack_value := With_family.Item_t Script_typed_ir.dep_bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef :=
        With_family.Item_t
          With_family.Key_t
          (With_family.Item_t
             With_family.Signature_t
             (With_family.Item_t With_family.Bytes_t rest));
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_BLAKE2B [] annot,
      @With_family.Item_t (Ty.Bytes) ty_rest With_family.Bytes_t rest =>
    let stack_value := (With_family.Item_t With_family.Bytes_t rest) in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IBlake2b loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SHA256 [] annot,
      @With_family.Item_t (Ty.Bytes) ty_rest With_family.Bytes_t rest =>
    let stack_value := (With_family.Item_t With_family.Bytes_t rest) in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISha256 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SHA512 [] annot,
      @With_family.Item_t (Ty.Bytes) ty_rest With_family.Bytes_t rest =>
    let stack_value := With_family.Item_t With_family.Bytes_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISha512 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_KECCAK [] annot,
      @With_family.Item_t (Ty.Bytes) ty_rest With_family.Bytes_t rest =>
    let stack_value := With_family.Item_t With_family.Bytes_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IKeccak loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SHA3 [] annot,
      @With_family.Item_t (Ty.Bytes) ty_rest With_family.Bytes_t rest =>
    let stack_value := With_family.Item_t With_family.Bytes_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.ISha3 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bytes_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t Ty.Bls12_381_g1 (Ty.Bls12_381_g1 :: ty_rest)
        With_family.Bls12_381_g1_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_g1 ty_rest _ rest) := stack_value in
    let aft := With_family.Item_t With_family.Bls12_381_g1_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IAdd_bls12_381_g1 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_g1_t aft;
      dep_descr.aft := aft;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
      @With_family.Item_t Ty.Bls12_381_g2 (Ty.Bls12_381_g2 :: ty_rest)
        With_family.Bls12_381_g2_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_g2 ty_rest _ rest) := stack_value in
    let aft := With_family.Item_t With_family.Bls12_381_g2_t rest in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IAdd_bls12_381_g2 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_g2_t aft;
      dep_descr.aft := aft;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_ADD [] annot,
    @With_family.Item_t Ty.Bls12_381_fr (Ty.Bls12_381_fr :: ty_rest)
      With_family.Bls12_381_fr_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_fr ty_rest _ rest) := stack_value in
    let aft := With_family.Item_t With_family.Bls12_381_fr_t rest in  
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value :=
          With_family.IAdd_bls12_381_fr loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_fr_t aft;
      dep_descr.aft := aft;
      dep_descr.instr := instr;
    |}, ctxt)
  
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Bls12_381_g1) (Ty.Bls12_381_fr :: ty_rest) 
      With_family.Bls12_381_g1_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_fr _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_g1 loc_value k_value |} in
    let bef_stack_value :=
      With_family.Item_t With_family.Bls12_381_g1_t 
      (With_family.Item_t With_family.Bls12_381_fr_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_g1_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t Ty.Bls12_381_g2 (Ty.Bls12_381_fr :: ty_stack) 
        With_family.Bls12_381_g2_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_fr _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_g2 loc_value k_value |} in
    let bef_stack_value :=
      With_family.Item_t With_family.Bls12_381_g2_t 
      (With_family.Item_t With_family.Bls12_381_fr_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_g2_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t Ty.Bls12_381_fr (Ty.Bls12_381_fr :: ty_stack)
       With_family.Bls12_381_fr_t (stack_value) =>
    let '(@With_family.Item_t Ty.Bls12_381_fr _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_fr loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Bls12_381_fr_t
      (With_family.Item_t With_family.Bls12_381_fr_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_fr_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Nat) (Ty.Bls12_381_fr :: ty_stack) 
        With_family.Nat_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_fr _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_fr_z loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Nat_t
      (With_family.Item_t With_family.Bls12_381_fr_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_fr_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Num Ty.Num.Int) (Ty.Bls12_381_fr :: ty_stack) 
        With_family.Int_t stack_value =>
    let '(@With_family.Item_t Ty.Bls12_381_fr _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_fr_z loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Int_t
      (With_family.Item_t With_family.Bls12_381_fr_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_fr_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Bls12_381_fr) (Ty.Num Ty.Num.Int :: ty_stack) 
        With_family.Bls12_381_fr_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Int) _ _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_z_fr loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Bls12_381_fr_t 
      (With_family.Item_t With_family.Int_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_fr_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_MUL [] annot,
      @With_family.Item_t (Ty.Bls12_381_fr) (Ty.Num Ty.Num.Nat :: ty_stack) 
        With_family.Bls12_381_fr_t stack_value =>
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) ty_stack _ rest) := stack_value in
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
              With_family.IMul_bls12_381_z_fr loc_value k_value |} in
    let bef_stack_value := With_family.Item_t With_family.Bls12_381_fr_t 
      (With_family.Item_t With_family.Nat_t rest) in
    let aft_stack_value := With_family.Item_t With_family.Bls12_381_fr_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_INT [] annot,
    @With_family.Item_t Ty.Bls12_381_fr t_rest 
      With_family.Bls12_381_fr_t rest =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
    {| dep_cinstr.apply _ k_value  :=
          With_family.IInt_bls12_381_fr loc_value k_value |} in
    let stack_value := With_family.Item_t With_family.Int_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_fr_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEG [] annot,
    (@With_family.Item_t Ty.Bls12_381_g1 _ With_family.Bls12_381_g1_t rest)
    as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
        With_family.INeg_bls12_381_g1 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_g1_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEG [] annot,
    (@With_family.Item_t Ty.Bls12_381_g2 _ With_family.Bls12_381_g2_t rest)
    as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
        With_family.INeg_bls12_381_g2 loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_g2_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_NEG [] annot,
    (@With_family.Item_t Ty.Bls12_381_fr _ With_family.Bls12_381_fr_t rest)
    as stack_value =>
    let? '_ := Script_ir_annot.check_var_annot loc_value annot in
    let instr :=
      {| dep_cinstr.apply _ k_value  :=
        With_family.INeg_bls12_381_fr loc_value k_value |} in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := With_family.Item_t With_family.Bls12_381_fr_t rest;
      dep_descr.aft := stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_PAIRING_CHECK [] annot,
    @With_family.Item_t
      (Ty.List (Ty.Pair Ty.Bls12_381_g1 Ty.Bls12_381_g2)) _ lp rest =>
      let bef := With_family.Item_t lp rest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IPairing_check_bls12_381 loc_value k_value |} in
      let stack_value := With_family.Item_t dep_bool_t rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_TICKET [] annot,
    @With_family.Item_t _ (Ty.Num Ty.Num.Nat :: _) t_value nrest =>
      let '@With_family.Item_t (Ty.Num Ty.Num.Nat) _ n rest := nrest in
      let bef := With_family.Item_t t_value (With_family.Item_t n rest) in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? 'Eq := dep_check_comparable loc_value t_value in
      let? res_ty := dep_ticket_t loc_value t_value in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ITicket loc_value t_value k_value |} in
      let stack_value := With_family.Item_t res_ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_READ_TICKET [] annot,
    @With_family.Item_t (Ty.Ticket _) _ ticket rest as full_stack =>
      let '@With_family.Ticket_t _ t_value meta := ticket in
      let full_stack :=
        With_family.Item_t (With_family.Ticket_t t_value meta) rest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let '_ := dep_check_dupable_comparable_ty t_value in
      let? opened_ticket_ty := dep_opened_ticket_type loc_value t_value in
      let result_value := dep_ty_of_comparable_ty opened_ticket_ty in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IRead_ticket loc_value t_value k_value |} in
      let stack_value := With_family.Item_t result_value full_stack in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := full_stack;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_SPLIT_TICKET [] annot,
    @With_family.Item_t
      (Ty.Ticket _) (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat) :: _)
      ticket prest =>
      let '@With_family.Item_t
            (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat))
            _ p rest := prest in
      let '@With_family.Ticket_t _ t_value meta := ticket in
      let ticket_t := With_family.Ticket_t t_value meta in
      let bef := With_family.Item_t ticket_t (With_family.Item_t p rest) in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let '_ := dep_check_dupable_comparable_ty t_value in
      let? 'Dep_ty_ex_c pair_tickets_ty :=
        dep_pair_t loc_value ticket_t ticket_t in
      let? res_ty := dep_option_t loc_value pair_tickets_ty in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.ISplit_ticket loc_value k_value |} in
      let stack_value := With_family.Item_t res_ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_JOIN_TICKETS [] annot,
    @With_family.Item_t (Ty.Pair (Ty.Ticket _) (Ty.Ticket _)) _ p rest =>
      let '@With_family.Pair_t
            (Ty.Ticket _) (Ty.Ticket _) ty_a' ty_b' pmeta pcmp := p in
      let '@With_family.Ticket_t _ contents_ty_a ameta := ty_a' in
      let '@With_family.Ticket_t _ contents_ty_b bmeta := ty_b' in
      let ty_a := With_family.Ticket_t contents_ty_a ameta in
      let ty_b := With_family.Ticket_t contents_ty_b bmeta in
      let bef :=
        With_family.Item_t (With_family.Pair_t ty_a ty_b pmeta pcmp) rest in
      let? '_ := Script_ir_annot.check_var_annot loc_value annot in
      let? '(eq_value, ctxt) :=
        Gas_monad.run ctxt
          (dep_ty_eq
             (Script_tc_errors.Dep_informative loc_value)
             contents_ty_a contents_ty_b) in
      let? e := eq_value in
      let? res_ty := dep_option_t loc_value ty_a in
      let instr := {|
        dep_cinstr.apply _ k_value :=
          With_family.IJoin_tickets loc_value contents_ty_a k_value |} in
      let instr := let 'eq_refl := e in instr in
      let stack_value := With_family.Item_t res_ty rest in
      return? (Dep_typed {|
        dep_descr.loc := loc_value;
        dep_descr.bef := bef;
        dep_descr.aft := stack_value;
        dep_descr.instr := instr;
      |}, ctxt)
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_OPEN_CHEST [] _,
      @With_family.Item_t (Ty.Chest_key)
        (Ty.Chest :: (Ty.Num Ty.Num.Nat) :: ty_stack)
        With_family.Chest_key_t stack_value =>
    let '(@With_family.Item_t Ty.Chest _ _ nat_rest) := stack_value in
    let '(@With_family.Item_t (Ty.Num Ty.Num.Nat) _ _ rest) := nat_rest in
    let instr :=
      {|
        dep_cinstr.apply _ k_value :=
          With_family.IOpen_chest loc_value k_value |} in
    let bef_stack_value :=
      With_family.Item_t With_family.Chest_key_t
        (With_family.Item_t With_family.Chest_t 
          (With_family.Item_t With_family.Nat_t rest)) in
    let aft_stack_value :=
      With_family.Item_t Script_typed_ir.dep_union_bytes_bool_t rest in
    return? (Dep_typed {|
      dep_descr.loc := loc_value;
      dep_descr.bef := bef_stack_value;
      dep_descr.aft := aft_stack_value;
      dep_descr.instr := instr;
    |}, ctxt)
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_DUP | Michelson_v1_primitives.I_SWAP |
      Michelson_v1_primitives.I_SOME | Michelson_v1_primitives.I_UNIT |
      Michelson_v1_primitives.I_PAIR | Michelson_v1_primitives.I_UNPAIR |
      Michelson_v1_primitives.I_CAR | Michelson_v1_primitives.I_CDR |
      Michelson_v1_primitives.I_CONS | Michelson_v1_primitives.I_CONCAT |
      Michelson_v1_primitives.I_SLICE | Michelson_v1_primitives.I_MEM |
      Michelson_v1_primitives.I_UPDATE | Michelson_v1_primitives.I_GET |
      Michelson_v1_primitives.I_EXEC | Michelson_v1_primitives.I_FAILWITH |
      Michelson_v1_primitives.I_SIZE | Michelson_v1_primitives.I_ADD |
      Michelson_v1_primitives.I_SUB | Michelson_v1_primitives.I_SUB_MUTEZ |
      Michelson_v1_primitives.I_MUL | Michelson_v1_primitives.I_EDIV |
      Michelson_v1_primitives.I_OR | Michelson_v1_primitives.I_AND |
      Michelson_v1_primitives.I_XOR | Michelson_v1_primitives.I_NOT |
      Michelson_v1_primitives.I_ABS | Michelson_v1_primitives.I_NEG |
      Michelson_v1_primitives.I_LSL | Michelson_v1_primitives.I_LSR |
      Michelson_v1_primitives.I_COMPARE | Michelson_v1_primitives.I_EQ |
      Michelson_v1_primitives.I_NEQ | Michelson_v1_primitives.I_LT |
      Michelson_v1_primitives.I_GT | Michelson_v1_primitives.I_LE |
      Michelson_v1_primitives.I_GE | Michelson_v1_primitives.I_TRANSFER_TOKENS |
      Michelson_v1_primitives.I_SET_DELEGATE | Michelson_v1_primitives.I_NOW |
      Michelson_v1_primitives.I_MIN_BLOCK_TIME |
      Michelson_v1_primitives.I_IMPLICIT_ACCOUNT |
      Michelson_v1_primitives.I_AMOUNT | Michelson_v1_primitives.I_BALANCE |
      Michelson_v1_primitives.I_LEVEL |
      Michelson_v1_primitives.I_CHECK_SIGNATURE |
      Michelson_v1_primitives.I_HASH_KEY | Michelson_v1_primitives.I_SOURCE |
      Michelson_v1_primitives.I_SENDER | Michelson_v1_primitives.I_BLAKE2B |
      Michelson_v1_primitives.I_SHA256 | Michelson_v1_primitives.I_SHA512 |
      Michelson_v1_primitives.I_ADDRESS | Michelson_v1_primitives.I_RENAME |
      Michelson_v1_primitives.I_PACK | Michelson_v1_primitives.I_ISNAT |
      Michelson_v1_primitives.I_INT | Michelson_v1_primitives.I_SELF |
      Michelson_v1_primitives.I_CHAIN_ID | Michelson_v1_primitives.I_NEVER |
      Michelson_v1_primitives.I_VOTING_POWER |
      Michelson_v1_primitives.I_TOTAL_VOTING_POWER |
      Michelson_v1_primitives.I_KECCAK | Michelson_v1_primitives.I_SHA3 |
      Michelson_v1_primitives.I_PAIRING_CHECK | Michelson_v1_primitives.I_TICKET
      | Michelson_v1_primitives.I_READ_TICKET |
      Michelson_v1_primitives.I_SPLIT_TICKET |
      Michelson_v1_primitives.I_JOIN_TICKETS |
      Michelson_v1_primitives.I_OPEN_CHEST) as name) ((cons _ _) as l_value) _,
      _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, name, 0, (List.length l_value)))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_NONE | Michelson_v1_primitives.I_LEFT |
      Michelson_v1_primitives.I_RIGHT | Michelson_v1_primitives.I_NIL |
      Michelson_v1_primitives.I_MAP | Michelson_v1_primitives.I_ITER |
      Michelson_v1_primitives.I_EMPTY_SET | Michelson_v1_primitives.I_LOOP |
      Michelson_v1_primitives.I_LOOP_LEFT | Michelson_v1_primitives.I_CONTRACT |
      Michelson_v1_primitives.I_CAST | Michelson_v1_primitives.I_UNPACK |
      Michelson_v1_primitives.I_CREATE_CONTRACT) as name)
      (([] | cons _ (cons _ _)) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, name, 1, (List.length l_value)))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_PUSH | Michelson_v1_primitives.I_VIEW |
      Michelson_v1_primitives.I_IF_NONE | Michelson_v1_primitives.I_IF_LEFT |
      Michelson_v1_primitives.I_IF_CONS | Michelson_v1_primitives.I_EMPTY_MAP |
      Michelson_v1_primitives.I_EMPTY_BIG_MAP | Michelson_v1_primitives.I_IF) as
        name) (([] | cons _ [] | cons _ (cons _ (cons _ _))) as l_value) _, _
    =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, name, 2, (List.length l_value)))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_LAMBDA
      (([] | cons _ [] | cons _ (cons _ []) |
      cons _ (cons _ (cons _ (cons _ _)))) as l_value) _, _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc_value, Michelson_v1_primitives.I_LAMBDA, 3, (List.length l_value)))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_ADD | Michelson_v1_primitives.I_SUB |
      Michelson_v1_primitives.I_SUB_MUTEZ | Michelson_v1_primitives.I_MUL |
      Michelson_v1_primitives.I_EDIV | Michelson_v1_primitives.I_AND |
      Michelson_v1_primitives.I_OR | Michelson_v1_primitives.I_XOR |
      Michelson_v1_primitives.I_LSL | Michelson_v1_primitives.I_LSR |
      Michelson_v1_primitives.I_CONCAT | Michelson_v1_primitives.I_PAIRING_CHECK)
        as name) [] _, With_family.Item_t ta (With_family.Item_t tb _)
    =>
    let ta := dep_serialize_ty_for_error ta in
    let tb := dep_serialize_ty_for_error tb in
    Error_monad.fail
      (Build_extensible "Undefined_binop"
        (Alpha_context.Script.location * Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim)
        (loc_value, name, ta, tb))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_NEG | Michelson_v1_primitives.I_ABS |
      Michelson_v1_primitives.I_NOT | Michelson_v1_primitives.I_SIZE |
      Michelson_v1_primitives.I_EQ | Michelson_v1_primitives.I_NEQ |
      Michelson_v1_primitives.I_LT | Michelson_v1_primitives.I_GT |
      Michelson_v1_primitives.I_LE | Michelson_v1_primitives.I_GE |
      Michelson_v1_primitives.I_CONCAT) as name) [] _,
      With_family.Item_t t_value _ =>
    let t_value := dep_serialize_ty_for_error t_value in
    Error_monad.fail
      (Build_extensible "Undefined_unop"
        (Alpha_context.Script.location * Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim)
        (loc_value, name, t_value))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_UPDATE | Michelson_v1_primitives.I_SLICE |
      Michelson_v1_primitives.I_OPEN_CHEST) as name) [] _, stack_value =>
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty) (loc_value, name, 3, stack_value))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_CREATE_CONTRACT _ _,
      stack_value =>
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.fail
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty)
        (loc_value, Michelson_v1_primitives.I_CREATE_CONTRACT, 7, stack_value))
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_TRANSFER_TOKENS [] _,
      stack_value =>
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty)
        (loc_value, Michelson_v1_primitives.I_TRANSFER_TOKENS, 4, stack_value))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_DROP | Michelson_v1_primitives.I_DUP |
      Michelson_v1_primitives.I_CAR | Michelson_v1_primitives.I_CDR |
      Michelson_v1_primitives.I_UNPAIR | Michelson_v1_primitives.I_SOME |
      Michelson_v1_primitives.I_BLAKE2B | Michelson_v1_primitives.I_SHA256 |
      Michelson_v1_primitives.I_SHA512 | Michelson_v1_primitives.I_DIP |
      Michelson_v1_primitives.I_IF_NONE | Michelson_v1_primitives.I_LEFT |
      Michelson_v1_primitives.I_RIGHT | Michelson_v1_primitives.I_IF_LEFT |
      Michelson_v1_primitives.I_IF | Michelson_v1_primitives.I_LOOP |
      Michelson_v1_primitives.I_IF_CONS |
      Michelson_v1_primitives.I_IMPLICIT_ACCOUNT | Michelson_v1_primitives.I_NEG
      | Michelson_v1_primitives.I_ABS | Michelson_v1_primitives.I_INT |
      Michelson_v1_primitives.I_NOT | Michelson_v1_primitives.I_HASH_KEY |
      Michelson_v1_primitives.I_EQ | Michelson_v1_primitives.I_NEQ |
      Michelson_v1_primitives.I_LT | Michelson_v1_primitives.I_GT |
      Michelson_v1_primitives.I_LE | Michelson_v1_primitives.I_GE |
      Michelson_v1_primitives.I_SIZE | Michelson_v1_primitives.I_FAILWITH |
      Michelson_v1_primitives.I_RENAME | Michelson_v1_primitives.I_PACK |
      Michelson_v1_primitives.I_ISNAT | Michelson_v1_primitives.I_ADDRESS |
      Michelson_v1_primitives.I_SET_DELEGATE | Michelson_v1_primitives.I_CAST |
      Michelson_v1_primitives.I_MAP | Michelson_v1_primitives.I_ITER |
      Michelson_v1_primitives.I_LOOP_LEFT | Michelson_v1_primitives.I_UNPACK |
      Michelson_v1_primitives.I_CONTRACT | Michelson_v1_primitives.I_NEVER |
      Michelson_v1_primitives.I_KECCAK | Michelson_v1_primitives.I_SHA3 |
      Michelson_v1_primitives.I_READ_TICKET |
      Michelson_v1_primitives.I_JOIN_TICKETS) as name) _ _, stack_value =>
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty) (loc_value, name, 1, stack_value))
  |
    Micheline.Prim loc_value
      ((Michelson_v1_primitives.I_SWAP | Michelson_v1_primitives.I_PAIR |
      Michelson_v1_primitives.I_CONS | Michelson_v1_primitives.I_GET |
      Michelson_v1_primitives.I_MEM | Michelson_v1_primitives.I_EXEC |
      Michelson_v1_primitives.I_CHECK_SIGNATURE | Michelson_v1_primitives.I_ADD
      | Michelson_v1_primitives.I_SUB | Michelson_v1_primitives.I_SUB_MUTEZ |
      Michelson_v1_primitives.I_MUL | Michelson_v1_primitives.I_EDIV |
      Michelson_v1_primitives.I_AND | Michelson_v1_primitives.I_OR |
      Michelson_v1_primitives.I_XOR | Michelson_v1_primitives.I_LSL |
      Michelson_v1_primitives.I_LSR | Michelson_v1_primitives.I_COMPARE |
      Michelson_v1_primitives.I_PAIRING_CHECK | Michelson_v1_primitives.I_TICKET
      | Michelson_v1_primitives.I_SPLIT_TICKET) as name) _ _, stack_value =>
    let stack_value := dep_serialize_stack_for_error ctxt stack_value in
    Error_monad.error_value
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty) (loc_value, name, 2, stack_value))

  | expr, _ =>
    Error_monad.fail
      (unexpected expr [ Script_tc_errors.Seq_kind ]
        Michelson_v1_primitives.Instr_namespace
        [
          Michelson_v1_primitives.I_DROP;
          Michelson_v1_primitives.I_DUP;
          Michelson_v1_primitives.I_DIG;
          Michelson_v1_primitives.I_DUG;
          Michelson_v1_primitives.I_VIEW;
          Michelson_v1_primitives.I_SWAP;
          Michelson_v1_primitives.I_SOME;
          Michelson_v1_primitives.I_UNIT;
          Michelson_v1_primitives.I_PAIR;
          Michelson_v1_primitives.I_UNPAIR;
          Michelson_v1_primitives.I_CAR;
          Michelson_v1_primitives.I_CDR;
          Michelson_v1_primitives.I_CONS;
          Michelson_v1_primitives.I_MEM;
          Michelson_v1_primitives.I_UPDATE;
          Michelson_v1_primitives.I_MAP;
          Michelson_v1_primitives.I_ITER;
          Michelson_v1_primitives.I_GET;
          Michelson_v1_primitives.I_GET_AND_UPDATE;
          Michelson_v1_primitives.I_EXEC;
          Michelson_v1_primitives.I_FAILWITH;
          Michelson_v1_primitives.I_SIZE;
          Michelson_v1_primitives.I_CONCAT;
          Michelson_v1_primitives.I_ADD;
          Michelson_v1_primitives.I_SUB;
          Michelson_v1_primitives.I_SUB_MUTEZ;
          Michelson_v1_primitives.I_MUL;
          Michelson_v1_primitives.I_EDIV;
          Michelson_v1_primitives.I_OR;
          Michelson_v1_primitives.I_AND;
          Michelson_v1_primitives.I_XOR;
          Michelson_v1_primitives.I_NOT;
          Michelson_v1_primitives.I_ABS;
          Michelson_v1_primitives.I_INT;
          Michelson_v1_primitives.I_NEG;
          Michelson_v1_primitives.I_LSL;
          Michelson_v1_primitives.I_LSR;
          Michelson_v1_primitives.I_COMPARE;
          Michelson_v1_primitives.I_EQ;
          Michelson_v1_primitives.I_NEQ;
          Michelson_v1_primitives.I_LT;
          Michelson_v1_primitives.I_GT;
          Michelson_v1_primitives.I_LE;
          Michelson_v1_primitives.I_GE;
          Michelson_v1_primitives.I_TRANSFER_TOKENS;
          Michelson_v1_primitives.I_CREATE_CONTRACT;
          Michelson_v1_primitives.I_NOW;
          Michelson_v1_primitives.I_MIN_BLOCK_TIME;
          Michelson_v1_primitives.I_AMOUNT;
          Michelson_v1_primitives.I_BALANCE;
          Michelson_v1_primitives.I_LEVEL;
          Michelson_v1_primitives.I_IMPLICIT_ACCOUNT;
          Michelson_v1_primitives.I_CHECK_SIGNATURE;
          Michelson_v1_primitives.I_BLAKE2B;
          Michelson_v1_primitives.I_SHA256;
          Michelson_v1_primitives.I_SHA512;
          Michelson_v1_primitives.I_HASH_KEY;
          Michelson_v1_primitives.I_PUSH;
          Michelson_v1_primitives.I_NONE;
          Michelson_v1_primitives.I_LEFT;
          Michelson_v1_primitives.I_RIGHT;
          Michelson_v1_primitives.I_NIL;
          Michelson_v1_primitives.I_EMPTY_SET;
          Michelson_v1_primitives.I_DIP;
          Michelson_v1_primitives.I_LOOP;
          Michelson_v1_primitives.I_IF_NONE;
          Michelson_v1_primitives.I_IF_LEFT;
          Michelson_v1_primitives.I_IF_CONS;
          Michelson_v1_primitives.I_EMPTY_MAP;
          Michelson_v1_primitives.I_EMPTY_BIG_MAP;
          Michelson_v1_primitives.I_IF;
          Michelson_v1_primitives.I_SOURCE;
          Michelson_v1_primitives.I_SENDER;
          Michelson_v1_primitives.I_SELF;
          Michelson_v1_primitives.I_SELF_ADDRESS;
          Michelson_v1_primitives.I_LAMBDA;
          Michelson_v1_primitives.I_NEVER;
          Michelson_v1_primitives.I_VOTING_POWER;
          Michelson_v1_primitives.I_TOTAL_VOTING_POWER;
          Michelson_v1_primitives.I_KECCAK;
          Michelson_v1_primitives.I_SHA3;
          Michelson_v1_primitives.I_PAIRING_CHECK;
          Michelson_v1_primitives.I_SAPLING_EMPTY_STATE;
          Michelson_v1_primitives.I_SAPLING_VERIFY_UPDATE;
          Michelson_v1_primitives.I_TICKET;
          Michelson_v1_primitives.I_READ_TICKET;
          Michelson_v1_primitives.I_SPLIT_TICKET;
          Michelson_v1_primitives.I_JOIN_TICKETS;
          Michelson_v1_primitives.I_OPEN_CHEST
        ])
  end.

(** Simulation of [parse_contract]. *)
Definition dep_parse_contract {err a} (stack_depth : int) :=
  @dep_parse_contract_fuel err a (stack_depth_to_fuel stack_depth).

(** Simulation of [parse_contract_for_script]. *)
Definition dep_parse_contract_for_script {a : Ty.t}
  (ctxt : Alpha_context.context) (loc_value : Alpha_context.Script.location)
  (arg : With_family.ty a) (contract : Alpha_context.Destination.t)
  (entrypoint : Alpha_context.Entrypoint.t)
  : M? (Alpha_context.context * option (With_family.typed_contract a)).
Admitted.

(** Simulation of [code_size]. *)
Definition dep_code_size {a b}
  (ctxt : Alpha_context.context) (code : With_family.lambda a b)
  (views : With_family.view_map)
  : M? (Cache_memory_helpers.sint * Alpha_context.context) :=
  let views_size :=
    Script_map.dep_fold
      (fun function_parameter =>
        let '_ := function_parameter in
        fun v_value  =>
          fun s_value  =>
            Script_typed_ir_size.op_plusplus (view_size v_value) s_value) views
      Script_typed_ir_size.zero in
  let ir_size := Script_typed_ir_size.lambda_size (With_family.to_lambda code) in
  let '(nodes, code_size) := Script_typed_ir_size.op_plusplus views_size ir_size in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt (Script_typed_ir_size_costs.nodes_cost nodes)
    in
  return? (code_size, ctxt).

(** Simulation of [parse_code]. *)
Definition dep_parse_code
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (code : Alpha_context.Script.lazy_expr)
  : M? (dep_ex_code * Alpha_context.context) :=
  (let? '(code, ctxt) :=
    Alpha_context.Script.force_decode_in_context
       Alpha_context.Script.When_needed ctxt code in
  let? '(ctxt, code) := Alpha_context.Global_constants_storage.expand ctxt code
    in
   let?
    '({|
        dep_toplevel.code_field := code_field;
        dep_toplevel.arg_type := arg_type;
        dep_toplevel.storage_type := storage_type;
        dep_toplevel.views := views
      |}, ctxt) := dep_parse_toplevel_aux ctxt legacy code in
  let arg_type_loc := location arg_type in
  let?
    '(Dep_ex_parameter_ty_and_entrypoints
      arg_type entrypoints, ctxt) :=
    Error_monad.record_trace
      (Build_extensible "Ill_formed_type"
        (option string * Alpha_context.Script.expr *
          Alpha_context.Script.location)
        ((Some "parameter"), code, arg_type_loc))
      (dep_parse_parameter_ty_and_entrypoints_aux ctxt 0 legacy arg_type) in
  let storage_type_loc := location storage_type in
  let? '(Dep_ex_ty storage_type, ctxt) :=
    Error_monad.record_trace
      (Build_extensible "Ill_formed_type"
        (option string * Alpha_context.Script.expr *
          Alpha_context.Script.location)
        ((Some "storage"), code, storage_type_loc))
      (dep_parse_storage_ty ctxt 0 legacy storage_type) in
  let? 'Dep_ty_ex_c arg_type_full :=
    Script_typed_ir.dep_pair_t
      storage_type_loc arg_type storage_type in
  let? 'Dep_ty_ex_c ret_type_full :=
    Script_typed_ir.dep_pair_t
      storage_type_loc dep_list_operation_t storage_type in
  let? '(code, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, nil))
      (dep_parse_returning type_logger_value 0
        (Script_tc_context.dep_toplevel_value
          storage_type arg_type entrypoints)
        ctxt
        legacy arg_type_full ret_type_full code_field) in
  let? '(code_size, ctxt) := dep_code_size ctxt code views in
  return? (
    (Dep_ex_code {|
      dep_code.code := code;
      dep_code.arg_type := arg_type;
      dep_code.storage_type := storage_type;
      dep_code.views := With_family.to_view_map views;
      dep_code.entrypoints := entrypoints;
      dep_code.code_size := code_size
    |}), ctxt)).

(** Simulation of [parse_storage]. *)
Definition dep_parse_storage {storage}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (allow_forged : bool) (storage_type : With_family.ty storage)
  (storage_value : Alpha_context.Script.lazy_expr)
  : M? (With_family.ty_to_dep_Set storage * Alpha_context.context) :=
  let? '(storage_value, ctxt) :=
    Alpha_context.Script.force_decode_in_context
      Alpha_context.Script.When_needed ctxt storage_value in
  Error_monad.trace_eval
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let storage_type := dep_serialize_ty_for_error storage_type in
      Build_extensible "Ill_typed_data"
        (option string * Alpha_context.Script.expr *
          Micheline.canonical Alpha_context.Script.prim)
        (None, storage_value, storage_type))
    (dep_parse_data_aux type_logger_value 0 ctxt legacy allow_forged storage_type
      (Micheline.root_value storage_value)).

(** Simulation of [parse_script]. *)
Definition dep_parse_script
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (allow_forged_in_storage : bool)
  (function_parameter : Alpha_context.Script.t)
  : M? (dep_ex_script * Alpha_context.context) :=
  let '{|
    Alpha_context.Script.t.code := code;
    Alpha_context.Script.t.storage := storage_value
    |} := function_parameter in
  let?
    '(Dep_ex_code {|
      dep_code.code := code;
      dep_code.arg_type := arg_type;
      dep_code.storage_type := storage_type;
      dep_code.views := views;
      dep_code.entrypoints := entrypoints;
      dep_code.code_size := code_size
    |},
    ctxt) := dep_parse_code type_logger_value ctxt legacy code in
  let? '(storage_value, ctxt) :=
    dep_parse_storage type_logger_value ctxt legacy allow_forged_in_storage
      storage_type storage_value in
  return?
    (Dep_ex_script {|
        With_family.script.code := code;
        With_family.script.arg_type := arg_type;
        With_family.script.storage := storage_value;
        With_family.script.storage_type := storage_type;
        With_family.script.views := views;
        With_family.script.entrypoints := entrypoints;
        With_family.script.code_size := code_size
      |},
    ctxt).

(** Dependent version
    of [typechecked_code_internal.Typechecked_code_internal]. *)
Module dep_typechecked_code_internal.
  Module Dep_typechecked_code_internal.
    Record record {arg storage : Ty.t} : Set := {
      toplevel : toplevel;
      arg_type : With_family.ty arg;
      storage_type : With_family.ty storage;
      entrypoints : Script_typed_ir.entrypoints;
      typed_views : dep_typed_view_map storage;
      type_map : Script_tc_errors.type_map;
    }.
    Arguments record : clear implicits.
  End Dep_typechecked_code_internal.
  Definition Dep_typechecked_code_internal :=
    Dep_typechecked_code_internal.record.

  (** Conversion back to [Typechecked_code_internal]. *)
  Definition to_Typechecked_code_internal {arg storage}
    (v : Dep_typechecked_code_internal arg storage) :
    typechecked_code_internal.Typechecked_code_internal :=
    {|
      typechecked_code_internal.Typechecked_code_internal.toplevel :=
        v.(Dep_typechecked_code_internal.toplevel);
      typechecked_code_internal.Typechecked_code_internal.arg_type :=
        With_family.to_ty v.(Dep_typechecked_code_internal.arg_type);
      typechecked_code_internal.Typechecked_code_internal.storage_type :=
        With_family.to_ty v.(Dep_typechecked_code_internal.storage_type);
      typechecked_code_internal.Typechecked_code_internal.entrypoints :=
        v.(Dep_typechecked_code_internal.entrypoints);
      typechecked_code_internal.Typechecked_code_internal.typed_views :=
        to_typed_view_map v.(Dep_typechecked_code_internal.typed_views);
      typechecked_code_internal.Typechecked_code_internal.type_map :=
        v.(Dep_typechecked_code_internal.type_map);
    |}.
End dep_typechecked_code_internal.

(** Dependent version of [typechecked_code_internal]. *)
Inductive dep_typechecked_code_internal : Set :=
| Dep_typechecked_code_internal {arg storage} :
  dep_typechecked_code_internal.Dep_typechecked_code_internal arg storage ->
  dep_typechecked_code_internal.

(** Conversion back to [typechecked_code_internal]. *)
Definition to_typechecked_code_internal (v : dep_typechecked_code_internal) :
  typechecked_code_internal :=
  match v with
  | Dep_typechecked_code_internal v =>
    Typechecked_code_internal
      (dep_typechecked_code_internal.to_Typechecked_code_internal v)
  end.

(** Simulation of [typecheck_code_aux]. *)
Definition dep_typecheck_code_aux
  (legacy : bool) (show_types : bool) (ctxt : Alpha_context.context)
  (code : Alpha_context.Script.expr)
  : M? (dep_typechecked_code_internal * Alpha_context.context) :=
  let? '(ctxt, code) :=
    Alpha_context.Global_constants_storage.expand ctxt code in
  let? '(toplevel_value, ctxt) := dep_parse_toplevel_aux ctxt legacy code in
  let '{|
    dep_toplevel.code_field := code_field;
    dep_toplevel.arg_type := arg_type;
    dep_toplevel.storage_type := storage_type;
    dep_toplevel.views := views
    |} := toplevel_value in
  let type_map := Pervasives.ref_value nil in
  let arg_type_loc := location arg_type in
  let? '(Dep_ex_parameter_ty_and_entrypoints arg_type entrypoints, ctxt) :=
    Error_monad.record_trace
      (Build_extensible "Ill_formed_type"
        (option string * Alpha_context.Script.expr *
          Alpha_context.Script.location)
        ((Some "parameter"), code, arg_type_loc))
      (dep_parse_parameter_ty_and_entrypoints_aux ctxt 0 legacy arg_type) in
  let storage_type_loc := location storage_type in
  let? '(ex_storage_type, ctxt) :=
    Error_monad.record_trace
      (Build_extensible "Ill_formed_type"
        (option string * Alpha_context.Script.expr *
          Alpha_context.Script.location)
        ((Some "storage"), code, storage_type_loc))
      (dep_parse_storage_ty ctxt 0 legacy storage_type) in
  let 'Dep_ex_ty storage_type := ex_storage_type in
  let? 'Dep_ty_ex_c arg_type_full :=
    Script_typed_ir.dep_pair_t storage_type_loc arg_type storage_type in
  let? 'Dep_ty_ex_c ret_type_full :=
    Script_typed_ir.dep_pair_t storage_type_loc
      Script_typed_ir.dep_list_operation_t storage_type in
  let type_logger_value
    (loc_value : Alpha_context.Script.location)
    (stack_ty_before : Script_tc_errors.unparsed_stack_ty)
    (stack_ty_after : Script_tc_errors.unparsed_stack_ty) : unit :=
    Pervasives.op_coloneq type_map
      (cons (loc_value, (stack_ty_before, stack_ty_after))
        (Pervasives.op_exclamation type_map)) in
  let type_logger_value :=
    if show_types then
      Some type_logger_value
    else
      None in
  let result_value :=
    dep_parse_returning type_logger_value 0
      (Script_tc_context.dep_toplevel_value
          storage_type arg_type entrypoints) ctxt legacy
      arg_type_full ret_type_full code_field in
  let? '(_, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, (Pervasives.op_exclamation type_map))) result_value in
  let views_result :=
    dep_parse_views
      type_logger_value ctxt legacy storage_type views in
  let? '(typed_views, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, (Pervasives.op_exclamation type_map))) views_result in
  return?
    (Dep_typechecked_code_internal {|
      dep_typechecked_code_internal.Dep_typechecked_code_internal.toplevel :=
         (to_toplevel toplevel_value);
      dep_typechecked_code_internal.Dep_typechecked_code_internal.arg_type :=
        arg_type;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.storage_type :=
        storage_type;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.entrypoints :=
        entrypoints;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.typed_views :=
        typed_views;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.type_map :=
        Pervasives.op_exclamation type_map |}, ctxt).

(** Simulation of [list_entrypoints_uncarbonated]. *)
Definition dep_list_entrypoints_uncarbonated {f}
  (full : With_family.ty f) (entrypoints : Script_typed_ir.entrypoints) :
  list (list Alpha_context.Script.prim) *
  Entrypoint_repr.Map.(Map.S.t) (dep_ex_ty * Alpha_context.Script.node).
Admitted.

(** Auxiliary simulation for [unparse_data_aux]. *)
Fixpoint dep_unparse_data_aux_fuel {t}
  (fuel : nat) (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (ty : With_family.ty t) (a_value : With_family.ty_to_dep_Set t) {struct fuel}
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Unparse_costs.unparse_data_cycle in
  let non_terminal_recursion {u}
    (ctxt : Alpha_context.context) (mode : unparsing_mode)
    (ty : With_family.ty u) (a_value : With_family.ty_to_dep_Set u) :
    M? (Alpha_context.Script.node * Alpha_context.context) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel => dep_unparse_data_aux_fuel fuel ctxt mode ty a_value
    end in
  let loc_value := Micheline.dummy_location in
  match fuel with
  | Datatypes.O =>
    Error_monad.fail
      (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
  | Datatypes.S fuel =>
    match ty, a_value with
    | With_family.Unit_t, v_value =>
        unparse_unit loc_value ctxt v_value
    | With_family.Int_t, v_value =>
        unparse_int loc_value ctxt v_value
    | With_family.Nat_t, v_value =>
      unparse_nat loc_value ctxt v_value
    | With_family.String_t, s_value =>
      unparse_string loc_value ctxt s_value
    | With_family.Bytes_t, s_value =>
      unparse_bytes loc_value ctxt s_value
    | With_family.Bool_t, b_value =>
      unparse_bool loc_value ctxt b_value
    | With_family.Timestamp_t, t_value =>
      unparse_timestamp loc_value ctxt mode t_value
    | With_family.Address_t, address =>
      unparse_address loc_value ctxt mode address
    | With_family.Tx_rollup_l2_address_t, address =>
      unparse_tx_rollup_l2_address loc_value ctxt mode address
    | With_family.Contract_t _ _, contract =>
      dep_unparse_contract loc_value ctxt mode contract
    | With_family.Signature_t, s_value =>
      unparse_signature loc_value ctxt mode s_value
    | With_family.Mutez_t, v_value =>
      unparse_mutez loc_value ctxt v_value
    | With_family.Key_t, k_value =>
      unparse_key loc_value ctxt mode k_value
    | With_family.Key_hash_t, k_value =>
      unparse_key_hash loc_value ctxt mode k_value
    | With_family.Operation_t, operation =>
      unparse_operation loc_value ctxt operation
    | With_family.Chain_id_t, chain_id =>
      unparse_chain_id loc_value ctxt mode chain_id
    | With_family.Bls12_381_g1_t, x_value =>
      unparse_bls12_381_g1 loc_value ctxt x_value
    | With_family.Bls12_381_g2_t, x_value =>
      unparse_bls12_381_g2 loc_value ctxt x_value
    | With_family.Bls12_381_fr_t, x_value =>
      unparse_bls12_381_fr loc_value ctxt x_value
    | With_family.Pair_t tl tr _ _, pair_value =>
      let r_witness := dep_comb_witness2 tr in
      let unparse_l (ctxt : Alpha_context.context) v_value
        : M? (Alpha_context.Script.node * Alpha_context.context) :=
        non_terminal_recursion ctxt mode tl v_value in
      let unparse_r (ctxt : Alpha_context.context) v_value
        : M? (Alpha_context.Script.node * Alpha_context.context) :=
        non_terminal_recursion ctxt mode tr v_value in
      unparse_pair loc_value unparse_l unparse_r ctxt mode r_witness pair_value
    | With_family.Union_t tl tr _ _, v_value =>
      let unparse_l (ctxt : Alpha_context.context) v_value
        : M? (Alpha_context.Script.node * Alpha_context.context) :=
        non_terminal_recursion ctxt mode tl v_value in
      let unparse_r (ctxt : Alpha_context.context) v_value
        : M? (Alpha_context.Script.node * Alpha_context.context) :=
        non_terminal_recursion ctxt mode tr v_value in
      unparse_union loc_value unparse_l unparse_r ctxt v_value
    | With_family.Option_t t_value _ _, v_value =>
      let unparse_v (ctxt : Alpha_context.context) v_value
        : M? (Alpha_context.Script.node * Alpha_context.context) :=
        non_terminal_recursion ctxt mode t_value v_value in
      unparse_option loc_value unparse_v ctxt v_value
    | With_family.List_t t_value _, items =>
      let? '(items, ctxt) :=
        List.fold_left_es
          (fun (function_parameter :
            list Alpha_context.Script.node * Alpha_context.context) =>
            let '(l_value, ctxt) := function_parameter in
            fun element =>
              let? '(unparsed, ctxt) :=
                non_terminal_recursion ctxt mode t_value element in
              return? ((cons unparsed l_value), ctxt)) (nil, ctxt)
          items.(Script_typed_ir.boxed_list.elements) in
      return? ((Micheline.Seq loc_value (List.rev items)), ctxt)
    | With_family.Ticket_t t_value _, x_value =>
      let '{|
        Script_typed_ir.ticket.ticketer := ticketer;
          Script_typed_ir.ticket.contents := contents;
          Script_typed_ir.ticket.amount := amount
          |} := x_value in
      let? opened_ticket_ty := dep_opened_ticket_type loc_value t_value in
      let t_value := dep_ty_of_comparable_ty opened_ticket_ty in
      let destination := Alpha_context.Destination.Contract ticketer in
      let addr :=
        {| Script_typed_ir.address.destination := destination;
          Script_typed_ir.address.entrypoint := Alpha_context.Entrypoint.default
          |} in
      dep_unparse_data_aux_fuel fuel ctxt mode t_value (addr, (contents, amount))
    | With_family.Set_t t_value _, set =>
      let? '(items, ctxt) :=
        List.fold_left_es
          (fun (function_parameter :
            list
              (Micheline.node Micheline.canonical_location
                Alpha_context.Script.prim) * Alpha_context.context) =>
            let '(l_value, ctxt) := function_parameter in
            fun item =>
              let? '(item, ctxt) :=
                dep_unparse_comparable_data loc_value ctxt mode t_value item in
              return? ((cons item l_value), ctxt)) (nil, ctxt)
          (Script_set.dep_fold
            (fun e_value acc_value => cons e_value acc_value)
            set nil) in
      return? ((Micheline.Seq loc_value items), ctxt)
    | With_family.Map_t kt vt _, map =>
      let items :=
        Script_map.dep_fold
          (fun k_value =>
            fun v_value =>
              fun acc_value =>
                cons (k_value, v_value) acc_value) map nil in
      let? '(items, ctxt) :=
        dep_unparse_items_fuel ctxt fuel mode kt vt items in
      return? ((Micheline.Seq loc_value items), ctxt)
    | With_family.Big_map_t kt vt _, x_value =>
      let '{|
        With_family.big_map.id := id;
        With_family.big_map.diff := {|
          Script_typed_ir.big_map_overlay.map := map;
          Script_typed_ir.big_map_overlay.size := size_value
        |}
      |} := x_value in
      match id with
      | Some id =>
        if size_value =i 0 then
          return?
            ((Micheline.Int loc_value (Alpha_context.Big_map.Id.unparse_to_z id)),
              ctxt)
        else
          let items :=
            Script_typed_ir.Big_map_overlay.(Map.S.fold)
              (fun (function_parameter : Script_expr_hash.t) =>
                let '_ := function_parameter in
                fun (function_parameter : _ * option _) =>
                  let '(k_value, v_value) := function_parameter in
                  fun (acc_value : list (_ * option _)) =>
                    cons (k_value, v_value) acc_value) map nil in
          let items :=
            List.sort
              (fun '(a_value, _) '(b_value, _) =>
                Script_comparable.dep_compare_comparable _
                  (With_family.to_value b_value) (With_family.to_value a_value)
              )
              items in
          let? vt := Script_typed_ir.dep_option_t loc_value vt in
          let? '(items, ctxt) :=
            dep_unparse_items_fuel ctxt fuel mode kt vt items in
          return?
            ((Micheline.Prim loc_value Michelson_v1_primitives.D_Pair
              [
                Micheline.Int loc_value (Alpha_context.Big_map.Id.unparse_to_z id);
                Micheline.Seq loc_value items
              ] nil), ctxt)
      | None =>
        let items :=
          Script_typed_ir.Big_map_overlay.(Map.S.fold)
            (fun (function_parameter : Script_expr_hash.t) =>
              let '_ := function_parameter in
              fun (function_parameter : _ * option _) =>
                let '(k_value, v_value) := function_parameter in
                fun (acc_value : list (_ * _)) =>
                  match v_value with
                  | None => acc_value
                  | Some v_value => cons (k_value, v_value) acc_value
                  end) map nil in
        let items :=
          List.sort
            (fun '(a_value, _) '(b_value, _) =>
              Script_comparable.dep_compare_comparable _
                (With_family.to_value b_value) (With_family.to_value a_value)
            )
            items in
        let? '(items, ctxt) :=
          dep_unparse_items_fuel ctxt fuel mode kt vt items in
        return? ((Micheline.Seq loc_value items), ctxt)
      end
    | With_family.Lambda_t _ _ _, x_value =>
      let '(_, original_code) := x_value in
      dep_unparse_code_aux_fuel ctxt fuel mode original_code
    | With_family.Sapling_transaction_t _, s_value =>
      let s_value := cast Alpha_context.Sapling.transaction s_value in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt (Unparse_costs.sapling_transaction s_value)
        in
      let bytes_value :=
        Data_encoding.Binary.to_bytes_exn None
          Alpha_context.Sapling.transaction_encoding s_value in
      return? ((Micheline.Bytes loc_value bytes_value), ctxt)
    | With_family.Sapling_transaction_deprecated_t _, s_value =>
      let s_value := cast Sapling_repr.legacy_transaction s_value in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt
          (Unparse_costs.sapling_transaction_deprecated s_value) in
      let bytes_value :=
        Data_encoding.Binary.to_bytes_exn None
          Alpha_context.Sapling.Legacy.transaction_encoding s_value in
      return? ((Micheline.Bytes loc_value bytes_value), ctxt)
    | With_family.Sapling_state_t _, x_value =>
      let x_value := cast Alpha_context.Sapling.state x_value in
      let '{|
        Alpha_context.Sapling.state.id := id;
          Alpha_context.Sapling.state.diff := diff_value
          |} := x_value in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt (Unparse_costs.sapling_diff diff_value) in
      return?
        (match diff_value with
        | {|
          Alpha_context.Sapling.diff.commitments_and_ciphertexts := [];
            Alpha_context.Sapling.diff.nullifiers := []
            |} =>
          match id with
          | None => Micheline.Seq loc_value nil
          | Some id =>
            let id := Alpha_context.Sapling.Id.unparse_to_z id in
            Micheline.Int loc_value id
          end
        | diff_value =>
          let diff_bytes :=
            Data_encoding.Binary.to_bytes_exn None
              Alpha_context.Sapling.diff_encoding diff_value in
          let unparsed_diff {B : Set}
            : Micheline.node Micheline.canonical_location B :=
            Micheline.Bytes loc_value diff_bytes in
          match id with
          | None => unparsed_diff
          | Some id =>
            let id := Alpha_context.Sapling.Id.unparse_to_z id in
            Micheline.Prim loc_value Michelson_v1_primitives.D_Pair
              [ Micheline.Int loc_value id; unparsed_diff ] nil
          end
        end, ctxt)
    | With_family.Chest_key_t, s_value =>
      unparse_with_data_encoding loc_value ctxt s_value
        Unparse_costs.chest_key_value
        Script_typed_ir.Script_timelock.chest_key_encoding
    | With_family.Chest_t, s_value =>
      unparse_with_data_encoding loc_value ctxt s_value
        (Unparse_costs.chest_value
          (Script_typed_ir.Script_timelock.get_plaintext_size s_value))
        Script_typed_ir.Script_timelock.chest_encoding
    | _, _ => unreachable_gadt_branch
    end
  end

(** Auxiliary simulation for [unparse_code_aux]. *)
with dep_unparse_code_aux_fuel
  (ctxt : Alpha_context.context) (fuel : nat) (mode : unparsing_mode)
  (code : Alpha_context.Script.node) {struct fuel} :
  M? (Alpha_context.Script.node * Alpha_context.context) :=
  let legacy := true in
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle
    in
  let non_terminal_recursion
    (ctxt : Alpha_context.context) (mode : unparsing_mode)
    (code : Alpha_context.Script.node)
    : M? (Alpha_context.Script.node * Alpha_context.context) :=
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel => dep_unparse_code_aux_fuel ctxt fuel mode code
    end in
  match code with
  |
    Micheline.Prim loc_value Michelson_v1_primitives.I_PUSH
      (cons ty (cons data [])) annot =>
    let? '(Dep_ex_ty t_value, ctxt) :=
      dep_parse_packable_ty_aux ctxt (fuel_to_stack_depth fuel) legacy ty in
    let allow_forged := false in
    match fuel with
    | Datatypes.O =>
      Error_monad.fail
        (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
    | Datatypes.S fuel =>
      let? '(data, ctxt) :=
        dep_parse_data_aux None (fuel_to_stack_depth fuel) ctxt legacy
          allow_forged t_value data in
      let? '(data, ctxt) :=
        dep_unparse_data_aux_fuel fuel ctxt mode t_value data in
      return?
        ((Micheline.Prim loc_value Michelson_v1_primitives.I_PUSH [ ty; data ]
          annot), ctxt)
    end
  | Micheline.Seq loc_value items =>
    let? '(items, ctxt) :=
      List.fold_left_es
        (fun (function_parameter :
          list Alpha_context.Script.node * Alpha_context.context) =>
          let '(l_value, ctxt) := function_parameter in
          fun (item : Alpha_context.Script.node) =>
            let? '(item, ctxt) := non_terminal_recursion ctxt mode item in
            return? ((cons item l_value), ctxt)) (nil, ctxt) items in
    return? ((Micheline.Seq loc_value (List.rev items)), ctxt)
  | Micheline.Prim loc_value prim items annot =>
    let? '(items, ctxt) :=
      List.fold_left_es
        (fun (function_parameter :
          list Alpha_context.Script.node * Alpha_context.context) =>
          let '(l_value, ctxt) := function_parameter in
          fun (item : Alpha_context.Script.node) =>
            let? '(item, ctxt) := non_terminal_recursion ctxt mode item in
            return? ((cons item l_value), ctxt)) (nil, ctxt) items in
    return? ((Micheline.Prim loc_value prim (List.rev items) annot), ctxt)
  | (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _) as atom =>
    return? (atom, ctxt)
  end

(** Auxiliary simulation for [unparse_items]. *)
with dep_unparse_items_fuel {k v}
  (ctxt : Alpha_context.context) (fuel : nat) (mode : unparsing_mode)
  (kt : With_family.ty k) (vt : With_family.ty v)
  (items : list (With_family.ty_to_dep_Set k * With_family.ty_to_dep_Set v))
  {struct fuel} :
  M? (list Alpha_context.Script.node * Alpha_context.context) :=
  List.fold_left_es
    (fun (function_parameter :
      list Alpha_context.Script.node * Alpha_context.context) =>
      let '(l_value, ctxt) := function_parameter in
      fun (function_parameter :
        With_family.ty_to_dep_Set k * With_family.ty_to_dep_Set v) =>
        let '(k_value, v_value) := function_parameter in
        let loc_value := Micheline.dummy_location in
        let? '(key_value, ctxt) :=
          dep_unparse_comparable_data loc_value ctxt mode kt k_value in
        let? '(value_value, ctxt) :=
          match fuel with
          | Datatypes.O =>
            Error_monad.fail
              (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
          | Datatypes.S fuel =>
            dep_unparse_data_aux_fuel fuel ctxt mode vt v_value
          end in
        return?
          ((cons
            (Micheline.Prim loc_value Michelson_v1_primitives.D_Elt
              [ key_value; value_value ] nil) l_value), ctxt)) (nil, ctxt)
    items.

(** Simulation of [unparse_data_aux]. *)
Definition dep_unparse_data_aux {a}
  (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
  (ty : With_family.ty a) (a_value : With_family.ty_to_dep_Set a) :
  M? (Alpha_context.Script.node * Alpha_context.context) :=
  dep_unparse_data_aux_fuel (stack_depth_to_fuel stack_depth) ctxt mode ty
    a_value.

(** Simulation of [unparse_code_aux]. *)
Definition dep_unparse_code_aux
  (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
  (code : Alpha_context.Script.node) :
  M? (Alpha_context.Script.node * Alpha_context.context) :=
  dep_unparse_code_aux_fuel ctxt (stack_depth_to_fuel stack_depth) mode code.

(** Simulation of [unparse_items]. *)
Definition dep_unparse_items {k v}
  (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
  (kt : With_family.ty k) (vt : With_family.ty v)
  (items : list (With_family.ty_to_dep_Set k * With_family.ty_to_dep_Set v)) :
  M? (list Alpha_context.Script.node * Alpha_context.context) :=
  dep_unparse_items_fuel ctxt (stack_depth_to_fuel stack_depth) mode kt vt
    items.

(** Simulation of [parse_and_unparse_script_unaccounted]. *)
Definition dep_parse_and_unparse_script_unaccounted
  (ctxt : Alpha_context.context) (legacy : bool)
  (allow_forged_in_storage : bool) (mode : unparsing_mode)
  (normalize_types : bool) (function_parameter : Alpha_context.Script.t)
  : M? (Alpha_context.Script.t * Alpha_context.context) :=
  let '{|
    Alpha_context.Script.t.code := code;
      Alpha_context.Script.t.storage := storage_value
      |} := function_parameter in
  let? '(code, ctxt) :=
    Alpha_context.Script.force_decode_in_context
      Alpha_context.Script.When_needed ctxt code in
  let?
    '(Dep_typechecked_code_internal {|
      dep_typechecked_code_internal.Dep_typechecked_code_internal.toplevel := {|
        toplevel.code_field := code_field;
        toplevel.arg_type := original_arg_type_expr;
        toplevel.storage_type := original_storage_type_expr;
        toplevel.views := views
      |};
      dep_typechecked_code_internal.Dep_typechecked_code_internal.arg_type := arg_type;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.storage_type :=
        storage_type;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.entrypoints :=
        entrypoints;
      dep_typechecked_code_internal.Dep_typechecked_code_internal.type_map := _
    |}, ctxt) := dep_typecheck_code_aux legacy false ctxt code in
  let? '(storage_value, ctxt) :=
    dep_parse_storage None ctxt legacy allow_forged_in_storage
      storage_type storage_value in
  let? '(code, ctxt) := unparse_code_aux ctxt 0 mode code_field in
  let? '(storage_value, ctxt) :=
    dep_unparse_data_aux ctxt 0 mode storage_type storage_value in
  let loc_value := Micheline.dummy_location in
  let? '(arg_type, storage_type, ctxt) :=
    if normalize_types then
      let? '(arg_type, ctxt) :=
        dep_unparse_parameter_ty loc_value ctxt arg_type entrypoints in
      let? '(storage_type, ctxt) :=
        dep_unparse_ty loc_value ctxt storage_type in
      return? (arg_type, storage_type, ctxt)
    else
      return? (original_arg_type_expr, original_storage_type_expr, ctxt) in
  return?
    (let unparse_view_unaccounted
      (name : Script_string.t)
      (function_parameter : Script_typed_ir.view)
      : list
        (Micheline.node Micheline.canonical_location Alpha_context.Script.prim)
      ->
      list
        (Micheline.node Micheline.canonical_location Alpha_context.Script.prim) :=
      let '{|
        Script_typed_ir.view.input_ty := input_ty;
          Script_typed_ir.view.output_ty := output_ty;
          Script_typed_ir.view.view_code := view_code
          |} := function_parameter in
      fun (views :
        list
          (Micheline.node Micheline.canonical_location Alpha_context.Script.prim))
        =>
        cons
          (Micheline.Prim loc_value Michelson_v1_primitives.K_view
            [
              Micheline.String loc_value
                (Script_string.to_string name);
              input_ty;
              output_ty;
              view_code
            ] nil) views in
    let views := List.rev (Script_map.fold unparse_view_unaccounted views nil)
      in
    let code :=
      Micheline.Seq loc_value
        (Pervasives.op_at
          [
            Micheline.Prim loc_value Michelson_v1_primitives.K_parameter
              [ arg_type ] nil;
            Micheline.Prim loc_value Michelson_v1_primitives.K_storage
              [ storage_type ] nil;
            Micheline.Prim loc_value Michelson_v1_primitives.K_code [ code ] nil
          ] views) in
    ({|
      Alpha_context.Script.t.code :=
        Alpha_context.Script.lazy_expr_value (Micheline.strip_locations code);
      Alpha_context.Script.t.storage :=
        Alpha_context.Script.lazy_expr_value
          (Micheline.strip_locations storage_value) |}, ctxt)).

(** Simulation of [pack_data_with_mode]. *)
Definition dep_pack_data_with_mode {a}
  (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (data : With_family.ty_to_dep_Set a) (mode : unparsing_mode) :
  M? (bytes * Alpha_context.context) :=
  let? '(unparsed, ctxt) := dep_unparse_data_aux ctxt 0 mode ty data in
  pack_node unparsed ctxt.

(** Simulation of [hash_data]. *)
Definition dep_hash_data {a}
  (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (data : With_family.ty_to_dep_Set a) :
  M? (Script_expr_hash.t * Alpha_context.context) :=
  let? '(bytes_value, ctxt) :=
    dep_pack_data_with_mode ctxt ty data Optimized_legacy in
  hash_bytes ctxt bytes_value.

(** Simulation of [pack_data]. *)
Definition dep_pack_data {a}
  (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (data : With_family.ty_to_dep_Set a) :
  M? (bytes * Alpha_context.context) :=
  dep_pack_data_with_mode ctxt ty data Optimized_legacy.

(** ** Big maps *)

(** Simulation of [diff_of_big_map]. *)
Definition dep_diff_of_big_map {a b}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (big_map : With_family.big_map a b)
  : M?
    (Alpha_context.Lazy_storage.diff Alpha_context.Big_map.Id.t
      Alpha_context.Big_map.alloc (list Alpha_context.Big_map.update) *
      Alpha_context.Big_map.Id.t * Alpha_context.context) :=
  let
    '{|
      With_family.big_map.id := id;
        With_family.big_map.diff := diff_value;
        With_family.big_map.key_type := key_type;
        With_family.big_map.value_type := value_type
        |} := big_map in
  let? '(ctxt, init_value, id) :=
    match id with
    | Some id =>
      if
        Alpha_context.Lazy_storage.IdSet.mem
          Alpha_context.Lazy_storage.Kind.Big_map id ids_to_copy
      then
        let? '(ctxt, duplicate) := Alpha_context.Big_map.fresh temporary ctxt in
        return?
          (ctxt,
            (Alpha_context.Lazy_storage.Copy
              {| Alpha_context.Lazy_storage.init.Copy.src := id |}), duplicate)
      else
        return? (ctxt, Alpha_context.Lazy_storage.Existing, id)
    | None =>
      let? '(ctxt, id) := Alpha_context.Big_map.fresh temporary ctxt in
      let kt := dep_unparse_comparable_ty_uncarbonated tt key_type in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt
          (Alpha_context.Script.strip_locations_cost kt) in
      let? '(kv, ctxt) := dep_unparse_ty tt ctxt value_type in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt
          (Alpha_context.Script.strip_locations_cost kv) in
      let key_type := Micheline.strip_locations kt in
      let value_type := Micheline.strip_locations kv in
      return?
        (ctxt,
          (Alpha_context.Lazy_storage.Alloc
            {| Lazy_storage_kind.Big_map.alloc.key_type := key_type;
              Lazy_storage_kind.Big_map.alloc.value_type := value_type |}), id)
    end in
  let pairs : list (Script_expr_hash.t * With_family.ty_to_dep_Set a *
    option (With_family.ty_to_dep_Set b)) :=
    Script_typed_ir.Big_map_overlay.(Map.S.fold)
      (fun (key_hash : Script_expr_hash.t) =>
        fun (function_parameter : With_family.ty_to_dep_Set a *
          option (With_family.ty_to_dep_Set b)) =>
          let '(key_value, value_value) := function_parameter in
          fun (acc_value : list (Script_expr_hash.t * With_family.ty_to_dep_Set a
            * option (With_family.ty_to_dep_Set b))) =>
            cons (key_hash, key_value, value_value) acc_value)
      diff_value.(Script_typed_ir.big_map_overlay.map) nil in
  let? '(updates, ctxt) :=
    List.fold_left_es
      (fun (function_parameter :
        list Alpha_context.Big_map.update * Alpha_context.context) =>
        let '(acc_value, ctxt) := function_parameter in
        fun (function_parameter : Script_expr_hash.t * With_family.ty_to_dep_Set a
          * option (With_family.ty_to_dep_Set b)) =>
          let '(key_hash, key_value, value_value) := function_parameter in
          let? ctxt :=
            Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
          let? '(key_node, ctxt) :=
            dep_unparse_comparable_data tt ctxt mode key_type key_value in
          let? ctxt :=
            Alpha_context.Gas.consume ctxt
              (Alpha_context.Script.strip_locations_cost key_node) in
          let key_value := Micheline.strip_locations key_node in
          let? '(value_value, ctxt) :=
            match value_value with
            | None => return? (None, ctxt)
            | Some x_value =>
              let? '(node_value, ctxt) :=
                dep_unparse_data_aux ctxt 0 mode value_type x_value in
              let? ctxt :=
                Alpha_context.Gas.consume ctxt
                  (Alpha_context.Script.strip_locations_cost node_value) in
              return? ((Some (Micheline.strip_locations node_value)), ctxt)
            end in
          let diff_item :=
            {| Lazy_storage_kind.Big_map.update.key := key_value;
              Lazy_storage_kind.Big_map.update.key_hash := key_hash;
              Lazy_storage_kind.Big_map.update.value := value_value |} in
          return? ((cons diff_item acc_value), ctxt)) (nil, ctxt)
      (List.rev pairs) in
  return?
    ((Alpha_context.Lazy_storage.Update
      {| Alpha_context.Lazy_storage.diff.Update.init := init_value;
        Alpha_context.Lazy_storage.diff.Update.updates := updates |}), id, ctxt).

Module Dep.
  Inductive dep_has_lazy_storage : Ty.t -> Set :=
  | Big_map_f : forall tk tv, dep_has_lazy_storage (Ty.Big_map tk tv)
  | Sapling_state_f : dep_has_lazy_storage Ty.Sapling_state
  | False_f t : dep_has_lazy_storage t
  | Pair_f tl tr : dep_has_lazy_storage tl -> dep_has_lazy_storage tr ->
                   dep_has_lazy_storage (Ty.Pair tl tr)
  | Union_f tl tr : dep_has_lazy_storage tl -> dep_has_lazy_storage tr ->
                    dep_has_lazy_storage (Ty.Union tl tr)
  | Option_f t : dep_has_lazy_storage t -> dep_has_lazy_storage (Ty.Option t)
  | List_f t : dep_has_lazy_storage t -> dep_has_lazy_storage (Ty.List t)
  | Map_f k v  : dep_has_lazy_storage v -> dep_has_lazy_storage (Ty.Map k v).
End Dep.

Fixpoint to_has_lazy_storage {t} (dhls : Dep.dep_has_lazy_storage t) :
  has_lazy_storage :=
  match dhls with
  | Dep.Big_map_f _ _ => Big_map_f
  | Dep.Sapling_state_f => Sapling_state_f
  | Dep.False_f _ => False_f
  | Dep.Pair_f _ _ dhls1 dhls2 =>
      Pair_f (to_has_lazy_storage dhls1) (to_has_lazy_storage dhls2)
  | Dep.Union_f _ _ dhls1 dhls2 =>
      Union_f (to_has_lazy_storage dhls1) (to_has_lazy_storage dhls2)
  | Dep.Option_f _ dhls => Option_f (to_has_lazy_storage dhls)
  | Dep.List_f _ dhls => List_f (to_has_lazy_storage dhls)
  | Dep.Map_f _ _ dhls => Map_f (to_has_lazy_storage dhls)
  end.

Fixpoint dep_has_lazy_storage_value {t} (ty : With_family.ty t) :
  Dep.dep_has_lazy_storage t :=
  let aux1 {t'}
    (cons_type : Ty.t -> Ty.t)
    (cons_value : Dep.dep_has_lazy_storage t' ->
                  Dep.dep_has_lazy_storage (cons_type t'))
    (t_value : With_family.ty t') : Dep.dep_has_lazy_storage (cons_type t') :=
    match dep_has_lazy_storage_value t_value with
    | Dep.False_f _ => Dep.False_f _
    | h_value => cons_value h_value
    end in
  let aux2 {t' t''}
    (cons_type : Ty.t -> Ty.t -> Ty.t)
    (cons_value : Dep.dep_has_lazy_storage t' ->
                  Dep.dep_has_lazy_storage t'' ->
                  Dep.dep_has_lazy_storage (cons_type t' t''))
    (l_value : With_family.ty t') (r_value : With_family.ty t'') :
    Dep.dep_has_lazy_storage (cons_type t' t'') :=
    match dep_has_lazy_storage_value l_value,
      dep_has_lazy_storage_value r_value with
    | Dep.False_f _, Dep.False_f _ => Dep.False_f _
    | left_value, right_value => cons_value left_value right_value
    end in
  match ty with
  | @With_family.Big_map_t k v _ _ _ => Dep.Big_map_f k v
  | With_family.Sapling_state_t _ => Dep.Sapling_state_f
  | With_family.Unit_t => Dep.False_f Ty.Unit
  | With_family.Int_t => Dep.False_f (Ty.Num Ty.Num.Int)
  | With_family.Nat_t => Dep.False_f (Ty.Num Ty.Num.Nat)
  | With_family.Signature_t => Dep.False_f Ty.Signature
  | With_family.String_t => Dep.False_f Ty.String
  | With_family.Bytes_t => Dep.False_f Ty.Bytes
  | With_family.Mutez_t => Dep.False_f Ty.Mutez
  | With_family.Key_hash_t => Dep.False_f Ty.Key_hash
  | With_family.Key_t => Dep.False_f Ty.Key
  | With_family.Timestamp_t => Dep.False_f Ty.Timestamp
  | With_family.Address_t => Dep.False_f Ty.Address
  | With_family.Tx_rollup_l2_address_t => Dep.False_f Ty.Tx_rollup_l2_address
  | With_family.Bool_t => Dep.False_f Ty.Bool
  | With_family.Lambda_t t1 t2 _ => Dep.False_f (Ty.Lambda _ _)
  | @With_family.Set_t t' ty _ => Dep.False_f (Ty.Set_ t')
  | @With_family.Contract_t t' _ _ => Dep.False_f (Ty.Contract t')
  | With_family.Operation_t => Dep.False_f _
  | With_family.Chain_id_t => Dep.False_f _
  | With_family.Never_t => Dep.False_f _
  | With_family.Bls12_381_g1_t => Dep.False_f _
  | With_family.Bls12_381_g2_t => Dep.False_f _
  | With_family.Bls12_381_fr_t => Dep.False_f _
  | With_family.Sapling_transaction_t _ => Dep.False_f Ty.Sapling_transaction
  | With_family.Sapling_transaction_deprecated_t _ =>
      Dep.False_f Ty.Sapling_transaction_deprecated
  | With_family.Ticket_t _ _ => Dep.False_f _
  | With_family.Chest_key_t => Dep.False_f Ty.Chest_key
  | With_family.Chest_t => Dep.False_f Ty.Chest
  | With_family.Pair_t l_value r_value _ _ =>
       aux2 (fun t t' => Ty.Pair t t') (fun l_val r_val =>
       Dep.Pair_f _ _ l_val r_val) l_value r_value
  | With_family.Union_t l_value r_value _ _ =>
      aux2 (fun t t' => Ty.Union t t') (fun l_val r_val =>
       Dep.Union_f _ _ l_val r_val) l_value r_value
  | With_family.Option_t t_value _ _ =>
     aux1 (fun t => Ty.Option t) (fun h_value => Dep.Option_f _ h_value) t_value
  | With_family.List_t t_value _ =>
      aux1 (fun t => Ty.List t) (fun h_value => Dep.List_f _ h_value) t_value
  | With_family.Map_t t_key t_value _ =>
      aux1 (fun t => Ty.Map _ t) (fun h_value => Dep.Map_f _ _ h_value) t_value
  end.

Fixpoint dep_extract_lazy_storage_updates_aux {a : Ty.t}
    (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
    (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
    (acc_value : Alpha_context.Lazy_storage.diffs) (ty : With_family.ty a)
    (x_value : With_family.ty_to_dep_Set a)
    (has_lazy_storage_value : has_lazy_storage)
    {struct has_lazy_storage_value} :
    M? (
      Alpha_context.context * With_family.ty_to_dep_Set a *
      Alpha_context.Lazy_storage.IdSet.t * Alpha_context.Lazy_storage.diffs
    ) :=
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
    match
      has_lazy_storage_value,
      ty in With_family.ty a,
      x_value
    return M? (_ * With_family.ty_to_dep_Set a * _ * _)
    with
    | False_f, _, _ => return? (ctxt, x_value, ids_to_copy, acc_value)
    | Big_map_f, With_family.Big_map_t _ _ _, map =>
      let? '(diff_value, id, ctxt) :=
        dep_diff_of_big_map ctxt mode temporary ids_to_copy map in
      let map := {|
        With_family.big_map.id := Some id;
        With_family.big_map.diff := {|
          Script_typed_ir.big_map_overlay.map :=
            Script_typed_ir.Big_map_overlay.(Map.S.empty);
          Script_typed_ir.big_map_overlay.size := 0;
        |};
        With_family.big_map.key_type := map.(With_family.big_map.key_type);
        With_family.big_map.value_type := map.(With_family.big_map.value_type);
      |} in
      let diff_value :=
        Alpha_context.Lazy_storage.make Alpha_context.Lazy_storage.Kind.Big_map
          id diff_value in
      let ids_to_copy :=
        Alpha_context.Lazy_storage.IdSet.add
          Alpha_context.Lazy_storage.Kind.Big_map id ids_to_copy in
      return? (ctxt, map, ids_to_copy, (cons diff_value acc_value))
    | Sapling_state_f, With_family.Sapling_state_t _, sapling_state =>
      let? '(diff_value, id, ctxt) :=
        diff_of_sapling_state ctxt temporary ids_to_copy sapling_state in
      let sapling_state :=
        Alpha_context.Sapling.empty_state (Some id)
          sapling_state.(Alpha_context.Sapling.state.memo_size) tt in
      let diff_value :=
        Alpha_context.Lazy_storage.make
          Alpha_context.Lazy_storage.Kind.Sapling_state id diff_value in
      let ids_to_copy :=
        Alpha_context.Lazy_storage.IdSet.add
          Alpha_context.Lazy_storage.Kind.Sapling_state id ids_to_copy in
      return? (ctxt, sapling_state, ids_to_copy, (cons diff_value acc_value))
    | Pair_f hl hr, With_family.Pair_t tyl tyr _ _, x_value =>
      let '(xl, xr) := x_value in
      let? '(ctxt, xl, ids_to_copy, acc_value) :=
        dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value tyl xl hl in
      let? '(ctxt, xr, ids_to_copy, acc_value) :=
        dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value tyr xr hr in
      return? (ctxt, (xl, xr), ids_to_copy, acc_value)
    | Union_f has_lazy_storage_l has_lazy_storage_r,
        With_family.Union_t tyl tyr _ _, x_value =>
      match x_value with
      | Script_typed_ir.L x_value =>
        let? '(ctxt, x_value, ids_to_copy, acc_value) :=
          dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value tyl x_value
            has_lazy_storage_l in
        return? (ctxt, (Script_typed_ir.L x_value), ids_to_copy, acc_value)
      | Script_typed_ir.R x_value =>
        let? '(ctxt, x_value, ids_to_copy, acc_value) :=
          dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value tyr x_value
            has_lazy_storage_r in
        return? (ctxt, (Script_typed_ir.R x_value), ids_to_copy, acc_value)
      end
    | Option_f has_lazy_storage_value, With_family.Option_t ty _ _,
        x_value =>
      match x_value with
      | Some x_value =>
        let? '(ctxt, x_value, ids_to_copy, acc_value) :=
          dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value ty x_value
            has_lazy_storage_value in
        return? (ctxt, (Some x_value), ids_to_copy, acc_value)
      | None => return? (ctxt, None, ids_to_copy, acc_value)
      end
    | List_f has_lazy_storage_value, With_family.List_t ty _, l_value =>
      let? '(ctxt, l_value, ids_to_copy, acc_value) :=
        List.fold_left_es
          (fun '(ctxt, l_value, ids_to_copy, acc_value) =>
            fun x_value =>
              let? '(ctxt, x_value, ids_to_copy, acc_value) :=
                dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value ty x_value
                  has_lazy_storage_value in
              return?
                (ctxt, (Script_list.cons_value x_value l_value), ids_to_copy,
                  acc_value)) (ctxt, Script_list.empty, ids_to_copy, acc_value)
          l_value.(Script_typed_ir.boxed_list.elements) in
      let reversed :=
        {|
          Script_typed_ir.boxed_list.elements :=
            List.rev l_value.(Script_typed_ir.boxed_list.elements);
          Script_typed_ir.boxed_list.length :=
            l_value.(Script_typed_ir.boxed_list.length) |} in
      return? (ctxt, reversed, ids_to_copy, acc_value)
    | Map_f has_lazy_storage_value, With_family.Map_t _ ty _, map =>
      let M := map in
      let bindings {C : Set}
        (m_value : With_family.map _ C) : list (_ * C) :=
        (With_family.Script_Map _).(Map.S.bindings) m_value in
      let? '(ctxt, m_value, ids_to_copy, acc_value) :=
        List.fold_left_es
          (fun '(ctxt, m_value, ids_to_copy, acc_value) '(k_value, x_value) =>
            let? '(ctxt, x_value, ids_to_copy, acc_value) :=
              dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value ty x_value
                has_lazy_storage_value in
            return? (
              ctxt,
              (With_family.Script_Map _).(Map.S.add) k_value x_value m_value,
              ids_to_copy,
              acc_value
            )
          )
          (
            ctxt,
            (With_family.Script_Map _).(Map.S.empty),
            ids_to_copy,
            acc_value
          )
          (bindings M) in
      return? (ctxt, M, ids_to_copy, acc_value)
    | _, _, _ =>
      Error_monad.fail
        (Build_extensible "Internal_error" unit tt)
    end.

(** Simulation of [extract_lazy_storage_updates]. *)
Definition dep_extract_lazy_storage_updates {a}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (acc_value : Alpha_context.Lazy_storage.diffs) (ty : With_family.ty a)
  (x_value : With_family.ty_to_dep_Set a) :
  M? (
    Alpha_context.context * With_family.ty_to_dep_Set a *
    Alpha_context.Lazy_storage.IdSet.t * Alpha_context.Lazy_storage.diffs
  ) :=

  let has_lazy_storage_value := dep_has_lazy_storage_value ty in
  dep_extract_lazy_storage_updates_aux ctxt mode temporary ids_to_copy acc_value ty x_value
    (to_has_lazy_storage has_lazy_storage_value).

(** Simulation of [fold_lazy_storage] *)
Fixpoint dep_fold_lazy_storage {acc : Set} {a : Ty.t}
  (f_value :
    Alpha_context.Lazy_storage.IdSet.fold_f (Fold_lazy_storage.result acc))
  (init_value : acc) (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (x_value : With_family.ty_to_dep_Set a) (has_lazy_storage_value : has_lazy_storage)
  {struct has_lazy_storage_value}
  : M? (Fold_lazy_storage.result acc * Alpha_context.context) :=
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle
    in
  match has_lazy_storage_value, ty, x_value with
  | Big_map_f, With_family.Big_map_t key value metadata,
      {| With_family.big_map.id := Some id |} =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
    return?
      ((f_value.(Alpha_context.Lazy_storage.IdSet.fold_f.f)
          Alpha_context.Lazy_storage.Kind.Big_map id
          (Fold_lazy_storage.Ok init_value)), ctxt)
  | Big_map_f, With_family.Big_map_t key value metadata,
      {| With_family.big_map.id := None |} =>
    return? (Fold_lazy_storage.Ok init_value, ctxt)
  | Sapling_state_f, With_family.Sapling_state_t _,
      {| Alpha_context.Sapling.state.id := Some id |} =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
    return?
      ((f_value.(Alpha_context.Lazy_storage.IdSet.fold_f.f)
          Alpha_context.Lazy_storage.Kind.Sapling_state id
          (Fold_lazy_storage.Ok init_value)), ctxt)
  | Sapling_state_f, With_family.Sapling_state_t _,
      {| Alpha_context.Sapling.state.id := None |} =>
    return? (Fold_lazy_storage.Ok init_value, ctxt)
  | False_f, _, _ =>
      return? (Fold_lazy_storage.Ok init_value, ctxt)
  | Pair_f hl hr, With_family.Pair_t tyl tyr _ _, (xl, xr) =>
      let? '(init, ctxt) := dep_fold_lazy_storage f_value init_value ctxt tyl
        xl hl in
      match init with
      | Fold_lazy_storage.Ok init =>
          dep_fold_lazy_storage f_value init ctxt tyr xr hr
      | Fold_lazy_storage.Error =>
          return? (init, ctxt)
      end
  |  Union_f hl _, With_family.Union_t ty _ _ _, Script_typed_ir.L x =>
      dep_fold_lazy_storage f_value init_value ctxt ty x hl
  |  Union_f _ hr, With_family.Union_t _ ty _ _, Script_typed_ir.R x =>
      dep_fold_lazy_storage f_value init_value ctxt ty x hr
  | Option_f has_lazy_storage, With_family.Option_t ty _ _, Some x =>
      dep_fold_lazy_storage f_value init_value ctxt ty x has_lazy_storage
  | Option_f has_lazy_storage, With_family.Option_t ty _ _, None =>
      return? (Fold_lazy_storage.Ok init_value, ctxt)
  | List_f has_lazy_storage, With_family.List_t ty _, l =>
      List.fold_left_e
        (fun '(init, ctxt) x =>
           match init with
           | Fold_lazy_storage.Ok init =>
               dep_fold_lazy_storage f_value init ctxt ty x has_lazy_storage
           | Fold_lazy_storage.Error =>
               return? (init, ctxt)
           end)
        ((Fold_lazy_storage.Ok init_value), ctxt)
        l.(Script_typed_ir.boxed_list.elements)
  | Map_f has_lazy_storage, With_family.Map_t _ ty _, m =>
      Script_map.dep_fold
        (fun _ v acc =>
           let? '(init, ctxt) := acc in
           match init with
           | Fold_lazy_storage.Ok init =>
               dep_fold_lazy_storage f_value init ctxt ty v has_lazy_storage
           | Fold_lazy_storage.Error =>
               return? (init, ctxt)
           end)
        m
        (return? (Fold_lazy_storage.Ok init_value, ctxt))
  | _, _, _ =>
      (* GADT version is exaustive with the above branches,
         here I need a default case *)
      return? (Fold_lazy_storage.Ok init_value, ctxt)
  end.

(** Simulation of [collect_lazy_storage] *)
Definition dep_collect_lazy_storage {a : Ty.t}
  (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (x_value : With_family.ty_to_dep_Set a)
  : M? (Alpha_context.Lazy_storage.IdSet.t * Alpha_context.context) :=
  let has_lazy_storage_value := has_lazy_storage_value (With_family.to_ty ty) in
  let f_value {B : Set}
    (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : B)
    (acc_value : Fold_lazy_storage.result Alpha_context.Lazy_storage.IdSet.t)
    : Fold_lazy_storage.result Alpha_context.Lazy_storage.IdSet.t :=
    let acc_value :=
      match acc_value with
      | Fold_lazy_storage.Ok acc_value => acc_value
      | Fold_lazy_storage.Error => Lazy_storage_kind.IdSet.empty
      end in
    Fold_lazy_storage.Ok
      (Alpha_context.Lazy_storage.IdSet.add kind_value id acc_value) in
  let? '(ids, ctxt) :=
    dep_fold_lazy_storage
      {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f_value |}
      no_lazy_storage_id ctxt ty x_value has_lazy_storage_value in
  match ids with
  | Fold_lazy_storage.Ok ids => return? (ids, ctxt)
  | Fold_lazy_storage.Error => return? (Lazy_storage_kind.IdSet.empty, ctxt)
  end.

(** Simulation of [extract_lazy_storage_diff]. *)
Definition dep_extract_lazy_storage_diff {a}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (to_duplicate : Alpha_context.Lazy_storage.IdSet.t)
  (to_update : Alpha_context.Lazy_storage.IdSet.t) (ty : With_family.ty a)
  (v_value : With_family.ty_to_dep_Set a)
  : M? (With_family.ty_to_dep_Set a * option Alpha_context.Lazy_storage.diffs * Alpha_context.context) :=
  let to_duplicate :=
    Alpha_context.Lazy_storage.IdSet.diff_value to_duplicate to_update in
  let? '(ctxt, v_value, alive, diffs) :=
    dep_extract_lazy_storage_updates ctxt mode temporary to_duplicate nil ty v_value
    in
  let diffs :=
    if temporary then
      diffs
    else
      let dead := Alpha_context.Lazy_storage.IdSet.diff_value to_update alive in
      let f_value {B : Set}
        (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : B)
        (acc_value : list Alpha_context.Lazy_storage.diffs_item)
        : list Alpha_context.Lazy_storage.diffs_item :=
        cons
          ((Alpha_context.Lazy_storage.make (a := unit) (u := unit)) kind_value
            id Alpha_context.Lazy_storage.Remove) acc_value in
      Alpha_context.Lazy_storage.IdSet.fold_all
        {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f_value |} dead diffs
    in
  match diffs with
  | [] => return? (v_value, None, ctxt)
  | diffs => return? (v_value, (Some diffs), ctxt)
  end.

(** Simulation of [parse_data]. *)
Definition dep_parse_data {a}
  : option type_logger -> Alpha_context.context -> bool -> bool ->
  With_family.ty a -> Alpha_context.Script.node ->
  M? (With_family.ty_to_dep_Set a * Alpha_context.context) :=
  fun x_1 => dep_parse_data_aux x_1 0.

(** Simulation of [parse_instr]. *)
Definition dep_parse_instr {s}
  (type_logger_value : option type_logger) (tc_context_value : dep_tc_context)
  (ctxt : Alpha_context.context) (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : With_family.stack_ty s)
  : M? (dep_judgement s * Alpha_context.context) :=
  dep_parse_instr_aux type_logger_value 0 tc_context_value ctxt legacy script_instr
    stack_ty.

(** Simulation of [unparse_data]. *)
Definition dep_unparse_data {a}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (ty : With_family.ty a) (a_value : With_family.ty_to_dep_Set a) :
  M? (Alpha_context.Script.node * Alpha_context.context) :=
  dep_unparse_data_aux ctxt 0 mode ty a_value.

(** Simulation of [unparse_code]. *)
Definition dep_unparse_code {A : Set}
  (ctxt : Alpha_context.t) (mode : unparsing_mode)
  (code : Micheline.node A Alpha_context.Script.prim)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? '(ctxt, code) :=
    Alpha_context.Global_constants_storage.expand ctxt
      (Micheline.strip_locations code) in
  dep_unparse_code_aux ctxt 0 mode (Micheline.root_value code).

(** Simulation of [parse_toplevel]. *)
Definition dep_parse_toplevel
  (ctxt : Alpha_context.t) (legacy : bool)
  (toplevel_value : Alpha_context.Script.expr)
  : M? (dep_toplevel * Alpha_context.context) :=
  let? '(ctxt, toplevel_value) :=
    Alpha_context.Global_constants_storage.expand ctxt toplevel_value in
  dep_parse_toplevel_aux ctxt legacy toplevel_value.

(** Simulation of [parse_comparable_ty]. *)
Definition dep_parse_comparable_ty ctxt :
  Alpha_context.Script.node ->
  M? (dep_ex_comparable_ty * Alpha_context.context) :=
  dep_parse_comparable_ty_aux ctxt 0.

(** Simulation of [parse_big_map_value_ty]. *)
Definition dep_parse_big_map_value_ty
  : Alpha_context.context -> bool ->
  Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 => dep_parse_big_map_value_ty_aux x_1 0.

(** Simulation of [parse_packable_ty]. *)
Definition dep_parse_packable_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 => dep_parse_packable_ty_aux x_1 0.

(** Simulation of [parse_passable_ty]. *)
Definition dep_parse_passable_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 => dep_parse_passable_ty_aux x_1 0.

(** Simulation of [parse_any_ty]. *)
Definition dep_parse_any_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 => dep_parse_any_ty_aux x_1 0.

(** Simulation of [parse_ty]. *)
Definition dep_parse_ty
  : Alpha_context.context -> bool -> bool -> bool -> bool -> bool ->
  Alpha_context.Script.node -> M? (dep_ex_ty * Alpha_context.context) :=
  fun x_1 x_2 x_3 x_4 x_5 x_6 =>
    dep_parse_ty_aux x_1 0 x_2 x_3 x_4 x_5 x_6
      Parse_ty_ret_family.Don't_parse_entrypoints.

(** Simulation of [parse_parameter_ty_and_entrypoints]. *)
Definition dep_parse_parameter_ty_and_entrypoints
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (dep_ex_parameter_ty_and_entrypoints * Alpha_context.context) :=
  fun x_1 => dep_parse_parameter_ty_and_entrypoints_aux x_1 0.

(** Simulation of [get_single_sapling_state]. *)
Definition dep_get_single_sapling_state {a}
  (ctxt : Alpha_context.context) (ty : With_family.ty a)
  (x_value : With_family.ty_to_dep_Set a) :
  M? (option Alpha_context.Sapling.Id.t * Alpha_context.context) :=
  let has_lazy_storage_value := dep_has_lazy_storage_value ty in
  let f_value {i : Set}
    (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : i)
    (single_id_opt :
      Fold_lazy_storage.result (option Alpha_context.Sapling.Id.t))
    : Fold_lazy_storage.result (option Alpha_context.Sapling.Id.t) :=
    match (kind_value, id) with
    | (Alpha_context.Lazy_storage.Kind.Sapling_state, id) =>
      let id := cast Alpha_context.Sapling.Id.t id in
      match single_id_opt with
      | Fold_lazy_storage.Ok None => Fold_lazy_storage.Ok (Some id)
      | Fold_lazy_storage.Ok (Some _) => Fold_lazy_storage.Error
      | Fold_lazy_storage.Error => single_id_opt
      end
    | _ => single_id_opt
    end in
  let? '(id, ctxt) :=
    fold_lazy_storage
      {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f_value |} None ctxt
      (With_family.to_ty ty)
      (With_family.to_value x_value)
      (to_has_lazy_storage has_lazy_storage_value) in
  match id with
  | Fold_lazy_storage.Ok (Some id) => return? ((Some id), ctxt)
  | (Fold_lazy_storage.Ok None | Fold_lazy_storage.Error) =>
    return? (None, ctxt)
  end.

(** Simulation of [script_size]. *)
Definition dep_script_size (function_parameter : dep_ex_script)
  : int * Gas_limit_repr.cost :=
  let
    'Dep_ex_script {|
      With_family.script.code := _;
      With_family.script.arg_type := _;
      With_family.script.storage := storage_value;
      With_family.script.storage_type := storage_type;
      With_family.script.views := _;
      With_family.script.entrypoints := _;
      With_family.script.code_size := code_size
    |} := function_parameter in
  let '(nodes, storage_size) :=
    Script_typed_ir_size.value_size
      (With_family.to_ty storage_type) (With_family.to_value storage_value) in
  let cost := Script_typed_ir_size_costs.nodes_cost nodes in
  ((Saturation_repr.to_int (Saturation_repr.add code_size storage_size)), cost).

(** Simulation of [typecheck_code]. *)
Definition dep_typecheck_code
  (legacy : bool) (show_types : bool) (ctxt : Alpha_context.context)
  (code : Alpha_context.Script.expr)
  : M? (Script_tc_errors.type_map * Alpha_context.context) :=
  let?
    '(Dep_typechecked_code_internal {|
      dep_typechecked_code_internal.Dep_typechecked_code_internal.type_map :=
        type_map
    |}, ctxt) := dep_typecheck_code_aux legacy show_types ctxt code in
  return? (type_map, ctxt).

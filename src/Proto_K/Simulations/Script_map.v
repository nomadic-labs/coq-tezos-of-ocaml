Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_map.

Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K.Script_int.

(** Simulation of the [empty_from] function. *)
Definition dep_empty_from {a : Ty.t} {b c : Set} (_ : With_family.map a b) :
  With_family.map a c :=
  (With_family.Script_Map a).(Map.S.empty).

(** Simulation of the [empty] function. *)
Definition dep_empty {a : Ty.t} {b : Set} (_ : With_family.ty a) :
  With_family.map a b :=
  (With_family.Script_Map a).(Map.S.empty).

(** Simulation of the [get] function. *)
Definition dep_get {key : Ty.t} {value : Set}
  (k : With_family.ty_to_dep_Set key) (x : With_family.map key value) :
  option value :=
  let k := With_family.to_value k in
  (With_family.Script_Map key).(Map.S.find) k x.

(** Simulation of the [update] function. *)
Definition dep_update {a : Ty.t} {b : Set}
  (k : With_family.ty_to_dep_Set a) (v : option b) (x : With_family.map a b) :
  With_family.map a b :=
  let k := With_family.to_value k in
  match v with
  | Some v => (With_family.Script_Map a).(Map.S.add) k v x
  | None => (With_family.Script_Map a).(Map.S.remove) k x
  end.

(** Simulation of the [mem] function. *)
Definition dep_mem {elt : Ty.t} {value : Set}
  (k : With_family.ty_to_dep_Set elt) (x : With_family.map elt value) : bool :=
  let k := With_family.to_value k in
  (With_family.Script_Map elt).(Map.S.mem) k x.

(** Simulation of the [fold] function. *)
Definition dep_fold {elt : Ty.t} {value acc : Set}
  (f : With_family.ty_to_dep_Set elt -> value -> acc -> acc)
  (x : With_family.map elt value) :
  acc -> acc :=
  (With_family.Script_Map elt).(Map.S.fold)
    (fun k v acc =>
      match With_family.of_value k with
      | None => acc
      | Some k => f k v acc
      end
    )
    x.

(** Simulation of the [fold_es] function. *)
Definition dep_fold_es {elt : Ty.t} {value acc : Set}
  (f : With_family.ty_to_dep_Set elt -> value -> acc -> M? acc)
  (x : With_family.map elt value) (init : acc) :
  M? acc :=
  dep_fold (fun k v acc => let? acc := acc in f k v acc) x (return? init).

(** Simulation of the [size_value] function. *)
Definition dep_size_value {elt : Ty.t} {value : Set}
  (x : With_family.map elt value) : Script_int.num :=
  let size := (With_family.Script_Map elt).(Map.S.cardinal) x in
  Script_int.abs (Script_int.of_int size).

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_commitment_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_repr.

Module Commitment_hash.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_commitment_repr.Hash.encoding.
  Proof.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Sc_rollup_commitment_repr.Hash.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.
End Commitment_hash.

Module V1.
  Module Valid.
    Import Sc_rollup_commitment_repr.V1.t.

    Record t (x : Sc_rollup_commitment_repr.V1.t) : Prop := {
      inbox_level : Raw_level_repr.Valid.t x.(inbox_level);
    }.
  End Valid.

  (** The encoding [V1.encoding] is valid. *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t Valid.t Sc_rollup_commitment_repr.V1.encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End V1.

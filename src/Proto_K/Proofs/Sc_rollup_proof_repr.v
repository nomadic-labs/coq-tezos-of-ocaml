Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_proof_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_inbox_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollups.

Module Valid.
  Import Sc_rollup_proof_repr.t.
  Record  t (r : Sc_rollup_proof_repr.t) : Prop := {
    inbox : Option.Forall Sc_rollup_inbox_repr.Proof.Valid.t r.(inbox)
  }.
End Valid.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Sc_rollup_proof_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [check true _] returns [Pervasives.Ok tt] and
    [check false _] returns [Pervasives.Error _] *)
Lemma check_eq (b : bool) (reason : string) : 
  match Sc_rollup_proof_repr.check b reason with
  | Pervasives.Ok tt => b = true
  | Pervasives.Error _ => b = false
  end.
Proof.
  destruct b eqn:H_b; easy.
Qed.
 
(** [cut_at_level level input] returns [Some input]
    when [level > input.(Sc_rollup_PVM_sem.input.inbox_level] *)
Lemma cut_at_level_gt_eq (level : int) (input : Sc_rollup_PVM_sem.input.record) 
  (H : 
    let input_level := input.(Sc_rollup_PVM_sem.input.inbox_level) in
    level > input_level) :
  Sc_rollup_proof_repr.cut_at_level level input = Some input.
Proof.
  unfold Sc_rollup_proof_repr.cut_at_level.
  step; [|reflexivity].
  simpl in *.
  unfold Raw_level_repr.op_lteq in *. simpl in *. lia.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_int.

Require TezosOfOCaml.Environment.V6.Proofs.Compare.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.

#[global] Hint Unfold 
  Script_int.zero
  Script_int.zero_n
  Script_int.one_n
  Script_int.of_int
  Script_int.of_int32
  Script_int.of_int64
  Script_int.of_zint
  Script_int.to_int
  Script_int.to_int64
  Script_int.to_zint
  Script_int.add
  Script_int.sub
  Script_int.mul
  Script_int.ediv
  Script_int.add_n
  Script_int.succ_n
  Script_int.mul_n
  Script_int.ediv_n
  Script_int.abs
  Script_int.neg
  Script_int.int_value
  : tezos_z.

Module N.
  Module Valid.
    Definition t x : Prop := 
      0 <= let 'Script_int.Num_tag n_value := x in n_value.
      #[global] Hint Unfold t : tezos_z.
  End Valid.
End N.

Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Script_int.compare.
Proof.
  apply (Compare.equality (
    let proj '(Script_int.Num_tag z) := z in
    Compare.projection proj Z.compare
  )); [sauto lq: on|].
  eapply Compare.implies.
  { eapply Compare.projection_is_valid.
    apply Compare.z_is_valid.
  }
  all: sauto.
Qed.

(** The function [to_int64] is the identity on inputs in [int64] bounds. *)
Lemma to_int64_eq (z : Z.t) :
  Int64.Valid.t z ->
  Script_int.to_int64 (Script_int.Num_tag z) = Some z.
Proof.
  intros.
  simpl.
  rewrite Z.to_int64_eq by lia.
  now rewrite Option.catch_no_errors.
Qed.

(** When there are no divisions by zero, [ediv] has a simpler expression. *)
Lemma ediv_not_zero (a b : Z.t) :
  b <> 0 ->
  Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag b) =
  Some (
    Script_int.Num_tag (Zeuclid.ZEuclid.div a b),
    Script_int.Num_tag (Zeuclid.ZEuclid.modulo a b)
  ).
Proof.
  intros.
  unfold Script_int.ediv, Z.ediv_rem.
  destruct (_ =? _) eqn:?.
  { lia. }
  { now rewrite Option.catch_no_errors. }
Qed.

(** The division by zero returns [None]. *)
Lemma ediv_zero (a : Z.t) :
  Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag 0) =
  None.
Proof.
  unfold Script_int.ediv, Z.ediv_rem; simpl.
  set (f := fun '(quo, rem) =>
    (Script_int.Num_tag quo, Script_int.Num_tag rem)
  ).
  epose proof (Pervasives.raise_let _ f) as H_raise.
  unfold f in H_raise.
  rewrite H_raise.
  now rewrite Option.catch_raise.
Qed.

(** Bounds of the [ediv] operator when used for the [IEdiv_teznat]
    instruction. *)
Lemma ediv_teznat_is_valid (a b : Z.t) :
  Tez_repr.Repr.Valid.t a ->
  0 <= b ->
  Option.Forall
    (fun '(Script_int.Num_tag quo, Script_int.Num_tag rem) =>
      Tez_repr.Repr.Valid.t quo /\ Tez_repr.Repr.Valid.t rem)
    (Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag b)).
Proof.
  intros H_a H_b.
  destruct (b =? 0) eqn:?.
  { replace b with 0 by lia.
    now rewrite ediv_zero.
  }
  { rewrite ediv_not_zero by lia; simpl.
    unfold Tez_repr.Repr.Valid.t in *; split.
    { unfold Zeuclid.ZEuclid.div.
      nia.
    }
    { unfold Zeuclid.ZEuclid.modulo.
      replace (BinInt.Z.abs b) with b by lia.
      assert (a mod b <= a) by (apply Z.mod_le; lia).
      lia.
    }
  }
Qed.

(** Bounds of the [ediv] operator when used for the [IEdiv_tez] instruction. *)
Lemma ediv_tez_is_valid (a b : Z.t) :
  Tez_repr.Repr.Valid.t a ->
  Tez_repr.Repr.Valid.t b ->
  Option.Forall
    (fun '(Script_int.Num_tag quo, Script_int.Num_tag rem) =>
      Tez_repr.Repr.Valid.t quo /\ Tez_repr.Repr.Valid.t rem)
    (Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag b)).
Proof.
  intros.
  apply ediv_teznat_is_valid; lia.
Qed.

(** Bounds of the [ediv] operator when used for the [IEdiv_int] instruction. *)
Lemma ediv_int_is_valid (a b : Z.t) :
  Option.Forall
    (fun '(_, rem) => N.Valid.t rem)
    (Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag b)).
Proof.
  destruct (b =? 0) eqn:?.
  { replace b with 0 by lia.
    now rewrite ediv_zero.
  }
  { rewrite ediv_not_zero by lia; simpl.
    unfold N.Valid.t.
    unfold Zeuclid.ZEuclid.modulo.
    nia.
  }
Qed.

(** Bounds of the [ediv] operator when used for the [IEdiv_nat] instruction. *)
Lemma ediv_nat_is_valid (a b : Z.t) :
  0 <= a ->
  0 <= b ->
  Option.Forall
    (fun '(quo, rem) => N.Valid.t quo /\ N.Valid.t rem)
    (Script_int.ediv (Script_int.Num_tag a) (Script_int.Num_tag b)).
Proof.
  intros.
  destruct (b =? 0) eqn:?.
  { replace b with 0 by lia.
    now rewrite ediv_zero.
  }
  { rewrite ediv_not_zero by lia; simpl.
    unfold N.Valid.t.
    unfold Zeuclid.ZEuclid.div, Zeuclid.ZEuclid.modulo.
    nia.
  }
Qed.

Lemma z_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Script_int.z_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve z_encoding_is_valid : Data_encoding_db.

Lemma n_encoding_is_valid :
  Data_encoding.Valid.t N.Valid.t Script_int.n_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve n_encoding_is_valid : Data_encoding_db.

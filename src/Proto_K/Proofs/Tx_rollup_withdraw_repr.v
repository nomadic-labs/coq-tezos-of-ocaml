Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_withdraw_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.Signature.
Require TezosOfOCaml.Proto_K.Proofs.Ticket_hash_repr.
Require TezosOfOCaml.Proto_K.Proofs.Tx_rollup_l2_qty.

Module Valid.
  Import Tx_rollup_withdraw_repr.order.

  Record t (x : Tx_rollup_withdraw_repr.t) : Prop := {
    amount : Tx_rollup_l2_qty.Valid.t x.(amount);
  }.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Tx_rollup_withdraw_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

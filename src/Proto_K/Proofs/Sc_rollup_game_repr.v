Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_inbox_repr.
Require TezosOfOCaml.Proto_K.Sc_rollup_game_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Context_hash.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.Option.
Require TezosOfOCaml.Environment.V6.Proofs.Signature.

Require TezosOfOCaml.Proto_K.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_inbox_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_tick_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_proof_repr.

(** The encoding [player_encoding_is_valid] is valid *)
Lemma player_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_game_repr.player_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve player_encoding_is_valid : Data_encoding_db.

Module Dissection.
(** Validity predicate for 
    [list (option Sc_rollup_repr.State_hash.t * Sc_rollup_tick_repr.t)]. *)
  Module Valid.
    Definition t 
      (l : list (option Sc_rollup_repr.State_hash.t * Sc_rollup_tick_repr.t))
      : Prop :=
      let valid '(_, x2) := Sc_rollup_tick_repr.Valid.t x2 in
      Forall valid l.
  End Valid.
End Dissection.

(** Validity predicate for [Sc_rollup_game_repr.t]. *)
Module Valid.
  Import Sc_rollup_game_repr.t.

  Record t (x : Sc_rollup_game_repr.t) : Prop := {
    inbox_snapshot : Sc_rollup_inbox_repr.Valid.t x.(inbox_snapshot);
    level : Raw_level_repr.Valid.t x.(level);
    dissection : Dissection.Valid.t x.(dissection);
  }.
End Valid.

(** The encoding [encoding_is_valid] is valid *)
Lemma encoding_is_valid : 
  Data_encoding.Valid.t Valid.t Sc_rollup_game_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros [] [].
  repeat (split; try easy); simpl in *.
  eapply List.Forall_impl.
  2: {
    match goal with
    | H : Dissection.Valid.t _ |- _ => apply H
    end.
  }
  hauto l: on.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Index.
(** Validity predicate for [Sc_rollup_game_repr.Index.t]. *)
  Module Valid.
    Definition t (x : Sc_rollup_game_repr.Index.t) : Prop := 
      Sc_rollup_repr.Staker.(SIGNATURE_PUBLIC_KEY_HASH.compare)
        x.(Sc_rollup_game_repr.Index.t.alice)
        x.(Sc_rollup_game_repr.Index.t.bob) <= 0.
  End Valid.

(** The encoding [encoding_is_valid] is valid *)
  Lemma encoding_is_valid : 
    Data_encoding.Valid.t Valid.t Sc_rollup_game_repr.Index.encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
    intuition.
    unfold Sc_rollup_game_repr.Index.make.
    destruct x eqn:H_x; unfold Valid.t in H;
    simpl in *.
    now replace (_ >? 0) with false by lia.
  Qed.
End Index.
#[global] Hint Resolve Index.encoding_is_valid : Data_encoding_db.

Module Step.
(** Validity predicate for [Sc_rollup_game_repr.step]. *)
  Module Valid.
    Import TezosOfOCaml.Proto_K.Proofs.Sc_rollup_proof_repr.
    Import Sc_rollup_game_repr.
    Definition t (x : Sc_rollup_game_repr.step) : Prop :=
      match x with
      | Dissection d => Dissection.Valid.t d
      | Proof p => Valid.t p
      end.
  End Valid.

(** The encoding [encoding_is_valid] is valid *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t Valid.t Sc_rollup_game_repr.step_encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
    intros [] H; simpl in *; [|easy].
    split; trivial.
    eapply List.Forall_impl; [|apply H].
    hauto l: on.
  Qed.
End Step.
#[global] Hint Resolve Step.encoding_is_valid : Data_encoding_db.

(** The encoding [reason_encoding_is_valid] is valid *)
Lemma reason_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_game_repr.reason_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve reason_encoding_is_valid : Data_encoding_db.

Module Refutation.
(** Validity predicate for [Sc_rollup_game_repr.refutation]. *)
  Module Valid.
    Record t (x : Sc_rollup_game_repr.refutation) : Prop := {
      choice : Sc_rollup_tick_repr.Valid.t x.(Sc_rollup_game_repr.refutation.choice);
      step : Step.Valid.t x.(Sc_rollup_game_repr.refutation.step);
    }.
  End Valid.

(** The encoding [encoding_is_valid] is valid *)
  Lemma encoding_is_valid : 
    Data_encoding.Valid.t Valid.t Sc_rollup_game_repr.refutation_encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
End Refutation.
#[global] Hint Resolve Refutation.encoding_is_valid : Data_encoding_db.

(** The encoding [status_encoding_is_valid] is valid *)
Lemma status_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_game_repr.status_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve status_encoding_is_valid : Data_encoding_db.

(** The encoding [outcome_encoding_is_valid] is valid *)
Lemma outcome_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_game_repr.outcome_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve outcome_encoding_is_valid : Data_encoding_db.

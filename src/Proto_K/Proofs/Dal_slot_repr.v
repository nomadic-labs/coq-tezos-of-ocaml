Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_K.Dal_slot_repr.
Require TezosOfOCaml.Proto_K.Proofs.Raw_level_repr.

Module Valid.
  Import Dal_slot_repr.t.
  
  (** The validity predicate for [Dal_slot_repr.t]. *)
  Record t (x : Dal_slot_repr.t) := {
    level : Raw_level_repr.Valid.t x.(level);
    index : Pervasives.UInt8.Valid.t x.(index);
    header : Pervasives.Int31.Valid.t x.(header);
  }.
End Valid.

(** Encoding [Dal_slot_repr.encoding] is valid. *)
Lemma encoding_is_valid
  : Data_encoding.Valid.t Valid.t Dal_slot_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

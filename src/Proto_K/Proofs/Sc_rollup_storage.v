Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_storage.

Require TezosOfOCaml.Environment.V6.Proofs.Pervasives.

(* @TODO this seems to be a very non-interesting property *)
Axiom originate_is_valid : forall {ctxt} {kind} {boot_sector} {parameters_ty},
  match Sc_rollup_storage.originate ctxt kind boot_sector parameters_ty with
  | Pervasives.Ok (_, size, _) => Pervasives.Int.Valid.non_negative size
  | Pervasives.Error _ => True
  end.

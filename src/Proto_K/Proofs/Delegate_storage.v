Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Delegate_storage.

Require TezosOfOCaml.Environment.V6.Proofs.Int64.
Require TezosOfOCaml.Environment.V6.Proofs.Map.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_K.Proofs.Receipt_repr.
Require TezosOfOCaml.Proto_K.Proofs.Storage.
Require TezosOfOCaml.Proto_K.Proofs.Storage_sigs.
Require TezosOfOCaml.Proto_K.Proofs.Contract_storage.
Require TezosOfOCaml.Proto_K.Proofs.Frozen_deposits_storage.
Require TezosOfOCaml.Proto_K.Proofs.Stake_storage.

Require TezosOfOCaml.Proto_K.Contract_storage.

Module participation_info.
  Module Valid.
    Import Proto_K.Delegate_storage.participation_info.

    Record t (x : Delegate_storage.participation_info) : Prop := {
      expected_cycle_activity :
        Pervasives.Int31.Valid.t x.(expected_cycle_activity);
      minimal_cycle_activity :
        Pervasives.Int31.Valid.t x.(minimal_cycle_activity);
      missed_slots :
        Pervasives.Int31.Valid.t x.(missed_slots);
      missed_levels :
        Pervasives.Int31.Valid.t x.(missed_levels);
      remaining_allowed_missed_slots :
        Pervasives.Int31.Valid.t x.(remaining_allowed_missed_slots);
      expected_endorsing_rewards :
        Tez_repr.Valid.t x.(expected_endorsing_rewards);
    }.
  End Valid.
End participation_info.

(** [set_frozen_deposits_limit] followed by [frozen_deposits_limit] is an
   identity *)
Lemma set_frozen_deposits_limit_frozen_deposits_limit_eq
  (absolute_key : Context.key)
  (ctxt : Raw_context.t)
  (delegate : _)
  (amount : Tez_repr.t) :
  Delegate_storage.frozen_deposits_limit
    (Delegate_storage.set_frozen_deposits_limit
       ctxt delegate (Some amount)) delegate =
    Pervasives.Ok (Some amount).
Proof.
  unfold Delegate_storage.set_frozen_deposits_limit.
  rewrite (Storage.Eq.Contracts.Frozen_deposits_limit.eq).(
    Storage_sigs.Indexed_data_storage.Eq.add_or_remove); simpl.
  unfold Delegate_storage.frozen_deposits_limit.
  rewrite (Storage.Eq.Contracts.Frozen_deposits_limit.eq).(
    Storage_sigs.Indexed_data_storage.Eq.find); simpl.
  unfold Storage_sigs.Indexed_data_storage.Op.find; simpl.
  unfold Storage.Eq.Contracts.Frozen_deposits_limit.parse.
  unfold Storage.Eq.Contracts.Frozen_deposits_limit.apply.
  rewrite Storage.parse_apply. simpl.
  unfold Storage_sigs.Indexed_data_storage.State.Map.
  rewrite Map.find_add_eq_some; [easy|].
  apply Storage.generic_Path_encoding_Valid.
Qed.

Lemma full_balance_is_valid (ctxt : Raw_context.t)
  (delegate : public_key_hash) :
    letP? frozen_deposits := Delegate_storage.frozen_deposits
        ctxt delegate in
    let ' Tez_repr.Tez_tag frozen_deposits := frozen_deposits.(
        Storage.deposits.current_amount) in
    let delegate_contract := Contract_repr.Implicit delegate in
    letP? 'Tez_repr.Tez_tag balance_and_frozen_bonds :=
      Contract_storage.get_balance_and_frozen_bonds ctxt delegate_contract in
    Tez_repr.Repr.Valid.t frozen_deposits ->
    Tez_repr.Repr.Valid.t balance_and_frozen_bonds ->
    Tez_repr.Repr.Valid.t (frozen_deposits +Z balance_and_frozen_bonds) ->
    letP? full_balance := Delegate_storage.full_balance ctxt delegate in
    Tez_repr.Valid.t full_balance.
Proof.
  intros.
  destruct Delegate_storage.frozen_deposits
    as [frozen_deposits'|] eqn:?; [|easy]; simpl.
  destruct Contract_storage.get_balance_and_frozen_bonds eqn:?;
    [|hauto l:on]; simpl.
  unfold Delegate_storage.full_balance.
  rewrite Heqt, Heqt0; simpl.
  destruct (frozen_deposits'.(Storage.deposits.current_amount)).
  destruct t.
  intros.
  unfold Tez_repr.op_plusquestion.
  unfold "+i64"; simpl.
  rewrite Int64.normalize_identity; [|lia].
  now replace (_ <? _) with false by lia.
Qed.

Lemma staking_balance_is_valid
  (ctxt : Raw_context.t) (delegate : public_key_hash)
  (balance : Tez_repr.t) :
  Delegate_storage.staking_balance ctxt delegate =
    Pervasives.Ok balance ->
  Tez_repr.Valid.t balance.
Proof.
  unfold Delegate_storage.staking_balance.
  destruct Contract_delegate_storage.registered; [|easy]; simpl.
  destruct b; [now apply Stake_storage.get_staking_storage_is_valid|].
  intros Hinj; injection Hinj as Hinj. now rewrite <- Hinj.
Qed.

Lemma balance_is_valid
  (absolute_key : Context.key)
  (ctxt : Raw_context.t) (delegate : public_key_hash)
  (balance : Tez_repr.t) :
  Tez_repr.Valid.t balance ->
  letP? ctxt' := Storage.Contract.Spendable_balance.(
    Storage_sigs.Indexed_data_storage.init_value)
    ctxt (Contract_repr.Implicit delegate) balance in
  letP? balance' := Delegate_storage.balance ctxt' delegate in
  Tez_repr.Valid.t balance'.
 Proof.
   unfold Delegate_storage.balance.
   now apply Contract_storage.spendable_balance_get_is_valid.
Qed.

Lemma frozen_deposits_is_valid :
 forall (absolute_key : Context.key) (ctxt ctxt' : Raw_context.t)
   (delegate : public_key_hash),
  let deposits := {|
    Storage.deposits.initial_amount := Tez_repr.zero;
    Storage.deposits.current_amount := Tez_repr.zero
  |} in
  Frozen_deposits_storage.init_value ctxt delegate = Pervasives.Ok ctxt' ->
  Delegate_storage.frozen_deposits ctxt' delegate = Pervasives.Ok deposits ->
  Tez_repr.Valid.t deposits.(Storage.deposits.current_amount).
Proof.
  do 5 intro.
  intro Hinit.
  unfold Delegate_storage.frozen_deposits.
  apply Frozen_deposits_storage.get_is_valid
        with (deposits := deposits)
    in Hinit; try easy.
  revert Hinit.
  unfold Frozen_deposits_storage.get,
    Frozen_deposits_storage.init_value.
  rewrite
    (Storage.Eq.Contracts.Frozen_deposits.eq).(
    Storage_sigs.Indexed_data_storage.Eq.init_value),
    (Storage.Eq.Contracts.Frozen_deposits.eq).(
    Storage_sigs.Indexed_data_storage.Eq.get); simpl.
  unfold
    Storage_sigs.Indexed_data_storage.Op.init_value,
    Storage_sigs.Indexed_data_storage.Op.get,
    Storage_sigs.Indexed_data_storage.Op.find,
    Storage_sigs.Indexed_data_storage.Op.mem,
    Storage_sigs.Indexed_data_storage.Op.add,
    Storage_sigs.Indexed_data_storage.State.Map.
  destruct (Map.Make _).(S.mem) eqn:?; [easy|simpl].
  intro Hinj. injection Hinj as Hinj. rewrite <- Hinj.
  unfold
    Storage.Eq.Contracts.Frozen_deposits.apply,
    Storage.Eq.Contracts.Frozen_deposits.parse.
  rewrite Storage.parse_apply; simpl.
  unfold Storage_sigs.Indexed_data_storage.State.Map.
  rewrite Map.find_add_eq_some; [easy|].
  apply Storage.generic_Path_encoding_Valid.
Qed.

(* @TODO: find the right hypothesis for this lemma *)
Lemma delegated_balance_is_valid
  (absolute_key : Context.key)
  (ctxt : Raw_context.t) (delegate : public_key_hash)
  (initial_balance : Tez_repr.t):
  Tez_repr.Valid.t initial_balance ->
  letP? ctxt' := Storage.Contract.Spendable_balance.(
    Storage_sigs.Indexed_data_storage.init_value)
    ctxt (Contract_repr.Implicit delegate) initial_balance in
  letP? '{| Storage.deposits.current_amount :=
            Tez_repr.Tez_tag current_amount |} :=
    Delegate_storage.frozen_deposits ctxt' delegate in
  letP? 'Tez_repr.Tez_tag balance := Delegate_storage.full_balance
    ctxt' delegate in
  Int64.Valid.t (balance +Z current_amount) ->
  letP? tez := Delegate_storage.delegated_balance ctxt' delegate in
  Tez_repr.Valid.t tez.
Proof.
  intros.
  destruct (_.(Storage_sigs.Indexed_data_storage.init_value) _ _ _)
    as [ctxt'|] eqn:Hinit; [simpl|easy].
  unfold Delegate_storage.delegated_balance.
Admitted.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_message_hash_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V6.Proofs.Compare.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Tx_rollup_message_hash_repr.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_typed_ir.

Require TezosOfOCaml.Environment.V6.Proofs.Compare.

Definition compare (x y : Script_typed_ir.never) : int :=
  0.

Lemma compare_is_valid : Compare.Valid.t (fun _ => True) id compare.
Proof.
  easy.
Qed.

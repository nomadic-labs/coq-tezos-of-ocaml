Require Import Coq.Program.Equality.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_interpreter.

Require TezosOfOCaml.Environment.V6.Proofs.List.
Require TezosOfOCaml.Environment.V6.Proofs.Z.
Require TezosOfOCaml.Proto_K.Proofs.Script_interpreter_defs.
Require TezosOfOCaml.Proto_K.Proofs.Script_big_map.
Require TezosOfOCaml.Proto_K.Proofs.Script_map.
Require TezosOfOCaml.Proto_K.Proofs.Script_set.
Require TezosOfOCaml.Proto_K.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_K.Proofs.Script_ir_translator.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_big_map.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

(** This [Opaque] seems to be necessary to terminate the evaluation. *)
Opaque Local_gas_counter.local_gas_counter_and_outdated_context.

(** This [Opaque] is mainly to clarify the output as the details of these
    functions are not really important. *)
Opaque Script_interpreter_defs.consume_instr
  Script_interpreter_defs.dep_consume_instr
  Script_interpreter_defs.consume_control.

(** Below group of Lemmas, related to [big_map] cases in [dep_step_eq].
    Lemmas state the validity of results after working out the functions
    - get,
    - update
    - get_and_update
*)
Lemma dep_big_map_get_is_valid {a b : Ty.t}
  (s1 : With_family.big_map.skeleton a b (With_family.ty_to_dep_Set a)
        (With_family.ty_to_dep_Set b)) t ctxt ctxt' accu :
  Script_typed_ir.With_family.Valid.big_map s1 ->
  dep_big_map_get ctxt accu s1 =
    Pervasives.Ok ((Some t), ctxt') ->
  Script_typed_ir.With_family.Valid.value t.
Proof.
  intros.
  unfold dep_big_map_get in H0.
  destruct (Script_ir_translator.dep_hash_comparable_data) eqn:Hash;
    [|scongruence].
  Tactics.destruct_pairs. simpl in H0.
  Admitted.

(** Within function [use_gas_counter_in_context], after the [get]
    procedure result is valid, if initial big_map was valid  *)
Lemma dep_big_map_get_gas_counter_is_valid {a b : Ty.t}
  (s1 : With_family.big_map.skeleton a b (With_family.ty_to_dep_Set a)
          (With_family.ty_to_dep_Set b)) o l0 o0 o1 l1 accu :
  Script_typed_ir.With_family.Valid.big_map s1 ->
  Local_gas_counter.use_gas_counter_in_context o l0
    (fun ctxt : Alpha_context.context =>
       dep_big_map_get ctxt accu s1) =
    Pervasives.Ok (o0, o1, l1) ->
  match o0 with
  | Some x => Script_typed_ir.With_family.Valid.value x
  | None => True
  end.
Proof.
  intros.
  destruct o0; [|trivial].
  unfold Local_gas_counter.use_gas_counter_in_context in H0.
  destruct (dep_big_map_get
    (Local_gas_counter.update_context l0 o) accu s1) eqn:Y; [|scongruence].
  Tactics.destruct_pairs. simpl in H0.
  unfold "return?" in H0. injection H0 as H0.
  rewrite H0 in Y.
  eapply dep_big_map_get_is_valid. apply H.
  apply Y.
Qed.

(** Within function [use_gas_counter_in_context], after the [update]
    procedure result is valid, if initial big_map was valid  *)
Lemma dep_big_map_update_gas_counter_is_valid {a b : Ty.t}
  (s1 : With_family.big_map.skeleton a b (With_family.ty_to_dep_Set a)
          (With_family.ty_to_dep_Set b)) o l0 o0 o1 l1 accu b0:
  Script_typed_ir.With_family.Valid.big_map s1 ->
  Local_gas_counter.use_gas_counter_in_context o l0
    (fun ctxt : Alpha_context.context =>
       dep_big_map_update ctxt accu o0 s1) =
    Pervasives.Ok (b0, o1, l1) ->
  Script_typed_ir.With_family.Valid.big_map b0.
Proof. Admitted.

(** Helper tactic. *)
Ltac dep_eq_case :=
  simpl;
  (
    try (
      match goal with
      | |- context[
          Script_interpreter_defs.dep_consume_instr _ _ (?accu, ?stack)
        ] =>
          rewrite (Script_interpreter_defs.dep_consume_instr_eq _ _ accu stack)
      end;
      simpl;
      destruct Script_interpreter_defs.consume_instr in |- *
    );
    try destruct (Script_interpreter_defs.consume_control _ _)
  ); [|discriminate];
  cbn in *;
  try rewrite cast_eval;
  try rewrite_cast_exists.

(** Helper tactic to apply [dep_step_eq] with the right parameters. *)
Ltac apply_dep_step_eq dep_step_eq :=
  match goal with
  | |- context [Script_interpreter.dep_step (s := ?s) _ _ _ ?i ?ks (?accu, ?stack)] =>
    match goal with
    | |- context [Script_interpreter.step _ _ ?i' ?ks' ?accu' ?stack'] =>
      replace i' with (With_family.to_kinstr i); [
      replace ks' with (With_family.to_continuation ks); [
      replace accu' with (With_family.to_stack_value_head (tys := s) accu); [
      replace stack' with (With_family.to_stack_value_tail (tys := s) stack); [
      apply dep_step_eq; simpl; try easy
      |]|]|]|];
      trivial
    end
  end.

(** Helper tactic to apply [dep_next_eq] with the right parameters. *)
Ltac apply_dep_next_eq dep_next_eq :=
  match goal with
  | |- context [Script_interpreter.dep_next (s := ?s) _ _ _ ?ks (?accu, ?stack)] =>
    match goal with
    | |- context [Script_interpreter.next _ _ ?ks' ?accu' ?stack'] =>
      replace ks' with (With_family.to_continuation ks); [
      replace accu' with (With_family.to_stack_value_head (tys := s) accu); [
      replace stack' with (With_family.to_stack_value_tail (tys := s) stack); [
      apply dep_next_eq; simpl; try easy
      |]|]|];
      trivial
    end
  end.

(** Verify that our definition [dep_step] is equivalent to the original
    function [step]. For unknown reasons, disabling the guard checking is
    needed. Maybe because of local recursive functions in the [step]
    function.

    To do the proof, we always:
    (1) start with a [grep] to explicit the instruction case we are handling;
    (2) run [dep_eq_case];
    (3) remove missing existential casts, and write the body of the proof;
    (4) run the tactics [apply_dep_step_eq dep_step_eq] or
        [apply_dep_next_eq dep_next_eq] to call the recursive hypothesis.
*)
#[bypass_check(guard)]
Fixpoint dep_step_eq {s t f}
  (fuel : nat)
  (g_value :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (i_value : With_family.kinstr s t)
  (ks : With_family.continuation t f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s)
  (dep_output_accu_stack : With_family.stack_ty_to_dep_Set f)
  (output_accu : Stack_ty.to_Set_head f)
  (output_stack : Stack_ty.to_Set_tail f)
  (dep_output_ctxt output_ctxt : Local_gas_counter.outdated_context)
  (dep_output_gas output_gas : Local_gas_counter.local_gas_counter)
  (H_i_value : Script_typed_ir.With_family.Valid.kinstr i_value)
  (H_ks : Script_typed_ir.With_family.Valid.continuation ks)
  (H_accu : Script_typed_ir.With_family.Valid.stack_value_head accu)
  (H_stack : Script_typed_ir.With_family.Valid.stack_value_tail stack)
  (H_step_constants : Script_typed_ir.Step_constants.Valid.t
     (snd g_value))
  {struct fuel} :
  Script_interpreter.dep_step
    fuel g_value gas i_value ks (accu, stack) =
    Pervasives.Ok (
      dep_output_accu_stack,
      dep_output_ctxt,
      dep_output_gas
    ) ->
  Script_interpreter.step
    g_value gas
      (With_family.to_kinstr i_value)
      (With_family.to_continuation ks)
      (With_family.to_stack_value_head accu)
      (With_family.to_stack_value_tail stack) =
    Pervasives.Ok (
      output_accu,
      output_stack,
      output_ctxt,
      output_gas
    ) ->
  With_family.to_stack_value dep_output_accu_stack =
  (output_accu, output_stack)

with dep_next_eq {s f}
  (fuel : nat)
  (g_value :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (ks : With_family.continuation s f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s)
  (dep_output_accu_stack : With_family.stack_ty_to_dep_Set f)
  (output_accu : Stack_ty.to_Set_head f)
  (output_stack : Stack_ty.to_Set_tail f)
  (dep_output_ctxt output_ctxt : Local_gas_counter.outdated_context)
  (dep_output_gas output_gas : Local_gas_counter.local_gas_counter)
  (H_ks : Script_typed_ir.With_family.Valid.continuation ks)
  (H_accu : Script_typed_ir.With_family.Valid.stack_value_head accu)
  (H_stack : Script_typed_ir.With_family.Valid.stack_value_tail stack)
  (H_step_constants : Script_typed_ir.Step_constants.Valid.t
     (snd g_value))
  {struct fuel} :
  Script_interpreter.dep_next
    fuel g_value gas ks (accu, stack) =
    Pervasives.Ok (
      dep_output_accu_stack,
      dep_output_ctxt,
      dep_output_gas
    ) ->
  Script_interpreter.next
    g_value gas
    (With_family.to_continuation ks)
    (With_family.to_stack_value_head accu)
    (With_family.to_stack_value_tail stack) =
    Pervasives.Ok (
      output_accu,
      output_stack,
      output_ctxt,
      output_gas
    ) ->
    With_family.to_stack_value dep_output_accu_stack =
    (output_accu, output_stack).
Proof.
{ destruct fuel, g_value, gas; [discriminate|]; dep_destruct i_value.
  { grep @With_family.IDrop.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IDup.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISwap.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConst.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_pair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICar.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICdr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IUnpair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_some.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_none.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf_none.
    dep_eq_case.
    step;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IOpt_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set a) (E2 := Ty.to_Set b))
    end.
    step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_left.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_right.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf_left.
    dep_eq_case.
    step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_list.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    now constructor.
  }
  { grep @With_family.INil.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
    constructor.
  }
  { grep @With_family.IIf_cons.
    dep_eq_case.
    step;
      simpl in *; Tactics.destruct_pairs;
      inversion H_accu;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IList_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_4 (T := T)
        (E1 := Stack_ty.to_Set_head s)
        (E2 := Stack_ty.to_Set_tail s)
        (E3 := Ty.to_Set a)
        (E4 := Ty.to_Set b)
      )
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; hauto l: on.
  }
  { grep @With_family.IList_iter.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.IList_size.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    simpl.
    unfold Script_int.N.Valid.t.
    lia.
  }
  { grep @With_family.IEmpty_set.
    with_strategy opaque [Script_set.dep_empty] dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
    { hauto l: on
        use: Script_typed_ir.With_family.is_Comparable_implies_family.
    }
    { now apply Script_set.dep_empty_eq. }
  }
  { grep @With_family.ISet_iter.
    with_strategy opaque [Script_set.dep_fold] dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_3 (T := T))
    end.
    unfold Script_set.dep_fold, Script_set.fold,
      With_family.Script_Set; cbn.
    with_strategy transparent [V0.Map.Make] unfold V0.Map.Make; cbn.
    unfold V0.Map.Make.fold.
    repeat rewrite <- rev_alt.
    unfold Script_interpreter_defs.id, V0.List.fold_left.
    repeat rewrite <- fold_left_rev_right.
    unfold V0.List.map in * |-.
    rename_by_type (Script_family.Ty.is_Comparable _)
      into H_comparable.
    rename_by_type (Forall _ _) into H_Forall.
    apply List.Forall_P_rev in H_Forall.
    rewrite List.rev_eq in H_Forall.
    rewrite <- map_rev in H_Forall.
    revert H_Forall.
    set (accu' := Lists.List.rev accu).
    intros ?.
    apply_dep_next_eq dep_next_eq.
    { repeat match goal with
      | |- _ /\ _ => split
      | |- _ => try assumption
      end.
      rewrite <- List.rev_eq; apply List.Forall_P_rev.
      induction accu'; [constructor|].
      cbn. step. destruct (With_family.of_value) eqn:H_of_value.
      { constructor.
        { inversion H_Forall as [|? ? H_Option].
          rewrite H_of_value in H_Option. now cbn in H_Option.
        }
        { apply IHaccu'. now inversion H_Forall.
        }
      }
      { now pose proof
          Script_typed_ir.With_family.of_value_comparable_none_neq
          (ty := a) k H_comparable as H_none_neq.
      }
    }
    { cbn. f_equal.
      unfold V6.List.map.
      rewrite map_rev. f_equal.
      induction accu'; [reflexivity|]. cbn. 
      step; destruct (With_family.of_value _) eqn:H_of_value; cbn.
      { f_equal.
        { pose proof Script_typed_ir.With_family.to_value_of_value
            (ty := a) k H_comparable as H_to_value_of_value.
          now rewrite H_of_value in H_to_value_of_value.
        }
        { apply IHaccu'. now inversion H_Forall.
        }
      }
      { now pose proof
          Script_typed_ir.With_family.of_value_comparable_none_neq
          (ty := a) k H_comparable as H_none_neq.
      }
    }
  }
  { grep @With_family.ISet_mem.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISet_update.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    { now apply Script_set.dep_update_is_valid. }
    { apply Script_set.dep_update_eq. }
  }
  { grep @With_family.ISet_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    unfold Script_int.N.Valid.t; lia.
  }
  { grep @With_family.IEmpty_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set b) (E2 := Ty.to_Set c))
    end.
    rewrite <- Script_map.dep_empty_eq by easy.
    apply_dep_step_eq dep_step_eq.
    simpl.
    hauto l: on use: Script_typed_ir.With_family.is_Comparable_implies_family.
  }
  { grep @With_family.IMap_map.
    dep_eq_case.
    fold Ty.to_Set_aux in *.
    simpl in *; Tactics.destruct_pairs.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_5 (T := T) (E5 := Ty.to_Set c))
    end.
    with_strategy transparent [Map.Make]
      repeat (unfold Script_map.dep_fold, With_family.Script_Map,
        Map.Make, Make.fold, Make.bindings, Make.empty in *;
      cbn in *).
    match goal with
    | |- context [Ty.to_Set_aux ?pset _] =>
        set (parametrized_set := pset)
    end.
    pose proof
      Script_typed_ir.With_family.of_value_comparable_none_neq
      (ty := a) as H_none_neq.
    rewrite List.fold_left_eq.
    repeat rewrite List.fold_left_rev_right_eq,
      <- Coq.Lists.List.rev_alt,
      List.fold_right_eq.
    apply_dep_next_eq dep_next_eq;
    match goal with
    | H : Forall _ _ |- _ =>
        rename H into H_Forall
    end;
    match goal with
    | H : Script_family.Ty.is_Comparable _ |- _ =>
        rename H into H_comparable
    end.
    { split; [easy|].
      split;
        [|do 2 split; try easy; constructor].
      apply List.Forall_P_rev in H_Forall.
      induction (rev _) as [|? ? IHl1].
      { constructor. }
      { intros.
        rewrite <- List.rev_eq.
        apply List.Forall_P_rev.
        apply List.Forall_P_rev in IHl1;
          [|now apply List.Forall_cons in H_Forall].
        rewrite List.rev_eq,
         rev_involutive in IHl1.
        cbn in *.
        step.
        specialize (H_none_neq k H_comparable).
        apply List.Forall_head in H_Forall.
        ez destruct (With_family.of_value _).
        now constructor.
      }
    }
    { cbn.
      rewrite Map.map_list_eq; unfold List.map.
      repeat rewrite List.rev_eq.
      rewrite List.fold_right_rev_map_eq.
      rewrite Coq.Lists.List.map_rev.
      unfold Script_interpreter_defs.id.
      repeat f_equal.
      induction (Lists.List.rev _) as [|? ? IHlst]; [easy|]; cbn.
      Tactics.destruct_pairs.
      specialize (H_none_neq k H_comparable).
      ez destruct (With_family.of_value _) eqn:H_of_value.
      rewrite IHlst.
      repeat f_equal.
      pose proof
         (@Script_typed_ir.With_family.to_value_of_value
            _ k H_comparable) as
         H_to_value_of_value.
      now rewrite H_of_value in H_to_value_of_value.
    }
  }
  { grep @With_family.IMap_iter.
    dep_eq_case.
    repeat Tactics.destruct_pairs.
    fold @Ty.to_Set_aux.
    match goal with
    | |- context [Ty.to_Set_aux ?P] =>
        set (parametrized_set := P)
    end.
    unfold Script_map.dep_fold, Script_map.fold in *; cbn.
    unfold With_family.Script_Map in *; cbn in *.
    with_strategy transparent [V6.Map.Make]
     unfold V6.Map.Make, Make.functor, Make.fold, Make.bindings,
      V0.List.fold_left in *; cbn in *.
    rewrite Map.map_list_eq.
    repeat rewrite <- rev_alt,
     List.fold_left_eq.
    rewrite List.fold_left_map_eq.
    repeat rewrite List.fold_left_rev_right_eq,
      List.fold_right_eq, List.rev_eq.
    rename_by_type (Forall _ _) into H_Forall.
    rename_by_type (Script_family.Ty.is_Comparable _)
      into H_comparable.
    apply List.Forall_rev in H_Forall.
    set (accu' := Lists.List.rev accu).
    revert H_Forall; change (Lists.List.rev accu) with accu'.
    intros ?. apply_dep_next_eq dep_next_eq.
    { repeat match goal with
        | |- _ /\ _ => split
        | |- _ => try assumption
        end.
      rewrite <- List.rev_eq; apply List.Forall_P_rev.
      induction accu'; [constructor|]. cbn. step.
      destruct (With_family.of_value k) eqn:H_of_value.
      { inversion H_Forall as [|? ? H_Option_Forall]. constructor.
        { rewrite H_of_value in H_Option_Forall.
          now cbn in H_Option_Forall.
        }
        { now apply IHaccu'.
        }
      }
      { now pose proof
          Script_typed_ir.With_family.of_value_comparable_none_neq
          (ty := a) k H_comparable.
      }
    }
    { cbn. unfold Script_interpreter_defs.id. repeat f_equal.
      unfold V6.List.map. rewrite map_rev. f_equal.
      induction accu'; cbn; [reflexivity|]. cbn. step.
      destruct (With_family.of_value _) eqn:H_of_value.
      { cbn. repeat f_equal.
        { pose proof Script_typed_ir.With_family.to_value_of_value
            (ty := a) k H_comparable as H_to_value_of_value.
          now rewrite H_of_value in H_to_value_of_value.
        }
        { apply IHaccu'. now inversion H_Forall.
        }
      }
      { now pose proof
          Script_typed_ir.With_family.of_value_comparable_none_neq 
          (ty := a) k H_comparable.
      }
    }
  }
  { grep @With_family.IMap_mem.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    rewrite Script_map.dep_mem_eq.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMap_get.
    dep_eq_case.
    Tactics.destruct_pairs.
    epose proof Script_map.dep_get_eq as H_mem.
    unfold With_family.to_map in H_mem.
    rewrite <- H_mem.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    with_strategy transparent [V6.Map.Make] 
      unfold Script_map.dep_get, With_family.Script_Map,
      V6.Map.Make in *; cbn in *.
    unfold Make.bindings in *.
    specialize (H_mem accu m1).
    rewrite Map.map_list_eq in H_mem.
    rename_by_type (Forall _ _) into H_Forall.
    induction m1 as [|? ? IHm]; cbn in *; [easy|].
    { do 2 step; trivial.
      { inversion H_Forall as [|? ? H_opt].
        apply H_opt.
      }
      { apply IHm; [now inversion H_Forall|].
        apply H_mem.
      }
    }
  }
  { grep @With_family.IMap_update.
    dep_eq_case.
    epose proof Script_map.dep_update_eq as H_update.
    unfold With_family.to_map, Option.map in H_update.
    rewrite <- H_update; clear H_update.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    split; [assumption|].
    with_strategy transparent [V6.Map.Make]
      unfold With_family.Script_Map, V6.Map.Make in *; cbn in *.
    unfold Make.bindings in *.
    unfold Script_map.dep_update.
     with_strategy transparent [V6.Map.Make]
      unfold With_family.Script_Map, V6.Map.Make in *; cbn in *.
    step.
    { apply Map.make_add_preserves_P.
      { rewrite Script_typed_ir.With_family.of_value_to_value
          by assumption; cbn.
        split; assumption.
      }
      { assumption. }
    }
    { now apply Map.make_remove_preserves_P.
    }
  }
  { grep @With_family.IMap_get_and_update.
    dep_eq_case.
    epose proof Script_map.dep_update_eq as H_update.
    unfold With_family.to_map, Option.map in H_update.
    rewrite <- H_update; clear H_update.
    epose proof Script_map.dep_get_eq as H_get.
    unfold With_family.to_map in H_get.
    rewrite <- H_get; clear H_get.
    simpl in *. destruct stack as [head tail].
    destruct tail as [map ?]. Tactics.destruct_pairs.
    rename_by_type (Forall _ _) into H_Forall.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; 
      simpl; unfold Script_map.dep_update, Script_map.dep_get,
      With_family.Script_Map in *; cbn in *;
      with_strategy transparent [Map.Make] unfold Map.Make in *; 
      cbn in *; unfold Make.bindings in *.
    { (* get *)
      induction map as [|? ? IHmap]; cbn; trivial.
      inversion H_Forall as [|? ? H_opt H_Forall'].
      do 2 step; [apply H_opt|constructor|].
      now apply IHmap.
    }
    { (* update *)
      step; cbn.
      { (* add *)
        apply Map.make_add_preserves_P.
        { rewrite Script_typed_ir.With_family.of_value_to_value
            by assumption; cbn.
          split; assumption.
        }
        { assumption. }
      }
      { (* remove *)
        now apply Map.make_remove_preserves_P.
      }
    }
  }
  { grep @With_family.IMap_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    red. unfold abs. lia.
  }
  { grep @With_family.IEmpty_big_map.
    dep_eq_case. Tactics.destruct_pairs.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set b) (E2 := Ty.to_Set c))
    end.
    apply_dep_step_eq dep_step_eq.
    repeat split; [apply Forall_nil | trivial..].
  }
  { grep @With_family.IBig_map_mem.
    dep_eq_case.
    Tactics.destruct_pairs.
    do 2 match goal with
    | |- context [Local_gas_counter.use_gas_counter_in_context o l0 ?f'] =>
        let gas_counter_f := fresh "gas_counter_f" in
        set (gas_counter_f := f'); intros G1 G2; revert G2 G1
    end.
    assert (H_gas_counter_eq : gas_counter_f0 = gas_counter_f).
    { subst gas_counter_f0 gas_counter_f.
      apply FunctionalExtensionality.functional_extensionality; intro.
      now rewrite Script_big_map.dep_big_map_mem_eq.
    }
    rewrite H_gas_counter_eq.
    destruct (Local_gas_counter.use_gas_counter_in_context _ _ _);
      [simpl|discriminate];
      Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IBig_map_get.
    dep_eq_case.
    Tactics.destruct_pairs.
    match goal with
    | |- context[cast_exists ?T ?e] =>
        erewrite (cast_exists_eval_2 (T:=T)
        (E1 := (Ty.to_Set b)))
    end.
    simpl fst. simpl snd.
    assert(Hxx :
    Result.map
      (fun '(v,ctxt,lgc) =>
         (option_map (fun v => With_family.to_value v) v,ctxt, lgc))
      (Local_gas_counter.use_gas_counter_in_context o l0
         (fun ctxt : Alpha_context.context => Script_big_map
          .dep_big_map_get ctxt accu s1)) =
      Local_gas_counter.use_gas_counter_in_context o l0
        (fun ctxt : Alpha_context.context =>
         Script_big_map.get ctxt (With_family.to_value accu)
           (With_family.to_big_map_aux With_family.to_value
              With_family.to_value s1))).
    assert (Y : Script_typed_ir.With_family.Valid.big_map s1) by hauto l: on.
    { clear - l0 o s1 Y.
      unfold Local_gas_counter.use_gas_counter_in_context.
      unfold Result.map.
      pose proof (Script_big_map.dep_big_map_get_eq
                    (Local_gas_counter.update_context l0 o) accu s1 Y).
      unfold With_family.to_big_map in H.
      rewrite <- H. step; [|reflexivity].
      Tactics.destruct_pairs. simpl. reflexivity. }
    rewrite <- Hxx.
    step. Tactics.destruct_pairs; simpl.
    apply_dep_step_eq dep_step_eq.
    assert (Y : Script_typed_ir.With_family.Valid.big_map s1) by hauto l: on.
    apply (dep_big_map_get_gas_counter_is_valid s1 o l0 o0 o1 l1 accu Y).
    hauto l: on.
    simpl. discriminate.
  }
  { grep @With_family.IBig_map_update.
    dep_eq_case.
    Tactics.destruct_pairs.
    simpl fst; simpl snd.
    assert (Similar_functions :
             Result.map
               (fun '(big_map, ctxt, gas) =>
                  (With_family.to_big_map big_map, ctxt, gas))
               (Local_gas_counter.use_gas_counter_in_context o l0
                  (fun ctxt : Alpha_context.context =>
                     Script_big_map.dep_big_map_update ctxt
                       accu o0 s1)) =
               Local_gas_counter.use_gas_counter_in_context o l0
                 (fun ctxt : Alpha_context.context =>
                    Script_big_map.update ctxt (With_family.to_value accu)
                      (match o0 with
                       | Some x => Some (With_family.to_value x)
                       | None => None
                       end)
                      (With_family.to_big_map_aux With_family.to_value
                         With_family.to_value s1))).
    assert (Y : Script_typed_ir.With_family.Valid.big_map s1) by hauto l: on.
    { clear - l0 o s1 Y.
      unfold Local_gas_counter.use_gas_counter_in_context.
      unfold Result.map.
      pose proof (Script_big_map.dep_big_map_update_eq
                    (Local_gas_counter.update_context l0 o) accu o0 s1 Y).
      unfold With_family.to_big_map in H. unfold Option.map in H.
      rewrite <- H. step; [|reflexivity].
      Tactics.destruct_pairs. simpl. reflexivity.
    }
    (* we can not [rewrite <- Simular_functions] right now by unknown reason.
       We make coq to to see that functions are the same by applying some
       actions:
    *)
    intro.
    unfold "let? _ := _ in _" at 1.
    match goal with
    | H : Result.map ?f ?v = ?lgc
      |- context [match ?lgc1 with _ => _ end] =>
        change lgc1 with lgc
    end.
    (* after these actions we can [rewrite <- Simular_functions] *)
    rewrite <- Similar_functions. clear Similar_functions.
    revert H6.
    destruct (Local_gas_counter.use_gas_counter_in_context o l0
               (fun ctxt : Alpha_context.context =>
                  Script_big_map.dep_big_map_update ctxt accu o0 s1))
      eqn:B0;
      [|scongruence].
    Tactics.destruct_pairs;simpl.
    apply_dep_step_eq dep_step_eq.
    assert (Y : Script_typed_ir.With_family.Valid.big_map s1) by hauto l: on.
    eapply dep_big_map_update_gas_counter_is_valid.
    apply Y. apply B0.
  }
  { grep @With_family.IBig_map_get_and_update.
    dep_eq_case.
    Tactics.destruct_pairs.
    simpl fst. simpl snd.
    assert(Hxx :
            Result.map
              (fun '(v', map', ctxt, gas) =>
                 (option_map (fun v => With_family.to_value v) v',
                   With_family.to_big_map map', ctxt, gas))
              (Local_gas_counter.use_gas_counter_in_context o l0
                 (fun ctxt : Alpha_context.context =>
                    Script_big_map.dep_big_map_get_and_update
                      ctxt accu o0 s1)) =
              Local_gas_counter.use_gas_counter_in_context o l0
                (fun ctxt : Alpha_context.context =>
                   Script_big_map.get_and_update ctxt (With_family.to_value
                                                         accu)
                     match o0 with
                     | Some x => Some (With_family.to_value x)
                     | None => None
                     end (With_family.to_big_map_aux With_family.to_value
                            With_family.to_value s1))).
    assert (Y : Script_typed_ir.With_family.Valid.big_map s1) by hauto l: on.
    { clear - l0 o s1 Y.
      unfold Local_gas_counter.use_gas_counter_in_context.
      unfold Result.map.
      pose proof (Script_big_map.dep_big_map_get_and_update_eq
                    (Local_gas_counter.update_context l0 o) accu o0 s1 Y).
      unfold With_family.to_big_map in H.
      unfold Option.map in H.
      rewrite <- H.
      step; [|reflexivity].
      Tactics.destruct_pairs.
      unfold option_map.
      unfold With_family.to_big_map. simpl.
      reflexivity.
    }
    (* we can not [rewrite <- Hxx] right now by unknown reason.
       We make coq to to see that functions are the same by applying some
       actions:
    *)
    intro.
    unfold "let? _ := _ in _" at 1.
    match goal with
    | H : Result.map ?f ?v = ?lgc
      |- context [match ?lgc1 with _ => _ end] =>
        change lgc1 with lgc
    end.
    (* after these actions we can [rewrite <- Hxx] *)
    rewrite <- Hxx.
    revert H6.
    destruct (Local_gas_counter.use_gas_counter_in_context o l0
                (fun ctxt : Alpha_context.context =>
                   Script_big_map.dep_big_map_get_and_update
                     ctxt accu o0 s1))
      eqn:R1; [|scongruence].
    Tactics.destruct_pairs; simpl.
    apply_dep_step_eq dep_step_eq.
    admit. admit. (* @TODO
                    - add validity for o2 = Some x (updated value).
                    - add validity for b0 - big_map which was updated.
                    write Lemma which says that within Local_gas_counter...
                    [get_and_update] will return valid map,
                    if initial map was valid. *)
  }
  { grep @With_family.IConcat_string.
    dep_eq_case.
    unfold Script_string.concat.
    destruct accu; simpl.
    match goal with
    | |- context [List.map ?f elements] => replace (List.map f elements) with elements
      by (induction elements; hauto q: on)
    end.
    do 2 match goal with
    | |- context [fold_left ?f ?acc ?l] =>
        let name := fresh "fold" in
        set (name := fold_left f acc l)
    end.
    replace fold with fold0 by
      (subst fold0 fold;
      induction elements; [easy|simpl];
      hauto lq: on).
    set (local_gas := Local_gas_counter.consume _ _).
    destruct local_gas; [simpl|discriminate].
    set (string_tag := Script_string.String_tag _).
    destruct string_tag.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConcat_string_pair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISlice_string.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    unfold of_int.
    destruct_all Script_int.num; simpl.
    match goal with
    | |- context [if ?e then _ else _] =>
        destruct e eqn:H_cond
    end; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IString_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    unfold Script_int.N.Valid.t; lia.
  }
  { grep @With_family.IConcat_bytes.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    match goal with
    | |- context [List.map ?f elements] => replace (List.map f elements) with elements
      by (induction elements; hauto q: on)
    end.
    do 2 match goal with
    | |- context [fold_left ?f ?acc ?l] =>
        let name := fresh "fold" in
        set (name := fold_left f acc l)
    end.
    replace fold with fold0 by
      (subst fold0 fold;
      induction elements; [easy|simpl];
      hauto lq: on).
    set (local_gas := Local_gas_counter.consume _ _).
    destruct local_gas; [simpl|discriminate].
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConcat_bytes_pair.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISlice_bytes.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    unfold of_int.
    destruct_all Script_int.num; simpl.
    match goal with
    | |- context [if ?e then _ else _] =>
        destruct e eqn:H_cond
    end; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IBytes_size.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq; simpl.
    unfold Script_int.N.Valid.t; lia.
  }
  { grep @With_family.IAdd_seconds_to_timestamp.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_timestamp_to_seconds.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISub_timestamp_seconds.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IDiff_timestamps.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_tez.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl; step; simpl; [|discriminate].
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    match goal with
    | H_eq : Tez_repr.op_plusquestion ?t1 ?t2 = _ |- _ =>
      pose proof (Tez_repr.op_plusquestion_is_valid (t1 := t1) (t2 := t2))
        as H_tez_valid;
      rewrite H_eq in H_tez_valid
    end.
    hauto drew: off.
  }
  { grep @With_family.ISub_tez.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    unfold Alpha_context.Tez.sub_opt.
    destruct_all Tez_repr.t; simpl.
    destruct (_ <=? _) eqn:?; apply_dep_step_eq dep_step_eq; simpl.
    lia.
  }
  { grep @With_family.ISub_tez_legacy.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl; step; simpl; [|discriminate].
    apply_dep_step_eq dep_step_eq.
    simpl.
    match goal with
    | H : Tez_repr.op_minusquestion ?t1 ?t2 = _ |- _ =>
      pose proof (@Tez_repr.op_minusquestion_is_valid t1 t2) as H_valid;
      rewrite H in H_valid
    end.
    hauto lq: on.
  }
  { grep @With_family.IMul_teznat.
    dep_eq_case.
    Tactics.destruct_pairs.
    destruct_all Script_int.num.
    unfold Script_int.to_int64, Z.to_int64; simpl.
    destruct ((_ && _)%bool) eqn:? in |- *.
    { rewrite Option.catch_no_errors.
      match goal with
      | |- context[Tez_repr.op_starquestion ?a ?b] =>
        pose proof (Tez_repr.op_starquestion_is_valid a b) as H_mul
      end.
      destruct Tez_repr.op_starquestion; simpl; [|discriminate].
      apply dep_step_eq; try easy; simpl.
      apply H_mul; lia.
    }
    { rewrite Option.catch_raise.
      discriminate.
    }
  }
  { grep @With_family.IMul_nattez.
    dep_eq_case.
    Tactics.destruct_pairs.
    destruct_all Script_int.num.
    unfold Script_int.to_int64, Z.to_int64.
    destruct ((_ && _)%bool) eqn:? in |- *.
    { rewrite Option.catch_no_errors.
      match goal with
      | |- context[Tez_repr.op_starquestion ?a ?b] =>
        pose proof (Tez_repr.op_starquestion_is_valid a b) as H_mul
      end.
      destruct Tez_repr.op_starquestion; simpl; [|discriminate].
      apply dep_step_eq; try easy; simpl.
      apply H_mul; lia.
    }
    { rewrite Option.catch_raise.
      discriminate.
    }
  }
  { grep @With_family.IEdiv_teznat.
    with_strategy opaque [Script_int.ediv] dep_eq_case.
    Tactics.destruct_pairs.
    destruct_all Script_int.num.
    destruct_all Tez_repr.t.
    with_strategy opaque [Script_int.ediv] simpl.
    unfold Script_int.of_int64, Z.of_int64.
    match goal with
    | H_tez : Tez_repr.Valid.t _, H_num : Script_int.N.Valid.t _ |-
      context[
        Script_int.ediv (Script_int.Num_tag ?a) (Script_int.Num_tag ?b)
      ] =>
      pose proof (Script_int.ediv_teznat_is_valid a b H_tez H_num) as H_ediv
    end.
    destruct Script_int.ediv as [[[quo] [rem]]|]; [|now apply dep_step_eq].
    simpl in H_ediv.
    repeat rewrite Script_int.to_int64_eq by lia.
    repeat rewrite Tez_repr.of_mutez_eq by lia.
    apply dep_step_eq; try easy; lia.
  }
  { grep @With_family.IEdiv_tez.
    with_strategy opaque [Script_int.ediv_n] dep_eq_case.
    unfold Script_int.ediv_n.
    Tactics.destruct_pairs.
    destruct_all Tez_repr.t.
    with_strategy opaque [Script_int.ediv] simpl.
    unfold Script_int.of_int64, Z.of_int64.
    unfold Tez_repr.Valid.t in *.
    repeat match goal with
    | |- context[Z.abs ?z] => replace (Z.abs z) with z by lia
    end.
    match goal with
    | H_a : Tez_repr.Repr.Valid.t ?a, H_b : Tez_repr.Repr.Valid.t ?b |-
      context[
        Script_int.ediv (Script_int.Num_tag ?a) (Script_int.Num_tag ?b)
      ] =>
      pose proof (Script_int.ediv_tez_is_valid a b H_a H_b) as H_ediv
    end.
    repeat match goal with
    | |- context[Z.abs ?z] => replace (Z.abs z) with z by lia
    end.
    destruct Script_int.ediv as [[[quo] [rem]]|]; [|now apply dep_step_eq].
    simpl in H_ediv.
    repeat rewrite Script_int.to_int64_eq by lia.
    repeat rewrite Tez_repr.of_mutez_eq by lia.
    apply dep_step_eq; simpl; try easy; lia.
  }
  { grep @With_family.IOr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAnd.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IXor.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all bool; simpl; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INot.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIs_nat.
    dep_eq_case.
    destruct Script_int.is_nat eqn:H_is_nat;
      apply_dep_step_eq dep_step_eq.
    destruct_all Script_int.num.
    revert H_is_nat.
    unfold Script_int.is_nat.
    cbn. unfold Z.zero.
    step; [easy|].
    inj. rewrite <- Hinj.
    unfold Script_int.N.Valid.t; lia.
  }
  { grep @With_family.INeg.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAbs_int.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.abs,
      Script_int.N.Valid.t.
    destruct accu; cbn.
    lia.
  }
  { grep @With_family.IInt_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_int.num; simpl.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.N.Valid.t, Script_int.add_n in *;
      destruct accu, n; cbn.
    lia.
  }
  { grep @With_family.ISub_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    cbn.
    destruct a; cbn; [constructor|].
    unfold Script_int.N.Valid.t, Script_int.mul_n in *;
      destruct accu, n; cbn.
    lia.
  }
  { grep @With_family.IEdiv_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_int.num.
    with_strategy opaque [Script_int.ediv] simpl.
    match goal with
    | |-
      context[
        Script_int.ediv (Script_int.Num_tag ?a) (Script_int.Num_tag ?b)
      ] =>
      pose proof (Script_int.ediv_int_is_valid a b) as H_ediv
    end.
    destruct Script_int.ediv as [[[quo] [rem]]|]; [|now apply dep_step_eq].
    simpl in H_ediv.
    now apply dep_step_eq.
  }
  { grep @With_family.IEdiv_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_int.num.
    unfold Script_int.ediv_n.
    with_strategy opaque [Script_int.ediv] simpl.
    destruct_all Ty.Num.t.
    { match goal with
      | |-
        context[
          Script_int.ediv (Script_int.Num_tag ?a) (Script_int.Num_tag ?b)
        ] =>
        pose proof (Script_int.ediv_int_is_valid a b) as H_ediv
      end.
      destruct Script_int.ediv as [[[quo] [rem]]|]; [|now apply dep_step_eq].
      simpl in H_ediv.
      now apply dep_step_eq.
    }
    { match goal with
    | |-
        context[
          Script_int.ediv (Script_int.Num_tag ?a) (Script_int.Num_tag ?b)
        ] =>
        pose proof (Script_int.ediv_nat_is_valid a b) as H_ediv
      end.
      destruct Script_int.ediv as [[[quo] [rem]]|]; [|now apply dep_step_eq].
      simpl in H_ediv.
      apply dep_step_eq; try easy.
      apply H_ediv; lia.
    }
  }
  { grep @With_family.ILsl_nat.
    dep_eq_case.
    Tactics.destruct_pairs.
    cbn.
    ez destruct (Script_int.shift_left_n _ _) as [n_shifted|]
      eqn:H_shift_left.
    apply_dep_step_eq dep_step_eq.
    revert H_shift_left.
    unfold Script_int.shift_left_n.
    unfold Script_int.shift_left.
    unfold of_int.
    destruct accu as [accu], n as [n].
    cbn.
    step; [easy|].
    inj; subst. red.
    destruct (Z.gtb_spec (Z.compare n 256) 0) as [|H_bool]; [easy|].
    apply Z.compare_le_0_impl in H_bool. clear Heqb.
    rename_by_type (Script_int.N.Valid.t (_ n)) into H_n.
    red. red in H_accu, H_n.
    rewrite Z.to_int_eq by lia.
    unfold shift_left.
    now apply Z.shiftl_nonneg.
  }
  { grep @With_family.ILsr_nat.
    dep_eq_case.
    Tactics.destruct_pairs. cbn.
    ez destruct (Script_int.shift_right_n _ _) as [n_shifted|] eqn:H_shift_right.
    apply_dep_step_eq dep_step_eq.
    revert H_shift_right.
    unfold Script_int.shift_right_n.
    unfold Script_int.shift_right.
    destruct accu as [accu], n as [n]. cbn.
    unfold of_int.
    unfold Z.compare.
    destruct (_ <? _) eqn:?; simpl.
    2: destruct (_ =? _) eqn:?; simpl; [|discriminate].
    all: rewrite Z.to_int_eq by lia.
    all: intro H_some_eq; inversion_clear H_some_eq.
    all: now apply Z.shift_right_pos.
  }
  { grep @With_family.IOr_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.logor.
    destruct accu as [accu], n as [n].
    rename_by_type (Script_int.N.Valid.t (_ n)) into H_n.
    red. red in H_accu, H_n.
    red.
    now apply Z.logor_pos.
  }
  { grep @With_family.IAnd_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    destruct accu as [accu], n as [n].
    unfold Script_int.logand.
    rename_by_type (Script_int.N.Valid.t (_ n)) into H_n.
    red. red in H_accu, H_n.
    apply Z.logand_pos; now left.
  }
  { grep @With_family.IAnd_int_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    destruct accu as [accu], n as [n].
    unfold Script_int.logand.
    rename_by_type (Script_int.N.Valid.t (_ n)) into H_n.
    red. red in H_n.
    apply Z.logand_pos.
    now right.
  }
  { grep @With_family.IXor_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.logxor.
    destruct accu as [accu], n as [n].
    rename_by_type (Script_int.N.Valid.t (_ n)) into H_n.
    red. red in H_accu, H_n.
    apply Z.logxor_pos.
    now split.
  }
  { grep @With_family.INot_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf.
    dep_eq_case.
    repeat step;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILoop.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.ILoop_left.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.IDip.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IExec.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    match goal with
    | code : With_family.kdescr.skeleton _ _ _ _ |- _ =>
      destruct code; simpl
    end.
    apply_dep_step_eq dep_step_eq; hauto l: on.
  }
  { grep @With_family.IApply.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Script_interpreter_defs.dep_apply_eq.
    destruct Script_interpreter_defs.dep_apply eqn:H_apply; [|discriminate].
    simpl in *; Tactics.destruct_pairs.
    simpl.
    apply_dep_step_eq dep_step_eq.
    now eapply Script_interpreter_defs.dep_apply_is_valid; try apply H_apply.
  }
  { grep @With_family.ILambda.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IFailwith.
    dep_eq_case.
    destruct trace_value; simpl; Tactics.destruct_pairs; discriminate.
  }
  { grep @With_family.ICompare.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T))
    end.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Script_comparable.dep_compare_comparable_eq by assumption.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IEq.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeq.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILt.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IGt.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILe.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IGe.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAddress.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_typed_ir.typed_contract.
    step; simpl.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IContract.
    dep_eq_case.
    simpl in *.
    destruct (String.compare _ _ =? 0); [|destruct (String.compare _ _ =? 0)].
    3: apply_dep_step_eq dep_step_eq.
    all: rewrite <- Script_ir_translator.dep_parse_contract_for_script_eq
      by easy.
    all: destruct Script_ir_translator.dep_parse_contract_for_script
      eqn:H_parse_contract;
      simpl; [|discriminate].
    all: simpl in *; Tactics.destruct_pairs; simpl.
    all: destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    all: apply_dep_step_eq dep_step_eq.
    all: step; trivial; step.
    all: now apply Script_ir_translator.dep_parse_contract_for_script_is_valid
      in H_parse_contract.
  }
  { grep @With_family.IView.
    dep_eq_case.
    admit.
  }
  { grep @With_family.ITransfer_tokens.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IImplicit_account.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICreate_contract.
    dep_eq_case.
    Tactics.destruct_pairs; cbn in *.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_3 (T := T))
    end.
    repeat (step; cbn); try discriminate;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISet_delegate.
    dep_eq_case.
    destruct Alpha_context.fresh_internal_nonce; simpl; [|discriminate].
    simpl in *; Tactics.destruct_pairs.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    destruct accu; simpl; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INow.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMin_block_time.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IBalance.
    dep_eq_case.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ILevel.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ICheck_signature.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IHash_key.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IPack.
    dep_eq_case.
    do 2 match goal with
    | |- context [Local_gas_counter.use_gas_counter_in_context _ _ ?f'] =>
        let gas_counter_f := fresh "gas_counter_f" in
        set (gas_counter_f := f'); intros H1 H2; revert H2 H1
    end.
    assert (H_gas_counter_eq : gas_counter_f0 = gas_counter_f).
    { subst gas_counter_f0 gas_counter_f.
      apply FunctionalExtensionality.functional_extensionality; intro.
      now rewrite Script_ir_translator.dep_pack_data_eq.
    }
    rewrite H_gas_counter_eq.
    destruct (Local_gas_counter.use_gas_counter_in_context _ _ _);
      [simpl|discriminate];
      Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IUnpack.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    unfold Local_gas_counter.use_gas_counter_in_context.
    rewrite <- Script_interpreter_defs.dep_unpack_eq.
    destruct Script_interpreter_defs.dep_unpack eqn:H_unpack_eq; simpl;
      [|discriminate].
    Tactics.destruct_pairs.
    cbn - [Script_interpreter.step].
    apply_dep_step_eq dep_step_eq.
    step; [|trivial].
    apply (Script_interpreter_defs.dep_unpack_is_valid _ _ _ _ _ H_unpack_eq).
  }
  { grep @With_family.IBlake2b.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha256.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha512.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISource.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISender.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISelf.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISelf_address.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAmount.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ISapling_empty_state.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISapling_verify_update.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Sapling.verify_update; simpl; [|discriminate].
    Tactics.destruct_pairs.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    step; Tactics.destruct_pairs; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISapling_verify_update_deprecated.
    dep_eq_case. simpl in *; Tactics.destruct_pairs. cbn. 
    step; [|discriminate]; cbn. Tactics.destruct_pairs.
    do 2 step; [step|]; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IDig.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_5 (T := T))
    end.
    admit.
  }
  { grep @With_family.IDug.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IDipn.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IDropn.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IChainId.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INever.
    dep_eq_case.
    step.
  }
  { grep @With_family.IVoting_power.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Vote.get_voting_power eqn:?; simpl; [|discriminate].
    Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.N.Valid.t. lia.
  }
  { grep @With_family.ITotal_voting_power.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Vote.get_total_voting_power; simpl; [|discriminate].
    Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int.N.Valid.t. lia.
  }
  { grep @With_family.IKeccak.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha3.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_g1.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_g2.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_g1.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_g2.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_z_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_fr_z.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IInt_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_g1.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_g2.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IPairing_check_bls12_381.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    set (elements := accu.(Script_typed_ir.boxed_list.elements)).
    replace (List.map _ _) with elements by (induction elements; hauto q: on).
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IComb.
    dep_eq_case.
    match goal with
    | |- context[let '(a,b) := (?e _ _ _ _ _ _ _ _) in
                   Script_interpreter.step _ _ _ _ a b] =>
      set (aux := e)
    end.
    destruct c.
    { cbn in *. destruct s.
      { admit. (* TODO: dependent type issues *) }
      { destruct stack. simpl.
        match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_5 (T := T))
        end.
        simpl. rewrite cast_eval. apply_dep_step_eq dep_step_eq.
      }
    }
    { simpl cast_exists.
      match goal with
      | |- context[cast_exists ?T ?e] =>
        erewrite (cast_exists_eval_5 (T := T))
      end.
      match goal with
      | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
        match goal with
        | |- context[let '_ := ?a in _] =>
          assert (With_family.to_stack_value v = a) as Hto_stack_value
        end
      end.
      { clear. revert c. dependent induction c.
        { cbn in *.
          match goal with
          | |- context[cast_exists ?T ?e] =>
            erewrite (cast_exists_eval_3 (T := T) (x := e))
          end.
          simpl. rewrite cast_eval, cast_eval. now destruct stack.
        }
        { simpl.
          match goal with
          | |- context[cast_exists ?T ?e] =>
            erewrite (cast_exists_eval_3 (T := T) (x := e))
          end.
          destruct stack eqn:stack_eq. simpl in IHc.
          specialize IHc
            with (accu := accu) (stack := m0) (b := b0) (s'0:= s')
                 (c0 := c).
          dependent destruction c.
          { cbn.
            match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_3 (T := T) (x := e))
            end.
            cbn. repeat rewrite cast_eval. now step.
          }
          { destruct m0. simpl cast_exists in IHc. revert IHc.
            match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_3 (T := T) (x := e))
            end.
            simpl Script_interpreter.eval_comb_gadt_witness. step. step. intro.
            simpl.
            match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_3 (T := T) (x := e))
            end.
            step. rewrite Heqp in IHc. simpl in *. repeat rewrite cast_eval.
            rewrite cast_eval in IHc. destruct t0.
            repeat specialize (IHc JMeq_refl). inversion IHc.
            now inversion Heqt.
          }
        }
      }
      { rewrite <- Hto_stack_value. clear Hto_stack_value.
        match goal with
        | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
          assert (Script_typed_ir.With_family.Valid.stack_value v)
            as Hvalid_stack_value
        end.
        { revert c H_accu H_stack. clear.
          dependent induction c; simpl in *; intros.
          { hauto l: on. }
          { destruct stack.
            specialize IHc
              with (accu := accu) (stack := p) (b := b0) (s'0:= s')
                   (c0 := c).
            hauto l: on.
          }
        }
        { destruct Script_interpreter.eval_comb_gadt_witness. cbn.
          apply_dep_step_eq dep_step_eq; hauto l: on.
        }
      }
    }
  }
  { grep @With_family.IUncomb.
    dep_eq_case.
    match goal with
    | |- context[let '(a,b) := (?e _ _ _ _ _ _ _ _) in
                   Script_interpreter.step _ _ _ _ a b] =>
      set (aux := e)
    end.
    destruct u0.
    { cbn in *. destruct s.
      { admit. (* TODO: dependent type issues *) }
      { destruct stack. simpl.
        match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_5 (T := T))
        end.
        simpl. rewrite cast_eval. apply_dep_step_eq dep_step_eq.
      }
    }
    { simpl cast_exists.
      match goal with
      | |- context[cast_exists ?T ?e] =>
        erewrite (cast_exists_eval_5 (T := T))
      end.
      match goal with
      | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
        match goal with
        | |- context[let '_ := ?a in _] =>
          assert (With_family.to_stack_value v = a) as Hto_stack_value
        end
      end.
      { clear. revert u0. dependent induction u0.
        { cbn in *.
          match goal with
          | |- context[cast_exists ?T ?e] =>
            erewrite (cast_exists_eval_4 (T := T) (x := e))
          end.
          step. simpl. now rewrite cast_eval, cast_eval.
        }
        { simpl. step. step.
          match goal with
          | |- context[cast_exists ?T' ?e] => set (T := T'); set (x := e)
          end.
          erewrite (cast_exists_eval_4 (T := T) (x := x)).
          rewrite cast_eval. simpl in *. f_equal.
          specialize IHu0
            with (b := b0) (s0 := s) (accu := (t, t2)) (stack := stack)
                 (u1 := u0).
          simpl in IHu0. clear x. revert u0 IHu0. clear.
          dependent destruction u0.
          { cbn. intro IHu0.
            match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_4 (T := T) (x := e))
            end.
            cbn. now repeat rewrite cast_eval.
          }
          { simpl aux.
            match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_4 (T := T) (x := e))
            end.
            match goal with
            | |- context[cast_exists ?T' ?e] => set (T := T'); set (x := e)
            end.
            erewrite (cast_exists_eval_4 (T := T) (x := x)).
            rewrite cast_eval, cast_eval. intro IHu0. f_equal.
            repeat specialize (IHu0 JMeq_refl). now inversion IHu0.
          }
        }
      }
      { rewrite <- Hto_stack_value. clear Hto_stack_value.
        match goal with
        | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
          assert (Script_typed_ir.With_family.Valid.stack_value v)
            as Hvalid_stack_value
        end.
        { revert u0 H_accu H_stack. clear.
          dependent induction u0; simpl in *; intros.
          { hauto l: on. }
          { destruct accu. destruct t0.
            specialize IHu0
              with (b := b0) (s0:= s) (accu := (t, t1)) (stack := stack)
                   (u1 := u0).
            hauto l: on.
          }
        }
        { destruct Script_interpreter.eval_uncomb_gadt_witness. cbn.
          destruct accu.
          apply_dep_step_eq dep_step_eq; hauto l: on.
        }
      }
    }
  }
  { grep @With_family.IComb_get.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_1 (T := T))
    end.
    match goal with
    | |- context[Script_interpreter.step _ _ _ _ (?e _ _ _ _)] =>
      set (aux := e)
    end.
    assert (@With_family.to_stack_value_head (v::s)
              (Script_interpreter.eval_comb_get_gadt_witness c accu) =
            aux (Ty.to_Set t) (Stack_ty.to_Set_head (v :: s))
              (With_family.to_comb_get_gadt_witness c)
              (With_family.to_value accu))
      as Hto_stack_value_head.
    { clear. induction c; cbn; [ now rewrite cast_eval | |]; destruct accu.
      { now match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_1 (T := T))
            end.
      }
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          rewrite (cast_exists_eval_2 (T := T))
        end.
        apply IHc.
      }
    }
    { simpl. rewrite <- Hto_stack_value_head. apply_dep_step_eq dep_step_eq.
      revert H_accu; clear.
      intros; induction c; cbn; trivial; destruct accu; [| apply IHc ];
      hauto lq: on.
    }
  }
  { grep @With_family.IComb_set.
    dep_eq_case.
    destruct stack. cbn.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_4 (T := T))
    end.
    match goal with
    | |- context[Script_interpreter.step _ _ _ _ (?e _ _ _ _ _ _)] =>
      set (aux := e)
    end.
    assert (@With_family.to_stack_value_head (c :: s)
              (Script_interpreter.eval_comb_set_gadt_witness c0 accu t) =
            aux (Ty.to_Set a) (Ty.to_Set b) (Stack_ty.to_Set_head (c :: s))
              (With_family.to_comb_set_gadt_witness c0)
              (With_family.to_value accu) (With_family.to_value t))
      as Hto_stack_value_head.
    { clear. induction c0; cbn; [ now rewrite cast_eval | |]; destruct t.
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_2 (T := T))
        end.
        symmetry. apply cast_eval.
      }
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_3 (T := T))
        end.
        rewrite cast_eval. f_equal. apply IHc0.
      }
    }
    { simpl. rewrite <- Hto_stack_value_head. apply_dep_step_eq dep_step_eq.
      revert H_accu H_stack; clear.
      intros; induction c0; cbn; trivial; hauto lq: on.
    }
  }
  { grep @With_family.IDup_n.
    dep_eq_case.
    destruct d; cbn in *;
    destruct stack;
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_3 (T := T))
    end;
    apply_dep_step_eq dep_step_eq.
    { cbn. symmetry. apply cast_eval. }
    { revert H_accu H_stack. clear. induction d; cbn.
      { intuition. }
      { destruct m0. intros. apply IHd; intuition. }
    }
    { clear. simpl. revert a accu.
      induction d; cbn; intros;
      match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_2 (T := T))
        end.
      { symmetry. apply cast_eval. }
      { destruct m0. simpl. apply IHd. }
    }
  }
  { grep @With_family.ITicket.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    split; [now destruct_all Script_int.num|apply H_accu].
  }
  { grep @With_family.IRead_ticket.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    split; [constructor|].
    destruct H_accu as [H_accu H_accu'].
    split; [apply H_accu'|].
    destruct_all (Script_typed_ir.ticket (With_family.ty_to_dep_Set a)); simpl in * |-.
    destruct_all Script_int.num.
    lia.
  }
  { grep @With_family.ISplit_ticket.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    destruct (_ =? _); apply_dep_step_eq dep_step_eq.
    do 2 split; try easy;
    destruct_all Script_int.num; lia.
  }
  { grep @With_family.IJoin_tickets.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    rewrite <- Script_comparable.dep_compare_comparable_eq by easy.
    repeat (step; simpl); apply_dep_step_eq dep_step_eq;
     split; try easy;
      destruct_all (Script_typed_ir.ticket (With_family.ty_to_dep_Set a)); simpl in *;
      destruct_all Script_int.num; simpl in *; lia.
  }
  { grep @With_family.IOpen_chest.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    repeat step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IHalt.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
}
{ destruct fuel, g_value, gas; [discriminate|];
    dep_destruct ks.
  { grep @With_family.KNil.
    dep_eq_case.
    destruct dep_output_accu_stack; simpl.
    congruence.
  }
  { grep @With_family.KCons.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.KReturn.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KMap_head.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KUndip.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KLoop_in.
    dep_eq_case.
    now destruct_all bool;
      simpl in *; Tactics.destruct_pairs;
      [apply_dep_step_eq dep_step_eq | apply_dep_next_eq dep_next_eq].
  }
  { grep @With_family.KLoop_in_left.
    dep_eq_case.
    now step; [apply_dep_step_eq dep_step_eq | apply_dep_next_eq dep_next_eq].
  }
  { grep @With_family.KIter.
    dep_eq_case.
    step; simpl; [apply_dep_next_eq dep_next_eq | apply_dep_step_eq dep_step_eq]; sauto l: on.
  }
  { grep @With_family.KList_enter_body.
    dep_eq_case.
    destruct l; simpl.
    { rewrite List.rev_map_eq.
      apply_dep_next_eq dep_next_eq; try easy.
      apply List.Forall_P_rev.
      now destruct H_ks as [_ [_ [H_ks _]]].
    }
    { apply_dep_step_eq dep_step_eq.
      { destruct H_ks as [H_ks1 [H_ks2 [H_ks3 H_ks4]]].
        now apply List.Forall_cons in H_ks2. }
      { destruct H_ks as [_ [H_ks [_ _]]].
        now apply List.Forall_head in H_ks. }
    }
  }
  { grep @With_family.KList_exit_body.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; hauto l: on.
  }
  { grep @With_family.KMap_enter_body.
    dep_eq_case.
    step; simpl; Tactics.destruct_pairs;
      [apply_dep_next_eq dep_next_eq|].
    apply_dep_step_eq dep_step_eq;
      [|now apply List.Forall_head in H0].
    split; [easy|].
    let t := type of H0 in assert (H0' : t) by assumption.
    apply List.Forall_cons in H0.
    apply List.Forall_head in H0'.
    split; [easy|].
    split; [easy|].
    split; [|easy].
    apply H0'.
  }
  { grep @With_family.KMap_exit_body.
    dep_eq_case.
    match goal with
    | |- context[
        Script_map.update
          (With_family.to_value ?k)
          (Some (With_family.to_value ?v))
          (With_family.to_map ?m)
      ] =>
      pose proof (Script_map.dep_update_eq k (Some v) m) as H_update
    end.
    with_strategy opaque [Script_map.update] simpl in H_update.
    rewrite <- H_update.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; try easy.
    split; [easy|].
    split; [easy|].
    split; [|easy].
    now apply Script_map.dep_map_add.
  }
  { grep @With_family.KView_exit.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
}
Admitted.

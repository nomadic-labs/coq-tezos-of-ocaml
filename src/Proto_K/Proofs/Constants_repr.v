Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Constants_repr.
Require TezosOfOCaml.Proto_K.Gas_limit_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.List.
Require TezosOfOCaml.Environment.V6.Proofs.Signature.

Require TezosOfOCaml.Proto_K.Proofs.Constants_parametric_repr.
Require TezosOfOCaml.Proto_K.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_K.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_K.Proofs.Round_repr.
Require TezosOfOCaml.Proto_K.Proofs.Seed_repr.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.

(** Encoding [fixed_encoding] is valid. *)
Lemma fixed_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Constants_repr.fixed_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros []; now repeat constructor.
Qed.
#[global] Hint Resolve fixed_encoding_is_valid : Data_encoding_db.

(** The validity predicate for [Constants_repr.t]. *)
Module Valid.
  Definition t (x : Constants_repr.t) : Prop :=
    Constants_parametric_repr.Valid.t x.(Constants_repr.t.parametric).
End Valid.

(** Encoding [encoding] is valid. *)
Lemma encoding_is_valid
  : Data_encoding.Valid.t Valid.t Constants_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Pervasives.
Require TezosOfOCaml.Proto_K.Proofs.Error.

Require TezosOfOCaml.Proto_K.Tx_rollup_gas.
Require TezosOfOCaml.Proto_K.Tx_rollup_errors_repr.

(** Verify the [Internal_error] absense in [hash_value] 
    
    Assuming that the decoding works, and that the returned
    bytes are greater than 0. And also that there is enough
    gas to the finish the operation. [hash_value] returns Ok.
 *)
Lemma hash_value_is_ok {A B : Set} bytes bytes' ctxt encoding (b : B) :
  Binary.to_bytes_opt None encoding b = Some bytes' ->
  Bytes.length bytes' >= 0 ->
  exists cost,  Pervasives.is_ok (Raw_context.consume_gas ctxt cost) ->
  Pervasives.is_ok (Tx_rollup_gas.hash_value (A := A) bytes ctxt encoding b).
Proof.
  intros.
  unfold Tx_rollup_gas.hash_value.
  rewrite H.
  unfold Tx_rollup_gas.hash_cost.
  replace (0 <=i Bytes.length bytes') with true by lia; simpl.
  set (cost := Tx_rollup_gas.S.add _ _).
  exists cost. intros.
  destruct Raw_context.consume_gas; easy.
Qed.

(** [hash_value] only returns [Internal_error] in case of a decoding error *)
Lemma hash_value_no_internal_error {A B : Set} bytes bytes' ctxt encoding (b : B) :
  Binary.to_bytes_opt None encoding b = Some bytes' ->
  Error.No_internal_errors.t
    (Tx_rollup_gas.hash_value (A := A) bytes ctxt encoding b).
Proof.
  intros.
  unfold Tx_rollup_gas.hash_value.
  rewrite H.
  unfold op_gtgtquestion.
  repeat match goal with 
  | |- context [ match ?f _ with | Pervasives.Ok _ => _ | Pervasives.Error _ => _ end ] => unfold f
  end.
  unfold error_unless.
  unfold Raw_context.consume_gas.
   match goal with
   | |- context [ if ?e then _ else _ ] => destruct e
   end; simpl; [|unfold Error_monad.error_value; sfirstorder].
   destruct Raw_context.unlimited_operation_gas;
   destruct (Gas_limit_repr.raw_consume _ _); simpl;
     unfold Error_monad.error_value; sauto.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_big_map.

Require TezosOfOCaml.Environment.V6.Proofs.Map.
Require TezosOfOCaml.Proto_K.Proofs.Gas_comparable_input_size.
Require TezosOfOCaml.Proto_K.Proofs.Script_comparable.
Require Import TezosOfOCaml.Proto_K.Proofs.Script_ir_translator.
Require TezosOfOCaml.Proto_K.Proofs.Script_family.
Require TezosOfOCaml.Proto_K.Proofs.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_big_map.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

(** The simulation [dep_empty] is valid. *)
Lemma dep_empty_eq {a b : Ty.t} (key_type : With_family.ty a)
  (value_type : With_family.ty b) :
  With_family.to_big_map (ty_v := b) (dep_empty_big_map key_type value_type) =
    Script_big_map.empty (a:= Ty.to_Set a) (b:= Ty.to_Set b)
      (With_family.to_ty key_type) (With_family.to_ty value_type).
Proof.
  unfold With_family.to_big_map, With_family.to_big_map_aux,
  Script_big_map.empty. f_equal.
Qed.

(* The simulation [dep_big_map_mem] is valid. *)
Lemma dep_big_map_mem_eq {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (big_map : With_family.big_map a b) :
  Script_typed_ir.With_family.Valid.big_map big_map ->
  dep_big_map_mem ctxt key_value big_map =
  Script_big_map.mem ctxt (With_family.to_value key_value)
    (With_family.to_big_map big_map).
Proof.
  intros [].
  unfold Script_big_map.dep_big_map_mem, mem.
  rewrite dep_hash_comparable_data_eq by easy.
  simpl.
  destruct Script_ir_translator.hash_comparable_data; simpl; [|reflexivity].
  Tactics.destruct_pairs.
  unfold Script_typed_ir.Big_map_overlay.
  rewrite <- Map.find_map.
  destruct (_.(Map.S.find)); simpl; [|reflexivity].
  Tactics.destruct_pairs.
  now step.
Qed.

(** The simulation [dep_big_map_get_by_hash] is valid. *)
Lemma dep_big_map_get_by_hash_eq {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : Script_expr_hash.t)
  (big_map : With_family.big_map a b) :
  (let? '(result, ctxt) := dep_big_map_get_by_hash ctxt key_value big_map in
  return? (Option.map With_family.to_value result, ctxt)) =
  Script_big_map.get_by_hash (B := Ty.to_Set a) ctxt key_value
    (With_family.to_big_map big_map).
Proof.
  unfold dep_big_map_get_by_hash, Script_big_map.get_by_hash.
  simpl.
  rewrite cast_eval.
  simpl.
  unfold Script_typed_ir.Big_map_overlay.
  rewrite <- Map.find_map.
  destruct (_.(Map.S.find)); simpl.
  { Tactics.destruct_pairs; reflexivity. }
  { destruct big_map; simpl.
    destruct id; simpl; [|reflexivity].
    destruct Alpha_context.Big_map.get_opt; simpl; [|reflexivity].
    Tactics.destruct_pairs.
    step; simpl; [|reflexivity].
    unfold Script_ir_translator.parse_data.
    rewrite <- dep_parse_data_aux_eq.
    destruct Script_ir_translator.dep_parse_data_aux; simpl; [|reflexivity].
    hauto lq:on.
}
Qed.

(** The simulation [dep_big_map_get] is valid. *)
Lemma dep_big_map_get_eq {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (big_map : With_family.big_map a b) :
  Script_typed_ir.With_family.Valid.big_map big_map ->
  (let? '(result, ctxt) := dep_big_map_get ctxt key_value big_map in
    return? (Option.map With_family.to_value result, ctxt)) =
  Script_big_map.get ctxt (With_family.to_value key_value)
    (With_family.to_big_map big_map).
Proof.
  Opaque Script_big_map.get_by_hash.
  intros [].
  unfold dep_big_map_get, get; simpl.
  rewrite dep_hash_comparable_data_eq by easy.
  destruct Script_ir_translator.hash_comparable_data; simpl; [|reflexivity].
  Tactics.destruct_pairs.
  now rewrite dep_big_map_get_by_hash_eq.
Qed.

(** The simulation [dep_big_map_update] is valid. *)
Lemma dep_big_map_update_eq {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (value_value : option (With_family.ty_to_dep_Set b))
  (big_map : With_family.big_map a b) :
  Script_typed_ir.With_family.Valid.big_map big_map ->
  (let? '(result, ctxt) := dep_big_map_update ctxt key_value
                             value_value big_map in
   return? (With_family.to_big_map result, ctxt)) =
    Script_big_map.update ctxt (With_family.to_value key_value)
      (Option.map With_family.to_value value_value)
      (With_family.to_big_map big_map).
Proof.
Admitted.

(** The simulation [dep_big_map_get_and_update] is valid. *)
Lemma dep_big_map_get_and_update_eq {a b : Ty.t}
  (ctxt : Alpha_context.context) (key_value : With_family.ty_to_dep_Set a)
  (value_value : option (With_family.ty_to_dep_Set b))
  (function_parameter : With_family.big_map a b) :
  Script_typed_ir.With_family.Valid.big_map function_parameter ->
  (let? '((result, bgm), ctxt) := dep_big_map_get_and_update ctxt key_value value_value
                                  function_parameter in
   return? ((Option.map With_family.to_value result,
       (With_family.to_big_map bgm)), ctxt)) =
    Script_big_map.get_and_update ctxt (With_family.to_value key_value) (Option.map With_family.to_value
      value_value)
       (With_family.to_big_map function_parameter).
Proof.
Admitted.

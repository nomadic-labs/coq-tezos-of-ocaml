Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Main.
Require TezosOfOCaml.Proto_K.Proofs.Block_header_repr.
Require TezosOfOCaml.Proto_K.Proofs.Operation_repr.

(** The encoding [block_header_data_encoding] is valid. *)
Lemma block_header_data_encoding_is_valid :
  Data_encoding.Valid.t Block_header_repr.Protocol_data.Valid.t
    Main.block_header_data_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve block_header_data_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom block_metadata_encoding_is_valid : Data_encoding.Valid.t (fun _ => True)
                        Apply_results.block_metadata_encoding.
#[global] Hint Resolve block_metadata_encoding_is_valid : Data_encoding_db.

(** The encoding [block_header_metadata_encoding] is valid. *)
Lemma block_header_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Main.block_header_metadata_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve block_header_metadata_encoding_is_valid :
  Data_encoding_db.

(** The encoding [operation_data_encoding] is valid. *)
Lemma operation_data_encoding_is_valid :
  Data_encoding.Valid.t Operation_repr.Packed_protocol_data.Valid.t
    Main.operation_data_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_data_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom operation_metadata_encoding_is_valid : Data_encoding.Valid.t (fun _ => True)
                        Apply_results.operation_metadata_encoding.
#[global] Hint Resolve operation_metadata_encoding_is_valid : Data_encoding_db.

(** The encoding [operation_receipt_encoding] is valid. *)
Lemma operation_receipt_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Main.operation_receipt_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_receipt_encoding_is_valid : Data_encoding_db.

(* Proved in [Proofs/Apply_results.v] *)
Axiom operation_data_and_metadata_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Apply_results.operation_data_and_metadata_encoding.
#[global] Hint Resolve operation_data_and_metadata_encoding_is_valid :
  Data_encoding_db.

(** The encoding [operation_data_and_receipt_encoding] is valid. *)
Lemma operation_data_and_receipt_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Main.operation_data_and_receipt_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve operation_data_and_receipt_encoding_is_valid :
  Data_encoding_db.

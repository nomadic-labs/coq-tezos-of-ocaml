Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require Import TezosOfOCaml.Proto_K_alpha.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K_alpha.Proofs.Script_interpreter_defs.

Module Old := TezosOfOCaml.Proto_K.Simulations.Script_interpreter.
Module New := TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter.

Module Make_single_data_storage.
  (** Migration of single data storage values *)
  Definition migrate_value
     {key value : Set}
     `{HFargs0 : Storage_functors.Make_single_data_storage.FArgs
         (C_t := (Raw_context.New.t * key)) (V_t := value)}
     `{HFArgs1 : Proto_K.Storage_functors.Make_single_data_storage.FArgs
         (C_t := (Proto_K.Raw_context.t * key)) (V_t := value)} :
      M? Proto_K.Storage_functors.Make_single_data_storage.V
        .(Proto_K.Storage_sigs.VALUE.t) ->
      M? Storage_functors.Make_single_data_storage.V.(Storage_sigs.VALUE.t).
    Admitted.
End Make_single_data_storage.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Level_repr.
Require TezosOfOCaml.Proto_alpha.Level_repr.

Module Old := TezosOfOCaml.Proto_K.Level_repr.
Module New := TezosOfOCaml.Proto_alpha.Level_repr.

(** Migrate [Level_repr.t] *)
Definition migrate (l : Old.t) : New.t :=
{|
  New.t.level := l.(Old.t.level);
  New.t.level_position := l.(Old.t.level_position);
  New.t.cycle := l.(Old.t.cycle);
  New.t.cycle_position := l.(Old.t.cycle_position);
  New.t.expected_commitment := l.(Old.t.expected_commitment);
|}.

Module cycle_era.
  Module Old := Old.cycle_era.
  Module New := New.cycle_era.

  (** Migrate [Level_repr.cycle_era] *)
  Definition migrate (ce : Old.record) : New.record :=
  {|
    New.first_level := ce.(Old.first_level);
    New.first_cycle := ce.(Old.first_cycle);
    New.blocks_per_cycle := ce.(Old.blocks_per_cycle);
    New.blocks_per_commitment := ce.(Old.blocks_per_commitment);
  |}.
End cycle_era.

(** Migrate [Level_repr.cycle_eras] *)
Definition migrate_cycle_eras (ce : Old.cycle_eras) : New.cycle_eras :=
  List.map cycle_era.migrate ce.

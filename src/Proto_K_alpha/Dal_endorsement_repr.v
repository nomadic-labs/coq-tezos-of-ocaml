Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Dal_endorsement_repr.
Require TezosOfOCaml.Proto_alpha.Dal_endorsement_repr.

Module Old := TezosOfOCaml.Proto_K.Dal_endorsement_repr.
Module New := TezosOfOCaml.Proto_alpha.Dal_endorsement_repr.

(** Migrate [Dal_endorsement_repr.t] *)
Definition migrate (k : Old.t) : New.t := k.

(** Migrate [Dal_endorsement_repr.Accountability] *)
Module Accountability.
  Definition migrate (k : Old.Accountability.t) : New.Accountability.t :=
    List.map migrate k.
End Accountability.

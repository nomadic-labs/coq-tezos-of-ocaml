Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K_alpha.Simulations.Script_family.
Require TezosOfOCaml.Proto_K_alpha.Dependent_bool.
Require TezosOfOCaml.Proto_K_alpha.Sapling_storage.
Require TezosOfOCaml.Proto_K_alpha.Script_repr.
Require TezosOfOCaml.Proto_K_alpha.Script_string.
Require TezosOfOCaml.Proto_K_alpha.Script_timestamp.
Require TezosOfOCaml.Proto_K_alpha.Script_typed_ir.
Require TezosOfOCaml.Proto_K_alpha.Tez_repr.

Module Old := TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Module New := TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.

Module With_family.
  (** Migrate [comb_gadt_witness]. *)
  Fixpoint migrate_comb_gadt_witness {s f}
    (w : Old.With_family.comb_gadt_witness s f) :
    New.With_family.comb_gadt_witness
      (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match w with
    | Old.With_family.Comb_one => New.With_family.Comb_one
    | Old.With_family.Comb_succ w' =>
      New.With_family.Comb_succ (migrate_comb_gadt_witness w')
    end.

  (** Migrate [uncomb_gadt_witness]. *)
  Fixpoint migrate_uncomb_gadt_witness {s f}
    (w : Old.With_family.uncomb_gadt_witness s f) :
    New.With_family.uncomb_gadt_witness
      (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match w with
    | Old.With_family.Uncomb_one => New.With_family.Uncomb_one
    | Old.With_family.Uncomb_succ w' =>
      New.With_family.Uncomb_succ (migrate_uncomb_gadt_witness w')
    end.

  (** Migrate [comb_get_gadt_witness]. *)
  Fixpoint migrate_comb_get_gadt_witness {a b}
    (w : Old.With_family.comb_get_gadt_witness a b) :
    New.With_family.comb_get_gadt_witness (Ty.migrate a) (Ty.migrate b) :=
    match w with
    | Old.With_family.Comb_get_zero => New.With_family.Comb_get_zero
    | Old.With_family.Comb_get_one => New.With_family.Comb_get_one
    | Old.With_family.Comb_get_plus_two w' =>
      New.With_family.Comb_get_plus_two (migrate_comb_get_gadt_witness w')
    end.

  (** Migrate [comb_set_gadt_witness]. *)
  Fixpoint migrate_comb_set_gadt_witness {a b c}
    (w : Old.With_family.comb_set_gadt_witness a b c) :
    New.With_family.comb_set_gadt_witness
      (Ty.migrate a) (Ty.migrate b) (Ty.migrate c) :=
    match w with
    | Old.With_family.Comb_set_zero => New.With_family.Comb_set_zero
    | Old.With_family.Comb_set_one => New.With_family.Comb_set_one
    | Old.With_family.Comb_set_plus_two w' => New.With_family.Comb_set_plus_two
      (migrate_comb_set_gadt_witness w')
    end.

  (** Migrate [dup_n_gadt_witness]. *)
  Fixpoint migrate_dup_n_gadt_witness {b s}
    (w : Old.With_family.dup_n_gadt_witness s b) :
    New.With_family.dup_n_gadt_witness (Stack_ty.migrate s) (Ty.migrate b) :=
    match w with
    | Old.With_family.Dup_n_zero => New.With_family.Dup_n_zero
    | Old.With_family.Dup_n_succ w' =>
      New.With_family.Dup_n_succ (migrate_dup_n_gadt_witness w')
    end.

  (** Migrate [ty]. *)
  Fixpoint migrate_ty {a} (ty : Old.With_family.ty a) :
    New.With_family.ty (Ty.migrate a) :=
    match
      ty
      in Old.With_family.ty a
      return New.With_family.ty (Ty.migrate a)
    with
    | Old.With_family.Unit_t => New.With_family.Unit_t
    | Old.With_family.Int_t => New.With_family.Int_t
    | Old.With_family.Nat_t => New.With_family.Nat_t
    | Old.With_family.Signature_t => New.With_family.Signature_t
    | Old.With_family.String_t => New.With_family.String_t
    | Old.With_family.Bytes_t => New.With_family.Bytes_t
    | Old.With_family.Mutez_t => New.With_family.Mutez_t
    | Old.With_family.Key_hash_t => New.With_family.Key_hash_t
    | Old.With_family.Key_t => New.With_family.Key_t
    | Old.With_family.Timestamp_t => New.With_family.Timestamp_t
    | Old.With_family.Address_t => New.With_family.Address_t
    | Old.With_family.Tx_rollup_l2_address_t =>
      New.With_family.Tx_rollup_l2_address_t
    | Old.With_family.Bool_t => New.With_family.Bool_t
    | Old.With_family.Pair_t ty1 ty2 m b =>
      New.With_family.Pair_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dand b)
    | Old.With_family.Union_t ty1 ty2 m b =>
      New.With_family.Union_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dand b)
    | Old.With_family.Lambda_t ty1 ty2 m =>
      New.With_family.Lambda_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Option_t ty m b =>
      New.With_family.Option_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dbool b)
    | Old.With_family.List_t ty m =>
      New.With_family.List_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Set_t ty m =>
      New.With_family.Set_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Map_t ty1 ty2 m =>
      New.With_family.Map_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Big_map_t ty1 ty2 m =>
      New.With_family.Big_map_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Contract_t ty m =>
      New.With_family.Contract_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Sapling_transaction_t size =>
      New.With_family.Sapling_transaction_t size
    | Old.With_family.Sapling_transaction_deprecated_t size =>
      New.With_family.Sapling_transaction_deprecated_t size
    | Old.With_family.Sapling_state_t size =>
      New.With_family.Sapling_state_t size
    | Old.With_family.Operation_t => New.With_family.Operation_t
    | Old.With_family.Chain_id_t => New.With_family.Chain_id_t
    | Old.With_family.Never_t => New.With_family.Never_t
    | Old.With_family.Bls12_381_g1_t => New.With_family.Bls12_381_g1_t
    | Old.With_family.Bls12_381_g2_t => New.With_family.Bls12_381_g2_t
    | Old.With_family.Bls12_381_fr_t => New.With_family.Bls12_381_fr_t
    | Old.With_family.Ticket_t ty m =>
      New.With_family.Ticket_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Chest_key_t => New.With_family.Chest_key_t
    | Old.With_family.Chest_t => New.With_family.Chest_t
    end.

  (** Migrate [stack_ty]. *)
  Fixpoint migrate_stack_ty {s} (ts : Old.With_family.stack_ty s) :
    New.With_family.stack_ty (Stack_ty.migrate s) :=
    match ts with
    | Old.With_family.Item_t t rest =>
      New.With_family.Item_t (migrate_ty t) (migrate_stack_ty rest)
    | Old.With_family.Bot_t => New.With_family.Bot_t
    end.

  Definition stack_ty_head {a s} (sty : Old.With_family.stack_ty (a :: s)) :
    Old.With_family.ty a :=
    match sty with
    | Old.With_family.Item_t t _ => t
    end.

  Definition stack_ty_tail {a s} (sty : Old.With_family.stack_ty (a :: s)) :
    Old.With_family.stack_ty s :=
    match sty with
    | Old.With_family.Item_t _ sty => sty
    end.

  (** Migrate [stack_prefix_preservation_witness]. *)
  Fixpoint migrate_stack_prefix_preservation_witness {s t u v}
    (sty : Old.With_family.stack_ty u)
    (w : Old.With_family.stack_prefix_preservation_witness s t u v) :
    New.With_family.stack_prefix_preservation_witness
      (Stack_ty.migrate s)
      (Stack_ty.migrate t)
      (Stack_ty.migrate u)
      (Stack_ty.migrate v) :=
    match
      w in Old.With_family.stack_prefix_preservation_witness s t u v,
      sty
    with
    | Old.With_family.KRest, _ => New.With_family.KRest
    | Old.With_family.KPrefix loc _ w', _ =>
      New.With_family.KPrefix loc
        (migrate_ty (stack_ty_head sty))
        (migrate_stack_prefix_preservation_witness (stack_ty_tail sty) w')
    end.

  (** Migrate [view_signature]. *)
  Definition migrate_view_signature {a b}
    (signature : Old.With_family.view_signature a b) :
    New.With_family.view_signature (Ty.migrate a) (Ty.migrate b) :=
    match signature with
    | Old.With_family.View_signature name input_ty output_ty =>
      New.With_family.View_signature
        (Script_string.migrate name)
        (migrate_ty input_ty)
        (migrate_ty output_ty)
    end.

  (** Migrate [typed_contract]. *)
  (** From K to alpha, the form of [typed_contract] has changed.
      In particular, typed_contract does not use [address] anymore.
      Notice the cast_exists for the 3rd constructor in New:
      - [Typed_tx_rollup {a}] has type
        [ty (Theta a) -> Tx_rollup.t -> typed_contract (Theta a)]
        with [Theta:= fun a  => (a * Tx_rollup_l2_address)%Ty.t : Ty.t -> Ty.t]
      - However, matching the parameter [x] in Old gives us [ty : ty arg],
        whereas in New, [ty] should be cast into [ty (Theta a)].
        In practice, this means that [arg] should always be of the form
        [ty (Theta a)].
        This is handled with a suitable [cast_exists] and a projection w.r.t.
        [existT], outputting [ty_arg_cast].
      - Moreover, type variable [a] has two occurrences in the type of
        [Typed_tx_rollup {a}]: in particular, the return type
        [Typed_tx_rollup {a}] is [typed_contract (Theta a)] (in New) whereas
        the type of [migrate_typed_contract x] is [typed_contract arg].
        Another (non-existential) [cast] is needed to handle this.
  *)
  Definition migrate_typed_contract {arg}
  (x : Old.With_family.typed_contract arg) :
  New.With_family.typed_contract (Ty.migrate arg) :=
  match x with
  | Old.With_family.Typed_contract ty address =>
    let '{| Script_typed_ir.Old.address.entrypoint := entrypoint ;
    Script_typed_ir.Old.address.destination := destination |} := address in
     match destination with
      | Destination_repr.Old.Contract contract =>
         match contract with
        | Contract_repr.Old.Implicit public_key => cast _
            New.With_family.Typed_implicit public_key
        | Contract_repr.Old.Originated contract_hash =>
          New.With_family.Typed_originated (migrate_ty ty) contract_hash
          entrypoint
        end
        | Destination_repr.Old.Tx_rollup rollup => 
        let 'existT _ _ ty_arg_cast := cast_exists (fun (a : New.Ty.t) =>
              New.With_family.ty (New.Ty.Pair (New.Ty.Ticket a) 
              New.Ty.Tx_rollup_l2_address)) (migrate_ty ty) in 
              cast _ (New.With_family.Typed_tx_rollup ty_arg_cast rollup)
        | Destination_repr.Old.Sc_rollup rollup => 
            New.With_family.Typed_sc_rollup (migrate_ty ty) rollup entrypoint 
    end      
end.

  (** Migrate [set]. *)
  Definition migrate_set {a} (x : Old.With_family.set a) :
    New.With_family.set (Ty.migrate a) :=
    List.map (fun '(k, v) => (Script_family.Ty.migrate_to_Set k, v)) x.

  (** Migrate [map]. *)
  Definition migrate_map {k : Old.Ty.t} {v v' : Set}
    (migrate : v -> v') (x : Old.With_family.map k v) :
    New.With_family.map (Ty.migrate k) v' :=
    List.map (fun '(k, v) =>
      (Script_family.Ty.migrate_to_Set k, migrate v)) x.

  (** Migrate [big_map_overlay]. *)
  Definition migrate_big_map_overlay {a b}
    (migrate_a : Old.With_family.ty_to_dep_Set a ->
      New.With_family.ty_to_dep_Set (Ty.migrate a))
    (migrate_b : Old.With_family.ty_to_dep_Set b ->
      New.With_family.ty_to_dep_Set (Ty.migrate b))
    (diff : Proto_K.Script_typed_ir.big_map_overlay
         (Old.With_family.ty_to_dep_Set a)
         (Old.With_family.ty_to_dep_Set b)) :
    Script_typed_ir.big_map_overlay
      (New.With_family.ty_to_dep_Set (Ty.migrate a))
      (New.With_family.ty_to_dep_Set (Ty.migrate b)) := {|
    Script_typed_ir.big_map_overlay.map :=
      List.map (fun '(k, (a, ob)) => (k, (migrate_a a,
        match ob with
        | Some b => Some (migrate_b b)
        | None => None
        end)))
      (Proto_K.Script_typed_ir.big_map_overlay.map diff);
    Script_typed_ir.big_map_overlay.size :=
      Proto_K.Script_typed_ir.big_map_overlay.size diff
    |}.

  (** Migrate [big_map_skeleton]. *)
  Definition migrate_big_map_skeleton {a b}
    (migrate_a : Old.With_family.ty_to_dep_Set a ->
      New.With_family.ty_to_dep_Set (Ty.migrate a))
    (migrate_b : Old.With_family.ty_to_dep_Set b ->
      New.With_family.ty_to_dep_Set (Ty.migrate b))
    (skel : Old.With_family.big_map.skeleton a b
      (Old.With_family.ty_to_dep_Set a)
      (Old.With_family.ty_to_dep_Set b)) :
    New.With_family.big_map.skeleton (Ty.migrate a)
      (Ty.migrate b) (New.With_family.ty_to_dep_Set (Ty.migrate a))
      (New.With_family.ty_to_dep_Set (Ty.migrate b)) := {|
    New.With_family.big_map.id := Old.With_family.big_map.id skel;
    New.With_family.big_map.diff := migrate_big_map_overlay
      migrate_a migrate_b
      (Old.With_family.big_map.diff skel);
    New.With_family.big_map.key_type :=
      migrate_ty (Old.With_family.big_map.key_type skel);
    New.With_family.big_map.value_type :=
      migrate_ty (Old.With_family.big_map.value_type skel);
    |}.

  (** Migrate [kinstr]. *)
  #[bypass_check(guard)]
  Fixpoint migrate_kinstr {s f} (i : Old.With_family.kinstr s f) {struct i} :
    New.With_family.kinstr (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match
      i
      in Old.With_family.kinstr s f
      return New.With_family.kinstr (Stack_ty.migrate s) (Stack_ty.migrate f)
    with
    (*
      Stack
      -----
    *)
    | Old.With_family.IDrop loc k =>
      New.With_family.IDrop loc (migrate_kinstr k)
    | Old.With_family.IDup loc k =>
      New.With_family.IDup loc (migrate_kinstr k)
    | Old.With_family.ISwap loc k =>
      New.With_family.ISwap loc (migrate_kinstr k)
    | Old.With_family.IConst loc sty v k =>
      New.With_family.IConst
        loc
        (migrate_ty sty)
        (migrate_value v)
        (migrate_kinstr k)
    (*
      Pairs
      -----
    *)
    | Old.With_family.ICons_pair loc k =>
      New.With_family.ICons_pair loc (migrate_kinstr k)
    | Old.With_family.ICar loc k =>
      New.With_family.ICar loc (migrate_kinstr k)
    | Old.With_family.ICdr loc k =>
      New.With_family.ICdr loc (migrate_kinstr k)
    | Old.With_family.IUnpair loc k =>
      New.With_family.IUnpair loc (migrate_kinstr k)
    (*
      Options
      -------
    *)
    | Old.With_family.ICons_some loc k =>
      New.With_family.ICons_some loc (migrate_kinstr k)
    | Old.With_family.ICons_none loc sty k =>
      New.With_family.ICons_none loc (migrate_ty sty) (migrate_kinstr k)
    | Old.With_family.IIf_none loc branch_if_none branch_if_some k =>
      New.With_family.IIf_none
        loc
        (migrate_kinstr branch_if_none)
        (migrate_kinstr branch_if_some)
        (migrate_kinstr k)
    | Old.With_family.IOpt_map loc body k =>
      New.With_family.IOpt_map loc (migrate_kinstr body) (migrate_kinstr k)
    (*
      Unions
      ------
    *)
    | Old.With_family.ICons_left loc sty k =>
      New.With_family.ICons_left loc (migrate_ty sty) (migrate_kinstr k)
    | Old.With_family.ICons_right loc sty k =>
      New.With_family.ICons_right loc (migrate_ty sty) (migrate_kinstr k)
    | Old.With_family.IIf_left loc branch_if_left branch_if_right k =>
      New.With_family.IIf_left
        loc
        (migrate_kinstr branch_if_left)
        (migrate_kinstr branch_if_right)
        (migrate_kinstr k)
    (*
      Lists
      -----
    *)
    | Old.With_family.ICons_list loc k =>
      New.With_family.ICons_list loc (migrate_kinstr k)
    | Old.With_family.INil loc sty k =>
      New.With_family.INil loc (migrate_ty sty) (migrate_kinstr k)
    | Old.With_family.IIf_cons loc branch_if_cons branch_if_nil k =>
      New.With_family.IIf_cons
        loc
        (migrate_kinstr branch_if_cons)
        (migrate_kinstr branch_if_nil)
        (migrate_kinstr k)
    | Old.With_family.IList_map loc body sty k =>
      New.With_family.IList_map
        loc
        (migrate_kinstr body)
        (migrate_ty sty)
        (migrate_kinstr k)
    | Old.With_family.IList_iter loc sty body k =>
      New.With_family.IList_iter
        loc
        (migrate_ty sty)
        (migrate_kinstr body)
        (migrate_kinstr k)
    | Old.With_family.IList_size loc k =>
      New.With_family.IList_size loc (migrate_kinstr k)
    (*
    Sets
    ----
    *)
    | Old.With_family.IEmpty_set loc t instr =>
      New.With_family.IEmpty_set loc (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.ISet_iter loc sty instr1 instr2 =>
      New.With_family.ISet_iter
        loc
        (migrate_ty sty)
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.ISet_mem loc instr =>
      New.With_family.ISet_mem loc (migrate_kinstr instr)
    | Old.With_family.ISet_update loc instr =>
      New.With_family.ISet_update loc (migrate_kinstr instr)
    | Old.With_family.ISet_size loc instr =>
      New.With_family.ISet_size loc (migrate_kinstr instr)
    (*
      Maps
      ----
    *)
    | Old.With_family.IEmpty_map loc key sty instr =>
      New.With_family.IEmpty_map
        loc
        (migrate_ty key)
        (migrate_ty sty)
        (migrate_kinstr instr)
    | Old.With_family.IMap_map loc sty instr1 instr2 =>
      New.With_family.IMap_map
        loc
        (migrate_ty sty)
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IMap_iter loc sty instr1 instr2 =>
      New.With_family.IMap_iter
        loc
        (migrate_ty sty)
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IMap_mem loc instr =>
      New.With_family.IMap_mem loc (migrate_kinstr instr)
    | Old.With_family.IMap_get loc instr =>
      New.With_family.IMap_get loc (migrate_kinstr instr)
    | Old.With_family.IMap_update loc instr =>
      New.With_family.IMap_update loc (migrate_kinstr instr)
    | Old.With_family.IMap_get_and_update loc instr =>
      New.With_family.IMap_get_and_update loc (migrate_kinstr instr)
    | Old.With_family.IMap_size loc instr =>
      New.With_family.IMap_size loc (migrate_kinstr instr)
    (*
      Big maps
      --------
    *)
    | Old.With_family.IEmpty_big_map loc key val instr =>
      New.With_family.IEmpty_big_map
        loc
        (migrate_ty key)
        (migrate_ty val)
        (migrate_kinstr instr)
    | Old.With_family.IBig_map_mem loc instr =>
      New.With_family.IBig_map_mem loc (migrate_kinstr instr)
    | Old.With_family.IBig_map_get loc instr =>
      New.With_family.IBig_map_get loc (migrate_kinstr instr)
    | Old.With_family.IBig_map_update loc instr =>
      New.With_family.IBig_map_update loc (migrate_kinstr instr)
    | Old.With_family.IBig_map_get_and_update loc instr =>
      New.With_family.IBig_map_get_and_update loc (migrate_kinstr instr)
    (*
      Strings
      -------
    *)
    | Old.With_family.IConcat_string loc instr =>
      New.With_family.IConcat_string loc (migrate_kinstr instr)
    | Old.With_family.IConcat_string_pair loc instr =>
      New.With_family.IConcat_string_pair loc (migrate_kinstr instr)
    | Old.With_family.ISlice_string loc instr =>
      New.With_family.ISlice_string loc (migrate_kinstr instr)
    | Old.With_family.IString_size loc instr =>
      New.With_family.IString_size loc (migrate_kinstr instr)
    (*
      Bytes
      -----
    *)
    | Old.With_family.IConcat_bytes loc instr =>
      New.With_family.IConcat_bytes loc (migrate_kinstr instr)
    | Old.With_family.IConcat_bytes_pair loc instr =>
      New.With_family.IConcat_bytes_pair loc (migrate_kinstr instr)
    | Old.With_family.ISlice_bytes loc instr =>
      New.With_family.ISlice_bytes loc (migrate_kinstr instr)
    | Old.With_family.IBytes_size loc instr =>
      New.With_family.IBytes_size loc (migrate_kinstr instr)
    (*
      Timestamps
      ----------
    *)
    | Old.With_family.IAdd_seconds_to_timestamp loc instr =>
      New.With_family.IAdd_seconds_to_timestamp loc (migrate_kinstr instr)
    | Old.With_family.IAdd_timestamp_to_seconds loc instr =>
      New.With_family.IAdd_timestamp_to_seconds loc (migrate_kinstr instr)
    | Old.With_family.ISub_timestamp_seconds loc instr =>
      New.With_family.ISub_timestamp_seconds loc (migrate_kinstr instr)
    | Old.With_family.IDiff_timestamps loc instr =>
      New.With_family.IDiff_timestamps loc (migrate_kinstr instr)
    (*
      Tez
      ---
    *)
    | Old.With_family.IAdd_tez loc instr =>
      New.With_family.IAdd_tez loc (migrate_kinstr instr)
    | Old.With_family.ISub_tez loc instr =>
      New.With_family.ISub_tez loc (migrate_kinstr instr)
    | Old.With_family.ISub_tez_legacy loc instr =>
      New.With_family.ISub_tez_legacy loc (migrate_kinstr instr)
    | Old.With_family.IMul_teznat loc instr =>
      New.With_family.IMul_teznat loc (migrate_kinstr instr)
    | Old.With_family.IMul_nattez loc instr =>
      New.With_family.IMul_nattez loc (migrate_kinstr instr)
    | Old.With_family.IEdiv_teznat loc instr =>
      New.With_family.IEdiv_teznat loc (migrate_kinstr instr)
    | Old.With_family.IEdiv_tez loc instr =>
      New.With_family.IEdiv_tez loc (migrate_kinstr instr)
    (*
      Booleans
      --------
    *)
    | Old.With_family.IOr loc k =>
      New.With_family.IOr loc (migrate_kinstr k)
    | Old.With_family.IAnd loc k =>
      New.With_family.IAnd loc (migrate_kinstr k)
    | Old.With_family.IXor loc k =>
      New.With_family.IXor loc (migrate_kinstr k)
    | Old.With_family.INot loc k =>
      New.With_family.INot loc (migrate_kinstr k)
    (*
      Integers
      --------
    *)
    | Old.With_family.IIs_nat loc instr =>
      New.With_family.IIs_nat loc (migrate_kinstr instr)
    | Old.With_family.INeg loc instr =>
      New.With_family.INeg loc (migrate_kinstr instr)
    | Old.With_family.IAbs_int loc instr =>
      New.With_family.IAbs_int loc (migrate_kinstr instr)
    | Old.With_family.IInt_nat loc instr =>
      New.With_family.IInt_nat loc (migrate_kinstr instr)
    | Old.With_family.IAdd_int loc instr =>
      New.With_family.IAdd_int loc (migrate_kinstr instr)
    | Old.With_family.IAdd_nat loc instr =>
      New.With_family.IAdd_nat loc (migrate_kinstr instr)
    | Old.With_family.ISub_int loc instr =>
      New.With_family.ISub_int loc (migrate_kinstr instr)
    | Old.With_family.IMul_int loc instr =>
      New.With_family.IMul_int loc (migrate_kinstr instr)
    | Old.With_family.IMul_nat loc instr =>
      New.With_family.IMul_nat loc (migrate_kinstr instr)
    | Old.With_family.IEdiv_int loc instr =>
      New.With_family.IEdiv_int loc (migrate_kinstr instr)
    | Old.With_family.IEdiv_nat loc instr =>
      New.With_family.IEdiv_nat loc (migrate_kinstr instr)
    | Old.With_family.ILsl_nat loc instr =>
      New.With_family.ILsl_nat loc (migrate_kinstr instr)
    | Old.With_family.ILsr_nat loc instr =>
      New.With_family.ILsr_nat loc (migrate_kinstr instr)
    | Old.With_family.IOr_nat loc instr =>
      New.With_family.IOr_nat loc (migrate_kinstr instr)
    | Old.With_family.IAnd_nat loc instr =>
      New.With_family.IAnd_nat loc (migrate_kinstr instr)
    | Old.With_family.IAnd_int_nat loc instr =>
      New.With_family.IAnd_int_nat loc (migrate_kinstr instr)
    | Old.With_family.IXor_nat loc instr =>
      New.With_family.IXor_nat loc (migrate_kinstr instr)
    | Old.With_family.INot_int loc instr =>
      New.With_family.INot_int loc (migrate_kinstr instr)
    (*
      Control
      -------
    *)
    | Old.With_family.IIf loc branch_if_true branch_if_false k =>
      New.With_family.IIf
        loc
        (migrate_kinstr branch_if_true)
        (migrate_kinstr branch_if_false)
        (migrate_kinstr k)
    | Old.With_family.ILoop loc body k =>
      New.With_family.ILoop
        loc
        (migrate_kinstr body)
        (migrate_kinstr k)
    | Old.With_family.ILoop_left loc bl br =>
      New.With_family.ILoop_left
        loc
        (migrate_kinstr bl)
        (migrate_kinstr br)
    | Old.With_family.IDip loc b sty k =>
      New.With_family.IDip
        loc
        (migrate_kinstr b)
        (migrate_ty sty)
        (migrate_kinstr k)
    | Old.With_family.IExec loc sty k =>
      New.With_family.IExec
        loc
        (migrate_stack_ty sty)
        (migrate_kinstr k)
    | Old.With_family.IApply loc capture_ty k =>
      New.With_family.IApply
        loc
        (migrate_ty capture_ty)
        (migrate_kinstr k)
    | Old.With_family.ILambda loc lam k =>
      New.With_family.ILambda
        loc
        (migrate_lambda lam)
        (migrate_kinstr k)
    | Old.With_family.IFailwith kloc tv =>
      New.With_family.IFailwith
        kloc
        (migrate_ty tv)
    (*
      Comparison
      ----------
    *)
    | Old.With_family.ICompare loc t instr =>
      New.With_family.ICompare
        loc
        (migrate_ty t)
        (migrate_kinstr instr)
    (*
      Comparators
      -----------
    *)
    | Old.With_family.IEq loc instr =>
      New.With_family.IEq loc (migrate_kinstr instr)
    | Old.With_family.INeq loc instr =>
      New.With_family.INeq loc (migrate_kinstr instr)
    | Old.With_family.ILt loc instr =>
      New.With_family.ILt loc (migrate_kinstr instr)
    | Old.With_family.IGt loc instr =>
      New.With_family.IGt loc (migrate_kinstr instr)
    | Old.With_family.ILe loc instr =>
      New.With_family.ILe loc (migrate_kinstr instr)
    | Old.With_family.IGe loc instr =>
      New.With_family.IGe loc (migrate_kinstr instr)
    (*
      Protocol
      --------
    *)
    | Old.With_family.IAddress loc instr =>
      New.With_family.IAddress loc (migrate_kinstr instr)
    | Old.With_family.IContract loc t str instr =>
      New.With_family.IContract
        loc
        (migrate_ty t)
        str
        (migrate_kinstr instr)
    | Old.With_family.IView loc sign sty instr =>
      New.With_family.IView
        loc
        (migrate_view_signature sign)
        (migrate_stack_ty sty)
        (migrate_kinstr instr)
    | Old.With_family.ITransfer_tokens loc instr =>
      New.With_family.ITransfer_tokens loc (migrate_kinstr instr)
    | Old.With_family.IImplicit_account loc instr =>
      New.With_family.IImplicit_account loc (migrate_kinstr instr)
    | Old.With_family.ICreate_contract loc storage_type code instr =>
      New.With_family.ICreate_contract
        loc
        (migrate_ty storage_type)
        (Script_repr.migrate_expr code)
        (migrate_kinstr instr)
    | Old.With_family.ISet_delegate loc instr =>
      New.With_family.ISet_delegate loc (migrate_kinstr instr)
    | Old.With_family.INow loc instr =>
      New.With_family.INow loc (migrate_kinstr instr)
    | Old.With_family.IMin_block_time loc instr =>
      New.With_family.IMin_block_time loc (migrate_kinstr instr)
    | Old.With_family.IBalance loc instr =>
      New.With_family.IBalance loc (migrate_kinstr instr)
    | Old.With_family.ILevel loc instr =>
      New.With_family.ILevel loc (migrate_kinstr instr)
    | Old.With_family.ICheck_signature loc instr =>
      New.With_family.ICheck_signature loc (migrate_kinstr instr)
    | Old.With_family.IHash_key loc instr =>
      New.With_family.IHash_key loc (migrate_kinstr instr)
    | Old.With_family.IPack loc t instr =>
      New.With_family.IPack loc (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.IUnpack loc t instr =>
      New.With_family.IUnpack loc (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.IBlake2b loc instr =>
      New.With_family.IBlake2b loc (migrate_kinstr instr)
    | Old.With_family.ISha256 loc instr =>
      New.With_family.ISha256 loc (migrate_kinstr instr)
    | Old.With_family.ISha512 loc instr =>
      New.With_family.ISha512 loc (migrate_kinstr instr)
    | Old.With_family.ISource loc instr =>
      New.With_family.ISource loc (migrate_kinstr instr)
    | Old.With_family.ISender loc instr =>
      New.With_family.ISender loc (migrate_kinstr instr)
    | Old.With_family.ISelf loc t str instr =>
      New.With_family.ISelf
        loc
        (migrate_ty t)
        str
        (migrate_kinstr instr)
    | Old.With_family.ISelf_address loc instr =>
      New.With_family.ISelf_address loc (migrate_kinstr instr)
    | Old.With_family.IAmount loc instr =>
      New.With_family.IAmount loc (migrate_kinstr instr)
    | Old.With_family.ISapling_empty_state loc sz instr =>
      New.With_family.ISapling_empty_state
        loc
        sz
        (migrate_kinstr instr)
    | Old.With_family.ISapling_verify_update loc instr =>
      New.With_family.ISapling_verify_update loc (migrate_kinstr instr)
    | Old.With_family.ISapling_verify_update_deprecated loc instr =>
      New.With_family.ISapling_verify_update_deprecated loc (migrate_kinstr instr)
    | Old.With_family.IDig loc v spref instr =>
      New.With_family.IDig
        loc
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IDug loc v spref instr =>
      New.With_family.IDug
        loc
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IDipn loc v spref instr1 instr2 =>
      New.With_family.IDipn
        loc
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IDropn loc v spref instr =>
      New.With_family.IDropn
        loc
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IChainId loc instr =>
      New.With_family.IChainId loc (migrate_kinstr instr)
    | Old.With_family.INever loc =>
      New.With_family.INever loc
    | Old.With_family.IVoting_power loc instr =>
      New.With_family.IVoting_power loc (migrate_kinstr instr)
    | Old.With_family.ITotal_voting_power loc instr =>
      New.With_family.ITotal_voting_power loc (migrate_kinstr instr)
    | Old.With_family.IKeccak loc instr =>
      New.With_family.IKeccak loc (migrate_kinstr instr)
    | Old.With_family.ISha3 loc instr =>
      New.With_family.ISha3 loc (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_g1 loc instr =>
      New.With_family.IAdd_bls12_381_g1 loc (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_g2 loc instr =>
      New.With_family.IAdd_bls12_381_g2 loc (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_fr loc instr =>
      New.With_family.IAdd_bls12_381_fr loc (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_g1 loc instr =>
      New.With_family.IMul_bls12_381_g1 loc (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_g2 loc instr =>
      New.With_family.IMul_bls12_381_g2 loc (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_fr loc instr =>
      New.With_family.IMul_bls12_381_fr loc (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_z_fr loc instr =>
      New.With_family.IMul_bls12_381_z_fr loc (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_fr_z loc instr =>
      New.With_family.IMul_bls12_381_fr_z loc (migrate_kinstr instr)
    | Old.With_family.IInt_bls12_381_fr loc instr =>
      New.With_family.IInt_bls12_381_fr loc (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_g1 loc instr =>
      New.With_family.INeg_bls12_381_g1 loc (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_g2 loc instr =>
      New.With_family.INeg_bls12_381_g2 loc (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_fr loc instr =>
      New.With_family.INeg_bls12_381_fr loc (migrate_kinstr instr)
    | Old.With_family.IPairing_check_bls12_381 loc instr =>
      New.With_family.IPairing_check_bls12_381 loc (migrate_kinstr instr)
    | Old.With_family.IComb loc v comb instr =>
      New.With_family.IComb
        loc
        v
        (migrate_comb_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IUncomb loc v comb instr =>
      New.With_family.IUncomb
        loc
        v
        (migrate_uncomb_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IComb_get loc v comb instr =>
      New.With_family.IComb_get
        loc
        v
        (migrate_comb_get_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IComb_set loc v comb instr =>
      New.With_family.IComb_set
        loc
        v
        (migrate_comb_set_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IDup_n loc v dup_n instr =>
      New.With_family.IDup_n
        loc
        v
        (migrate_dup_n_gadt_witness dup_n)
        (migrate_kinstr instr)
    | Old.With_family.ITicket loc sty instr =>
      New.With_family.ITicket
        loc
        (migrate_ty sty)
        (migrate_kinstr instr)
    | Old.With_family.IRead_ticket loc sty instr =>
      New.With_family.IRead_ticket
        loc
        (migrate_ty sty)
        (migrate_kinstr instr)
    | Old.With_family.ISplit_ticket loc instr =>
      New.With_family.ISplit_ticket loc (migrate_kinstr instr)
    | Old.With_family.IJoin_tickets loc t instr =>
      New.With_family.IJoin_tickets
        loc
        (migrate_ty t)
        (migrate_kinstr instr)
    | Old.With_family.IOpen_chest loc instr =>
      New.With_family.IOpen_chest loc (migrate_kinstr instr)
    (*
      Internal control instructions
      -----------------------------
    *)
    | Old.With_family.IHalt loc =>
      New.With_family.IHalt loc
    end

  (** Migrate [lambda]. *)
  with migrate_lambda {arg ret} (lam : Old.With_family.lambda arg ret)
    {struct lam} : New.With_family.lambda (Ty.migrate arg) (Ty.migrate ret) :=
    let '(descr, code) := lam in
    (migrate_kdescr descr, false, Script_repr.migrate_node code)

  (** Migrate [kdescr]. *)
  with migrate_kdescr {s f} (descr : Old.With_family.kdescr s f)
    {struct descr} :
    New.With_family.kdescr (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    {|
      New.With_family.kdescr.kloc := descr.(Old.With_family.kdescr.kloc);
      New.With_family.kdescr.kbef :=
        migrate_stack_ty descr.(Old.With_family.kdescr.kbef);
      New.With_family.kdescr.kaft :=
        migrate_stack_ty descr.(Old.With_family.kdescr.kaft);
      New.With_family.kdescr.kinstr :=
        migrate_kinstr descr.(Old.With_family.kdescr.kinstr);
    |}

  (** Migrate a Michelson value. *)
  with migrate_value {t : Old.Ty.t} {struct t} :
    Old.With_family.ty_to_dep_Set t ->
    New.With_family.ty_to_dep_Set (Ty.migrate t) :=
    match
      t
      return
        Old.With_family.ty_to_dep_Set t ->
        New.With_family.ty_to_dep_Set (Ty.migrate t)
    with
    | Old.Ty.Unit => fun x => x
    | Old.Ty.Num _ => Script_int.migrate
    | Old.Ty.Signature => Script_typed_ir.Script_signature.migrate
    | Old.Ty.String => Script_string.migrate
    | Old.Ty.Bytes => fun x => x
    | Old.Ty.Mutez => Tez_repr.migrate
    | Old.Ty.Key_hash => fun x => x
    | Old.Ty.Key => fun x => x
    | Old.Ty.Timestamp => Script_timestamp.migrate
    | Old.Ty.Address => Script_typed_ir.migrate_address
    | Old.Ty.Tx_rollup_l2_address =>
      Script_typed_ir.migrate_tx_rollup_l2_address
    | Old.Ty.Bool => fun x => x
    | Old.Ty.Pair ty1 ty2 => fun '(x1, x2) =>
      (@migrate_value ty1 x1, @migrate_value ty2 x2)
    | Old.Ty.Union ty1 ty2 =>
      Script_typed_ir.migrate_union migrate_value migrate_value
    | Old.Ty.Lambda ty_arg ty_res => migrate_lambda
    | Old.Ty.Option ty => fun x =>
      match x with
      | None => None
      | Some x => Some (@migrate_value ty x)
      end
    | Old.Ty.List ty => Script_typed_ir.migrate_boxed_list migrate_value
    | Old.Ty.Set_ ty_k => migrate_set
    | Old.Ty.Map tyk _ => migrate_map migrate_value
    | Old.Ty.Big_map _ _ => migrate_big_map_skeleton migrate_value migrate_value
    | Old.Ty.Contract ty => migrate_typed_contract
    | Old.Ty.Sapling_transaction => fun x => x
    | Old.Ty.Sapling_transaction_deprecated => fun x => x
    | Old.Ty.Sapling_state => Sapling_storage.migrate_state
    | Old.Ty.Operation => axiom ("@TODO migrate_operation for ty_to_dep_Set")
    | Old.Ty.Chain_id => Script_typed_ir.Script_chain_id.migrate
    | Old.Ty.Never => fun x => match x with end
    | Old.Ty.Bls12_381_g1 => Script_typed_ir.Script_bls.G1.migrate
    | Old.Ty.Bls12_381_g2 => Script_typed_ir.Script_bls.G2.migrate
    | Old.Ty.Bls12_381_fr => Script_typed_ir.Script_bls.Fr.migrate
    | Old.Ty.Ticket ty => Script_typed_ir.migrate_ticket migrate_value
    | Old.Ty.Chest_key => Script_typed_ir.Script_timelock.migrate_chest_key
    | Old.Ty.Chest => Script_typed_ir.Script_timelock.migrate_chest
    end.

  (** Migrate the head of a stack. *)
  Definition migrate_stack_value_head {tys : Old.Stack_ty.t}
    (head : Old.With_family.stack_ty_to_dep_Set_head tys) :
    New.With_family.stack_ty_to_dep_Set_head (Stack_ty.migrate tys) :=
    match tys, head with
    | [], _ => Proto_alpha.Script_typed_ir.EmptyCell
    | ty :: _, head => @migrate_value ty head
    end.

  (** Migrate the tail of a stack. *)
  Fixpoint migrate_stack_value_tail {tys : Old.Stack_ty.t}
    (tail : Old.With_family.stack_ty_to_dep_Set_tail tys) :
    New.With_family.stack_ty_to_dep_Set_tail (Stack_ty.migrate tys) :=
    match tys, tail with
    | [], _ => Proto_alpha.Script_typed_ir.EmptyCell
    | _ :: tys, (head, tail) =>
      (@migrate_stack_value_head tys head, @migrate_stack_value_tail tys tail)
    end.

  (** Migrate the output of [stack_ty_to_dep_Set]. *)
  Definition migrate_stack_value {s}
    (v : Old.With_family.stack_ty_to_dep_Set s) :
    New.With_family.stack_ty_to_dep_Set (Stack_ty.migrate s) :=
  (migrate_stack_value_head (fst v), migrate_stack_value_tail (snd v)).

  (** Migrate [continuation]. *)
  Fixpoint migrate_continuation {s t} (c : Old.With_family.continuation s t) :
    New.With_family.continuation (Stack_ty.migrate s) (Stack_ty.migrate t) :=
    match c with
    | Old.With_family.KNil =>
      New.With_family.KNil
    | Old.With_family.KCons instr c =>
      New.With_family.KCons (migrate_kinstr instr) (migrate_continuation c)
    | Old.With_family.KReturn s sty c =>
      New.With_family.KReturn
        (migrate_stack_value s)
        (migrate_stack_ty sty)
        (migrate_continuation c)
    | Old.With_family.KMap_head c =>
      New.With_family.KMap_head (migrate_continuation c)
    | Old.With_family.KUndip b bty c =>
      New.With_family.KUndip
        (migrate_value b)
        (migrate_ty bty)
        (migrate_continuation c)
    | Old.With_family.KLoop_in i c =>
      New.With_family.KLoop_in (migrate_kinstr i) (migrate_continuation c)
    | Old.With_family.KLoop_in_left i c =>
      New.With_family.KLoop_in_left (migrate_kinstr i) (migrate_continuation c)
    | Old.With_family.KIter i a xs c =>
      New.With_family.KIter
        (migrate_kinstr i)
        (migrate_ty a)
        (List.map migrate_value xs)
        (migrate_continuation c)
    | Old.With_family.KList_enter_body i xs ys lty n c =>
      New.With_family.KList_enter_body
        (migrate_kinstr i)
        (List.map migrate_value xs)
        (List.map migrate_value ys)
        (migrate_ty lty)
        n
        (migrate_continuation c)
    | Old.With_family.KList_exit_body i xs ys lty n c =>
      New.With_family.KList_exit_body
        (migrate_kinstr i)
        (List.map migrate_value xs)
        (List.map migrate_value ys)
        (migrate_ty lty)
        n
        (migrate_continuation c)
    | Old.With_family.KMap_enter_body i xs m mty c =>
      New.With_family.KMap_enter_body
        (migrate_kinstr i)
        (List.map (fun '(x1, x2) => (migrate_value x1, migrate_value x2)) xs)
        (migrate_value m)
        (migrate_ty mty)
        (migrate_continuation c)
    | Old.With_family.KMap_exit_body i xs m x mty c =>
      New.With_family.KMap_exit_body
        (migrate_kinstr i)
        (List.map (fun '(x1, x2) => (migrate_value x1, migrate_value x2)) xs)
        (migrate_value m)
        (migrate_value x)
        (migrate_ty mty)
        (migrate_continuation c)
    | Old.With_family.KView_exit step c =>
      New.With_family.KView_exit
        (Script_typed_ir.migrate_step_constants step)
        (migrate_continuation c)
    end.
End With_family.

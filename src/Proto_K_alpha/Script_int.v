Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Script_int.
Require TezosOfOCaml.Proto_alpha.Script_int.

Module Old := TezosOfOCaml.Proto_K.Script_int.
Module New := TezosOfOCaml.Proto_alpha.Script_int.

(** Migrate [Script_int_repr.num]. *)
Definition migrate (x : Old.num) : New.num :=
  match x with
  | Old.Num_tag x => New.Num_tag x
  end.

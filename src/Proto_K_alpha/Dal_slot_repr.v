Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Dal_slot_repr.
Require TezosOfOCaml.Proto_alpha.Dal_slot_repr.

Module Old := TezosOfOCaml.Proto_K.Dal_slot_repr.
Module New := TezosOfOCaml.Proto_alpha.Dal_slot_repr.

(** Migrate [Dal_slot_repr.t] *)
Definition migrate (k : Old.t) : New.t :=
  {|
   New.t.published_level := k.(Old.t.level);
   New.t.index := k.(Old.t.index);
   New.t.header := axiom        (* TODO *)
 |}.

(** Migrate [Dal_slot_repr.Slot_market] *)
Module Slot_market.
  Definition migrate (k : Old.Slot_market.t) : New.Slot_market.t :=
  {|
   New.Slot_market.t.length := k.(Old.Slot_market.t.length);
   New.Slot_market.t.slots := List.map
    (fun '(x, mp) =>
       (x, Dal_slot_repr.migrate mp)
    )
    k.(Old.Slot_market.t.slots)
 |}.
End Slot_market.

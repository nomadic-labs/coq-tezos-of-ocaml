Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.

Require TezosOfOCaml.Proto_K.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_K_alpha.Lazy_storage_kind.

Module Old := TezosOfOCaml.Proto_K.Lazy_storage_diff.
Module New := TezosOfOCaml.Proto_alpha.Lazy_storage_diff.

Module ConstructorRecords_init.
  Module Old := Old.ConstructorRecords_init.
  Module New := New.ConstructorRecords_init.
  Module init.
    Module Old := Old.init.
    Module New := New.init.

(** Migrate [Lazy_storage_diff.ConstructorRecords_init.init.Copy_skeleton] *)
    Definition migrate_copy_skeleton (src new_src : Set)
      (migr_src : src -> new_src) (cs : Old.Copy_skeleton src)
      : New.Copy_skeleton new_src :=
      {|
        New.Copy.src := migr_src cs.(Old.Copy.src);
      |}.
  End init.
End ConstructorRecords_init.

Module ConstructorRecords_diff.
  Module Old := Old.ConstructorRecords_diff.
  Module New := New.ConstructorRecords_diff.

  Module diff.
    Module Old := Old.diff.
    Module New := New.diff.

(** Migrate [Lazy_storage_diff.ConstructorRecords_diff.diff.Update_skeleton] *)
    Definition migrate_update_skeleton
      (init new_init updates new_updates : Set)
      (migr_init : init -> new_init)
      (migr_updates : updates -> new_updates)
      (us : Old.Update_skeleton init updates)
      : New.Update_skeleton new_init new_updates :=
      {|
        New.Update.init := migr_init us.(Old.Update.init);
        New.Update.updates := migr_updates us.(Old.Update.updates);
      |}.
  End diff.
End ConstructorRecords_diff.

Definition migrate_init (id new_id alloc new_alloc : Set)
  (migr_id : id -> new_id) (migr_alloc : alloc -> new_alloc)
  (i : Old.init id alloc)
  : New.init new_id new_alloc :=
  match i with
  | Old.Existing => New.Existing
  | Old.Copy x => New.Copy (ConstructorRecords_init.init.migrate_copy_skeleton
      id new_id migr_id x)
  | Old.Alloc a => New.Alloc (migr_alloc a)
  end.

(** Migrate [diff] *)
Definition migrate_diff (id new_id alloc new_alloc updates new_updates : Set)
  (migr_id : id -> new_id) (migr_alloc : alloc -> new_alloc)
  (migr_updates : updates -> new_updates) (diff : Old.diff id alloc updates)
  : New.diff new_id new_alloc new_updates :=
  match diff with
  | Old.Remove => New.Remove
  | Old.Update x =>
    New.Update
      (ConstructorRecords_diff.diff.migrate_update_skeleton _ _ _ _
        (migrate_init _ _ _ _ migr_id migr_alloc) migr_updates x)
  end.

Definition migrate_diffs_item (di : Old.diffs_item) : New.diffs_item :=
  let '@Old.Item i a u k ii d := di in
  @New.Item i a u (Lazy_storage_kind.migrate k) ii
    (migrate_diff _ _ _ _ _ _ (@id i) (@id a) (@id u) d).

(** Migrate [diffs] *)
Definition migrate_diffs (diffs : Old.diffs) : New.diffs :=
  map migrate_diffs_item diffs.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_alpha.Ratio_repr.
Require TezosOfOCaml.Proto_K.Ratio_repr.

Module Old := TezosOfOCaml.Proto_K.Ratio_repr.
Module New := TezosOfOCaml.Proto_alpha.Ratio_repr.

(** Migrate [Ratio_repr.t.record] *)
Definition migrate (r : Old.t.record) : New.t :=
{|
  New.t.numerator := r.(Old.t.numerator);
  New.t.denominator := r.(Old.t.denominator);
|}.

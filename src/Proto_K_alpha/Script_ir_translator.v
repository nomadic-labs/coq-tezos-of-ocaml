Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator.

Require TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.

Module Old := TezosOfOCaml.Proto_K.Script_ir_translator.
Module New := TezosOfOCaml.Proto_alpha.Script_ir_translator.

(* Migrate [Script_ir_translator.parsing_mode] *)
Definition migrate_unparsing_mode (um : Old.unparsing_mode) : New.unparsing_mode :=
  match um with
  | Old.Optimized => New.Optimized
  | Old.Readable => New.Readable
  | Old.Optimized_legacy => New.Optimized_legacy
  end.

(* Migrate [Script_ir_translator.comb_witness] *)
Fixpoint migrate_comb_witness (cw : Old.comb_witness) : New.comb_witness :=
match cw with
| Old.Comb_Pair cw' => New.Comb_Pair (migrate_comb_witness cw')
| Old.Comb_Any => New.Comb_Any
end.

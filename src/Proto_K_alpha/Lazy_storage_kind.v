Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.

Module Old := TezosOfOCaml.Proto_K.Lazy_storage_kind.
Module New := TezosOfOCaml.Proto_alpha.Lazy_storage_kind.

(** Migrate [Lazy_storage_kind.t] *)
Definition migrate (k : Old.t) : New.t :=
  match k with
  | Old.Big_map => New.Big_map
  | Old.Sapling_state => New.Sapling_state
  end.

Module Temp_ids.
  Module Old := Old.Temp_ids.
  Module New := New.Temp_ids.

  (** Migrate [Lazy_storage_kind.Temp_ids.t *)
  Definition migrate (ti : Old.t) : New.t :=
    {|
      New.t.big_map := ti.(Old.t.big_map);
      New.t.sapling_state := ti.(Old.t.sapling_state);
    |}.
End Temp_ids.

Module IdSet.
  Module Old := Old.IdSet.
  Module New := New.IdSet.

  (** Migrate [Lazy_storage_kind.IdSet.t *)
  Definition migrate (ti : Old.t) : New.t :=
    {|
      New.t.big_map := ti.(Old.t.big_map);
      New.t.sapling_state := ti.(Old.t.sapling_state);
    |}.
End IdSet.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Indexable.
Require TezosOfOCaml.Proto_alpha.Indexable.

Module Old := TezosOfOCaml.Proto_K.Indexable.
Module New := TezosOfOCaml.Proto_alpha.Indexable.

(** Migrate [t]. *)
Definition migrate {a : Set} (x : Old.t a) : New.t a :=
  match x with
  | Old.Value x => New.Value x
  | Old.Hidden_value x => New.Hidden_value x
  | Old.Index x => New.Index x
  | Old.Hidden_index x => New.Hidden_index x
  end.

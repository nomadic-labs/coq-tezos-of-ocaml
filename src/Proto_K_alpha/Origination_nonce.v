Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Origination_nonce.
Require TezosOfOCaml.Proto_alpha.Origination_nonce.

Module Old := TezosOfOCaml.Proto_K.Origination_nonce.
Module New := TezosOfOCaml.Proto_alpha.Origination_nonce.

(** Migrate [Origination_nonce.t] *)
Definition migrate (on : Old.t) : New.t :=
{|
  New.t.operation_hash := on.(Old.t.operation_hash);
  New.t.origination_index := on.(Old.t.origination_index);
|}.

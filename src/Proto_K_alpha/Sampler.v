Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Sampler.
Require TezosOfOCaml.Proto_alpha.Sampler.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Module Old := TezosOfOCaml.Proto_K.Sampler.
Module New := TezosOfOCaml.Proto_alpha.Sampler.

(** Migrate [Sampler.t] *)
Definition migrate (l : Old.t (Signature.public_key * Signature.public_key_hash)) :
  New.t Raw_context.consensus_pk :=
  {|
    New.Make.t.total := l.(Old.Make.t.total);
    New.Make.t.support := axiom;
    New.Make.t.p := l.(Old.Make.t.p);
    New.Make.t.alias := l.(Old.Make.t.alias);
  |}.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_K.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_K.Raw_context.

Require TezosOfOCaml.Proto_K_alpha.Constants_repr.
Require TezosOfOCaml.Proto_K_alpha.Dal_endorsement_repr.
Require TezosOfOCaml.Proto_K_alpha.Dal_slot_repr.
Require TezosOfOCaml.Proto_K_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_K_alpha.Level_repr.
Require TezosOfOCaml.Proto_K_alpha.Origination_nonce.
Require TezosOfOCaml.Proto_K_alpha.Round_repr.
Require TezosOfOCaml.Proto_K_alpha.Sampler.
Require TezosOfOCaml.Proto_K_alpha.Seed_repr.
Require TezosOfOCaml.Proto_K_alpha.Tez_repr.
Require TezosOfOCaml.Proto_K_alpha.Tx_rollup_inbox_repr.

Module Old := TezosOfOCaml.Proto_K.Raw_context.
Module New := TezosOfOCaml.Proto_alpha.Raw_context.

Definition Sc_rollup_address_map_builder_migrate
  (l : Old.Sc_rollup_address_map_builder.(Carbonated_map.S_builder.t)
    Context.tree) :
  (New.Sc_rollup_address_map_builder
    .(Proto_alpha.Carbonated_map.S_builder.t) tree) :=
  let
    '{| Proto_K.Carbonated_map.Make_builder.t.map := k_map;
        Proto_K.Carbonated_map.Make_builder.t.size := k_size; |} := l in
     {| Proto_alpha.Carbonated_map.Make_builder.t.map := k_map;
        Proto_alpha.Carbonated_map.Make_builder.t.size := k_size;
     |}.

(** @TODO : not solvable yet, because in Signature.v this paramether is not
            defined
            [Parameter Included_SIGNATURE_Public_key_hash_Map_t : Set -> Set.] *)
Definition Signature_option_migrate
  (l : option (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map)
    .(S.INDEXES_MAP.t) Tez_repr.t)) :
  option (Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.Map)
          .(INDEXES_MAP.t) Proto_alpha.Tez_repr.t) :=
  match l with
  | Some res => axiom
  | None => None
  end.

Module Raw_consensus.
  Module Old := Old.Raw_consensus.
  Module New := New.Raw_consensus.

  (** Module [Raw_context.Raw_consensus.t] *)
  Definition migrate (r : Old.t) : New.t :=
  {|
    New.t.current_endorsement_power := r.(Old.t.current_endorsement_power);
    New.t.allowed_endorsements := axiom ; (* TODO *)
    New.t.allowed_preendorsements := axiom ; (* TODO *)
    New.t.grand_parent_endorsements_seen :=
      r.(Old.t.grand_parent_endorsements_seen);
    New.t.endorsements_seen := r.(Old.t.endorsements_seen);
    New.t.preendorsements_seen := r.(Old.t.preendorsements_seen);
    New.t.locked_round_evidence := r.(Old.t.locked_round_evidence);
    New.t.preendorsements_quorum_round :=
      r.(Old.t.preendorsements_quorum_round);
    New.t.endorsement_branch := r.(Old.t.endorsement_branch);
    New.t.grand_parent_branch := r.(Old.t.grand_parent_branch);
  |}.
End Raw_consensus.

(** Migrate [Raw_context.back]. *)
Definition migrate_back (b : Old.back) : New.back :=
{|
  New.back.context := b.(Old.back.context);
  New.back.constants := Constants_parametric_repr.migrate b
    .(Old.back.constants);
  New.back.round_durations :=
    Round_repr.Durations.migrate b.(Old.back.round_durations);
  New.back.cycle_eras := Level_repr.migrate_cycle_eras b.(Old.back.cycle_eras);
  New.back.level := Level_repr.migrate b.(Old.back.level);
  New.back.predecessor_timestamp := b.(Old.back.predecessor_timestamp);
  New.back.timestamp := b.(Old.back.timestamp);
  New.back.fees := Tez_repr.migrate b.(Old.back.fees);
  New.back.origination_nonce :=
    let* on := b.(Old.back.origination_nonce) in
    return* Origination_nonce.migrate on;
  New.back.temporary_lazy_storage_ids :=
    Lazy_storage_kind.Temp_ids.migrate b.(Old.back.temporary_lazy_storage_ids);
  New.back.internal_nonce := b.(Old.back.internal_nonce);
  New.back.internal_nonces_used := b.(Old.back.internal_nonces_used);
  New.back.remaining_block_gas := b.(Old.back.remaining_block_gas);
  New.back.unlimited_operation_gas := b.(Old.back.unlimited_operation_gas);
  New.back.consensus := Raw_consensus.migrate b.(Old.back.consensus);
  New.back.non_consensus_operations_rev :=
    b.(Old.back.non_consensus_operations_rev);
  New.back.sampler_state :=
    Map.Make.map
      (fun '(seed, sampler) =>
         (Seed_repr.migrate_seed seed,
          Sampler.migrate sampler)
      )
      b.(Old.back.sampler_state);
  New.back.stake_distribution_for_current_cycle :=
    let* 'x :=
      Signature_option_migrate b.(Old.back.stake_distribution_for_current_cycle)
    in return* x;
  New.back.tx_rollup_current_messages :=
    List.map
    (fun '(x, tree) =>
       (x, Tx_rollup_inbox_repr.Merkle.migrate_tree tree)
    )
    b.(Old.back.tx_rollup_current_messages);
  New.back.sc_rollup_current_messages :=
    Sc_rollup_address_map_builder_migrate
    b.(Old.back.sc_rollup_current_messages);
  New.back.dal_slot_fee_market := Dal_slot_repr.Slot_market.migrate
    b.(Old.back.dal_slot_fee_market);
  New.back.dal_endorsement_slot_accountability :=
    Dal_endorsement_repr.Accountability.migrate
    b.(Old.back.dal_endorsement_slot_accountability)
|}.

(** Migrate [Raw_context.t]. *)
Definition migrate (ctxt : Old.t) : New.t :=
{|
  New.t.remaining_operation_gas := ctxt.(Old.t.remaining_operation_gas);
  New.t.back := migrate_back ctxt.(Old.t.back);
|}.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_alpha.Sapling_validator.
Require TezosOfOCaml.Proto_K_alpha.Error.
Require TezosOfOCaml.Proto_K_alpha.Raw_context.
Require TezosOfOCaml.Proto_K_alpha.Sapling_storage.
Require TezosOfOCaml.Proto_K.Proofs.Sapling_storage.
Require TezosOfOCaml.Proto_K.Sapling_validator.

(** [check_and_update_nullifiers] is backward compatible *)
Lemma check_and_update_nullifiers_is_backward_compatible 
  (ctxt : Proto_K.Raw_context.t) state utxos :
  let migrate '(ctxt, s) :=
  (Raw_context.migrate ctxt,
    Option.map Sapling_storage.migrate_state s) in
  let x := (Proto_K.Sapling_validator.check_and_update_nullifiers
    ctxt state utxos) in
  Error.migrate_monad x migrate =
  Proto_alpha.Sapling_validator.check_and_update_nullifiers
    (Raw_context.migrate ctxt) (Sapling_storage.migrate_state state) utxos.
Admitted.

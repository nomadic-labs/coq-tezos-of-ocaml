Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Proofs.Script_int_repr.
Require TezosOfOCaml.Proto_K.Script_int_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_int.
Require TezosOfOCaml.Proto_alpha.Script_int.
Require TezosOfOCaml.Proto_K_alpha.Script_int_repr.

Module Old := TezosOfOCaml.Proto_K.Script_int_repr.
Module New := TezosOfOCaml.Proto_alpha.Script_int.

(** The function [to_int64] is backward compatible. *)
Lemma to_int64_is_backward_compatible (n : Old.num) :
  Old.to_int64 n =
  New.to_int64 (Script_int_repr.migrate n).
Proof.
  unfold Old.to_int64, New.to_int64.
  now destruct n.
Qed.

(** The function [ediv] is backward compatible. *)
Lemma ediv_is_backward_compatible (a b : Old.num) :
  (let* '(quo, rem) := Old.ediv a b in
  return* (Script_int_repr.migrate quo, Script_int_repr.migrate rem)) =
  New.ediv (Script_int_repr.migrate a) (Script_int_repr.migrate b).
Proof.
  destruct a as [a], b as [b].
  simpl Script_int_repr.migrate.
  destruct (b =? 0) eqn:?.
  { replace b with 0 by lia.
    rewrite Script_int_repr.ediv_zero, Script_int.ediv_zero.
    reflexivity.
  }
  { rewrite Script_int_repr.ediv_not_zero, Script_int.ediv_not_zero by lia.
    reflexivity.
  }
Qed.

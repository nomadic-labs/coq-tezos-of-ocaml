(** Generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_demo.Environment.
Import Environment.Notations.
Require TezosOfOCaml.Proto_demo.Raw_context.

Definition current : Raw_context.context -> Int64.t :=
  Raw_context.current_fitness.

Definition increase (op_staroptstar : option int)
  : Raw_context.context -> Raw_context.t :=
  let gap :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 1
    end in
  fun ctxt =>
    let fitness := current ctxt in
    Raw_context.set_current_fitness ctxt (Int64.add (Int64.of_int gap) fitness).

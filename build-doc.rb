# Run this script after coqdoc to populate the Docusaurus documentation in doc/.
require 'fileutils'
require 'json'
require 'pathname'

#backward_progress = `ruby scripts/status/list_backward_proofs_k_alpha.rb`
backward_progress = "0"
system("ruby scripts/status/list_compare_functions_k.rb")
simulations_progress = `ruby scripts/status/list_simulations_k.rb`

$base_url = "/coq-tezos-of-ocaml"
$coqdoc_path = "html"
$output_directory = File.join("doc", "docs")
$source_directory = File.join("src")

$nb_documentation_files = 0

def get_relative_path(path)
  source = Pathname.new($source_directory)
  target = Pathname.new(path)
  target.relative_path_from(source).to_s
end

# Fix the links in the generated coqdoc files.
def fix_links(content)
  content
    .gsub(/href="TezosOfOCaml.((\w|\.)+).html/) {
      "href=\"#{$base_url}/docs/#{$1.downcase.gsub(".", "/").gsub(/\/_+/, "/")}/"
    }
    .gsub("#\"", "\"")
end

# We treat the coqdoc as raw HTML, as it would slow-down React otherwise (too
# many links).
def to_jsx(content)
  "<div id=\"main\" dangerouslySetInnerHTML={#{JSON.generate({"__html" => content})}} />"
end

def write_if_different(path, content)
  if !File.exist?(path) || File.read(path, :encoding => 'utf-8') != content then
    FileUtils.mkdir_p(File.dirname(path))
    File.open(path, "w") do |file|
      file << content
    end
  end
end

def list_directories(path)
  Dir.glob(File.join(path, "*")).select {|path| File.directory?(path)}
end

$v_files_black_list = File.read("blacklist.txt").split("\n")

def not_in_blacklist(path)
  $v_files_black_list.find_index {|black_path| path.include?(black_path)} == nil
end

def list_md_files(path)
  Dir.glob(File.join(path, "*.md")).filter {|path| not_in_blacklist(path)}
end

def list_v_files(path)
  Dir.glob(File.join(path, "*.v")).filter {|path| not_in_blacklist(path)}
end

def get_target_path_for_sidebar(relative_path, suffix)
  Pathname.new(File.join(
    File.dirname(relative_path),
    File.basename(relative_path, suffix).sub(/\A_+/, "")
  )).cleanpath.to_s
end

def external_link_svg
  '<svg width="13.5" height="13.5" aria-hidden="true" viewBox="0 0 24 24"><path fill="currentColor" d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z" /></svg>'
end

def md_of_md(path)
  base_name = File.basename(path, ".md")
  content = <<-END
---
id: #{base_name.downcase}
title: #{base_name.gsub("_", " ").strip.capitalize}
---

[Gitlab #{external_link_svg}](https://gitlab.com/formal-land/coq-tezos-of-ocaml/-/blob/master/#{path})

#{File.read(path, :encoding => 'utf-8')}
  END
  relative_path = get_relative_path(path).downcase
  target_path = File.join($output_directory, relative_path)
  write_if_different(target_path, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".md")
end

# Url on the website of a file page, as we use downcased names (maybe a bad idea
# as it brings complexity, but to have urls that feels more like urls).
def url_of_path(path)
  File.join(
    "/docs",
    File.dirname(path).sub("src", "").downcase,
    File.basename(path, ".v").downcase.sub(/\A_+/, "")
  )
end

def code_path_of_path(path)
  path.sub("/Proofs/", "/").sub("/Simulations/", "/")
end

def code_link_of_path(path)
  code_path = code_path_of_path(path)
  if code_path != path && File.exist?(code_path)
    "[See code](#{url_of_path(code_path)}), "
  end
end

def proofs_link_of_path(path)
  code_path = code_path_of_path(path)
  proofs_path = File.join(
    File.dirname(code_path), "Proofs", File.basename(code_path))
  if
    proofs_path != path &&
    File.exist?(proofs_path) &&
    not_in_blacklist(proofs_path)
  then
    "[See proofs](#{url_of_path(proofs_path)}), "
  end
end

def simulations_link_of_path(path)
  code_path = code_path_of_path(path)
  simulations_path = File.join(
    File.dirname(code_path), "Simulations", File.basename(code_path))
  if
    simulations_path != path &&
    File.exist?(simulations_path) &&
    not_in_blacklist(simulations_path)
  then
    "[See simulations](#{url_of_path(simulations_path)}), "
  end
end

def ocaml_link_of_path(path)
  code_path = code_path_of_path(path)
  protocol_paths = {
    "src/Proto_alpha" => "src/proto_alpha",
    "src/Proto_J" => "src/proto_013_PtJakart",
    "src/Proto_K" => "src/proto_014_PtKathma"
  }
  protocol_path = protocol_paths[File.dirname(code_path)]
  if protocol_path then
    ", [OCaml #{external_link_svg}](https://gitlab.com/formal-land/tezos/-/blob/master-with-coq-j-and-k/#{protocol_path}/lib_protocol/#{File.basename(code_path, ".v").downcase}.ml)"
  end
end

def nbsp
  [160].pack('U*')
end

def title_for_v(path)
  name = File.basename(path)
  emoji = ""
  # A table of rules to choose the emoji from. We check for the beginning of the
  # file name.
  rules_rable = [
    ["Environment", "🍃"],
    ["Alpha_", "🌌"],
    ["Amendment", "🧑‍⚖️"],
    ["Apply", "🏗️"],
    ["Baking", "🥖"],
    ["Bitset", "🚥"],
    ["Blinded_public_key_hash", "🕶️"],
    ["Block_", "🧱"],
    ["Bond_id_repr", "🔫"],
    ["Bounded_history_repr", "📜"],
    ["Bootstrap_", "🐣"],
    ["Cache_", "🧠"],
    ["Carbonated_map", "🔥"],
    ["Commitment_", "💍"],
    ["Context", "💾"],
    ["Constants_", "🎛️"],
    ["Contract_", "✒️"],
    ["CPS", "🍬"],
    ["Cycle_repr", "➰"],
    ["Dal_", "🍲"],
    ["Delegate_", "👥"],
    ["Dependent_bool", "🅱️"],
    ["Destination_repr", "🎯"],
    ["Entrypoint_repr", "🚪"],
    ["Error", "💥"],
    ["Fees_storage", "💰"],
    ["Fitness_repr", "🏋️"],
    ["Fixed_point_repr", "🧮"],
    ["Frozen_deposits_storage", "⛄"],
    ["Gas_", "⛽"],
    ["Global_", "🌍"],
    ["Indexable", "🗂️"],
    ["Init_", "🏁"],
    ["Internal_error", "💥"],
    ["Lazy_", "🦥"],
    ["Legacy_", "🗿"],
    ["Level_", "🪜"],
    ["Liquidity_baking_", "⚗️"],
    ["Local_gas_counter", "⛽"],
    ["Main", "🧑"],
    ["Manager_", "👔"],
    ["Mempool", "🏊"],
    ["Merkle_list", "🔗"],
    ["Michelson_", "🍬"],
    ["Micho_to_dep", "🍬"],
    ["Migration_repr", "🚗"],
    ["Misc", "🃏"],
    ["Non_empty_string", "🔡"],
    ["Nonce_", "💎"],
    ["Operation_repr", "💸"],
    ["Origination_nonce", "💎"],
    ["Parameters_repr", "🕹️"],
    ["Path_encoding", "🐾"],
    ["Period_repr", "🕰️"],
    ["Ratio_repr", "➗"],
    ["Raw_context", "🖼️"],
    ["Raw_level_repr", "🪜"],
    ["Receipt_repr", "🧾"],
    ["Round_repr", "🥊"],
    ["Sampler", "🎲"],
    ["Sapling", "🥷"],
    ["Saturation_repr", "🧮"],
    ["Sc_rollup", "🦏"],
    ["Script_", "🍬"],
    ["Seed_", "🌱"],
    ["Services_registration", "🏪"],
    ["Skip_list_repr", "🔗"],
    ["Slot_repr", "🎰"],
    ["Stake_storage", "🪤"],
    ["State_hash", "📸"],
    ["Storage", "💾"],
    ["Tez_repr", "🪙"],
    ["Ticket_", "🎫"],
    ["Time_repr", "🕰️"],
    ["Token", "🪅"],
    ["Tx_rollup_", "🐆"],
    ["Utils", "🧰"],
    ["Validate", "✅"],
    ["Vote_", "🗳️"],
    ["Voting_", "🗳️"],
    ["Zk_", "🇿"]
  ]
  for prefix, icon in rules_rable do
    if name.start_with?(prefix) then
      emoji = icon
    end
  end
  # We have special rules overriding all the others
  if path.include?("Environment") then
    emoji = "🍃"
  end
  if path.include?("Michocoq") || path.include?("Script_typed_ir") then
    emoji = "🍬"
  end
  if path.include?("Tests") then
    emoji = "🧑‍⚕️"
  end
  
  if emoji == "" then
    puts "Emoji not found for '#{name}'"
  end
  (emoji != "" ? emoji + nbsp : "") + name
end

def is_mi_cho_coq(path)
  Pathname(path).each_filename.include?("Michocoq")
end

def is_backward_compatibility(path)
  Pathname(path).each_filename.include?("Proto_J_K") ||
  Pathname(path).each_filename.include?("Proto_K_alpha")
end

def is_proof(path)
  Pathname(path).each_filename.include?("Proofs")
end

def is_simulation(path)
  Pathname(path).each_filename.include?("Simulations")
end

def is_environment(path)
  !is_proof(path) &&
  Pathname(path).each_filename.include?("Environment")
end

def is_code(path)
  File.dirname(path) == File.join($source_directory, "Proto_J") ||
  File.dirname(path) == File.join($source_directory, "Proto_K") ||
  File.dirname(path) == File.join($source_directory, "Proto_alpha")
end

def is_test(path)
  Pathname(path).each_filename.include?("Tests")
end

def kind_of_file(path)
  if is_mi_cho_coq(path) then
    "Mi-Cho-Coq"
  elsif is_backward_compatibility(path) then
    "Backward compatibility"
  elsif is_proof(path) then
    "Proofs"
  elsif is_simulation(path) then
    "Simulations"
  elsif is_environment(path) then
    "Environment"
  elsif is_code(path) then
    "Translated OCaml"
  elsif is_test(path) then
    "Tests"
  else
    puts "Unknown kind for #{path}"
    nil
  end
end

def mdx_of_v(path)
  doc_path = File.join(
    $coqdoc_path,
    File.basename("TezosOfOCaml." + get_relative_path(path).gsub("/", "."), ".v") + ".html"
  )
  content = <<-END
---
id: #{File.basename(path, ".v").downcase.sub(/\A_+/, "")}
title: #{title_for_v(path)}
hide_table_of_contents: true
---

#{kind_of_file(path) && "<p className=\"curlyFont\">#{kind_of_file(path)}</p>"}

#{code_link_of_path(path)}#{proofs_link_of_path(path)}#{simulations_link_of_path(path)}[Gitlab #{external_link_svg}](https://gitlab.com/formal-land/coq-tezos-of-ocaml/-/blob/master/#{path})#{ocaml_link_of_path(path)}

#{to_jsx(fix_links(File.read(doc_path, :encoding => 'utf-8')))}
  END
  relative_path = get_relative_path(path).downcase
  target_path_v = File.join($output_directory, relative_path)
  target_path_mdx = File.join(
    File.dirname(target_path_v),
    File.basename(target_path_v, ".v").sub(/\A_+/, "") + ".mdx"
  )
  write_if_different(target_path_mdx, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".v")
end

def documentation_of_path(path)
  list_directories(path).sort_by {|name| name.downcase}.filter_map do |path|
    items = documentation_of_path(path)
    items.size == 0 ? nil :
      {
        "type" => "category",
        "label" => File.basename(path).gsub("_", " ").strip.capitalize,
        "items" => items
      }
  end +
  list_md_files(path).sort_by {|name| name.downcase}.map do |path|
    md_of_md(path)
  end +
  list_v_files(path).sort_by {|name| name.downcase}.map do |path|
    mdx_of_v(path)
  end
end

sidebars = { "docs" => documentation_of_path($source_directory) }
File.open("doc/sidebars.js", "w") do |file|
  file << "module.exports = " + JSON.pretty_generate(sidebars) + ";"
end

# Copy images.
Dir.glob(File.join($source_directory, "**", "*.png")) do |image_path|
  target_path =
    File.join($output_directory, get_relative_path(image_path)).downcase
  FileUtils.mkdir_p(File.dirname(target_path))
  FileUtils.cp(image_path, target_path)
end

def nb_lines(&predicate)
  (Dir.glob(File.join($source_directory, "**", "*.v")) - $v_files_black_list)
    .select(&predicate)
    .map {|path| File.read(path, :encoding => 'utf-8').split("\n").size}
    .reduce(:+)
end

File.open("doc/stats.js", "w") do |file|
  file << <<-END
export default {
  code: #{nb_lines {|path|
    not_in_blacklist(path) &&
    path.include?("Proto_alpha") &&
    is_code(path)
  }},
  environment: #{nb_lines {|path|
    not_in_blacklist(path) &&
    path.include?("Environment/Structs")
  }},
  proofs: #{nb_lines {|path|
    not_in_blacklist(path) &&
    (
      path.include?("Proto_alpha") ||
      path.include?("Proto_K_alpha") ||
      path.include?("Environment/V7")
    ) &&
    (
      is_proof(path) ||
      is_simulation(path) ||
      is_backward_compatibility(path)
    ) &&
    !path.include?("_generated.v")
  }},
  progress: {
    backward: #{backward_progress.chomp},
    simulations: #{simulations_progress.chomp}
  }
}
  END
end

puts "#{$nb_documentation_files} generated documentation files in #{$output_directory}"
